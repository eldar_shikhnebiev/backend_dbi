package ru.bostonsd.clientmanagement.smtp.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.smtp.service.SmtpService;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Set;

/**
 * Implementation for {@link SmtpService}
 *
 * Sending emails using the  {@link JavaMailSender} service
 *
 * @author Aleksey Saburov (asaburov@bostonsd.ru)
 * Created on December 12, 2019
 */
@Service
@Slf4j
public class SmtpServiceImpl implements SmtpService {

    @Autowired
    private JavaMailSender sender;

    @Value("${spring.mail.from}")
    private String EMAIL_FROM;
    /**
     * Creating and sending an email of the MimeMessage type with the addition of the recipient,
     * subject and message text
     *
     * Inside the method, {@link MessagingException}, {@link Exception} exceptions are caught
     *
     * @param recipient - who is the letter sent to
     * @param message - body of the email
     * @param subject - email header
     */
    @Async
    @Override
    public void sendMessage(String recipient, String message, String subject) {
        try {
            MimeMessage mimeMessage = sender.createMimeMessage();
            mimeMessage.setFrom(EMAIL_FROM);
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
            helper.setText(message, true);
            helper.setTo(recipient);
            helper.setSentDate(new Date());
            helper.setSubject(subject);
            Thread.sleep(1000);
            sender.send(mimeMessage);
            log.info("Message was send successfully to {}", recipient);
        } catch (MessagingException e) {
            log.error("CREATE OR SEND MESSAGE ERROR, MESSAGE: {}", e.getMessage());
        } catch (Exception ex) {
            log.error("ERROR WITH SMTP CONFIGURATION, MESSAGE: {}", ex.getMessage());
        }
    }

    /**
     * Creating and sending an email of the MimeMessage type with the addition of the list of recipients,
     * subject and message text for them
     *
     * Inside the method, {@link MessagingException} exceptions are caught
     *
     * @param recipients - who should I send the email to
     * @param message - body of the email
     * @param subject - email header
     */
    @Async
    @Override
    public void sendMessageToListRecipient(Set<String> recipients, String message, String subject) {
        try {
            MimeMessage mimeMessage = sender.createMimeMessage();
            mimeMessage.setFrom(EMAIL_FROM);
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
            helper.setText(message);
            helper.setTo(InternetAddress.parse(String.join(",", recipients)));
            helper.setSentDate(new Date());
            helper.setSubject(subject);
            sender.send(mimeMessage);
            log.info("Message was send successfully to {}", recipients);
        } catch (MessagingException e) {
            log.error("CREATE OR SEND MESSAGE ERROR, MESSAGE: {}", e.getMessage());
        } catch (Exception ex) {
            log.error("ERROR WITH SMTP CONFIGURATION, MESSAGE: {}", ex.getMessage());
        }
    }
}
