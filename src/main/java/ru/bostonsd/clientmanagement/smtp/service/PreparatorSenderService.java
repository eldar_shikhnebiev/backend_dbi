package ru.bostonsd.clientmanagement.smtp.service;

import ru.bostonsd.clientmanagement.domain.models.Client;
import ru.bostonsd.clientmanagement.domain.models.DiscountCollection;
import ru.bostonsd.clientmanagement.domain.models.Distributor;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientDto;
import ru.bostonsd.clientmanagement.domain.models.dto.DiscountCollectionDto;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUser;
import ru.bostonsd.clientmanagement.security.model.rest.BasicUpdateRequest;
import ru.bostonsd.clientmanagement.security.model.rest.FullUpdateRequest;
import ru.bostonsd.clientmanagement.security.model.rest.SignUpRequest;

/**
 * Service for preparing data for sending by mail
 *
 * @author Aleksey Saburov (asaburov@bostonsd.ru)
 * Created on December 12, 2019
 */
public interface PreparatorSenderService {

    /**
     * Preparation of the client's data to send in the mail
     *
     * @param client - instance of entity {@link Client}
     */
    void prepareChangeClientMessage(Client client);

    /**
     * Preparing data on changes in the instance of entity {@link DiscountCollectionDto} to send via email
     *
     * @param oldCollection - collection before change
     * @param newCollection - collection with change
     */
    void prepareChangeOfCollectionDiscount(DiscountCollectionDto oldCollection, DiscountCollectionDto newCollection);

    /**
     * Preparing data on changes in the instance of entity {@link ClientDto} to send via email
     *
     * @param newClient - client before change
     * @param oldClient - client with change
     */
    void prepareChangeDistributorsInUser(ClientDto newClient, ClientDto oldClient);

    /**
     * Preparation of the new user's data to send in the mail
     *
     * @param savedUser - new user
     */
    void prepareSaveNewUser(SignUpRequest savedUser);

    void prepareCreateLinkClientAndDist(Client client, Distributor distributor);

    /**
     * Preparing data on basic changes at user to send via email
     *
     * @param oldUser - old user data
     * @param updateRequest - updated basic information about the user
     */

    void prepareUpdateUsersPassword(SCHDUser oldUser, BasicUpdateRequest updateRequest);

    /**
     * Preparing data on full changes at user to send via email
     *
     * @param oldUser - old user data
     * @param updateRequest - updated basic information about the user
     */
    void prepareUpdateUsersPassword(SCHDUser oldUser, FullUpdateRequest updateRequest);
}
