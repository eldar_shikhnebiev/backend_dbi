package ru.bostonsd.clientmanagement.smtp.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.domain.models.Client;
import ru.bostonsd.clientmanagement.domain.models.Distributor;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientDto;
import ru.bostonsd.clientmanagement.domain.models.dto.DiscountCollectionDto;
import ru.bostonsd.clientmanagement.domain.models.dto.SalesVolumeClientDto;
import ru.bostonsd.clientmanagement.security.model.database.RoleEnum;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUser;
import ru.bostonsd.clientmanagement.security.model.rest.BasicUpdateRequest;
import ru.bostonsd.clientmanagement.security.model.rest.FullUpdateRequest;
import ru.bostonsd.clientmanagement.security.model.rest.SignUpRequest;
import ru.bostonsd.clientmanagement.security.service.impl.SCHDUserDao;
import ru.bostonsd.clientmanagement.smtp.service.PreparatorSenderService;
import ru.bostonsd.clientmanagement.smtp.service.SmtpService;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Implementation for {@link PreparatorSenderService}
 * <p>
 * Preparing data for sending via {@link SmtpService}
 *
 * @author Aleksey Saburov (asaburov@bostonsd.ru)
 * Created on December 12, 2019
 */
@Service
@Slf4j
public class PreparatorSenderServiceImpl implements PreparatorSenderService {

    @Autowired
    private SmtpService smtpService;

    @Autowired
    private SCHDUserDao schdUserDao;

    @Autowired
    private PasswordEncoder encoder;

    /**
     * Preparing a client data change message for users
     * In the process
     * - a list of distributors is determined, with which the client is connected through sales volumes
     * - created message about changes for sending
     *
     * @param client - instance of entity {@link Client}
     */
    @Override
    public void prepareChangeClientMessage(Client client) {
        if (client != null) {
            List<Long> distrIds = client.getSalesVolumes().stream()
                    .map(salesVolume -> salesVolume.getDistributor().getId())
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            List<SCHDUser> users = schdUserDao.getAllUsersByDistributorIds(distrIds);
            //Username is email according to the business requirements
            Set<String> emails = users.stream()
                    .map(SCHDUser::getUsername)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());

            log.info("Emails for informing users about changing client`s info, size emails: {}", emails.size());

            final String title = "Данные о клиенте были обновлены";

            String message = "Информация о вашем клиенте (ИНН: " + client.getInn() + ", Наименование: " + client.getName() + ") была обновлена.";
            if (!emails.isEmpty()) emails.forEach(email -> smtpService.sendMessage(email, message, title));
        }
    }

    /**
     * Preparing a Collection data change message for users
     * In the process
     * - checking collection data was changed
     * - select users with the role of distributor
     * - created message about changes for sending
     *
     * @param oldCollection - collection before change
     * @param newCollection - collection with change
     */
    @Override
    public void prepareChangeOfCollectionDiscount(DiscountCollectionDto oldCollection, DiscountCollectionDto newCollection) {
        if (oldCollection != null && newCollection != null
                //CHECK THAT DISCOUNT WAS CHANGED
                && (!Objects.equals(oldCollection.getBronze(), newCollection.getBronze())
                || !Objects.equals(oldCollection.getSilver(), newCollection.getSilver())
                || !Objects.equals(oldCollection.getGold(), newCollection.getGold()))
        ) {
            List<SCHDUser> allUsersDistributors = schdUserDao.getAllByRoleName(RoleEnum.ROLE_DISTRIBUTOR.name());
            //Username is email according to the business requirements
            Set<String> emails = allUsersDistributors.stream()
                    .map(SCHDUser::getUsername)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());

            log.info("Emails for informing users about changing collection`s discount, size emails: {}", emails.size());

            final String title = "Изменение скидки на коллекцию";
            final String message = "Для коллекции с наименованием \"" + newCollection.getName() + "\" была обновлена информация о размере скидки.";
            if (!emails.isEmpty()) emails.forEach(email -> smtpService.sendMessage(email, message, title));
        }
    }

    /**
     * Preparing entity {@link Client} in format DTO data change message for users with roles admins
     * In the process
     * - checking new version and old version Client not null
     * - determined lists of distributors with which the customer in the old and new versions are associated by sales volume
     * - deleted from lists of distributors  new version client, lists  of distributors  old version client
     * - if the previous operation was successful and result list not empty
     * then selected emails user with roles admin  and send them message
     *
     * @param newClient - client before change
     * @param oldClient - client with change
     */
    @Override
    public void prepareChangeDistributorsInUser(ClientDto newClient, ClientDto oldClient) {
        if (newClient != null && oldClient != null) {
            try {
                ClientDto cloneNewClient = (ClientDto) newClient.clone();
                ClientDto cloneOldClient = (ClientDto) oldClient.clone();
                List<Long> newDistrs = cloneNewClient.getSalesVolume().stream()
                        .map(SalesVolumeClientDto::getIdDistributor)
                        .collect(Collectors.toList());
                List<Long> oldDistrs = cloneOldClient.getSalesVolume().stream()
                        .map(SalesVolumeClientDto::getIdDistributor)
                        .collect(Collectors.toList());

                boolean success = newDistrs.removeAll(oldDistrs);
                if (success && !newDistrs.isEmpty()) {
                    List<SCHDUser> allAdmins = schdUserDao.getAllByRoleName(RoleEnum.ROLE_ADMIN.name());
                    Set<String> emails = allAdmins.stream().map(SCHDUser::getUsername).filter(Objects::nonNull).collect(Collectors.toSet());

                    final String title = "К клиенту был добавлен новый дистрибьютор";
                    StringBuilder message = new StringBuilder();
                    message.append("Клиенту (ИНН: ")
                            .append(newClient.getInn())
                            .append(", Наименование:")
                            .append(newClient.getName())
                            .append(") был добавлен новый дистрибьютор(ы): ");

                    for (Long id : newDistrs) {
                        newClient.getSalesVolume()
                                .stream()
                                .filter(c -> c.getIdDistributor().equals(id))
                                .findFirst()
                                .ifPresent(c -> message
                                        .append("\'")
                                        .append(c.getNameDistributor())
                                        .append("\', "));
                    }
                    String resultMessage = message.substring(0, message.length() - 2);

                    if (!emails.isEmpty()) emails.forEach(email -> smtpService.sendMessage(email, resultMessage, title));
                }
            } catch (CloneNotSupportedException ex) {
                log.error("CLONE CLIENT DTO OBJECT, MESSAGE: {}", ex.getMessage());
            }
        }
    }

    /**
     * Checking and preparing information about changes to the user's basic data, including the password
     *
     * @param oldUser       - old user data
     * @param updateRequest - updated basic information about the user
     */
    @Override
    public void prepareUpdateUsersPassword(SCHDUser oldUser, BasicUpdateRequest updateRequest) {
        if (oldUser != null && updateRequest != null && updateRequest.getNewPassword() != null
                //OLD PASSWORD IS ENCODED AND NEW PASSWORD IS NOT ENCODED
                && !encoder.matches(updateRequest.getNewPassword(), oldUser.getPassword())) {
            //USERNAME IS EMAIL
            String email = oldUser.getUsername();
            final String title = "Обновление вашего пароля в \"Базе клиентов\"";
            final String message = " Произошло обновление вашего пароля в \"Базе клиентов\".\n" +
                    "Ваш новый пароль: " + updateRequest.getNewPassword();
            smtpService.sendMessage(email, message, title);
        }
    }

    /**
     * Checking and preparing information about changes to the user's full data, including the password
     *
     * @param oldUser       - old user data
     * @param updateRequest - updated basic information about the user
     */
    @Override
    public void prepareUpdateUsersPassword(SCHDUser oldUser, FullUpdateRequest updateRequest) {
        if (oldUser != null && updateRequest != null && updateRequest.getPassword() != null
                && !encoder.matches(updateRequest.getPassword(), oldUser.getPassword())) {
            //USERNAME IS EMAIL
            String email = oldUser.getUsername();
            final String title = "Обновление вашего пароля в \"Базе клиентов\"";
            final String message = " Произошло обновление вашего пароля в \"Базе клиентов\".\n" +
                    "Ваш новый пароль: " + updateRequest.getPassword();
            smtpService.sendMessage(email, message, title);
        }
    }

    /**
     * Checking and preparing reports on the establishment of a new user
     *
     * @param savedUser - new user
     */
    @Override
    public void prepareSaveNewUser(SignUpRequest savedUser) {
        if (savedUser != null) {
            final String title = "Добро пожаловать в \"Базу клиентов\"";
            final String message = "Для вас был открыт доступ к \"Базе клиентов\".\n" +
                    "Ваши данные для авторизации:\n" +
                    "Логин: " + savedUser.getUsername() + "\n" +
                    "Пароль: " + savedUser.getPassword();
            smtpService.sendMessage(savedUser.getUsername(), message, title);
        }
    }

    @Override
    public void prepareCreateLinkClientAndDist(Client client, Distributor distributor) {
        if (client != null && distributor != null) {
            List<SCHDUser> allAdmins = schdUserDao.getAllByRoleName(RoleEnum.ROLE_ADMIN.name());
            final String title = "К клиенту был добавлен новый дистрибьютор";
            final String message = "К одному из клиентов (ИНН: " + client.getInn() + ", Наименование: " + client.getName()
                    + ") был добавлен новый дистрибьютор (ИНН: " + distributor.getInn() + ", Наименование: " + distributor.getName() + ").";
            Set<String> adminEmails = allAdmins.stream().map(SCHDUser::getUsername).collect(Collectors.toSet());
            if (!adminEmails.isEmpty()) adminEmails.forEach(email -> smtpService.sendMessage(email, message, title));
        }
    }
}
