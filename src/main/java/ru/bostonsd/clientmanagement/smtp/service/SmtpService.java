package ru.bostonsd.clientmanagement.smtp.service;

import java.util.Set;

/**
 * Service sending emails
 *
 * @author Aleksey Saburov (asaburov@bostonsd.ru)
 * Created on December 12, 2019
 */
public interface SmtpService {

    /**
     * Sending an email to a given recipient with the specified subject and message
     *
     * @param recipient - who is the mail sent to
     * @param message - body of the email
     * @param subject - email header
     */
    void sendMessage(String recipient, String message, String subject);

    /**
     * Sending an email to a given set of recipient with the specified subject and message
     *
     * @param recipients - who is the mail sent to
     * @param message - body of the email
     * @param subject - email header
     */
    void sendMessageToListRecipient(Set<String> recipients, String message, String subject);
}
