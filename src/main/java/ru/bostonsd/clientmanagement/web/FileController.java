package ru.bostonsd.clientmanagement.web;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.bostonsd.clientmanagement.domain.models.File;
import ru.bostonsd.clientmanagement.domain.models.dto.FileDto;
import ru.bostonsd.clientmanagement.domain.service.FileService;

/**
 * Controller for manage Files
 * <p>
 * Rest controller to handle http requests to the model {@link File}.
 * Processing a request using an entity {@link FileDto}.
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 6, 2019
 */
@RestController
@RequestMapping("/file")
@Slf4j
public class FileController {

    @Autowired
    private FileService fileService;

    /**
     * Processing request to provide a instance of entity {@link File} by id
     * <p>
     * If not specified, returned http-status - 400 Bad request.
     * <p>
     * If not fount in data store, returned http-status - 404 Not found.
     * otherwise the instances of entity that have the specified name  value is returned.
     *
     * @param id - id distributor for find.
     * @return instance of entity {@link ResponseEntity} includes instance of Distributor and HTTP-status
     */
    @ApiOperation(value = "download file by id")
    @GetMapping("/download/{id}")
    public ResponseEntity getFileById(@PathVariable Long id) {
        if (id != null) {
            File file = fileService.getFileById(id);
            if (file != null) {
                return ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_DISPOSITION,
                                "attachment; filename=" + file.getName())
                        .contentType(MediaType.IMAGE_JPEG)
                        .body(file.getValue());
            } else {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    /**
     * Processing request to saved a instance of entity {@link FileDto}
     * The received request body is passed to the service level for further processing
     * If file is null, returned http-status - 400 Bad request,
     *
     * @param file - file for upload in data store
     * @return instance of entity {@link ResponseEntity} includes saved  instance of FileDto and HTTP-status
     */
    @ApiOperation(value = "save file")
    @PostMapping
    public ResponseEntity save(@RequestBody MultipartFile file) {
        if (file != null) {
            FileDto result = fileService.save(file);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    /**
     * Processing request to deleted a instance of entity {@link FileDto} by id
     * The specified id file is passed to the service level for deletion
     * and a logical response is expected for the success of the operation.
     * <p>
     * If not specified id, returned http-status - 400 Bad request.
     * If logical response  - true, http status 200 is returned
     * - false, http status 400 is returned
     *
     * @param id - id file for deleted.
     * @return instance of entity {@link ResponseEntity} includes  HTTP-status
     */
    @ApiOperation(value = "delete file by id")
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (id != null) {
            boolean isDelete = fileService.delete(id);
            if (isDelete) {
                return new ResponseEntity(HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
}
