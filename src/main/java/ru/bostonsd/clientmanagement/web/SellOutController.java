package ru.bostonsd.clientmanagement.web;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientWithOnlySellOutSalesVolumeDto;
import ru.bostonsd.clientmanagement.security.model.annotation.ValidateRole;
import ru.bostonsd.clientmanagement.sellout.models.ClientAnnualSales;
import ru.bostonsd.clientmanagement.sellout.service.ClientAnnualSalesService;

import javax.validation.Valid;

/**
 * Controller for manage request to SellOut DB
 * <p>
 * Rest controller to handle http requests to the model {@link ClientAnnualSales}.
 * Processing a request using an entity {@link ClientWithOnlySellOutSalesVolumeDto}
 * <p>
 * Upon receipt of the body, primary validation is performed using the annotation {@link Valid}
 * to match the constraints set for the entity fields.
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on May 21, 2020
 */

@Api(value = "SellOut Sales Volume management", description = "Get operations with SellOut DB")
@RestController
@RequestMapping("/sellout")
public class SellOutController {

    @Autowired
    private ClientAnnualSalesService clientAnnualSalesService;

    /**
     * Processing a request to provide a list of entity instances {@link ClientWithOnlySellOutSalesVolumeDto}  by given id client.
     * If id not specified, returned http-status - 400 Bad request.
     *
     * @param inn - inn client for find linked entity instances {@link ClientWithOnlySellOutSalesVolumeDto}.
     * @return instance of entity {@link ResponseEntity} includes instance of Distributor and HTTP-status
     */
    @ApiOperation(value = "View a list of partner with sales volume based on a given Client inn ",
            response = ClientWithOnlySellOutSalesVolumeDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Client Id is incorrect")
    })
    @ValidateRole(roles = {"ROLE_ADMIN"})
    @GetMapping("/searchbyinn/{inn}")
    public ResponseEntity getClientById(
            @ApiParam(value = "specifies the client inn to search in the DB SellOut Sales Annual")
            @PathVariable String inn,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        if (inn != null) {
            return new ResponseEntity<>(clientAnnualSalesService.getClientSalesByInn(inn), HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

}
