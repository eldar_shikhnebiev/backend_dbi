package ru.bostonsd.clientmanagement.web;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bostonsd.clientmanagement.domain.models.ClientClassification;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientClassificationDto;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientClassificationDtoSave;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientClassificationDtoUpdate;
import ru.bostonsd.clientmanagement.domain.service.ClientClassificationService;
import ru.bostonsd.clientmanagement.security.model.annotation.ValidateRole;

import javax.validation.Valid;

/**
 * Controller for manage ClientClassification
 * <p>
 * Rest controller to handle http requests to the model {@link ClientClassification}.
 * Processing a request using an entity {@link ClientClassificationDto}.
 * Upon receipt of the body, primary validation is performed using the annotation {@link Valid}
 * to match the constraints set for the entity fields.
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 6, 2019
 * Updated on March 2, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * */

@Api(value="Classification management", description="CRUD operations with Classifications")
@RestController
@RequestMapping("/classification")
public class ClientClassificationController {

    @Autowired
    private ClientClassificationService clientClassificationService;

    /**
     * Processing a request to provide a list of entity instances {@link ClientClassificationDto}.
     * If not specified, the entire all list of entity instances is returned,
     * otherwise the list of instances that have the specified active value is returned.
     *
     * @param active - optional logical parameter for  define the list of elements of the returned list
     * @return instance of entity {@link ResponseEntity} includes list of ClientClassification and HTTP-status
     */
    @ApiOperation(value = "View a list of Classifications based on a given parameter", response = ClientClassificationDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Activity status you were trying to requested is not found")
    })
    @GetMapping("/list")
    public ResponseEntity getClientClassificationList(
            @ApiParam(value = "Optional logical activity parameter in Classifications - true/false")
            @RequestParam(value = "active", required = false) Boolean active) {
        if (active != null) {
            return new ResponseEntity<>(clientClassificationService.getIsActiveList(active), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(clientClassificationService.getAllList(), HttpStatus.OK);
        }
    }

    /**
     * Processing request to provide a instance of entity {@link ClientClassificationDto} by name
     * If not specified, returned http-status - 400 Bad request,
     * otherwise the instances of entity that have the specified name  value is returned.
     *
     * @param name - name class for find.
     * @return instance of entity {@link ResponseEntity} includes instance of ClientClassification and HTTP-status
     */
    @ApiOperation(value = "Classification by name", response = ClientClassificationDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved Classification"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Classification by name  you were trying to requested is not found.")
    })
    @GetMapping("/{name}")
    public ResponseEntity getClientClassificationByName (
            @ApiParam(value = "the name of the Classification for which data is requested")
            @PathVariable String name) {
        if (name != null) {
             return new ResponseEntity<>(clientClassificationService.getByName(name), HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    /**
     * Processing request to saved a instance of entity {@link ClientClassificationDto}
     * The received request body is passed to the service level for further processing
     *
     * @param body - body request in format entity {@link ClientClassificationDto}
     * @return instance of entity {@link ResponseEntity} includes saved instance of ClientClassification and HTTP-status
     */
    @ApiOperation(value = "Add new Classification in DB", response = ClientClassificationDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added Classification"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Classification by name exist in DB")
    })
    @PostMapping
    @ValidateRole(roles = {"ROLE_ADMIN"})
    public ResponseEntity saveClientClassification (
            @ApiParam(value = "Classification object of the specified form to be saved in the DB ")
            @RequestBody @Valid ClientClassificationDtoSave body,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
         return new ResponseEntity<>(clientClassificationService.save(body), HttpStatus.OK);
    }

    /**
     * Processing request to updated a instance of entity {@link ClientClassificationDto}
     * The received request body is passed to the service level for further processing
     *
     * @param body - body request in format entity {@link ClientClassificationDto}
     * @return instance of entity {@link ResponseEntity} includes updated instance of ClientClassification and HTTP-status
     */
    @ApiOperation(value = "Update Classification in DB", response = ClientClassificationDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully update Classification"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Classification by name not found in DB for update")
    })
    @PutMapping
    @ValidateRole(roles = {"ROLE_ADMIN"})
    public ResponseEntity updateClientClassification (
            @ApiParam(value = "Classification object of the specified form to be updated in the DB ")
            @RequestBody @Valid ClientClassificationDtoUpdate body,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
            return new ResponseEntity<>(clientClassificationService.update(body), HttpStatus.OK);
    }

    /**
     * Processing request to deleted a instance of entity {@link ClientClassificationDto} by name
     * The specified class name is passed to the service level for deletion
     * and a logical response is expected for the success of the operation
     * If logical response  - true, http status 200 is returned
     *                      - false, http status 400 is returned
     *
     * @param name - name class for deleted.
     * @return instance of entity {@link ResponseEntity} includes  HTTP-status
     */
    @ApiOperation(value = "Delete Classification from DB")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully delete Classification"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Classification by name not found in DB for delete")
    })
    @DeleteMapping("/{name}")
    @ValidateRole(roles = {"ROLE_ADMIN"})
    public ResponseEntity deleteClientClassification (
            @ApiParam(value = "Name of Classification for delete")
            @PathVariable("name") String name,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        boolean isDelete = clientClassificationService.delete(name);
        if (isDelete) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
