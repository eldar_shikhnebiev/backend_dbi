package ru.bostonsd.clientmanagement.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bostonsd.clientmanagement.bpm.model.CommentMessage;
import ru.bostonsd.clientmanagement.bpm.service.general.CommentService;

import javax.ws.rs.PathParam;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * @date 18.12.2019
 */
@RestController
@RequestMapping("/comments")
@Slf4j
public class CommentController {

    @Autowired
    private CommentService commentService;

    @GetMapping
    public ResponseEntity getByProcessId(@RequestParam String processId){
        if(processId != null){
            List<CommentMessage> comments = commentService.getByProcessId(processId);
            if(comments != null) {
                return new ResponseEntity<>(comments, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new ArrayList<>(),HttpStatus.OK);
            }
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @PostMapping
    public ResponseEntity save(@RequestBody CommentMessage comment){
        if(comment != null){
            commentService.save(comment);
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
}
