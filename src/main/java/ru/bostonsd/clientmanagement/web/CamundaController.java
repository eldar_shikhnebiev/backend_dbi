package ru.bostonsd.clientmanagement.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bostonsd.clientmanagement.bpm.model.CamundaUser;
import ru.bostonsd.clientmanagement.bpm.model.TaskDto;
import ru.bostonsd.clientmanagement.bpm.service.general.CamundaService;
import ru.bostonsd.clientmanagement.history.service.TicketService;
import ru.bostonsd.clientmanagement.security.model.annotation.ValidateRole;
import ru.bostonsd.clientmanagement.security.service.JwtTokenUtil;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/camunda")
public class CamundaController {

    @Autowired
    private CamundaService camundaService;

    @Autowired
    private TicketService ticketService;

    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR"})
    @GetMapping("/byUserId")
    public ResponseEntity getAllTaskByUserId(@RequestHeader("Authorization") String jwt) {
        if (jwt != null && !jwt.trim().isEmpty()) {
            String userId = JwtTokenUtil.getUsernameFromToken(jwt);
            List<TaskDto> allTasks = camundaService.getAllTaskByUserId(userId);
            return new ResponseEntity<>(allTasks, HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR"})
    @GetMapping("/byGroup")
    public ResponseEntity getAllTasksByGroup(@RequestHeader("Authorization") String jwt) {
        if (jwt != null && !jwt.trim().isEmpty()) {
            List<TaskDto> allTasks = camundaService.getAllTasksByGroup(jwt);
            return new ResponseEntity<>(allTasks, HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }


    @GetMapping("/value")
    public ResponseEntity getValueByTaskId(@RequestParam String taskId, @RequestParam String variableName) {
        if (taskId != null && !taskId.trim().isEmpty()
                && variableName != null && !variableName.trim().isEmpty()) {
            Object value = camundaService.getValueByTaskId(taskId, variableName);
            return new ResponseEntity<>(value, HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR"})
    @PostMapping("/startProcess/{processName}")
    public ResponseEntity startProcess(@RequestBody Map<String, Object> variables,
                                       @PathVariable String processName,
                                       @RequestHeader("Authorization") String jwt) {
        if (processName != null && !processName.trim().isEmpty()) {
            String user = JwtTokenUtil.getUsernameFromToken(jwt);
            variables.put("initiator_id", user);
            Map<String, String> result = camundaService.startProcess(processName, variables);
            return !result.isEmpty() ? new ResponseEntity<>(result, HttpStatus.OK) : new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR"})
    @PostMapping("/completeTask/{taskId}")
    public ResponseEntity completeTask(@PathVariable String taskId, @RequestBody Map<String, Object> variables,
                                       @RequestHeader("Authorization") String jwt) {
        if (taskId != null && !taskId.trim().isEmpty()) {
            String username = JwtTokenUtil.getUsernameFromToken(jwt);
            camundaService.completeTask(taskId, variables, username);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR"})
    @PostMapping("/assign")
    public ResponseEntity assignTask(@RequestParam String taskId, @RequestHeader("Authorization") String jwt) {
        if (taskId != null && !taskId.trim().isEmpty()
                && jwt != null && !jwt.trim().isEmpty()) {
            String userId = JwtTokenUtil.getUsernameFromToken(jwt);
            boolean result = camundaService.assignTask(taskId, userId);
            return result ? new ResponseEntity<>(HttpStatus.OK) : new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR"})
    @PostMapping("/usersById")
    public ResponseEntity getUsersByUserId(@RequestBody String[] userIds) {
        if (userIds != null && userIds.length > 0) {
            List<CamundaUser> users = camundaService.getUserInfoByUserId(userIds);
            return new ResponseEntity<>(users, HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR"})
    @DeleteMapping("/allProcess")
    public ResponseEntity deleteAllProcess(){
        camundaService.deleteAllProcesses();
        ticketService.deleteAll();
        return new ResponseEntity(HttpStatus.OK);
    }

}
