package ru.bostonsd.clientmanagement.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.bostonsd.clientmanagement.util.jsonparser.importData.service.ImportDataService;

@RestController
@RequestMapping("/importdata")
public class ImportDataController {

    @Autowired
    private ImportDataService importDataService;

    @GetMapping()
    public ResponseEntity importData(){
        importDataService.upload();
        return new ResponseEntity(HttpStatus.OK);
    }
}
