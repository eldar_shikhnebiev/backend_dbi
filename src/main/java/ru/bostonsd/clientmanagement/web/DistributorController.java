package ru.bostonsd.clientmanagement.web;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bostonsd.clientmanagement.domain.models.Distributor;
import ru.bostonsd.clientmanagement.domain.models.dto.DistributorDto;
import ru.bostonsd.clientmanagement.domain.service.DistributorService;
import ru.bostonsd.clientmanagement.history.service.crud.distributor.DistributorHistoryService;
import ru.bostonsd.clientmanagement.security.model.annotation.ValidateRole;

import javax.validation.Valid;

/**
 * Controller for manage Distributor
 * <p>
 * Rest controller to handle http requests to the model {@link Distributor}.
 * Processing a request using an entity {@link DistributorDto}
 * <p>
 * Upon receipt of the body, primary validation is performed using the annotation {@link Valid}
 * to match the constraints set for the entity fields.
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 6, 2019
 * Updated on March 2, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 */
@Api(value = "Distributor management", description = "CRUD operations with Distributor")
@RestController
@RequestMapping("/distributor")
public class DistributorController {

    @Autowired
    private DistributorService distributorService;

    @Autowired
    private DistributorHistoryService distributorHistoryService;

    /**
     * Processing a request to provide a list of entity instances {@link DistributorDto}.
     *
     * @return instance of entity {@link ResponseEntity} includes list of DistributorDto and HTTP-status
     */
    @ApiOperation(value = "View a list of all Distributors", response = DistributorDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @ValidateRole(roles = {"ROLE_ADMIN"})
    @GetMapping("/list")
    public ResponseEntity getDistributorList (@ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        return  new ResponseEntity<>(distributorService.getAllList(), HttpStatus.OK);
    }

    /**
     * Processing request to provide a instance of entity {@link DistributorDto} by id
     * If not specified, returned http-status - 400 Bad request,
     * otherwise the instances of entity that have the specified id is returned.
     *
     * @param id - id distributor for find.
     * @return instance of entity {@link ResponseEntity} includes instance of Distributor and HTTP-status
     */
    @ApiOperation(value = "Distributor by id", response = DistributorDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved Distributor"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Distributor by id you were trying to requested is not found.")
    })
    @ValidateRole(roles = {"ROLE_ADMIN"})
    @GetMapping("/{id}")
    public ResponseEntity getDistributorById(
            @ApiParam(value = "Id of the Distributor for which data is requested")
            @PathVariable Long id,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        if (id != null) {
            return new ResponseEntity<>(distributorService.getDistributorDtoById(id), HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Processing request to provide a instance of entity {@link DistributorDto} by inn
     * If not specified, returned http-status - 400 Bad request,
     * otherwise the instances of entity that have the specified inn  value is returned.
     *
     * @param inn - inn distributor for find.
     * @return instance of entity {@link ResponseEntity} includes instance of Distributor and HTTP-status
     */
    @ApiOperation(value = "Distributor by inn", response = DistributorDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved Distributor"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Distributor by inn you were trying to requested is not found.")
    })
    @ValidateRole(roles = {"ROLE_ADMIN"})
    @GetMapping("/searchbyinn/{inn}")
    public ResponseEntity getDistributorByInn(
            @ApiParam(value = "Inn of the Distributor for which data is requested")
            @PathVariable String inn,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        if (inn != null ) {
            return new ResponseEntity<>(distributorService.getByInn(inn), HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Processing request to saved a instance of entity {@link DistributorDto}
     * The received request body is passed to the service level for further processing
     *
     * @param body - body request in format entity {@link DistributorDto}
     * @return instance of entity {@link ResponseEntity} includes saved  instance of Distributor and HTTP-status
     */
    @ApiOperation(value = "Add new Distributor in DB", response = DistributorDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added Distributor"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Distributor by inn exist in DB")
    })
    @PostMapping
    @ValidateRole(roles = {"ROLE_ADMIN"})
    public ResponseEntity saveDistributor(
            @ApiParam(value = "Distributor object of the specified form to be saved in the DB ")
            @RequestBody @Valid DistributorDto body,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        return new ResponseEntity<>(distributorService.save(body), HttpStatus.OK);
    }

    /**
     * Processing request to updated a instance of entity {@link DistributorDto}
     * The received request body is passed to the service level for further processing
     *
     * @param body - body request in format entity {@link DistributorDto}
     * @return instance of entity {@link ResponseEntity} includes updated instance of Distributor and HTTP-status
     */
    @ApiOperation(value = "Update Distributor in DB", response = DistributorDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully update Distributor"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Distributor by id not found in DB for update")
    })
    @PutMapping
    @ValidateRole(roles = {"ROLE_ADMIN"})
    public ResponseEntity updateDistributor(@RequestBody @Valid DistributorDto body,
                                            @ApiParam(hidden = true)
                                            @RequestHeader("Authorization") String authorization) {
        if (body != null && authorization != null) {
            DistributorDto distributorDto = distributorHistoryService.updateClientAndCreateHistory(body, authorization);
            return new ResponseEntity<>(distributorDto, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * Processing request to deleted a instance of entity {@link DistributorDto} by id
     * The specified id distributor is passed to the service level for deletion
     * and a logical response is expected for the success of the operation
     * If logical response  - true, http status 200 is returned
     *                      - false, http status 400 is returned
     *
     * @param id - id distributor for deleted.
     * @return instance of entity {@link ResponseEntity} includes  HTTP-status
     */
    @ApiOperation(value = "Delete Distributor from DB")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully delete Distributor"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Distributor by id not found in DB for delete")
    })
    @DeleteMapping("/{id}")
    @ValidateRole(roles = {"ROLE_ADMIN"})
    public ResponseEntity deleteDistributor(
            @ApiParam(value = "Distributor Id for delete")
            @RequestParam Long id,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        boolean isDelete = distributorService.delete(id);
        if (isDelete) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
