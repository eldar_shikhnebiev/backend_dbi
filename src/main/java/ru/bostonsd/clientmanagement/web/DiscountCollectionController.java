package ru.bostonsd.clientmanagement.web;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bostonsd.clientmanagement.domain.models.DiscountCollection;
import ru.bostonsd.clientmanagement.domain.models.dto.DiscountCollectionDto;
import ru.bostonsd.clientmanagement.domain.service.DiscountCollectionService;
import ru.bostonsd.clientmanagement.security.model.annotation.ValidateRole;

import javax.validation.Valid;

/**
 * Controller for manage DiscountCollection
 * <p>
 * Rest controller to handle http requests to the model {@link DiscountCollection}.
 * Processing a request using an entity {@link DiscountCollectionDto}.
 * Upon receipt of the body, primary validation is performed using the annotation {@link Valid}
 * to match the constraints set for the entity fields.
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 6, 2019
 * Updated on March 2, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 */

@Api(value = "Collection management", description = "CRUD operations with Collections")
@RestController
@RequestMapping("/collection")
public class DiscountCollectionController {

    @Autowired
    private DiscountCollectionService discountCollectionService;

    /**
     * Processing a request to provide a list of entity instances {@link DiscountCollectionDto}.
     * If not specified, the entire all list of entity instances is returned,
     * otherwise the list of instances that have the specified active value is returned.
     *
     * @param active - optional logical parameter for  define the list of elements of the returned list
     * @return instance of entity {@link ResponseEntity} includes list of Discount Collection and HTTP-status
     */
    @ApiOperation(value = "View a list of Collections based on a given parameter", response = DiscountCollectionDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Activity status you were trying to requested is not found")
    })
    @GetMapping("/list")
    public ResponseEntity getDiscountCollectionList(
            @ApiParam(value = "Optional logical activity parameter in Collections - true/false", required = false)
            @RequestParam(value = "active", required = false) Boolean active) {
        if (active != null) {
                return new ResponseEntity<>(discountCollectionService.getIsActiveList(active), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(discountCollectionService.getAllList(), HttpStatus.OK);
        }
    }

    /**
     * Processing request to provide a instance of entity {@link DiscountCollectionDto} by name
     * If not specified, returned http-status - 400 Bad request,
     * otherwise the instances of entity that have the specified name  value is returned.
     *
     * @param name - name class for find.
     * @return instance of entity {@link ResponseEntity} includes instance of DiscountCollection and HTTP-status
     */
    @ApiOperation(value = "Collection by name", response = DiscountCollectionDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved Collection"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Collection by name you were trying to requested is not found.")
    })
    @GetMapping("/{name}")
    public ResponseEntity getDiscountCollectionByName(
            @ApiParam(value = "the name of the Collection for which data is requested")
            @PathVariable String name) {
        if (name != null) {
            return new ResponseEntity<>(discountCollectionService.getByName(name), HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    /**
     * Processing request to saved a instance of entity {@link DiscountCollectionDto}
     * The received request body is passed to the service level for further processing
     *
     * @param body - body request in format entity {@link DiscountCollectionDto}
     * @return instance of entity {@link ResponseEntity} includes saved instance of DiscountCollection and HTTP-status
     */
    @ApiOperation(value = "Add new Collection in DB", response = DiscountCollectionDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added Collection"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Collection by name exist in DB")
    })
    @PostMapping
    @ValidateRole(roles = {"ROLE_ADMIN"})
    public ResponseEntity saveDiscountCollection(
            @ApiParam(value = "Collection object of the specified form to be saved in the DB ")
            @RequestBody @Valid DiscountCollectionDto body,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
         return new ResponseEntity<>(discountCollectionService.save(body), HttpStatus.OK);
    }

    /**
     * Processing request to updated a instance of entity {@link DiscountCollectionDto}
     * The received request body is passed to the service level for further processing
     *
     * @param body - body request in format entity {@link DiscountCollectionDto}
     * @return instance of entity {@link ResponseEntity} includes updated instance of DiscountCollection and HTTP-status
     */
    @ApiOperation(value = "Update Collection in DB", response = DiscountCollectionDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully update Collection"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Collection by name not found in DB for update")
    })
    @PutMapping
    @ValidateRole(roles = {"ROLE_ADMIN"})
    public ResponseEntity updateDiscountCollection(
            @ApiParam(value = "Collection object of the specified form to be updated in the DB")
            @RequestBody @Valid DiscountCollectionDto body,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
         return new ResponseEntity<>(discountCollectionService.update(body), HttpStatus.OK);
    }

    /**
     * Processing request to deleted a instance of entity {@link DiscountCollectionDto} by name
     * The specified Collection name is passed to the service level for deletion
     * and a logical response is expected for the success of the operation
     * If logical response  - true, http status 200 is returned
     *                      - false, http status 400 is returned
     *
     * @param name - name Collection for deleted.
     * @return instance of entity {@link ResponseEntity} includes  HTTP-status
     */
    @ApiOperation(value = "Delete Collection from DB", response = DiscountCollectionDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully delete Collection"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Collection by name not found in DB for delete")
    })
    @DeleteMapping("/{name}")
    @ValidateRole(roles = {"ROLE_ADMIN"})
    public ResponseEntity deleteDiscountCollection(
            @ApiParam(value = "Name of Collection for delete")
            @PathVariable String name,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        boolean isDelete = discountCollectionService.delete(name);
        if (isDelete) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
