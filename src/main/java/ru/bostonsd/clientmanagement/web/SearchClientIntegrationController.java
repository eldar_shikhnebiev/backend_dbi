package ru.bostonsd.clientmanagement.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.bostonsd.clientmanagement.integration.model.ClientFns;
import ru.bostonsd.clientmanagement.integration.service.ClientParser;
import ru.bostonsd.clientmanagement.integration.service.SearchClientIntegration;

import java.util.ArrayList;

/**
 * Controller for working with API DaData.ru (a copy of the data from the Federal tax service)
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 4, 2019
 */
@RestController
@RequestMapping("/fns/search/client")
@Slf4j
public class SearchClientIntegrationController {

    @Autowired
    private SearchClientIntegration searchService;

    @Autowired
    private ClientParser clientParser;


    /**
     * Processing request to provide a instance of entity {@link ClientFns} by inn
     * If not specified or not found, returned http-status - ok with empty body,
     * otherwise the instances of entity that have the specified inn value is returned.
     *
     * @param inn - inn client for find.
     * @return instance of entity {@link ResponseEntity} includes instance of ClientFns and HTTP-status
     */
    @GetMapping("/{inn}")
    public ResponseEntity getClientByInn(@PathVariable String inn) {
        if (inn != null) {
            String clientsJson = searchService.getEntityJsonByInn(inn);
            log.info("clientsJson: {}", clientsJson);
            if (clientsJson != null) {
                ClientFns client = clientParser.parse(clientsJson);
                if (client != null) {
                    return new ResponseEntity<>(client, HttpStatus.OK);
                }
            }
        }
        return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
    }
}
