package ru.bostonsd.clientmanagement.web.handler;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.bostonsd.clientmanagement.domain.exception.*;
import ru.bostonsd.clientmanagement.domain.models.*;
import ru.bostonsd.clientmanagement.history.exception.TicketException;
import ru.bostonsd.clientmanagement.history.models.Ticket;
import ru.bostonsd.clientmanagement.security.exception.CustomSecurityException;

/**
 * Controller for handler internal exception and transformation in HTTP-response
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 6, 2019
 *
 * */


@RestControllerAdvice
public class AppExceptionHadler {

    @ExceptionHandler(CustomSecurityException.class)
    protected ResponseEntity<ResponseException> CustomSecurityException (CustomSecurityException ex) {
        return new ResponseEntity<>(new ResponseException(ex.getMessage()), HttpStatus.NOT_FOUND);
    }

    /**
     * Handling internal exceptions that occur when working with the {@link Client} model
     * and wrapping it as a response
     *
     * @param ex - an exception thrown at the service level
     * @return - instance of entity {@link ResponseEntity} includes message of exception and HTTP-status
     */
    @ExceptionHandler(ClientException.class)
    protected ResponseEntity<ResponseException> ClientException (ClientException ex) {
        return new ResponseEntity<>(new ResponseException(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handling internal exceptions that occur when working with the {@link Distributor} model
     * and wrapping it as a response
     *
     * @param ex - an exception thrown at the service level
     * @return - instance of entity {@link ResponseEntity} includes message of exception and HTTP-status
     */
    @ExceptionHandler(DistributorException.class)
    protected ResponseEntity<ResponseException> DistributorException (DistributorException ex) {
        return new ResponseEntity<>(new ResponseException(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handling internal exceptions that occur when working with the {@link DiscountCollection} model
     * and wrapping it as a response
     *
     * @param ex - an exception thrown at the service level
     * @return - instance of entity {@link ResponseEntity} includes message of exception and HTTP-status
     */
    @ExceptionHandler(DiscountCollectionException.class)
    protected ResponseEntity<ResponseException> DiscountCollectionException (DistributorException ex) {
        return new ResponseEntity<>(new ResponseException(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handling internal exceptions that occur when working with the {@link ClientClassification} model
     * and wrapping it as a response
     *
     * @param ex - an exception thrown at the service level
     * @return - instance of entity {@link ResponseEntity} includes message of exception and HTTP-status
     */
    @ExceptionHandler(ClientClassificationException.class)
    protected ResponseEntity<ResponseException> ClientClassificationException (ClientClassificationException ex) {
        return new ResponseEntity<>(new ResponseException(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handling internal exceptions that occur when working with the {@link SalesVolume} model
     * and wrapping it as a response
     *
     * @param ex - an exception thrown at the service level
     * @return - instance of entity {@link ResponseEntity} includes message of exception and HTTP-status
     */
    @ExceptionHandler(SalesVolumeException.class)
    protected ResponseEntity<ResponseException> ClientClassificationException (SalesVolumeException ex) {
        return new ResponseEntity<>(new ResponseException(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handling internal exceptions that occur when working with the {@link Ticket} model
     * and wrapping it as a response
     *
     * @param ex - an exception thrown at the service level
     * @return - instance of entity {@link ResponseEntity} includes message of exception and HTTP-status
     */
    @ExceptionHandler(TicketException.class)
    protected ResponseEntity<ResponseException> ClientClassificationException (TicketException ex) {
        return new ResponseEntity<>(new ResponseException(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handling an exception that occurs when checking the http request body.
     *
     * @param e - an exception thrown at the service level
     * @return - instance of entity {@link ResponseEntity} includes message of exception and HTTP-status
     */
    @ExceptionHandler(InvalidFormatException.class)
    public ResponseEntity<String> handleInvalidFormatException(InvalidFormatException e) {
        return ResponseEntity.badRequest().body(e.getMessage());
    }

    /**
     * Handling an exception that occurs when checking the http request body.
     *
     * @param e - an exception thrown at the service level
     * @return - instance of entity {@link ResponseEntity} includes message of exception and HTTP-status
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<String> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        return ResponseEntity.badRequest().body(e.getLocalizedMessage());
    }

    /**
     * Wrapper class for wrapping an exception message in {@link ResponseEntity}
     */
    @Data
    @AllArgsConstructor
    private static class ResponseException {
        private String message;
    }

}