package ru.bostonsd.clientmanagement.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bostonsd.clientmanagement.security.model.rest.LoginRequest;
import ru.bostonsd.clientmanagement.security.model.rest.SignUpRequest;
import ru.bostonsd.clientmanagement.security.model.rest.jwt.JwtResponse;
import ru.bostonsd.clientmanagement.security.service.AuthenticationService;
import ru.bostonsd.clientmanagement.security.service.impl.SCHDUserDao;
import ru.bostonsd.clientmanagement.util.jsonparser.importData.service.ImportDataService;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The JwtAuthenticationController is responsible for performing an authentication operation
 *
 * @author Sergey Petryaev (spetryaev@bostonsd.ru)
 * Created on December 12, 2019
 */
@RestController
@RequestMapping("/auth")
@CrossOrigin
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private SCHDUserDao userDao;

    @PostMapping("/signin")
    public ResponseEntity authenticate(@RequestBody LoginRequest loginRequest) throws Exception {
        String token = authenticationService.authenticateByCredentials(loginRequest.getUsername(), loginRequest.getPassword());
        if (token != null) {
            return ResponseEntity.ok(new JwtResponse(token));
        } else {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/signup")
    public ResponseEntity signUp(@RequestBody SignUpRequest signUpRequest) {
        try {
            Long userId = userDao.create(signUpRequest);
            Map<String, Object> response = new LinkedHashMap<>();
            response.put("status", "OK");
            response.put("message", "User has been created successfully");
            response.put("user_id", userId);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            Map<String, Object> response = new LinkedHashMap<>();
            response.put("status", "ERROR");
            response.put("message", e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

}
