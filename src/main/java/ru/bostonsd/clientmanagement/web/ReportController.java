package ru.bostonsd.clientmanagement.web;

import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.bostonsd.clientmanagement.reporter.service.ReportBuilder;
import ru.bostonsd.clientmanagement.security.model.annotation.ValidateRole;
import ru.bostonsd.clientmanagement.security.service.JwtTokenService;

import java.io.ByteArrayInputStream;

/**
 * Controller for processing requests to create reports
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 10, 2019
 */
@RestController
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private ReportBuilder reportBuilder;

    /**
     * Processing a request to receive a report on clients and distributors
     * in xls format with a jwt token containing authorization data
     *
     * @param jwt -token received during authorization.
     * @return - instance of entity {@link ResponseEntity} includes report in xls format and HTTP-status
     */
    @GetMapping
    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR"})
    public ResponseEntity getReport(
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String jwt) {
        if (jwt != null) {
            ByteArrayInputStream build = reportBuilder.buildReport(jwt);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,
                            "attachment; filename=clients_report.xlsx")
                    .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                    .body(new InputStreamResource(build));
        } else {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
    }


    @GetMapping("/check")
    @ValidateRole(roles = {"ROLE_ADMIN"})
    public ResponseEntity getCheckReport(@ApiParam(hidden = true)
                                         @RequestHeader("Authorization") String jwt) {
        ByteArrayInputStream build = reportBuilder.buildCheckReport();
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=clients_check_report.xlsx")
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(new InputStreamResource(build));
    }
}
