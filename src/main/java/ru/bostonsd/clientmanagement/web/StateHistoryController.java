package ru.bostonsd.clientmanagement.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bostonsd.clientmanagement.history.models.basic.ObjectHistory;
import ru.bostonsd.clientmanagement.history.models.basic.StateHistory;
import ru.bostonsd.clientmanagement.history.models.enums.HistoryObjectType;
import ru.bostonsd.clientmanagement.history.service.ObjectHistoryService;
import ru.bostonsd.clientmanagement.history.service.StateHistoryService;

import java.util.ArrayList;
import java.util.List;

/**
 * API for StateHistory entity
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * @date 19.12.2019
 */
@RestController
@RequestMapping("/history")
@Slf4j
public class StateHistoryController {

    @Autowired
    private StateHistoryService stateHistoryService;

    @Autowired
    private ObjectHistoryService objectHistoryService;

    @GetMapping("/{processId}")
    public ResponseEntity getByProcessId(@PathVariable String processId){
        if(processId != null){
            List<StateHistory> history = stateHistoryService.getByProcessId(processId);
            if(history != null) {
                return new ResponseEntity<>(history, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
            }
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{objectType}/{objectId}")
    public ResponseEntity getByTypeAndObjectId(@PathVariable String objectType, @PathVariable String objectId){
        if(objectType != null && objectId != null){
            try {
                List<ObjectHistory> objectHistories = objectHistoryService.getByTypeAndObjectId(HistoryObjectType.valueOf(objectType), objectId);
                return new ResponseEntity<>(objectHistories, HttpStatus.OK);
            }catch (Exception ex){
                log.error("Parse to history type error, message: {}", ex.getMessage());
            }
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


}
