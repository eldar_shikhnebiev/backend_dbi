package ru.bostonsd.clientmanagement.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUser;
import ru.bostonsd.clientmanagement.security.model.dto.UserModelDto;
import ru.bostonsd.clientmanagement.security.model.rest.BasicUpdateRequest;
import ru.bostonsd.clientmanagement.security.model.rest.FullUpdateRequest;
import ru.bostonsd.clientmanagement.security.model.rest.SignUpRequest;
import ru.bostonsd.clientmanagement.security.service.RoleService;
import ru.bostonsd.clientmanagement.security.service.UserService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * @date 11.12.2019
 */
@RestController
@RequestMapping("/users")
@Slf4j
public class UserManagementController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @GetMapping("/{id}")
    public ResponseEntity getUserById(@PathVariable Long id) {
        Map<String, Object> responseBody = new LinkedHashMap<>();
        if (id != null) {
            Optional<SCHDUser> userOptional = userService.getById(id);
            if (userOptional.isPresent()) {
                return new ResponseEntity<>(new UserModelDto(userOptional.get()), HttpStatus.OK);
            } else {
                responseBody.put("status", "INFO");
                responseBody.put("message", "User not found.");
                return new ResponseEntity<>(responseBody, HttpStatus.NOT_FOUND);
            }
        } else {
            responseBody.put("status", "ERROR");
            responseBody.put("message", "User id must be provided.");
            return new ResponseEntity<>(responseBody, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/search/distributor/{id}")
    public ResponseEntity getUsersByDistributorId(@PathVariable Long id,
                                                  @RequestParam(required = false) Integer page,
                                                  @RequestParam(required = false) Integer size) {
        Map<String, Object> responseBody = new LinkedHashMap<>();
        if (id != null) {
            List<SCHDUser> users = userService.getByDistributorId(id, page, size);
            List<UserModelDto> result = new ArrayList<>();
            if (users != null && !users.isEmpty()) {
                result = users.stream().map(UserModelDto::new).collect(Collectors.toList());
            }
            return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
            responseBody.put("status", "ERROR");
            responseBody.put("message", "Distributor id must be provided.");
            return new ResponseEntity<>(responseBody, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity getAllUsers(@RequestParam(required = false) Boolean isActive) {
        List<SCHDUser> users;
        if (isActive == null) {
            users = userService.getAll();
        } else {
            users = userService.getByIsActive(isActive);
        }
        List<UserModelDto> result = new ArrayList<>();
        if (users != null && !users.isEmpty()) {
            result = users.stream().map(UserModelDto::new).collect(Collectors.toList());
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/roleNames")
    public ResponseEntity getAllRoleNames() {
        List<String> roleNames = roleService.getAllRoleNames();
        return new ResponseEntity<>(roleNames, HttpStatus.OK);
    }

    @GetMapping("/search/role/{roleName}")
    public ResponseEntity getUsersByRoleName(@PathVariable String roleName,
                                             @RequestParam(required = false) Integer page,
                                             @RequestParam(required = false) Integer size) {
        Map<String, Object> responseBody = new LinkedHashMap<>();
        if (roleName != null) {
            List<SCHDUser> users = userService.getByRoleName(roleName, page, size);

            List<UserModelDto> result = new ArrayList<>();
            if (users != null && !users.isEmpty()) {
                result = users.stream().map(UserModelDto::new).collect(Collectors.toList());
            }
            return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
            responseBody.put("status", "ERROR");
            responseBody.put("message", "Role name must be provided.");
            return new ResponseEntity<>(responseBody, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity createUser(@RequestBody SignUpRequest user) {
        Map<String, Object> responseBody = new LinkedHashMap<>();
        if (user != null) {
            try {
                Long userId = userService.create(user);
                responseBody.put("status", "OK");
                responseBody.put("message", "Success.");
                responseBody.put("user_id", userId);
                return new ResponseEntity<>(responseBody, HttpStatus.OK);
            } catch (Exception e) {
                responseBody.put("status", "ERROR");
                responseBody.put("message", e.getMessage());
                return new ResponseEntity<>(responseBody, HttpStatus.BAD_REQUEST);
            }
        } else {
            responseBody.put("status", "ERROR");
            responseBody.put("message", "Missing request body.");
            return new ResponseEntity<>(responseBody, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/basic/{id}")
    public ResponseEntity updateBasicData(@PathVariable Long id, @RequestBody BasicUpdateRequest user) {
        Map<String, Object> responseBody = new LinkedHashMap<>();
        if (id != null && user != null) {
            try {
                userService.updateBasicUserData(id, user);
                responseBody.put("status", "OK");
                responseBody.put("message", "The user has been updated successfully.");
                return new ResponseEntity<>(responseBody, HttpStatus.OK);
            } catch (Exception e) {
                responseBody.put("status", "ERROR");
                responseBody.put("message", e.getMessage());
                return new ResponseEntity<>(responseBody, HttpStatus.BAD_REQUEST);
            }
        } else {
            responseBody.put("status", "ERROR");
            responseBody.put("message", "Missing User id or request body.");
            return new ResponseEntity<>(responseBody, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/full/{id}")
    public ResponseEntity updateFullData(@PathVariable Long id, @RequestBody FullUpdateRequest updateRequest) {
        Map<String, Object> responseBody = new LinkedHashMap<>();
        if (id != null && updateRequest != null) {
            try {
                userService.updateFullUserData(id, updateRequest);
                responseBody.put("status", "OK");
                responseBody.put("message", "The user has been updated successfully.");
                return new ResponseEntity<>(responseBody, HttpStatus.OK);
            } catch (Exception e) {
                responseBody.put("status", "ERROR");
                responseBody.put("message", e.getMessage());
                return new ResponseEntity<>(responseBody, HttpStatus.BAD_REQUEST);
            }
        } else {
            responseBody.put("status", "ERROR");
            responseBody.put("message", "Missing User id or request body.");
            return new ResponseEntity<>(responseBody, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteUser(@PathVariable Long id) {
        Map<String, Object> responseBody = new LinkedHashMap<>();
        if (id != null) {
            Optional<SCHDUser> user = userService.getById(id);
            if (!user.isPresent()) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
            try {
                userService.delete(id);
                responseBody.put("status", "OK");
                responseBody.put("message", "The user with ID=" + id + " has been deleted successfully.");
                return new ResponseEntity<>(responseBody, HttpStatus.OK);
            } catch (Exception e) {
                responseBody.put("status", "ERROR");
                responseBody.put("message", e.getMessage());
                return new ResponseEntity<>(responseBody, HttpStatus.BAD_REQUEST);
            }
        } else {
            responseBody.put("status", "ERROR");
            responseBody.put("message", "Missing User id.");
            return new ResponseEntity<>(responseBody, HttpStatus.BAD_REQUEST);
        }
    }

}
