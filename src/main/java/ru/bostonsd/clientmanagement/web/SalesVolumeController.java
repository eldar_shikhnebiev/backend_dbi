package ru.bostonsd.clientmanagement.web;


import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bostonsd.clientmanagement.domain.models.SalesVolume;
import ru.bostonsd.clientmanagement.domain.models.dto.SalesVolumeDto;
import ru.bostonsd.clientmanagement.domain.service.SalesVolumeService;
import ru.bostonsd.clientmanagement.security.model.annotation.ValidateRole;
import ru.bostonsd.clientmanagement.security.model.database.RoleEnum;
import ru.bostonsd.clientmanagement.security.model.rest.jwt.JwtTokenClaims;
import ru.bostonsd.clientmanagement.security.service.JwtTokenUtil;

import javax.validation.Valid;

/**
 * Controller for manage Sales Volume
 * <p>
 * Rest controller to handle http requests to the model {@link SalesVolume}.
 * Processing a request using an entity {@link SalesVolumeDto}
 * <p>
 * Upon receipt of the body, primary validation is performed using the annotation {@link Valid}
 * to match the constraints set for the entity fields.
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 9, 2019
 * Updated on March 2, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 */

@Api(value = "Sales Volume management", description = "CRUD operations with sales volume")
@RestController
@RequestMapping("/client/salesvolume")
public class SalesVolumeController {

    @Autowired
    private SalesVolumeService salesVolumeService;

    /**
     * Processing a request to provide a list of entity instances {@link SalesVolumeDto}  by given id client.
     * If id not specified, returned http-status - 400 Bad request.
     *
     * @param id - id client for find linked entity instances {@link SalesVolumeDto}.
     * @return instance of entity {@link ResponseEntity} includes instance of Distributor and HTTP-status
     */
    @ApiOperation(value = "View a list of sales volume based on a given Client ID ", response = SalesVolumeDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Client Id is incorrect")
    })
    @ValidateRole(roles = {"ROLE_ADMIN"})
    @GetMapping("/client/{id}")
    public ResponseEntity getClientById(
            @ApiParam(value = "specifies the Client ID to search in the DB Sales Volume with all linked distributors")
            @PathVariable Long id,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        if (id != null) {
            return new ResponseEntity<>(salesVolumeService.getListByClientId(id), HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Processing a request to provide a list of entity instances {@link SalesVolumeDto}  by given id distributor.
     * If id not specified, returned http-status - 400 Bad request.
     *
     * @param id - id distributor for find linked entity instances {@link SalesVolumeDto}.
     * @return instance of entity {@link ResponseEntity} includes list of SalesVolumeDto and HTTP-status
     */
    @ApiOperation(value = "View a list of sales volume based on a given Distributors ID ", response = SalesVolumeDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Distibutor Id is incorrect")
    })
    @ValidateRole(roles = {"ROLE_ADMIN"})
    @GetMapping("/distributor/{id}")
    public ResponseEntity getDistributorById(
            @ApiParam(value = "specifies the Distibutor ID to search in the DB Sales Volume with all linked clients")
            @PathVariable Long id,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        if (id != null) {
            return new ResponseEntity<>(salesVolumeService.getListByDistributorId(id), HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Processing request to saved a instance of entity {@link SalesVolumeDto}
     * The received request body is passed to the service level for further processing
     *
     * @param salesVolumeDto - body request in format entity {@link SalesVolumeDto}
     * @return instance of entity {@link ResponseEntity} includes saved instance of SalesVolume and HTTP-status
     */
    @ApiOperation(value = "Adding a new Distributor with sales volumes to the specified Client in the DB", response = SalesVolumeDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added new distributor with sales volume"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Distributor is already connected with client, only update is possible")
    })
    @ValidateRole(roles = {"ROLE_ADMIN"})
    @PostMapping
    public ResponseEntity saveClient(
            @ApiParam(value = "Client Id, Distributor Id, not previously associated whith new sales volume")
            @RequestBody @Valid SalesVolumeDto salesVolumeDto,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        return new ResponseEntity<>(salesVolumeService.save(salesVolumeDto), HttpStatus.OK);
    }

    /**
     * Processing a request to create an instance entity {@link SalesVolume} in data store
     * which includes creating a customer and distributor relationship by their ID
     * Before creating an instance of an entity, it checks for its presence in the data store
     * If exist linked  returned http-status - 400 Bad request.
     * <p>
     * If nor found, data is passed for creation
     *
     * @param clientId      - id client for created relationship with distributor
     * @param distributorId - id distributor for created relationship with client
     * @return instance of entity {@link ResponseEntity} includes message and HTTP-status
     */

    @ApiOperation(value = "Linked Client by Distributor in the DB")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully create link client and distributor"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Distributor is already connected with client, only update is possible")
    })
    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR"})
    @PostMapping("/createlink/")
    public ResponseEntity linkedClientAndDistributor(
            @ApiParam(value = "Client Id for linked ")
            @RequestParam(value = "idclient") Long clientId,
            @ApiParam(value = "Distributor Id for linked ")
            @RequestParam(value = "iddistributor") Long distributorId,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        JwtTokenClaims jwtTokenClaims  = JwtTokenUtil.getUserClaims(authorization);
        String message;
        if (salesVolumeService.existLinkClientWithDistributor(clientId, distributorId)) {
            if (jwtTokenClaims.getRoleName().equals(RoleEnum.ROLE_ADMIN.name())) {
                message = "Данный Клиент уже привязан к Дистрибьютору";
            } else if (jwtTokenClaims.getRoleName().equals(RoleEnum.ROLE_DISTRIBUTOR.name())) {
                message = "Данный Клиент уже привязан к Вам!";
            } else {
                message= "Клиент с ИД " + clientId + " и Дистрибьютор с ИД " + distributorId + " - уже связаны.";
            }
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        } else {
            salesVolumeService.linkClientWithDistributor(clientId, distributorId);
            if (jwtTokenClaims.getRoleName().equals(RoleEnum.ROLE_ADMIN.name())) {
                message = "Данный Клиент успешно привязан к Дистрибьютору";
            } else if (jwtTokenClaims.getRoleName().equals(RoleEnum.ROLE_DISTRIBUTOR.name())) {
                message = "Вы успешно привязали Клиента!";
            } else {
                message= "Клиент с ИД " + clientId + " и Дистрибьютор с ИД " + distributorId + " успешно связаны связаны.";
            }
            return new ResponseEntity<>(message, HttpStatus.OK);
        }
    }

    /**
     * Processing request to updated a instance of entity {@link SalesVolumeDto}
     * The received request body is passed to the service level for further processing
     *
     * @param salesVolumeDto - body request in format entity {@link SalesVolumeDto}
     * @return instance of entity {@link ResponseEntity} includes updated instance of SalesVolume and HTTP-status
     */
    @ApiOperation(value = "Update sales volumes Client by Distributor in the DB", response = SalesVolumeDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully update sales volume"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Update impossible, not linked Client and Distributor")
    })
    @ValidateRole(roles = {"ROLE_ADMIN"})
    @PutMapping
    public ResponseEntity updateSalesVolume(
            @ApiParam(value = "Sales volume for update")
            @RequestBody @Valid SalesVolumeDto salesVolumeDto,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        return new ResponseEntity<>(salesVolumeService.update(salesVolumeDto), HttpStatus.OK);
    }

    /**
     * Processing request to deleted a instance of entity {@link SalesVolumeDto} by id client and distributor
     * The specified id client and distributor is passed to the service level for deletion
     * and a logical response is expected for the success of the operation
     * If logical response  - true, http status 200 is returned
     * - false, http status 400 is returned
     *
     * @param clientId      - id client for deleted instance entity {@link SalesVolume}.
     * @param distributorId -id distributor for deleted instance entity {@link SalesVolume}.
     * @return instance of entity {@link ResponseEntity} includes  HTTP-status
     */
    @ApiOperation(value = "Delete sales volume to the specified Client and Distributor", response = Boolean.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully delete record sales volume"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Sales volume by Client Id and Distributor Id not found in DB for delete")
    })
    @ValidateRole(roles = {"ROLE_ADMIN"})
    @DeleteMapping
    public ResponseEntity deleteSalesVolumeByDistributorIdAndClientId(
            @ApiParam(value = "Client Id for delete sales volume")
            @RequestParam(value = "clientId") Long clientId,
            @ApiParam(value = "Distributor Id for delete sales volume")
            @RequestParam (value = "distributorId") Long distributorId,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        return new ResponseEntity<>(salesVolumeService.delete(clientId, distributorId), HttpStatus.OK);
    }

}
