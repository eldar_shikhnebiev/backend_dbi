package ru.bostonsd.clientmanagement.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.bostonsd.clientmanagement.security.exception.CustomSecurityException;
import ru.bostonsd.clientmanagement.security.model.rest.RecoveryPassword;
import ru.bostonsd.clientmanagement.security.service.RecoveryPasswordService;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

/**
 * Controller for manage Recovery password
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on February 5, 2020
 * Updated on February 7, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 *
 * */
@Slf4j
@Controller
@RequestMapping("/recovery")
public class RecoveryPasswordController {

    @Autowired
    private RecoveryPasswordService recoveryPasswordService;

    @Value("${frontend.base.url}")
    private String frondEndUrl;

    @Value("${backend.base.url}")
    private String backEndUrl;

    @Value("${server.port}")
    private String serverPort;

    private static final String PATH_CHECK = "/recovery/check";

    @PostMapping("/mail")
    public ResponseEntity sendMail(@RequestParam(value = "email") String email) {
        if (email != null) {
            String uriMail = backEndUrl + ":" + serverPort + PATH_CHECK;
            recoveryPasswordService.sendMail(email, uriMail);
            return new ResponseEntity<>("Ссылка для восстановления пароля направлена на указанную почту", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Не указан email для восстановления пароля", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/check")
    public ModelAndView checkLink(@RequestParam(value = "code") String code) {
        if (code != null) {
            String codeNew = recoveryPasswordService.recovery(code);
            if (codeNew != null) {
                RecoveryPassword recoveryPassword =  new RecoveryPassword();
                recoveryPassword.setCode(codeNew);
                return new ModelAndView("recovery", "recoveryModel", recoveryPassword);
            } else {
                return new ModelAndView("redirect:" + frondEndUrl);
            }
        } else {
            return new ModelAndView("redirect:" + frondEndUrl);
        }

    }

    @PostMapping("/password")
    public String saveNewPassword(@ModelAttribute("recoveryModel") RecoveryPassword password, ModelMap modelMap) {
        if (password != null) {
            try {
                recoveryPasswordService.updatePassword(password);
                modelMap.addAttribute("message", "Пароль успешно обновлен");
            } catch (CustomSecurityException e){
                modelMap.addAttribute("message", e.getMessage());
            }
            modelMap.addAttribute("urlRedirect", frondEndUrl);
            return "finish";
        } else {
            return "redirect:" + frondEndUrl;
        }

    }
}
