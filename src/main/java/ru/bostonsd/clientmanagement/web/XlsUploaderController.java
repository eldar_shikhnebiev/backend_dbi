package ru.bostonsd.clientmanagement.web;

import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.bostonsd.clientmanagement.security.model.annotation.ValidateRole;
import ru.bostonsd.clientmanagement.uploader.service.Uploader;

import javax.ws.rs.POST;

/**
 * Controller for processing requests to load data into storage
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 9, 2019
 * Updated on April 9, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 */
@RestController
@RequestMapping("/uploader")
public class XlsUploaderController {

    @Autowired
    private Uploader xlsUploader;

    /**
     * Processing a request to download data from an xls file.
     * It checks that the entity instance {@link MultipartFile} is not empty
     * and passes it to the Service level {@link Uploader} for further processing
     *
     * @param file - the file attached to the body of the http request to download
     * @return instance of entity {@link ResponseEntity} includes  HTTP-status
     */
    @PostMapping("/total")
    @ValidateRole(roles = {"ROLE_ADMIN"})
    public ResponseEntity uploadXls(
                                    @ApiParam(value = "Excel File with new data")
                                    @RequestBody MultipartFile file,
                                    @ApiParam(hidden = true)
                                    @RequestHeader("Authorization") String authorization) {
        if (file != null) {
            xlsUploader.upload(file);
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @PostMapping
    @ValidateRole(roles = {"ROLE_ADMIN"})
    public ResponseEntity uploadXlsUpdate(
                                        @ApiParam(value = "Excel File with updated data of Client")
                                        @RequestBody MultipartFile file,
                                        @ApiParam(hidden = true)
                                        @RequestHeader("Authorization") String authorization) {
        if (file != null) {
            xlsUploader.uploadUpdate(file);
            return new ResponseEntity<>("Данные успешно загружены.", HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

}
