package ru.bostonsd.clientmanagement.web;

import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bostonsd.clientmanagement.domain.models.Client;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientDto;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientShortDto;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientsUpdateTypeDto;
import ru.bostonsd.clientmanagement.domain.service.ClientService;
import ru.bostonsd.clientmanagement.history.service.crud.client.ClientHistoryService;
import ru.bostonsd.clientmanagement.security.model.annotation.ValidateRole;
import ru.bostonsd.clientmanagement.security.model.database.RoleEnum;
import ru.bostonsd.clientmanagement.security.model.rest.jwt.JwtTokenClaims;
import ru.bostonsd.clientmanagement.security.service.JwtTokenService;
import ru.bostonsd.clientmanagement.security.service.JwtTokenUtil;

import javax.validation.Valid;

/**
 * Controller for manage Client
 * <p>
 * Rest controller to handle http requests to the model {@link Client}.
 * Processing a request using an entity {@link ClientDto}
 * <p>
 * Upon receipt of the body, primary validation is performed using the annotation {@link Valid}
 * to match the constraints set for the entity fields.
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 6, 2019
 * Updated on May 21, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 *
 * */
@Slf4j
@Api(value = "Client management", description = "CRUD operations with Client")
@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Autowired
    private ClientHistoryService clientHistoryService;


    /**
     * Processing a request to provide a list of entity instances {@link ClientDto}.
     * To call the desired service method, the given parameters  view, pagination (page and size),
     * and authentication parameters are analyzed.
     * <p>
     * Authentication data is used to limit the list of received data about the entity {@link Client}
     * that the user has access to according to the role provided.
     * If the authentication data does not meet the requirements
     * (the admin role without the distributor ID
     * or the distributor role with the ID of an existing distributor in the repository),
     * a response with the http status 403 is returned
     * <p>
     * The value of the View parameter takes one of two values - short/full.
     * The first value - short is specified to get a list of the {@link Client}
     * entity sheet in DTO format {@link ClientShortDto}
     * The second value - full is specified to get a list of the {@link Client}
     * entity sheet in DTO format {@link ClientDto}
     * If a different value is specified the response is returned http-status - 400 Bad request.
     *
     * @param view           - selects the entity for the returned list
     * @param page           - number page in split list all instance entity {@link Client} in data store
     * @param sizePage       - count returned row in list
     * @param authorization - token authentication for header HTTP-request.
     * @return instance of entity {@link ResponseEntity} includes list of DistributorDto and HTTP-status
     */
    @ApiOperation(value = "View a list of Clients based on a given parameter", response = ClientDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "View you were trying to requested is not found")
    })
    @GetMapping("/list")
    public ResponseEntity getClientList(
            @ApiParam(value = "Optional parameter of the return list view - short/full")
            @RequestParam(value = "view", required = false, defaultValue = "short") String view,
            @ApiParam(value = "Optional parameter number page, linked parameter page. By Default -1")
            @RequestParam(value = "page", required = false, defaultValue = "-1") Integer page,
            @ApiParam(value = "Optional parameter size page, linked parameter page. By Default size 10")
            @RequestParam(value = "size", required = false, defaultValue = "10") Integer sizePage,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {

        JwtTokenClaims jwtTokenClaims  = JwtTokenUtil.getUserClaims(authorization);
        if (jwtTokenClaims.getRoleName().equals(RoleEnum.ROLE_ADMIN.name()) && jwtTokenClaims.getDistributorId() == null) {
            if (page > -1) {
                Pageable pageable = PageRequest.of(page, sizePage);
                switch (view) {
                    case "short": return new ResponseEntity<>(clientService.getShortFormPage(pageable), HttpStatus.OK);
                    case "full": return new ResponseEntity<>(clientService.getPage(pageable), HttpStatus.OK);
                    default: return new ResponseEntity(HttpStatus.BAD_REQUEST);
                }
            } else {
                switch (view) {
                    case "short": return new ResponseEntity<>(clientService.getShortFormAllList(), HttpStatus.OK);
                    case "full": return new ResponseEntity<>(clientService.getAllList(), HttpStatus.OK);
                    default: return new ResponseEntity(HttpStatus.BAD_REQUEST);
                }
            }
        } else if (jwtTokenClaims.getRoleName().equals(RoleEnum.ROLE_GOLDEN_DEALER.name()) && jwtTokenClaims.getDistributorId() == null)  {
            if (page > -1) {
                Pageable pageable = PageRequest.of(page, sizePage);
                return new ResponseEntity<>(clientService.getShortFormPage(pageable, true), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(clientService.getShortFormAllList(true), HttpStatus.OK);
            }
        } else if (jwtTokenClaims.getRoleName().equals(RoleEnum.ROLE_DISTRIBUTOR.name()) && jwtTokenClaims.getDistributorId() != null) {
            Long idDistributor = jwtTokenClaims.getDistributorId();
            if (page > -1) {
                Pageable pageable = PageRequest.of(page, sizePage);
                switch (view) {
                    case "short": return new ResponseEntity<>(clientService.getShortFormPageByDistributorId(idDistributor, pageable, true), HttpStatus.OK);
                    case "full": return new ResponseEntity<>(clientService.getPageByDistributorId(idDistributor, pageable, true), HttpStatus.OK);
                    default: return new ResponseEntity(HttpStatus.BAD_REQUEST);
                }
            } else {
                switch (view) {
                    case "short": return new ResponseEntity<>(clientService.getShortFormByDistributorId(idDistributor, true), HttpStatus.OK);
                    case "full": return new ResponseEntity<>(clientService.getListByDistributorId(idDistributor, true), HttpStatus.OK);
                    default: return new ResponseEntity(HttpStatus.BAD_REQUEST);
                }
            }
        } else {
            return new ResponseEntity<>("Forbidden – you don’t have permission to access ‘/client/list’ on this server", HttpStatus.FORBIDDEN);
        }
    }

    /**
     * Processing request to provide a instance of entity {@link ClientDto} by id
     * If not specified, returned http-status - 400 Bad request,
     * otherwise the instances of entity that have the specified id value is returned.
     *
     * @param id - id client for find.
     * @return instance of entity {@link ResponseEntity} includes instance of Client and HTTP-status
     */
    @ApiOperation(value = "Client by id", response = ClientDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved Client"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Client by id you were trying to requested is not found.")
    })
    @GetMapping("/{id}")
    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR"})
    public ResponseEntity getClientById(
            @ApiParam(value = "Id of the Client for which data is requested")
            @PathVariable Long id,
            @ApiParam(value = "Optional logical activity parameter in Client - true/false")
            @RequestParam(value = "checkActive", required = false) Boolean checkActive,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        if (id != null) {
            JwtTokenClaims jwtTokenClaims  = JwtTokenUtil.getUserClaims(authorization);
            if ((jwtTokenClaims.getRoleName().equals(RoleEnum.ROLE_ADMIN.name()) && jwtTokenClaims.getDistributorId() == null)) {
                if (checkActive == null) {
                        return new ResponseEntity<>(clientService.getClientDtoById(id), HttpStatus.OK);
                } else {
                        return new ResponseEntity<>(clientService.getActiveClientById(id, true), HttpStatus.OK);
                    }
            } else if (jwtTokenClaims.getRoleName().equals(RoleEnum.ROLE_DISTRIBUTOR.name()) && jwtTokenClaims.getDistributorId() != null) {
                return new ResponseEntity<>(clientService.getActiveClientById(id, false), HttpStatus.OK);
            } else  return new ResponseEntity(HttpStatus.FORBIDDEN);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Processing request to provide a instance of entity {@link ClientDto} by inn
     * If not specified, returned http-status - 400 Bad request,
     * otherwise the instances of entity that have the specified inn value is returned.
     *
     * @param inn - inn client for find.
     * @return instance of entity {@link ResponseEntity} includes instance of Client and HTTP-status
     */
    @ApiOperation(value = "Client by inn", response = ClientDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved Client"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Client by inn you were trying to requested is not found.")
    })
    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR", "ROLE_GOLDEN_DEALER"})
    @GetMapping("/searchbyinn/{inn}")
    public ResponseEntity getClientByInn(
            @ApiParam(value = "Inn of the Client for which data is requested")
            @PathVariable String inn,
            @ApiParam(value = "Optional logical activity parameter in Client - true/false")
            @RequestParam(value = "checkActive", required = false) Boolean checkActive,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        if (inn != null) {
            JwtTokenClaims jwtTokenClaims  = JwtTokenUtil.getUserClaims(authorization);
            if ((jwtTokenClaims.getRoleName().equals(RoleEnum.ROLE_ADMIN.name()) && jwtTokenClaims.getDistributorId() == null)) {
                if (checkActive == null) {
                    return new ResponseEntity<>(clientService.getByInn(inn), HttpStatus.OK);
                } else {
                    return new ResponseEntity<>(clientService.getActiveClientByInn(inn, true), HttpStatus.OK);
                }
            } else if (jwtTokenClaims.getRoleName().equals(RoleEnum.ROLE_DISTRIBUTOR.name()) && jwtTokenClaims.getDistributorId() != null) {
                return new ResponseEntity<>(clientService.getActiveClientByInn(inn, false), HttpStatus.OK);
            } else if (jwtTokenClaims.getRoleName().equals(RoleEnum.ROLE_GOLDEN_DEALER.name()) && jwtTokenClaims.getDistributorId() == null) {
                return new ResponseEntity<>(clientService.getActiveClientShortByInn(inn), HttpStatus.OK);
            } else  return new ResponseEntity(HttpStatus.FORBIDDEN);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }



    /**
     * Processing request to provide a instance of entity {@link ClientDto} by inn
     * If not specified, returned http-status - 400 Bad request,
     * otherwise the instances of entity that have the specified inn value is returned.
     *
     * @param inn - inn client for find.
     * @return instance of entity {@link ResponseEntity} includes instance of Client and HTTP-status
     */
    @ApiOperation(value = "Client by inn ", response = ClientDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved Client"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Client by inn you were trying to requested is not found.")
    })
    @ValidateRole(roles = {"ROLE_ADMIN"})
    @GetMapping("/searchbyinn/{inn}/selloutsalesavolume")
    public ResponseEntity getClientWithSalesVolumeByInn(
            @ApiParam(value = "Inn of the Client for which data is requested")
            @PathVariable String inn,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        if (inn != null) {
            return new ResponseEntity<>(clientService.getWithOnlySalesVolumeByInn(inn), HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Processing request to checking exist Client in DB by inn
     * If not specified, returned http-status - 400 Bad request,
     * otherwise the instances of entity that have the specified inn value is returned.
     *
     * @param inn - inn client for find.
     * @return message and HTTP-status
     */
    @ApiOperation(value = "Check exist the client by inn", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Client by inn you were trying to requested is not found.")
    })
    @ValidateRole(roles = {"ROLE_DISTRIBUTOR"})
    @GetMapping("/checkbyinn/{inn}")
    public ResponseEntity existByInn(
            @ApiParam(value = "Inn of the Client for which data is requested")
            @PathVariable String inn,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        if (inn != null) {
            if (clientService.existByInn(inn)) {
                return new ResponseEntity<>("Клиент найден в базе iCheck", HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Клиент не найден в базе iCheck", HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }


    @ApiOperation(value = "Creating link Client to Client", response = ClientDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully entity Client with created link"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Link Client to Client is not found.")
    })
    @ValidateRole(roles = {"ROLE_ADMIN"})
    @PostMapping("/link/")
    public ResponseEntity createLinkToClient(
            @ApiParam(value = "Id of the Client Parent for linked")
            @RequestParam Long clientParentId,
            @ApiParam(value = "Id of the Client Child for linked")
            @RequestParam Long clientChildId,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        if (clientParentId != null && clientChildId != null) {
            ClientDto clientDto = clientService.createLinkClientToClient(clientParentId, clientChildId);
            return new ResponseEntity<>(clientDto, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Deleting link Client to Client", response = ClientDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully entity Client without deleted link"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Link Client to Client is not exist for deleting")
    })
    @ValidateRole(roles = {"ROLE_ADMIN"})
    @DeleteMapping("/link/")
    public ResponseEntity deleteLinkToClient(
            @ApiParam(value = "Id of the Client Parent for linked")
            @RequestParam Long clientParentId,
            @ApiParam(value = "Id of the Client Child for linked")
            @RequestParam Long clientChildId,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        if (clientParentId != null && clientChildId != null) {
            ClientDto clientDto = clientService.deleteLinkClientToClient(clientParentId, clientChildId);
            return new ResponseEntity<>(clientDto, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Processing request to saved a instance of entity {@link ClientDto}
     * The received request body is passed to the service level for further processing
     *
     * @param body - body request in format entity {@link ClientDto}
     * @return instance of entity {@link ResponseEntity} includes saved instance of Client and HTTP-status
     */
    @ApiOperation(value = "Add new Client in DB", response = ClientDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added Client"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Client by inn exist in DB")
    })
    @ValidateRole(roles = "ROLE_ADMIN")
    @PostMapping
    public ResponseEntity saveClient(
            @ApiParam(value = "Client object of the specified form to be saved in the DB ")
            @RequestBody @Valid ClientDto body,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
          return new ResponseEntity<>(clientService.save(body), HttpStatus.OK);
    }

    /**
     * Processing request to updated a instance of entity {@link ClientDto}
     * The received request body is passed to the service level for further processing
     *
     * @param body - body request in format entity {@link ClientDto}
     * @return instance of entity {@link ResponseEntity} includes updated instance of Client and HTTP-status
     */
    @ApiOperation(value = "Update Client in DB", response = ClientDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully update Client"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Client by id not found in DB for update")
    })
    @ValidateRole(roles = "ROLE_ADMIN")
    @PutMapping
    public ResponseEntity updateClient(
            @ApiParam(value = "Client object of the specified form to be saved in the DB ")
            @RequestBody @Valid ClientDto body,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        if (body != null && authorization != null) {
            ClientDto clientDto = clientHistoryService.updateClientAndCreateHistory(body, authorization);
            return new ResponseEntity<>(clientDto, HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }


    /**
     * Processing request to mass updated value of Type of Client at instances of entity {@link ClientDto}
     * The received request body is passed to the service level for further processing
     *
     * @param body - body request in format entity {@link ClientDto}
     * @return HTTP-status
     */
    @ApiOperation(value = "mass updated value of Type of Collection at Clients in DB", response = ClientDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully update Client"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Client by id not found in DB for update")
    })
    @ValidateRole(roles = "ROLE_ADMIN")
    @PutMapping("/type")
    public ResponseEntity updateTypeOfClient(
             @ApiParam(value = "List id of Clients and new value Type to be update in the DB ")
             @RequestBody @Valid ClientsUpdateTypeDto body,
             @ApiParam(hidden = true)
             @RequestHeader("Authorization") String authorization) {
        if (body != null && authorization != null) {
            JwtTokenClaims jwtTokenClaims  = JwtTokenUtil.getUserClaims(authorization);
            if (jwtTokenClaims != null && jwtTokenClaims.getRoleName().equals(RoleEnum.ROLE_ADMIN.name()) && jwtTokenClaims.getDistributorId() == null) {
                boolean result = clientHistoryService.updateClientAndCreateHistory(body, authorization);
                if (result) {
                    return new ResponseEntity(HttpStatus.OK);
                }
            } else
                return new ResponseEntity<>("Forbidden – you don’t have permission to access ‘/client/list’ on this server", HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    /**
     * Processing request to deleted a instance of entity {@link ClientDto} by id
     * The specified id client is passed to the service level for deletion
     * and a logical response is expected for the success of the operation
     * If logical response  - true, http status 200 is returned
     *                      - false, http status 400 is returned
     *
     * @param id - id client for deleted.
     * @return instance of entity {@link ResponseEntity} includes  HTTP-status
     */
    @ApiOperation(value = "Delete Client from DB")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully delete Client"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Client by id not found in DB for delete")
    })
    @ValidateRole(roles = "ROLE_ADMIN")
    @DeleteMapping("/{id}")
    public ResponseEntity deleteClient(
            @ApiParam(value = "Client Id for delete")
            @PathVariable Long id,
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        boolean isDelete = clientService.delete(id);
        if (isDelete) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @ValidateRole(roles = "ROLE_ADMIN")
    @GetMapping("/updateSellOut")
    public ResponseEntity update(
            @ApiParam(hidden = true)
            @RequestHeader("Authorization") String authorization) {
        boolean resultUpdate = clientService.updateFromSellOut();
        if (resultUpdate) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
