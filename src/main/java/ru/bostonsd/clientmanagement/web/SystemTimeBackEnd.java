package ru.bostonsd.clientmanagement.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/time")
public class SystemTimeBackEnd {

    @GetMapping
    public ResponseEntity getSystemTimeBackEnd() {
        return new ResponseEntity(System.currentTimeMillis(), HttpStatus.OK);
    }
}
