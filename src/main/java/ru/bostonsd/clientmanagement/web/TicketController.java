package ru.bostonsd.clientmanagement.web;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bostonsd.clientmanagement.history.models.Ticket;
import ru.bostonsd.clientmanagement.history.models.dto.TicketDto;
import ru.bostonsd.clientmanagement.history.models.dto.TicketDtoShort;
import ru.bostonsd.clientmanagement.history.service.TicketService;
import ru.bostonsd.clientmanagement.security.model.annotation.ValidateRole;
import ru.bostonsd.clientmanagement.security.model.database.RoleEnum;
import ru.bostonsd.clientmanagement.security.model.rest.jwt.JwtTokenClaims;
import ru.bostonsd.clientmanagement.security.service.JwtTokenService;
import ru.bostonsd.clientmanagement.security.service.JwtTokenUtil;

import javax.validation.Valid;

/**
 * Controller for manage Ticket
 * <p>
 * Rest controller to handle http requests to the model {@link Ticket}.
 * Processing a request using an entity {@link TicketDto}.
 * Upon receipt of the body, primary validation is performed using the annotation {@link Valid}
 * to match the constraints set for the entity fields.
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 12, 2019
 */

@Api(value = "Ticket management", description = "CRUD operations with Ticket")
@RestController
@RequestMapping("/ticket")
public class TicketController {

    @Autowired
    private TicketService ticketService;

    @Autowired
    private JwtTokenService jwtTokenService;

    /**
     * Processing a request to provide a list of entity instances {@link TicketDto}.
     * To call the desired service method, the given parameters  view and distributor id are analyzed.
     * <p>
     * The value of the View parameter takes one of two values - short/full.
     * The first value - short is specified to get a list of the {@link Ticket}
     * entity sheet in DTO format {@link TicketDtoShort}
     * The second value - full is specified to get a list of the {@link Ticket}
     * entity sheet in DTO format {@link TicketDto}
     * If a different value is specified the response is returned http-status - 400 Bad request.
     * <p>
     * The distributor ID is an optional parameter.
     * If not specified, the default value is 0 and a list of all instances of the entity {@link Ticket} is requested
     * Otherwise the list of tickets is returned only for the distributor with the specified ID.
     *
     * @param view - selects the entity for the returned list
     * @return instance of entity {@link ResponseEntity} includes list of DistributorDto and HTTP-status
     */
    @ApiOperation(value = "View a list of Tickets based on a given parameter", response = TicketDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "View you were trying to requested is not found")
    })
    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR"})
    @GetMapping("/list")
    public ResponseEntity getTicketList(
             @ApiParam(value = "Optional parameter of the return list view - short/full")
             @RequestParam(value = "view", required = false, defaultValue = "short") String view,
             @ApiParam(value = "Optional parameter of the distributor name by which tickets are selected")
             @RequestParam(value = "distributorInitiator", required = false) String distributorInitiator,
             @ApiParam(value = "Optional parameter number page of list tickets. By Default -1 return all page")
             @RequestParam (value = "page", required = false, defaultValue = "-1") Integer page,
             @ApiParam(value = "Optional parameter size page, linked parameter page. By Default size 10")
             @RequestParam (value = "size", required = false, defaultValue = "10") Integer sizePage,
             @ApiParam(hidden = true)
             @RequestHeader("Authorization") String authorization)
    {

        JwtTokenClaims userDetails  = JwtTokenUtil.getUserClaims(authorization);
        if ((userDetails.getRoleName().equals(RoleEnum.ROLE_DISTRIBUTOR.name()) && userDetails.getDistributorId() != null) ||
                (userDetails.getRoleName().equals(RoleEnum.ROLE_ADMIN.name()) && userDetails.getDistributorId() == null)) {
            if (userDetails.getRoleName().equals(RoleEnum.ROLE_DISTRIBUTOR.name())) {
                distributorInitiator = userDetails.getDistributorName();
            }
            if (page > -1) {
                Pageable pageable = PageRequest.of(page, sizePage);
                switch (view) {
                    case "short":
                        if (distributorInitiator == null) {
                            return new ResponseEntity<>(ticketService.getShortFormPage(pageable), HttpStatus.OK);
                        } else {
                            return new ResponseEntity<>(ticketService
                                    .getPageShortFormByDistributorInitiator(distributorInitiator, pageable), HttpStatus.OK);
                        }
                    case "full":
                        if (distributorInitiator == null) {
                            return new ResponseEntity<>(ticketService.getPage(pageable), HttpStatus.OK);
                        } else {
                            return new ResponseEntity<>(ticketService
                                    .getPageByDistributorInitiator(distributorInitiator, pageable), HttpStatus.OK);
                        }
                    default:
                        return new ResponseEntity(HttpStatus.BAD_REQUEST);
                }
            } else {
                switch (view) {
                    case "short":
                        if (distributorInitiator == null) {
                            return new ResponseEntity<>(ticketService.getShortFormAllList(), HttpStatus.OK);
                        } else {
                            return new ResponseEntity<>(ticketService
                                    .getListShortFormByDistributorInitiator(distributorInitiator), HttpStatus.OK);
                        }
                    case "full":
                        if (distributorInitiator == null) {
                            return new ResponseEntity<>(ticketService.getAllList(), HttpStatus.OK);
                        } else {
                            return new ResponseEntity<>(ticketService
                                    .getListByDistributorInitiator(distributorInitiator), HttpStatus.OK);
                        }
                    default:
                        return new ResponseEntity(HttpStatus.BAD_REQUEST);
                }
            }
        } else return new ResponseEntity("Forbidden – you don’t have permission to access ‘/client/list’ on this server", HttpStatus.FORBIDDEN);
    }

    /**
     * Processing request to provide a instance of entity {@link TicketDto} by id
     * If not specified, returned http-status - 400 Bad request,
     * otherwise the instances of entity that have the specified id  value is returned.
     *
     * @param id - id ticket for find.
     * @return instance of entity {@link ResponseEntity} includes instance of {@link TicketDto} and HTTP-status
     */
    @ApiOperation(value = "Ticket by id ", response = TicketDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved Ticket"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Ticket by id you were trying to requested is not found.")
    })
    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR"})
    @GetMapping("/{id}")
    public ResponseEntity getTicketById(
            @ApiParam(value = "Id of the Ticket for which data is requested")
            @PathVariable Long id) {
        if (id != null) {
            return new ResponseEntity<>(ticketService.getTicketDtoById(id), HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "List ticket by process id ", response = TicketDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Ticket by process id you were trying to requested is not found.")
    })
    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR"})
    @GetMapping("/search/process/{id}")
    public ResponseEntity getTicketByProcessId (
            @ApiParam(value = "Process id of the List Tickets for which data is requested")
            @PathVariable  Long id,
            @ApiParam(value = "Optional parameter number page of list tickets. By Default -1 return all page")
            @RequestParam (value = "page", required = false, defaultValue = "-1") Integer page,
            @ApiParam(value = "Optional parameter size page, linked parameter page. By Default size 10")
            @RequestParam (value = "size", required = false, defaultValue = "10") Integer sizePage) {
        if (id != null) {
            if (page > -1) {
                Pageable pageable = PageRequest.of(page, sizePage);
                return new ResponseEntity<>(ticketService.getPageByProcessId(id, pageable), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(ticketService.getListByProcessId(id), HttpStatus.OK);
            }
        } else  {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "List ticket by process name ", response = TicketDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Ticket by process name you were trying to requested is not found.")
    })
    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR"})
    @GetMapping("/search/process/name/{name}")
    public ResponseEntity getTicketByProcessName (
            @ApiParam(value = "Process name of the List Tickets for which data is requested")
            @PathVariable  String name,
            @ApiParam(value = "Optional parameter number page of list tickets. By Default -1 return all page")
            @RequestParam (value = "page", required = false, defaultValue = "-1") Integer page,
            @ApiParam(value = "Optional parameter size page, linked parameter page. By Default size 10")
            @RequestParam (value = "size", required = false, defaultValue = "10") Integer sizePage) {
        if (name != null) {
            if (page > -1) {
                Pageable pageable = PageRequest.of(page, sizePage);
                return new ResponseEntity<>(ticketService.getPageByProcessName(name, pageable), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(ticketService.getListByProcessName(name), HttpStatus.OK);
            }
        } else  {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "List ticket by initiator id", response = TicketDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Ticket by initiator id you were trying to requested is not found.")
    })
    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR"})
    @GetMapping("/search/initiator/{id}")
    public ResponseEntity getTicketByInitiatorId (
            @ApiParam(value = "Initiator id of the List Tickets for which data is requested")
            @PathVariable  String id,
            @ApiParam(value = "Optional parameter number page of list tickets. By Default -1 return all page")
            @RequestParam (value = "page", required = false, defaultValue = "-1") Integer page,
            @ApiParam(value = "Optional parameter size page, linked parameter page. By Default size 10")
            @RequestParam (value = "size", required = false, defaultValue = "10") Integer sizePage) {
        if (id != null) {
            if (page > -1) {
                Pageable pageable = PageRequest.of(page, sizePage);
                return new ResponseEntity<>(ticketService.getPageDtoByInitiatorId(id, pageable), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(ticketService.getListByInitiatorId(id), HttpStatus.OK);
            }
        } else  {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Processing request to saved a instance of entity {@link TicketDto}
     * The received request body is passed to the service level for further processing
     *
     * @param body - body request in format entity {@link TicketDto}
     * @return instance of entity {@link ResponseEntity} includes saved instance of {@link TicketDto} and HTTP-status
     */
    @ApiOperation(value = "Add new Ticket in DB", response = TicketDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully added Ticket"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Ticket by id exist in DB")
    })
    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR"})
    @PostMapping
    public ResponseEntity saveTicket(
            @ApiParam(value = "Ticket object of the specified form to be saved in the DB ")
            @RequestBody @Valid TicketDto body) {
          return new ResponseEntity<>(ticketService.save(body), HttpStatus.OK);
    }


    /**
     * Processing request to deleted a instance of entity {@link TicketDto} by id
     * The specified id ticket is passed to the service level for deletion
     * and a logical response is expected for the success of the operation
     * If logical response  - true, http status 200 is returned
     *                      - false, http status 400 is returned
     *
     * @param id - id ticket for deleted.
     * @return instance of entity {@link ResponseEntity} includes  HTTP-status
     */
    @ApiOperation(value = "Delete Ticket from DB", response = TicketDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully delete Ticket"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "Ticket by id not found in DB for delete")
    })
    @ValidateRole(roles = {"ROLE_ADMIN", "ROLE_DISTRIBUTOR"})
    @DeleteMapping("/{id}")
    public ResponseEntity deleteTicket(
            @ApiParam(value = "Ticket Id for delete")
            @RequestParam Long id) {
        boolean isDelete = ticketService.delete(id);
        if (isDelete) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
