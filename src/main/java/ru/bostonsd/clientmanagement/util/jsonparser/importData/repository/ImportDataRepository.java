package ru.bostonsd.clientmanagement.util.jsonparser.importData.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bostonsd.clientmanagement.util.jsonparser.importData.model.ImportData;

@Repository
public interface ImportDataRepository extends JpaRepository<ImportData, Long> {
}
