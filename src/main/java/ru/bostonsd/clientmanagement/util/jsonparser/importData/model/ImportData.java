package ru.bostonsd.clientmanagement.util.jsonparser.importData.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "ss_clients")
public class ImportData implements Serializable {

    @Id
    @Column(name = "_id")
    private Long id;
    @Column(name = "_disabled")
    private Long disabled;
    @Column
    private String inn;
    @Column
    private String email;
    @Column
    private String registered;
    @Column
    private String name;
    @Column
    private String phone;
    @Column(name = "mobile_phone")
    private String mobilePhone;
    @Column
    private String address;
    @Column
    private String site;
    @Column
    private Long confirmed;
    @Column
    private Long type;
    @Column(name = "class")
    private Long clazz;
}
