package ru.bostonsd.clientmanagement.util.jsonparser;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author asaburov on 12/10/2019
 */

@Slf4j
@Service
public class JsonParserServiceImpl implements JsonParserService {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public <T> T parseJsonToObject(String json, Class<T> clazz){
        T result = null;
        try {
            result = objectMapper.readValue(json, clazz);
        } catch (JsonProcessingException e) {
            log.error("JSON PARSE ERROR, MESSAGE: {}", e.getMessage());
        }
        return result;
    }

    public <T> T parseJsonToListObjects(String json, TypeReference<T> typeReference){
        T result = null;
        try {
            result = objectMapper.readValue(json, typeReference);
        }catch (Exception ex){
            log.error("Parse json to list objects, message: {}", ex.getMessage());
        }
        return result;
    }


    @Override
    public <T> String parseObjectToJson(T object){
        String json = null;
        try {
            json = objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error("PARSE OBJECT TO JSON ERROR, MESSAGE: {}", e.getMessage());
        }
        return json;
    }

    @Override
    public <T> T convertMapToObject(Object object, Class<T> tClass){
        T result = null;
        try {
            result = objectMapper.convertValue(object, tClass);
        }catch (Exception ex){
            log.error("Convert Map to object error, message: {}", ex.getMessage());
        }
        return result;
    }
}
