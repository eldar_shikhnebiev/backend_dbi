package ru.bostonsd.clientmanagement.util.jsonparser.importData.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.domain.models.Client;
import ru.bostonsd.clientmanagement.domain.repository.ClientRepository;
import ru.bostonsd.clientmanagement.util.jsonparser.importData.model.ImportData;
import ru.bostonsd.clientmanagement.util.jsonparser.importData.repository.ImportDataRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ImportDataServiceImpl implements ImportDataService {

    @Autowired
    private ImportDataRepository repository;

    @Autowired
    private ClientRepository clientRepository;

    public void upload() {
        List<ImportData> allImports = repository.findAll();
        Map<String, ImportData> importDataMap = allImports.stream().filter(e -> e.getInn() != null).collect(Collectors.toMap(
                ImportData::getInn,
                e -> e,
                (e1, e2) -> e1
        ));
        long withSite = allImports.stream().filter(e -> isValid(e.getSite())).count();
        long withPhone = allImports.stream().filter(e -> isValid(e.getPhone())).count();
        long withAddress = allImports.stream().filter(e -> isValid(e.getAddress())).count();
        long withEmail = allImports.stream().filter(e -> isValid(e.getEmail())).count();

        log.info("All import record data: {} \n" +
                "With site: {} \n" +
                "With phones: {} \n" +
                "With addresses : {} \n" +
                "With email: {}", allImports.size(), withSite, withPhone, withAddress, withEmail);

        List<Client> allClients = clientRepository.findAll();

        long withSite2 = allClients.stream().filter(e -> isValid(e.getSite())).count();
        long withPhone2 = allClients.stream().filter(e -> isValid(e.getPhone())).count();
        long withAddress2 = allClients.stream().filter(e -> isValid(e.getAddress())).count();
        long withEmail2 = allClients.stream().filter(e -> isValid(e.getEmail())).count();

        log.info("All clients: {} \n" +
                "With site: {} \n" +
                "With phones: {} \n" +
                "With addresses : {} \n" +
                "With email: {}", allClients.size(), withSite2, withPhone2, withAddress2, withEmail2);

        log.info("Imports: {}, {}, {}", allImports.get(0), allImports.get(100), allImports.get(1000));

        List<Client> forUpdate = new ArrayList<>();
        for (Client client : allClients) {
            if (importDataMap.containsKey(client.getInn())) {
                ImportData imp = importDataMap.get(client.getInn());
                if (imp != null
                        && (isValid(imp.getEmail()) || isValid(imp.getAddress()) || isValid(imp.getPhone()) || isValid(imp.getSite()))) {

                    if (isValid(imp.getEmail())) client.setEmail(imp.getEmail());
                    if (isValid(imp.getAddress())) client.setAddress(imp.getAddress());
                    if (isValid(imp.getPhone())) client.setSite(imp.getSite());
                    if (isValid(imp.getSite())) client.setPhone(imp.getPhone());
                    forUpdate.add(client);
                }
            }
        }
        log.info("Size client for update: {}", forUpdate.size());
        clientRepository.saveAll(forUpdate);
    }

    private boolean isValid(String value) {
        if (value != null) {
            return !value.isEmpty() && !value.equals("''");
        }
        return false;
    }
}
