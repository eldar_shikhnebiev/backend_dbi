package ru.bostonsd.clientmanagement.util.jsonparser;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * @author asaburov on 12/10/2019
 */
public interface JsonParserService {

    <T> T parseJsonToObject(String json, Class<T> clazz);
    <T> T parseJsonToListObjects(String json, TypeReference<T> typeReference);
    <T> String parseObjectToJson(T object);
    <T> T convertMapToObject(Object object, Class<T> tClass);
}
