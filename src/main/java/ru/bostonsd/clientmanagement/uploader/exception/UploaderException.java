package ru.bostonsd.clientmanagement.uploader.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.bostonsd.clientmanagement.domain.exception.AppException;

import java.io.PrintWriter;
import java.io.StringWriter;


/**
 * Exception for Client Service
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 5, 2019
 * Updated on April 2, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 *
 * */

@Slf4j
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Bad upload file ")
public class UploaderException extends AppException {
    public UploaderException(String message) {
        super(message);
        printLog();
    }

    public void printLog () {
        StringWriter trace = new StringWriter();
        printStackTrace(new PrintWriter(trace));
        log.error(trace.toString());
    }
}