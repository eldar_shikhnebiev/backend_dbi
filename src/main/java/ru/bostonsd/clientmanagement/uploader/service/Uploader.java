package ru.bostonsd.clientmanagement.uploader.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * Service for uploading data from a file to storage
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 9, 2019
 * Updated on April 9, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 */
public interface Uploader {
    /**
     * Uploading data into storage from the file specified in the arguments
     *
     * @param file - the file containing the data to download
     */
    void upload(MultipartFile file);

    void uploadUpdate(MultipartFile file);
}
