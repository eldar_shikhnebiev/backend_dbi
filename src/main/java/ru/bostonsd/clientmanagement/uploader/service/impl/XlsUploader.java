package ru.bostonsd.clientmanagement.uploader.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.UnsupportedFileFormatException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.bostonsd.clientmanagement.domain.models.Client;
import ru.bostonsd.clientmanagement.domain.models.ClientClassification;
import ru.bostonsd.clientmanagement.domain.models.Distributor;
import ru.bostonsd.clientmanagement.domain.models.SalesVolume;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientDto;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientLinkedDto;
import ru.bostonsd.clientmanagement.domain.models.dto.DistributorDto;
import ru.bostonsd.clientmanagement.domain.models.dto.SalesVolumeClientDto;
import ru.bostonsd.clientmanagement.domain.models.enums.ClientType;
import ru.bostonsd.clientmanagement.domain.repository.ClientClassificationRepository;
import ru.bostonsd.clientmanagement.domain.repository.ClientRepository;
import ru.bostonsd.clientmanagement.domain.repository.DistributorRepository;
import ru.bostonsd.clientmanagement.domain.service.ClientService;
import ru.bostonsd.clientmanagement.domain.service.DistributorService;
import ru.bostonsd.clientmanagement.domain.service.TransformationService;
import ru.bostonsd.clientmanagement.uploader.service.Uploader;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Implementation for {@link Uploader}
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 9, 2019
 * Updated on April 9, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 */
@Service
@Slf4j
public class XlsUploader implements Uploader {

    @Autowired
    public DistributorRepository distributorRepository;

    @Autowired
    public DistributorService distributorService;

    @Autowired
    private ClientRepository clientRepository;


    @Autowired
    private ClientClassificationRepository classificationRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private TransformationService transformationService;


    private static final String DATA_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private static final String PARTNER_ID_PATTERN = "[0-9]+";

    /**
     * Implementation of the method defined in the interface {@link Uploader}
     * <p>
     * Loading client and distributor data into storage from the xls file specified in the arguments
     * with using entity {@link Client} and {@link Distributor}, respectively.
     * During data processing, it checks whether the client  or distributor is not in the storage before adding.
     * You also create a relationship in the and distributor repository using the entity {@link SalesVolume}
     *
     * @param xlsFile
     */
    @Override
    public void upload(MultipartFile xlsFile) {
        try {
            Workbook workbook = null;
            try {
                workbook = new XSSFWorkbook(xlsFile.getInputStream());
            } catch (IOException e) {
                log.error("GET DATA FROM XML ERROR: {}", e.getMessage());
            }
            if (workbook != null) {
                Sheet worksheet = workbook.getSheetAt(0);

                for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
                    Row row = worksheet.getRow(i);
                    if (row != null) {
                        Distributor distributor = Distributor.builder()
                                .name(row.getCell(0).getStringCellValue())
                                .inn(getInn(row.getCell(1)))
//                            .partnerId(getPartnerId(row.getCell(2)))
                                .build();

                        Distributor dis = distributorRepository.findByInn(distributor.getInn());

                        if (dis == null) {
                            dis = distributorRepository.save(distributor);
                        }
                        log.info("DISTRIBUTOR: {}", dis);

                        Client client = Client.builder()
                                .inn(getInn(row.getCell(4)))
                                .name(getClientName(row.getCell(3)))
                                .dateRegistration(parseDateTime(row.getCell(5).getStringCellValue()))
                                .clientType(ClientType.getByString(row.getCell(6).getStringCellValue()))
                                .clientClassification(getClassification(row.getCell(7).getStringCellValue()))
                                .salesVolumes(new ArrayList<>())
                                .build();

                        Client cl = clientRepository.findByInn(client.getInn());
                        if (cl == null) {
                            cl = clientRepository.save(client);
                        }
                        log.info("CLIENT: {}", cl);

                        if (dis != null && cl != null) {
                            boolean checkRelationship = clientService.existLinkClientWithDistributor(cl.getId(), dis.getId());
                            if (!checkRelationship) {
                                clientService.linkClientWithDistributor(cl.getId(), dis.getId());
                                log.info("LINK COMPLETE");
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("Uploading data error, message: {}", e.getMessage());
        }
    }

    @Override
    public void uploadUpdate(MultipartFile xlsFile) {
        Workbook workbook = null;
        try {
            try {
                workbook = new HSSFWorkbook(xlsFile.getInputStream());
            } catch (UnsupportedFileFormatException e) {
                workbook = new XSSFWorkbook(xlsFile.getInputStream());

            }
        } catch (IOException e) {
            log.error("GET DATA FROM XML ERROR: {}", e.getMessage());
        }
        if (workbook != null) {
            Sheet worksheet = workbook.getSheetAt(0);

            log.info("START: {}");
            // Get All Distributor from DB
            Map<String, Long> distributorDtoMapDb = distributorService.getMapInnAndId();
            String clientInn;
            ClientDto clientDtoFile;

            String distributorInn;
            Long distributorId;
            SalesVolumeClientDto salesVolumeClientDto;

            String clientLinkInn;
            ClientDto clientDtoLinkFile;
            ClientLinkedDto clientLinkedDto;

            // Map for data File
            Map<String, ClientDto> clientMapFile = new HashMap<>();
            Map<String, ClientLinkedDto> clientLinkedMapFile = new HashMap<>();
            // Extract from Xls File
            for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
                Row row = worksheet.getRow(i);
                if (row != null) {
                    //Extract Distributor info
                    distributorInn = getInn(row.getCell(1));
                    if (distributorInn != null && !distributorInn.equals("0")) {
                        distributorId = distributorDtoMapDb.get(distributorInn);
                        if (distributorId == null) {
                            DistributorDto distributorDto = DistributorDto.builder()
                                    .name(getStringValueCell(row.getCell(0)))
                                    .inn(distributorInn)
                                    .build();
                            distributorDto = distributorService.save(distributorDto);
                            log.info("SAVE NEW DISTRIBUTOR: {}", distributorDto);
                            distributorId = distributorDto.getId();
                            distributorDtoMapDb.put(distributorInn, distributorId);
                        }
                        // Create Sales Volume Entity DTI for setting in client Entity
                        salesVolumeClientDto = SalesVolumeClientDto.builder()
                                .idDistributor(distributorId)
                                .build();
                    } else {
                        salesVolumeClientDto = null;
                    }

                    //Extract linking Client
                    clientLinkInn = getInn(row.getCell(8));
                    if (clientLinkInn != null && !clientLinkInn.equals("0")) {
                        clientLinkedDto = clientLinkedMapFile.get(clientLinkInn);
                        if (clientLinkedDto == null) {
                            if (clientService.existByInn(clientLinkInn)) {
                                clientDtoLinkFile = clientService.getByInn(clientLinkInn);
                            } else {
                                clientDtoLinkFile = ClientDto.builder()
                                        .name(getStringValueCell(row.getCell(9)))
                                        .inn(clientLinkInn)
                                        .isActive(true)
                                        .build();
                                clientDtoLinkFile = clientService.save(clientDtoLinkFile);
                                log.info("SAVE NEW LINKED CLIENT: {}", clientDtoLinkFile);
                            }
                            clientLinkedDto = ClientLinkedDto.builder()
                                    .id(clientDtoLinkFile.getId())
                                    .inn(clientDtoLinkFile.getInn())
                                    .name(clientDtoLinkFile.getName())
                                    .build();
                            clientLinkedMapFile.put(clientLinkInn, clientLinkedDto);
                        }
                    } else {
                        clientLinkedDto = null;
                    }

                    //Extract Client info and add Distributor and links with other Client
                    clientInn = getInn(row.getCell(3));
                    if (clientInn != null && !clientInn.equals("0")) {
                        clientDtoFile = clientMapFile.get(clientInn);
                        if (clientDtoFile == null) {
                            clientDtoFile = ClientDto.builder()
                                    .name(getClientName(row.getCell(2)))
                                    .inn(clientInn)
                                    .email(getStringValueCell(row.getCell(4)))
                                    .address(getStringValueCell(row.getCell(5)))
                                    .site(getStringValueCell(row.getCell(6)))
                                    .phone(getStringValueCell(row.getCell(7)))
                                    .clientTypeString(getStringValueCell(row.getCell(10)))
                                    .clientClassificationString(getStringValueCell(row.getCell(11)))
                                    .salesVolume(new ArrayList<>())
                                    .clientLinks(new ArrayList<>())
                                    .build();
                        }
                        //Add in Client new links with Distributor ;
                        if (salesVolumeClientDto != null) {
                            clientDtoFile.addSalesVolumeClientDto(salesVolumeClientDto);
                        }

                        //Add in Client new links with other Client;
                        if (clientLinkedDto != null) {
                            clientDtoFile.addClientLinkedDto(clientLinkedDto);
                        }
                        clientMapFile.put(clientInn, clientDtoFile);
                        log.info("CLIENT: {}", clientDtoFile);
                    }
                }
            }
            log.info("END: {}");

            Client clientDbOld;
            ClientDto clientDtoOld;
            for (Map.Entry<String, ClientDto> clientDtoEntry : clientMapFile.entrySet()) {
                clientDbOld = clientService.getClientByInn(clientDtoEntry.getKey());
                clientDtoFile = clientDtoEntry.getValue();
                if (clientDbOld != null) {
                    clientDtoOld = transformationService.transformationFromClient(clientDbOld);
                    clientDtoFile.setId(clientDtoOld.getId());
                    clientDtoFile.setActive(clientDtoOld.isActive());
                    List<ClientLinkedDto> clientAllLink = clientService.getClientAllLink(clientDtoOld.getId());
                    clientDtoFile.addListClientLinkedDto(clientAllLink);
                    clientDtoFile.addListSalesVolumeClientDto(clientDtoOld.getSalesVolume());
                    clientDtoFile = clientService.update(clientDtoFile, clientDbOld);
                    log.info("UPDATE CLIENT: {}", clientDtoFile);
                } else {
                    clientDtoFile.setActive(true);
                    clientDtoFile = clientService.save(clientDtoFile);
                    log.info("SAVE NEW CLIENT: {}", clientDtoFile);
                }
            }

        }
    }

    private String getStringValueCell(Cell cell) {
        if (cell == null) return null;
        String result;
        try {
            result = NumberToTextConverter.toText(cell.getNumericCellValue());
        } catch (IllegalStateException ex) {
            result = cell.getStringCellValue();
        }
        return result;
    }

    /**
     * Getting the client name from an Excel table cell.
     * <p>
     * First attempt to extract data of type {@link String}.
     * If an {@link IllegalStateException} is thrown,
     * data of type double is retrieved and converted to {@link String}
     *
     * @param cell - Excel table cell
     * @return - contents from an Excel table cell
     */
    private String getClientName(Cell cell) {
        if (cell == null) return null;
        String result;
        try {
            result = NumberToTextConverter.toText(cell.getNumericCellValue());
        } catch (IllegalStateException ex) {
            result = cell.getStringCellValue();
        }
        return result;
    }

    /**
     * Getting the client inn from an Excel table cell.
     * <p>
     * First attempt to extract data of type double is retrieved and converted to {@link String}
     * If an {@link IllegalStateException} is thrown,
     * data of type {@link String}.
     *
     * @param cell - Excel table cell
     * @return - contents from an Excel table cell
     */
    private String getInn(Cell cell) {
        if (cell == null) return null;
        String result;
        try {
            result = NumberToTextConverter.toText(cell.getNumericCellValue());
        } catch (IllegalStateException ex) {
            result = cell.getStringCellValue();
        }
        return result;
    }

    private Long getPartnerId(Cell cell) {
        Long l = null;
        try {
            if (cell != null) {
                String stringCellValue = cell.getStringCellValue();
                if (Pattern.matches(PARTNER_ID_PATTERN, stringCellValue)) {
                    l = Long.valueOf(stringCellValue);
                }
            }
        } catch (RuntimeException ex) {
            log.error("{} . Value  {} incorrect for transformation to Type Long", ex.getMessage(), cell.getStringCellValue());
        }
        return l;
    }

    /**
     * Retrieving an instance of the entity {@link ClientClassification} by name from storage using {@link ClientClassification}
     *
     * @param classificationName - name of Classification for searching in storage
     * @return - instance of the entity {@link ClientClassification}  found in storage
     */
    private ClientClassification getClassification(String classificationName) {
        return classificationRepository.findByName(classificationName);
    }

    /**
     * Converting a date from a {@link String} type to a {@link Date} type
     *
     * @param date - date in String format
     * @return date in Date format
     */
    private Date parseDateTime(String date) {
        Date result = null;
        try {
            result = new SimpleDateFormat(DATA_TIME_PATTERN).parse(date);
        } catch (ParseException e) {
            log.error("DATE PARSE ERROR {}", e.getMessage());
        }
        return result;
    }

}
