package ru.bostonsd.clientmanagement.bpm.service.process.update;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bostonsd.clientmanagement.bpm.model.update.ComplexClientJson;
import ru.bostonsd.clientmanagement.domain.models.Client;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientDto;
import ru.bostonsd.clientmanagement.domain.models.enums.ClientType;
import ru.bostonsd.clientmanagement.domain.service.ClientService;
import ru.bostonsd.clientmanagement.domain.service.validation.ValidationService;
import ru.bostonsd.clientmanagement.history.models.basic.ObjectHistory;
import ru.bostonsd.clientmanagement.history.service.ObjectHistoryService;
import ru.bostonsd.clientmanagement.history.service.generator.GeneratorHistoryService;
import ru.bostonsd.clientmanagement.util.jsonparser.JsonParserService;

import java.util.List;

@Component
@Slf4j
public class ApproveUpdateClientService implements JavaDelegate {

    @Autowired
    private ClientService clientService;

    @Autowired
    private JsonParserService parserService;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private GeneratorHistoryService generatorHistoryService;

    @Autowired
    private ObjectHistoryService objectHistoryService;

    /**
     * Print value of variable approve_status
     *
     * @param execution object with current info about business client
     * @throws Exception
     */
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Object clientJson = execution.getVariable("full_client_json");
        if (clientJson != null) {
            ComplexClientJson clientDto = parserService.convertMapToObject(clientJson, ComplexClientJson.class);
            if (clientDto == null) {
                clientDto = parserService.parseJsonToObject(String.valueOf(clientJson), ComplexClientJson.class);
            }
            if (clientDto != null) {
                ClientDto newValue = clientDto.getNewValue();
                ClientDto oldValue = clientDto.getOldValue();

                boolean oldValid = validationService.validateClient(oldValue);
                boolean newValid = validationService.validateClient(newValue);

                if (newValid && oldValid && oldValue.getInn().equals(newValue.getInn())) {
                    boolean update = clientService.updateForCamundaApproveUpdateClient(newValue);
                    if (update) {
                        Object initiatorId = execution.getVariable("initiator_id");
                        List<ObjectHistory> objectHistories = generatorHistoryService.generateObjectHistory(newValue, oldValue, initiatorId != null ? String.valueOf(initiatorId) : null);
                        boolean saveHistory = objectHistoryService.saveAll(objectHistories);
                        log.info("UPDATE CLIENT: {}, result save history: {}, history size: {}", newValue, saveHistory, objectHistories.size());
                        return;
                    }
                }
            }
        }
        log.warn("VALIDATION ERROR");
    }

}
