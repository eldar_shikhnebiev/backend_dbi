package ru.bostonsd.clientmanagement.bpm.service.process.ticket.listener;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/**
 * @author asaburov on 12/5/2019
 */
@Slf4j
public class ApproveTicketListener implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Object approve_admin = execution.getVariable("approve_status");
        log.info("APPROVE STATUS: {}", approve_admin);
    }
}
