package ru.bostonsd.clientmanagement.bpm.service.history;

import ru.bostonsd.clientmanagement.bpm.model.history.OldAndNewStateVariable;

public interface HistoryCamundaService {

    OldAndNewStateVariable getOldAndNewState(String instanceId, String variableName);

    OldAndNewStateVariable getOldAndNewStatus(String instanceId, String variableName);

    void saveAttachmentHistory(String processId, String variableName);
}
