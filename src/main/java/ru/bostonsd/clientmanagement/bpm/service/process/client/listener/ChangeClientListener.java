package ru.bostonsd.clientmanagement.bpm.service.process.client.listener;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.bostonsd.clientmanagement.bpm.model.history.OldAndNewStateVariable;
import ru.bostonsd.clientmanagement.bpm.service.history.HistoryCamundaService;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientDto;
import ru.bostonsd.clientmanagement.domain.service.DistributorService;
import ru.bostonsd.clientmanagement.history.models.basic.StateHistory;
import ru.bostonsd.clientmanagement.history.service.StateHistoryService;
import ru.bostonsd.clientmanagement.history.service.generator.GeneratorHistoryService;
import ru.bostonsd.clientmanagement.util.jsonparser.JsonParserService;

import java.util.List;

/**
 * @author asaburov on 1/6/2020
 */
@Component
@Slf4j
public class ChangeClientListener implements JavaDelegate {

    @Autowired
    private GeneratorHistoryService generatorHistoryService;

    @Autowired
    private HistoryCamundaService historyCamundaService;

    @Autowired
    private StateHistoryService stateHistoryService;

    @Autowired
    private JsonParserService parserService;

    @Autowired
    private DistributorService distributorService;

    @Value("${process.client.json}")
    private String CLIENT_JSON;

    @Value("${process.attachmentid}")
    private String ATTACHMENT_ID;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        OldAndNewStateVariable states = historyCamundaService.getOldAndNewState(execution.getProcessInstanceId(), CLIENT_JSON);
        if (states != null) {
            ClientDto oldClient = parserService.parseJsonToObject(states.getOldState(), ClientDto.class);
            ClientDto newClient = parserService.parseJsonToObject(states.getNewState(), ClientDto.class);
            List<StateHistory> stateHistories = generatorHistoryService.generateStateHistory(newClient, oldClient, states.getProcessId(), states.getAssignee());
            boolean result = stateHistoryService.saveAll(stateHistories);
            log.info("result save client history: {}, history size: {}", result, stateHistories.size());
        }
        historyCamundaService.saveAttachmentHistory(execution.getProcessInstanceId(), ATTACHMENT_ID);
    }
}
