package ru.bostonsd.clientmanagement.bpm.service.process.client.exception.handler;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/**
 * @author asaburov on 12/5/2019
 *
 * Catching exception and pring task id and name
 */
@Slf4j
public class SimpleExceptionHandler implements JavaDelegate {

    /**
     * Print out name and task id, where arise exception
     *
     * @param execution current state of execution
     * @throws Exception
     */
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        log.error("Exception due execution of task(id/name) : {}/{}", execution.getCurrentActivityId(), execution.getCurrentActivityName());
    }
}
