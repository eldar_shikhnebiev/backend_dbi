package ru.bostonsd.clientmanagement.bpm.service.general.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.bpm.model.CommentMessage;
import ru.bostonsd.clientmanagement.bpm.repository.CommentRepository;
import ru.bostonsd.clientmanagement.bpm.service.general.CommentService;

import java.util.List;

/**
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * @date 18.12.2019
 */
@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public List<CommentMessage> getByProcessId(String processId) {
        return commentRepository.findByProcessId(processId);
    }
    @Override
    public void save(CommentMessage comment) {
        commentRepository.save(comment);
    }

}
