package ru.bostonsd.clientmanagement.bpm.service.process.client.listener;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bostonsd.clientmanagement.smtp.service.SmtpService;
import ru.bostonsd.clientmanagement.security.model.database.RoleEnum;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author asaburov on 12/10/2019
 */

@Slf4j
@Component
public class SendMessageCreationClientListener implements JavaDelegate {

    @Autowired
    private SmtpService smtpService;

    @Autowired
    private IdentityService identityService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        List<User> allAdmins = identityService.createUserQuery().memberOfGroup(RoleEnum.ROLE_ADMIN.name()).list();
        if (allAdmins != null && !allAdmins.isEmpty()) {
            Set<String> emails = allAdmins.stream().map(User::getEmail).filter(Objects::nonNull).collect(Collectors.toSet());
            emails.forEach(email -> smtpService.sendMessage(email, "Была создана новая заявка для пользователей с ролью администратор.",
                    "Новая заявка"));

        } else {
            log.warn("Users with role \'{}\' are absented", RoleEnum.ROLE_ADMIN);
        }
    }
}
