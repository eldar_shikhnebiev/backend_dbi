package ru.bostonsd.clientmanagement.bpm.service.general;

import ru.bostonsd.clientmanagement.bpm.model.CommentMessage;

import java.util.List;

/**
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * @date 18.12.2019
 */
public interface CommentService {

    List<CommentMessage> getByProcessId(String processId);
    void save(CommentMessage comment);
}
