package ru.bostonsd.clientmanagement.bpm.service.general;

import ru.bostonsd.clientmanagement.bpm.model.CamundaUser;
import ru.bostonsd.clientmanagement.bpm.model.TaskDto;
import ru.bostonsd.clientmanagement.security.model.rest.jwt.JwtTokenClaims;

import java.util.List;
import java.util.Map;

/**
 * @author asaburov on 12/5/2019
 * <p>
 * Interface for working with Camunda BPM
 * Implamented main method
 */
public interface CamundaService {

    Map<String, String> startProcess(String processName, Map<String, Object> variables);

    List<TaskDto> getAllTaskByUserId(String userId);

    List<TaskDto> getAllTasksByGroup(String jwt);

    boolean assignTask(String taskId, String userId);

    Object getValueByTaskId(String taskId, String variableName);

    void completeTask(String taskId, Map<String, Object> variables, String username);

    boolean createUserAndRole(JwtTokenClaims userClaims);

    List<CamundaUser> getUserInfoByUserId(String[] userIds);

    void deleteAllProcesses();
}
