package ru.bostonsd.clientmanagement.bpm.service.general.listener;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Component;
import ru.bostonsd.clientmanagement.bpm.model.history.OldAndNewStateVariable;
import ru.bostonsd.clientmanagement.bpm.service.history.HistoryCamundaService;
import ru.bostonsd.clientmanagement.history.models.basic.StateHistory;
import ru.bostonsd.clientmanagement.history.service.StateHistoryService;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUser;
import ru.bostonsd.clientmanagement.security.service.UserService;

import java.util.Date;

/**
 * @author asaburov on 1/6/2020
 */
@Component
@Slf4j
public class ChangeStatusListener implements JavaDelegate {

    @Autowired
    private HistoryCamundaService historyCamundaService;

    @Autowired
    private StateHistoryService stateHistoryService;

    @Autowired
    private UserService userService;

    @Value("${process.status.flow}")
    private String STATUS_FLOW_VARIABLE;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
//        OldAndNewStateVariable statusFlow = historyCamundaService.getOldAndNewState(execution.getProcessInstanceId(), STATUS_FLOW_VARIABLE);
        OldAndNewStateVariable statusFlow = historyCamundaService.getOldAndNewStatus(execution.getProcessInstanceId(), STATUS_FLOW_VARIABLE);
        if (statusFlow != null) {
            SCHDUser user = userService.getByUserName(statusFlow.getAssignee());
            if(user != null) {
                StateHistory history = StateHistory.builder()
                        .processId(statusFlow.getProcessId())
                        .fieldName("Статус заявки")
                        .newValue(statusFlow.getNewState())
                        .oldValue(statusFlow.getOldState())
                        .userName(statusFlow.getAssignee())
                        .fullName(user.getFullName())
                        .date(new Date().getTime())
                        .build();
                log.info("History ticket: {}", history);
                stateHistoryService.save(history);
                return;
            }
        }
        log.info("History for status or user is null");
    }
}
