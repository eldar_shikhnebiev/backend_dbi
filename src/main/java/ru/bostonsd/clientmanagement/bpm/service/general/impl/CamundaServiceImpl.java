package ru.bostonsd.clientmanagement.bpm.service.general.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.identity.Group;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.runtime.ActivityInstance;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.bpm.model.AppealDto;
import ru.bostonsd.clientmanagement.bpm.model.CamundaUser;
import ru.bostonsd.clientmanagement.bpm.model.TaskDto;
import ru.bostonsd.clientmanagement.bpm.service.general.CamundaService;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientDto;
import ru.bostonsd.clientmanagement.history.models.dto.TicketDto;
import ru.bostonsd.clientmanagement.history.service.TicketService;
import ru.bostonsd.clientmanagement.history.service.TicketTransformationService;
import ru.bostonsd.clientmanagement.security.model.database.RoleEnum;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUser;
import ru.bostonsd.clientmanagement.security.model.rest.jwt.JwtTokenClaims;
import ru.bostonsd.clientmanagement.security.service.JwtTokenUtil;
import ru.bostonsd.clientmanagement.security.service.impl.SCHDUserDao;
import ru.bostonsd.clientmanagement.util.jsonparser.JsonParserService;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author asaburov on 12/5/2019
 * <p>
 * Camunda service implements all necessary logic for
 * execution of business processes and working with tasks
 */
@Service
@Slf4j
public class CamundaServiceImpl implements CamundaService {

    @Autowired
    private TaskService taskService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private SCHDUserDao userService;

    @Autowired
    private IdentityService identityService;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private TicketTransformationService ticketTransformationService;

    @Autowired
    private JsonParserService jsonParserService;

    @Value("${camunda.custom.password}")
    private String customPassword;

    private Map<String, Integer> sortName = new HashMap<>();

    public CamundaServiceImpl() {
        sortName.put("На согласовании", 1);
        sortName.put("Корректировка", 2);
        sortName.put("Отклонена", 3);
        sortName.put("Согласована", 4);
    }

    /**
     * Start business client in Camunda BPM
     *
     * @param processName - name of business client
     * @param variables   - key = name of variable, value = value of variable
     * @return true if client was started successfully, false if fail
     */
    @Override
    public Map<String, String> startProcess(String processName, Map<String, Object> variables) {
        Map<String, String> result = new HashMap<>();
        try {
            ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processName, variables);
            String processInstanceId = processInstance.getProcessInstanceId();
            log.info("PROCESS WAS STARTED WITH ID: {}", processInstanceId);
            variables.put("start_process_date", new Date().getTime());
            this.addHistoryVariable(variables);
            runtimeService.setVariables(processInstanceId, variables);
            result.put("process_id", processInstanceId);
        } catch (Exception ex) {
            log.error("START PROCESS ERROR, MESSAGE: {}", ex.getMessage());
        }
        return result;
    }

    /**
     * Getting all task`s id by user id
     *
     * @param userId - user id
     * @return list of task`s id
     */
    @Override
    public List<TaskDto> getAllTaskByUserId(String userId) {
        List<TaskDto> result = new ArrayList<>();
        try {
            List<Task> allTasks = new ArrayList<>();
            List<Task> tasks = taskService.createTaskQuery().active().taskAssignee(userId).list();
            if(!tasks.isEmpty()) allTasks.addAll(tasks);

            //getting task by initiator
            List<Task> allInitiatorTasks = taskService.createTaskQuery()
                    .processVariableValueLike("initiator_id", userId)
                    .active().list();
            if(!allInitiatorTasks.isEmpty())allTasks.addAll(allInitiatorTasks);
            allTasks = allTasks.stream().filter(this.distinctByKey(Task::getId)).collect(Collectors.toList());

            for (Task task : allTasks) {
                Map<String, Object> variables = taskService.getVariables(task.getId());
                SCHDUser user = userService.getByUserName(variables.get("initiator_id").toString());
                result.add(TaskDto.builder()
                        .processId(task.getProcessInstanceId())
                        .processName(this.extractProcessName(task.getProcessInstanceId()))
                        .taskId(task.getId())
                        .taskName(task.getName())
                        .distributorInitiator(user.getDistributorName())
                        .variables(variables)
                        .build());
            }
            List<TicketDto> usersTickets = ticketService.getListByInitiatorId(userId);
            if (usersTickets != null && !usersTickets.isEmpty()) {
                List<TaskDto> convertedTickets = usersTickets.stream()
                        .map(ticketTransformationService::transformationTaskFromTicketDto)
                        .collect(Collectors.toList());
                result.addAll(convertedTickets);
            }
        } catch (Exception ex) {
            log.error("GETTING TASKS BY USER", ex.getMessage());
        }
        result.sort(comparator());
        return result;
    }


    /**
     * Getting all task`s id by group
     *
     * @param jwt - jwt token
     * @return list of task`s id
     */
    @Override
    public List<TaskDto> getAllTasksByGroup(String jwt) {
        String group = JwtTokenUtil.getRoleFromToken(jwt);
        String username = JwtTokenUtil.getUsernameFromToken(jwt);

        List<TaskDto> result = new ArrayList<>();
        log.info("GROUP IS : {}", group);
        try {
            List<Task> tasks = taskService.createTaskQuery().active().taskCandidateGroup(group).list();
            for (Task task : tasks) {
                Map<String, Object> variables = taskService.getVariables(task.getId());
                SCHDUser user = userService.getByUserName(variables.get("initiator_id").toString());
                result.add(TaskDto.builder()
                        .processId(task.getProcessInstanceId())
                        .processName(this.extractProcessName(task.getProcessInstanceId()))
                        .taskId(task.getId())
                        .taskName(task.getName())
                        .distributorInitiator(user != null ? user.getDistributorName() : null)
                        .variables(variables)
                        .build());
            }
        } catch (Exception ex) {
            log.error("GETTING TASKS BY GROUP, message: {}", ex.getMessage());
        }

        List<TicketDto> allTickets;
        //TODO added sorting and filtering
        if (RoleEnum.ROLE_ADMIN.name().equals(group)) {
            allTickets = ticketService.getAllList();
        } else {
            allTickets = ticketService.getListByInitiatorId(username);
        }
        if (allTickets != null && !allTickets.isEmpty()) {
            List<TaskDto> convertedTickets = allTickets.stream()
                    .map(ticketTransformationService::transformationTaskFromTicketDto)
                    .collect(Collectors.toList());
            result.addAll(convertedTickets);
        }
        result.sort(comparator());
        return result;
    }

    /**
     * Assign task on the appointed user
     *
     * @param taskId - task id
     * @param userId - user id
     * @return true if task was assigned successfully, false if fail
     */
    @Override
    public boolean assignTask(String taskId, String userId) {
        boolean result = false;
        try {
            taskService.setAssignee(taskId, userId);
            result = true;
        } catch (Exception ex) {
            log.error("TASK ASSIGN ERROR, MESSAGE: {}", ex.getMessage());
        }
        return result;
    }

    /**
     * Getting value of the appointed variable by task id
     *
     * @param taskId       - task id
     * @param variableName - variable`s name
     * @return value of variable or null
     */
    @Override
    public Object getValueByTaskId(String taskId, String variableName) {
        Object variable = null;
        try {
            variable = taskService.getVariable(taskId, variableName);
            log.info("VALUE OF VARIABLE : {}", variable);
        } catch (Exception ex) {
            log.error("ERROR GETTING VALUE BY TASK ID, MESSAGE : {}", ex.getMessage());
        }
        return variable;
    }

    /**
     * Complete task by task id and set value of variables
     *
     * @param taskId    task id
     * @param variables key = name of variable, value = value of variable
     * @return true if task was finished successfully, false if fail
     */
    @Override
    public void completeTask(String taskId, Map<String, Object> variables, String username) {
        if (variables != null && !variables.isEmpty()) {
            this.addHistoryVariable(variables);
            taskService.setVariables(taskId, variables);
        }
        Task currentTask = taskService.createTaskQuery().active().taskId(taskId).singleResult();
        if (currentTask != null) {
            if (currentTask.getAssignee() != null &&
                    !currentTask.getAssignee().equalsIgnoreCase(username)) {
                currentTask.setAssignee(username);
            } else if (currentTask.getAssignee() == null) {
                currentTask.setAssignee(username);
            }
        }
        taskService.saveTask(currentTask);
        taskService.complete(taskId);
    }

    /**
     * Create user and group
     *
     * @param userClaims user`s info
     * @return true if user was saved successfully, false if fail
     */
    public boolean createUserAndRole(JwtTokenClaims userClaims) {
        if (userClaims != null) {
            User existUser = identityService.createUserQuery().userId(userClaims.getUsername()).singleResult();
            if (existUser != null) {
                identityService.deleteUser(userClaims.getUsername());
            }
            User newUser = identityService.newUser(userClaims.getUsername());
            newUser.setId(userClaims.getUsername());
            newUser.setEmail(userClaims.getUsername());
            newUser.setFirstName(userClaims.getFullName());
            newUser.setLastName(userClaims.getFullName());
            newUser.setPassword(customPassword);
            identityService.saveUser(newUser);

            Group group = identityService.createGroupQuery().groupId(userClaims.getRoleName()).singleResult();
            if (group == null) {
                group = identityService.newGroup(userClaims.getRoleName());
                group.setName(userClaims.getRoleName());
                group.setType(userClaims.getRoleName());
                identityService.saveGroup(group);
            }
            identityService.createMembership(newUser.getId(), group.getId());
            log.info("CREATE/UPDATE USER: {}, WITH ROLE: {}", newUser.getId(), group.getId());
            return true;
        }
        return false;
    }

    /**
     * Getting all users by array of user ids
     *
     * @param userIds array of user ids
     * @return list of users
     */
    public List<CamundaUser> getUserInfoByUserId(String[] userIds) {
        List<CamundaUser> result = new ArrayList<>();
        if (userIds != null && userIds.length > 0) {
            List<User> usersById = identityService.createUserQuery().userIdIn(userIds).list();
            if (usersById != null) {
                usersById.stream().map(user -> CamundaUser.builder()
                        .id(user.getId())
                        .firstName(user.getFirstName())
                        .lastName(user.getLastName())
                        .email(user.getEmail())
                        .build())
                        .forEach(result::add);
            }
        }
        return result;
    }

    public void deleteAllProcesses() {
        List<ProcessInstance> allProcesses = runtimeService.createProcessInstanceQuery().list();
        for (ProcessInstance process : allProcesses) {
            runtimeService.deleteProcessInstance(process.getProcessInstanceId(), null);
        }
        log.info("All active business processes were deleted");
    }

    private String extractProcessName(String processInstanceId) {
        if (processInstanceId != null) {
            ActivityInstance activityInstance = runtimeService.getActivityInstance(processInstanceId);
            return activityInstance != null ? activityInstance.getActivityName() : null;
        }
        return null;
    }

    private void addHistoryVariable(Map<String, Object> variables) {
        final String variableClient = "client_json";
        final String variableHistoryClient = "history_client_json";
        final String variableTicket = "ticket_json";
        final String variableHistoryTicket = "history_ticket_json";
        if (variables != null && !variables.isEmpty() && variables.containsKey(variableClient)) {
            Object clientJson = variables.get(variableClient);
            ClientDto client = jsonParserService.convertMapToObject(clientJson, ClientDto.class);
            if (client != null) {
                String json = jsonParserService.parseObjectToJson(client);
                variables.put(variableHistoryClient, json);
            } else {
                variables.put(variableHistoryClient, clientJson);
            }
        } else if (variables != null && !variables.isEmpty() && variables.containsKey(variableTicket)) {
            Object ticketJson = variables.get(variableTicket);
            AppealDto ticket = jsonParserService.convertMapToObject(ticketJson, AppealDto.class);
            if (ticket != null) {
                String json = jsonParserService.parseObjectToJson(ticket);
                variables.put(variableHistoryTicket, json);
            } else {
                variables.put(variableHistoryTicket, ticketJson);
            }
        }
    }

    private Comparator<TaskDto> comparator() {
        final String statusFlow = "status_flow";
        final String startDate = "start_process_date";
        return (t1, t2) -> {
            Map<String, Object> variables1 = t1.getVariables();
            Map<String, Object> variables2 = t2.getVariables();
            if (variables1.containsKey(statusFlow) && variables2.containsKey(statusFlow)
                    && variables1.containsKey(startDate) && variables2.containsKey(startDate)) {
                Object stat1 = variables1.get(statusFlow);
                Object stat2 = variables2.get(statusFlow);
                String status1 = stat1 != null ? String.valueOf(stat1) : null;
                String status2 = stat2 != null ? String.valueOf(stat2) : null;
                Integer comp1 = sortName.getOrDefault(status1, null);
                Integer comp2 = sortName.getOrDefault(status2, null);
                if (comp1 != null && comp2 == null) {
                    return 1;
                } else if (comp1 == null && comp2 != null) {
                    return -1;
                } else if (comp1 != null && comp2 != null) {
                    int resultComp = comp1.compareTo(comp2);
                    if (resultComp == 0) {
                        Object date1 = variables1.getOrDefault(startDate, null);
                        Object date2 = variables2.getOrDefault(startDate, null);
                        if (date1 != null && date2 == null) {
                            return 1;
                        } else if (date1 == null && date2 != null) {
                            return -1;
                        } else if (date1 != null && date2 != null) {
                            Long aLong1 = Long.valueOf(String.valueOf(date1));
                            Long aLong2 = Long.valueOf(String.valueOf(date2));
                            return -aLong1.compareTo(aLong2);
                        }
                    }else{
                        return resultComp;
                    }
                }
            }
            return 0;
        };
    }

    public <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }
}
