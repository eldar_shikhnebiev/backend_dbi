package ru.bostonsd.clientmanagement.bpm.service.process.ticket.listener;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bostonsd.clientmanagement.security.model.database.RoleEnum;
import ru.bostonsd.clientmanagement.smtp.service.SmtpService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author asaburov on 12/10/2019
 */

@Slf4j
@Component
public class SendMessageCreationTicketListener implements JavaDelegate {

    @Autowired
    private SmtpService smtpService;

    @Autowired
    private IdentityService identityService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        List<User> allAdmins = identityService.createUserQuery().memberOfGroup(RoleEnum.ROLE_ADMIN.name()).list();

        final String message = "Была создана новая заявка для пользователей с ролью администратор.";
        final String title = "Новая заявка";
        if (allAdmins != null && !allAdmins.isEmpty()) {
            Set<String> emails = allAdmins.stream().map(User::getEmail).collect(Collectors.toSet());
            if (!emails.isEmpty()) emails.forEach(email -> smtpService.sendMessage(email, message, title));
        } else {
            log.warn("Users with role \'{}\' are absented", RoleEnum.ROLE_ADMIN);
        }

    }
}
