package ru.bostonsd.clientmanagement.bpm.service.process.ticket.listener;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bostonsd.clientmanagement.domain.service.validation.ValidationService;
import ru.bostonsd.clientmanagement.util.jsonparser.JsonParserService;

/**
 * @author asaburov on 12/12/2019
 */
@Slf4j
@Component
public class ValidationCreationTickitListener implements JavaDelegate {

    @Autowired
    private ValidationService validationService;

    @Autowired
    private JsonParserService jsonParserService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Object ticketJson = execution.getVariable("ticket_json");
        log.info("TICKET JSON: {}", ticketJson);
        if (ticketJson != null) {
//            ClientDto clientDto = jsonParserService.parseJsonToObject(String.valueOf(ticketJson), ClientDto.class);
//            boolean validateClient = validationService.validateClient(clientDto);
            execution.setVariable("valid", true);
        } else {
            log.warn("CLIENT JSON IS NULL");
            execution.setVariable("valid", true);
        }
    }
}
