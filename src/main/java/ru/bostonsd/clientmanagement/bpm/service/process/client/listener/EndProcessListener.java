package ru.bostonsd.clientmanagement.bpm.service.process.client.listener;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.runtime.ActivityInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bostonsd.clientmanagement.history.models.dto.TicketDto;
import ru.bostonsd.clientmanagement.history.service.TicketService;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUser;
import ru.bostonsd.clientmanagement.security.service.impl.SCHDUserDao;

import java.util.Map;

@Component
@Slf4j
public class EndProcessListener implements JavaDelegate {

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private SCHDUserDao userService;

    @Autowired
    private TicketService ticketService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        String processInstanceId = execution.getProcessInstanceId();
        Map<String, Object> variables = execution.getVariables();
        if (variables.containsKey("initiator_id")) {
            SCHDUser user = userService.getByUserName(variables.get("initiator_id").toString());
            if (user != null) {
                TicketDto ticketDto = TicketDto.builder()
                        .distributorInitiator(user.getDistributorName())
                        .processId(processInstanceId != null ? Long.valueOf(processInstanceId) : null)
                        .processName(this.extractProcessName(processInstanceId))
                        .variables(variables)
                        .build();
                TicketDto saved = ticketService.save(ticketDto);
                if (saved != null) {
                    log.info("Process with process id : {} was finished and added to history with id: {}",
                            processInstanceId, saved.getId());
                }
            }
        }
    }

    private String extractProcessName(String processInstanceId) {
        if (processInstanceId != null) {
            ActivityInstance activityInstance = runtimeService.getActivityInstance(processInstanceId);
            return activityInstance != null ? activityInstance.getActivityName() : null;
        }
        return null;
    }
}
