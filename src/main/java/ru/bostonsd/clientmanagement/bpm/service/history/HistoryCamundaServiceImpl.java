package ru.bostonsd.clientmanagement.bpm.service.history;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.history.HistoricDetail;
import org.camunda.bpm.engine.history.HistoricTaskInstance;
import org.camunda.bpm.engine.impl.persistence.entity.HistoricDetailVariableInstanceUpdateEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.bpm.model.history.OldAndNewStateVariable;
import ru.bostonsd.clientmanagement.domain.models.dto.FileDto;
import ru.bostonsd.clientmanagement.history.models.basic.StateHistory;
import ru.bostonsd.clientmanagement.history.service.StateHistoryService;
import ru.bostonsd.clientmanagement.history.service.generator.GeneratorHistoryService;
import ru.bostonsd.clientmanagement.util.jsonparser.JsonParserService;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class HistoryCamundaServiceImpl implements HistoryCamundaService {

    @Autowired
    private HistoryService historyService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private JsonParserService parserService;

    @Autowired
    private GeneratorHistoryService generatorHistoryService;

    @Autowired
    private StateHistoryService stateHistoryService;

    public OldAndNewStateVariable getOldAndNewState(String instanceId, String variableName) {
        OldAndNewStateVariable result = null;
        if (instanceId != null && variableName != null) {
            try {
                List<HistoricDetail> historicDetails = historyService.createHistoricDetailQuery()
                        .variableUpdates()
                        .processInstanceId(instanceId)
                        .orderByTime().desc().list();

                if (historicDetails != null && !historicDetails.isEmpty()) {
                    List<String> oldAndNewVariables = historicDetails.stream()
                            .map(e -> (HistoricDetailVariableInstanceUpdateEntity) e)
                            .filter(e -> e.getVariableName().equalsIgnoreCase(variableName))
                            .map(e -> e.getTextValue() != null ? e.getTextValue() : e.getTextValue2())
                            .limit(2).collect(Collectors.toList());

                    List<HistoricTaskInstance> lastTasks = historyService.createHistoricTaskInstanceQuery()
                            .processInstanceId(instanceId)
                            .list();

                    lastTasks.sort(Comparator.comparing(HistoricTaskInstance::getStartTime, Comparator.nullsLast(Comparator.reverseOrder())));

                    Object currentValue = runtimeService.getVariable(instanceId, variableName);

                    if (lastTasks != null && !lastTasks.isEmpty() && oldAndNewVariables.size() == 2
                            && !Objects.equals(oldAndNewVariables.get(0), oldAndNewVariables.get(1))
                            && (oldAndNewVariables.get(0) != null || oldAndNewVariables.get(1) != null)) {
                        result = OldAndNewStateVariable.builder()
                                .variableName(variableName)
                                .processId(instanceId)
                                .assignee(lastTasks.get(0).getAssignee())
                                .newState(oldAndNewVariables.get(0))
                                .oldState(oldAndNewVariables.get(1))
                                .build();

                    } else if (lastTasks != null && !lastTasks.isEmpty()
                            && currentValue != null && oldAndNewVariables.size() == 1
                            && !Objects.equals(oldAndNewVariables.get(0), String.valueOf(currentValue))) {
                        result = OldAndNewStateVariable.builder()
                                .variableName(variableName)
                                .processId(instanceId)
                                .assignee(lastTasks.get(0).getAssignee())
                                .newState(String.valueOf(currentValue))
                                .oldState(oldAndNewVariables.get(0))
                                .build();
                    }
                }
            } catch (Exception e) {
                log.error("GETTING CAMUNDA HISTORY BY VARIABLE NAME ERROR, MESSAGE : {}", e.getMessage());
            }
        }
        log.info("OLD AND NEW VALUE : {}", result);
        return result;
    }

    @Override
    public OldAndNewStateVariable getOldAndNewStatus(String instanceId, String variableName) {
        OldAndNewStateVariable result = null;
        if (instanceId != null && variableName != null) {
            try {
                List<HistoricDetail> historicDetails = historyService.createHistoricDetailQuery()
                        .variableUpdates()
                        .processInstanceId(instanceId)
                        .orderByTime().desc().list();

                if (historicDetails != null && !historicDetails.isEmpty()) {
                    String oldValue = historicDetails.stream()
                            .map(e -> (HistoricDetailVariableInstanceUpdateEntity) e)
                            .filter(e -> e.getVariableName().equalsIgnoreCase(variableName))
                            .map(e -> e.getTextValue() != null ? e.getTextValue() : e.getTextValue2())
                            .findFirst().orElse(null);

                    List<HistoricTaskInstance> lastTasks = historyService.createHistoricTaskInstanceQuery()
                            .processInstanceId(instanceId)
                            .list();

                    lastTasks.sort(Comparator.comparing(HistoricTaskInstance::getStartTime, Comparator.nullsLast(Comparator.reverseOrder())));

                    Object currentValue = runtimeService.getVariable(instanceId, variableName);

                    if (oldValue != null && currentValue != null
                            && !Objects.equals(oldValue, currentValue)) {
                        result = OldAndNewStateVariable.builder()
                                .variableName(variableName)
                                .processId(instanceId)
                                .assignee(lastTasks.get(0).getAssignee())
                                .newState(String.valueOf(currentValue))
                                .oldState(oldValue)
                                .build();
                    }
                }
            } catch (Exception e) {
                log.error("GETTING CAMUNDA HISTORY BY VARIABLE NAME ERROR, MESSAGE : {}", e.getMessage());
            }
        }
        log.info("OLD AND NEW VALUE : {}", result);
        return result;
    }

    @Override
    public void saveAttachmentHistory(String processId, String variableName) {
        OldAndNewStateVariable attachmentIdStates = this.getOldAndNewState(processId, variableName);
        if (attachmentIdStates != null) {

            List<FileDto> oldFiles = parserService.parseJsonToListObjects(attachmentIdStates.getOldState(), new TypeReference<List<FileDto>>() {
            });
            List<FileDto> newFiles = parserService.parseJsonToListObjects(attachmentIdStates.getNewState(), new TypeReference<List<FileDto>>() {
            });
            List<StateHistory> filesHistories = new ArrayList<>();

            if (newFiles != null && oldFiles != null && !newFiles.isEmpty() && !oldFiles.isEmpty()) {

                Set<Long> ids = oldFiles.stream().map(FileDto::getId).collect(Collectors.toSet());
                Set<Long> newIds = newFiles.stream().map(FileDto::getId).collect(Collectors.toSet());
                ids.addAll(newIds);

                for (Long id : ids) {
                    FileDto oldFile = oldFiles.stream().filter(f -> f.getId().equals(id)).findFirst().orElse(null);
                    FileDto newFile = newFiles.stream().filter(f -> f.getId().equals(id)).findFirst().orElse(null);
                    if (oldFile != null && newFile == null) {
                        StateHistory history = generatorHistoryService.createAddedOrDeleteState(newFile, oldFile, attachmentIdStates.getAssignee(), processId);
                        if (history != null) {
                            filesHistories.add(history);
                        }
                    } else if (oldFile == null && newFile != null) {
                        StateHistory history = generatorHistoryService.createAddedOrDeleteState(newFile, oldFile, attachmentIdStates.getAssignee(), processId);
                        if (history != null) {
                            filesHistories.add(history);
                        }
                    }
                }
            }
            boolean result = stateHistoryService.saveAll(filesHistories);
            log.info("result save client attachment history: {}, history size: {}", result, filesHistories.size());
        }
    }
}
