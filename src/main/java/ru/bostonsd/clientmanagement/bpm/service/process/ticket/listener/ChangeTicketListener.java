package ru.bostonsd.clientmanagement.bpm.service.process.ticket.listener;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.bostonsd.clientmanagement.bpm.model.AppealDto;
import ru.bostonsd.clientmanagement.bpm.model.history.OldAndNewStateVariable;
import ru.bostonsd.clientmanagement.bpm.service.history.HistoryCamundaService;
import ru.bostonsd.clientmanagement.domain.models.dto.FileDto;
import ru.bostonsd.clientmanagement.history.models.basic.StateHistory;
import ru.bostonsd.clientmanagement.history.service.StateHistoryService;
import ru.bostonsd.clientmanagement.history.service.generator.GeneratorHistoryService;
import ru.bostonsd.clientmanagement.util.jsonparser.JsonParserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * @date 19.12.2019
 */
@Component
@Slf4j
public class ChangeTicketListener implements JavaDelegate {

    @Autowired
    private GeneratorHistoryService generatorHistoryService;

    @Autowired
    private HistoryCamundaService historyCamundaService;

    @Autowired
    private StateHistoryService stateHistoryService;

    @Autowired
    private JsonParserService parserService;

    @Value("${process.ticket.json}")
    private String TICKET_JSON;

    @Value("${process.attachmentid}")
    private String ATTACHMENT_ID;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        OldAndNewStateVariable states = historyCamundaService.getOldAndNewState(execution.getProcessInstanceId(), TICKET_JSON);
        if (states != null) {
            AppealDto oldTicket = parserService.parseJsonToObject(states.getOldState(), AppealDto.class);
            AppealDto newTicket = parserService.parseJsonToObject(states.getNewState(), AppealDto.class);
            List<StateHistory> stateHistories = generatorHistoryService.generateStateHistory(newTicket, oldTicket, states.getProcessId(), states.getAssignee());
            boolean result = stateHistoryService.saveAll(stateHistories);
            log.info("result save ticket history: {}, history size: {}", result, stateHistories.size());
        }
        historyCamundaService.saveAttachmentHistory(execution.getProcessInstanceId(), ATTACHMENT_ID);
    }
}
