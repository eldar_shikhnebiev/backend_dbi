package ru.bostonsd.clientmanagement.bpm.service.process.client.listener;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientDto;
import ru.bostonsd.clientmanagement.domain.service.validation.ValidationService;
import ru.bostonsd.clientmanagement.util.jsonparser.JsonParserService;

/**
 * @author asaburov on 12/12/2019
 */

@Slf4j
@Component
public class ValidationCreationClientListener implements JavaDelegate {

    @Autowired
    private ValidationService validationService;

    @Autowired
    private JsonParserService jsonParserService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
//        Object clientJson = execution.getVariable("client_json");
//        log.info("CLIENT JSON: {}", clientJson);
//        if(clientJson != null){
//            ClientDto clientDto = jsonParserService.convertMapToObject(clientJson, ClientDto.class);
//            boolean validateClient = validationService.validateClient(clientDto);
//            execution.setVariable("valid", validateClient);
//        }else {
//            log.warn("CLIENT JSON IS NULL");
//            execution.setVariable("valid", false);
//        }
        execution.setVariable("valid", true);
    }
}
