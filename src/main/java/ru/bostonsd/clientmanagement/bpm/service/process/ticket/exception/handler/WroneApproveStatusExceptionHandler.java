package ru.bostonsd.clientmanagement.bpm.service.process.ticket.exception.handler;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/**
 * @author asaburov on 12/5/2019
 */
@Slf4j
public class WroneApproveStatusExceptionHandler implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        log.error("Exception due execution of task(id/name) : {}/{}", execution.getCurrentActivityId(), execution.getCurrentActivityName());
    }
}
