package ru.bostonsd.clientmanagement.bpm.service.general.listener;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bostonsd.clientmanagement.bpm.model.StatusApprove;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientDto;
import ru.bostonsd.clientmanagement.smtp.service.SmtpService;
import ru.bostonsd.clientmanagement.util.jsonparser.JsonParserService;

import java.util.Map;

/**
 * @author asaburov on 12/10/2019
 */
@Slf4j
@Component
public class SendMessageStatusListener implements JavaDelegate {

    @Autowired
    private SmtpService smtpService;
    @Autowired
    private JsonParserService jsonParserService;

    private final String MESSAGE_REJECT_CLIENT = "Ваша заявка на создание нового клиента (ИНН: ?1, Наименование:?2) была отклонена администратором.\n" +
            "Для получения более подробной информации по этой заявке вы можете перейти в раздел заявок и воспользоваться поиском по ИНН, взяв данные из этого письма.";
    private final String MESSAGE_REJECT_TICKET = "Ваша заявка была отклонена администратором.\n" +
            "Для получения более подробной информации вы можете перейти в раздел заявок.";
    private final String TITLE_REJECT = "Ваша заявка была отклонена";

    private final String MESSAGE_RETURNED_CLIENT = "Ваша заявка на создание нового клиента (ИНН: ?1, Наименование: ?2) была возвращена администратором на корректировку.\n" +
            "Для получения более подробной информации по этой заявке вы можете перейти в раздел заявок и воспользоваться поиском по ИНН, взяв данные из этого письма.";
    private final String MESSAGE_RETURNED_TICKET = "Ваша заявка была возвращена администратором на корректировку.\n" +
            "Для получения более подробной информации вы можете перейти в раздел заявок.";
    private final String TITLE_RETURNED = "Ваша заявка была возвращена на корректировку";

    private final String MESSAGE_APPROVED_CLIENT = "Ваша заявка на создание нового клиента (ИНН: ?1, Наименование: ?2) была согласована администратором.\n" +
            "Для получения более подробной информации по этой заявке вы можете перейти в раздел заявок и воспользоваться поиском по статусу заявки.";
    private final String MESSAGE_APPROVED_TICKET = "Ваша заявка была согласована администратором.\n" +
            "Для получения более подробной информации вы можете перейти в раздел заявок.";
    private final String TITLE_APPROVED = "Ваша заявка была согласована";

    private static String CLIENT_ATTR = "client_json";

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Map<String, Object> variables = execution.getVariables();
        Object approveStatus = execution.getVariable("approve_status");
        Object recipient = execution.getVariable("initiator_id");
        if (approveStatus != null && recipient != null) {
            log.info("STATUS: {}, RECIPIENT: {}", approveStatus, recipient);
            switch (StatusApprove.valueOf(String.valueOf(approveStatus))) {
                case APPROVED:
                    if (variables.containsKey(CLIENT_ATTR)) {
                        smtpService.sendMessage(String.valueOf(recipient), this.generateMessage(variables, MESSAGE_APPROVED_CLIENT), TITLE_APPROVED);
                    } else {
                        smtpService.sendMessage(String.valueOf(recipient), MESSAGE_APPROVED_TICKET, TITLE_APPROVED);
                    }
                    break;
                case RETURNED:
                    if (variables.containsKey(CLIENT_ATTR)) {
                        smtpService.sendMessage(String.valueOf(recipient), this.generateMessage(variables, MESSAGE_RETURNED_CLIENT), TITLE_RETURNED);
                    } else {
                        smtpService.sendMessage(String.valueOf(recipient), MESSAGE_RETURNED_TICKET, TITLE_RETURNED);
                    }
                    break;
                case REJECTED:
                    if (variables.containsKey(CLIENT_ATTR)) {
                        smtpService.sendMessage(String.valueOf(recipient), this.generateMessage(variables, MESSAGE_REJECT_CLIENT), TITLE_REJECT);
                    } else {
                        smtpService.sendMessage(String.valueOf(recipient), MESSAGE_REJECT_TICKET, TITLE_REJECT);
                    }
                    break;
                default:
                    log.warn("UNKNOWN APPROVE STATUS: {}, {}", approveStatus, recipient);
            }
        } else {
            log.warn("RECIPIENT OR APPROVE STATUS IS NULL( status: {}, recipient: {} )", approveStatus, recipient);
        }
    }

    private String generateMessage(Map<String, Object> variables, String message) {
        if (variables != null && !variables.isEmpty()) {
            Object client_json = variables.get(CLIENT_ATTR);
            ClientDto clientDto = jsonParserService.parseJsonToObject(String.valueOf(client_json), ClientDto.class);
            if (clientDto == null) {
                clientDto = jsonParserService.convertMapToObject(client_json, ClientDto.class);
            }
            if (clientDto != null) {
                String withInn = message.replace("?1", clientDto.getInn());
                return withInn.replace("?2", clientDto.getName());
            }
        }
        return message;
    }
}
