package ru.bostonsd.clientmanagement.bpm.service.process.client.listener;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientDto;
import ru.bostonsd.clientmanagement.domain.service.ClientService;
import ru.bostonsd.clientmanagement.domain.service.validation.ValidationService;
import ru.bostonsd.clientmanagement.util.jsonparser.JsonParserService;

/**
 * @author asaburov on 12/5/2019
 * <p>
 * Listening action from Camunda BPM
 */
@Slf4j
@Component
public class ApproveClientService implements JavaDelegate {

    @Autowired
    private ClientService clientService;

    @Autowired
    private JsonParserService parserService;

    @Autowired
    private ValidationService validationService;

    /**
     * Print value of variable approve_status
     *
     * @param execution object with current info about business client
     * @throws Exception
     */
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Object clientJson = execution.getVariable("client_json");
        if (clientJson != null) {
            ClientDto clientDto = parserService.convertMapToObject(clientJson, ClientDto.class);
            if(clientDto == null){
                clientDto = parserService.parseJsonToObject(String.valueOf(clientJson), ClientDto.class);
            }
            if(clientDto != null){
                boolean valid = validationService.validateClient(clientDto);
                if (valid) {
                    clientService.save(clientDto);
                    return;
                }
            }
        }
        log.warn("VALIDATION ERROR");
    }
}
