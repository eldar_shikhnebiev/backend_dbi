package ru.bostonsd.clientmanagement.bpm.service.process.update;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bostonsd.clientmanagement.bpm.model.StatusApprove;
import ru.bostonsd.clientmanagement.bpm.model.update.ComplexClientJson;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientDto;
import ru.bostonsd.clientmanagement.smtp.service.SmtpService;
import ru.bostonsd.clientmanagement.util.jsonparser.JsonParserService;

import java.util.Map;

@Component
@Slf4j
public class SendUpdateStatusListener implements JavaDelegate {

    @Autowired
    private SmtpService smtpService;

    @Autowired
    private JsonParserService jsonParserService;

    private final String MESSAGE_REJECT_CLIENT = "Ваша заявка на обновление клиента (ИНН: ?1, Наименование:?2) была отклонена администратором.\n" +
            "Для получения более подробной информации по этой заявке вы можете перейти в раздел заявок и воспользоваться поиском по ИНН, взяв данные из этого письма.";
    private final String TITLE_REJECT = "Ваша заявка была отклонена";

    private final String MESSAGE_RETURNED_CLIENT = "Ваша заявка на обновление клиента (ИНН: ?1, Наименование: ?2) была возвращена администратором на корректировку.\n" +
            "Для получения более подробной информации по этой заявке вы можете перейти в раздел заявок и воспользоваться поиском по ИНН, взяв данные из этого письма.";
    private final String TITLE_RETURNED = "Ваша заявка была возвращена на корректировку";

    private final String MESSAGE_APPROVED_CLIENT = "Ваша заявка на обновление клиента (ИНН: ?1, Наименование: ?2) была согласована администратором.\n" +
            "Для получения более подробной информации по этой заявке вы можете перейти в раздел заявок и воспользоваться поиском по статусу заявки.";
    private final String TITLE_APPROVED = "Ваша заявка была согласована";

    private final String CLIENT_ATTR = "full_client_json";

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Map<String, Object> variables = execution.getVariables();
        Object approveStatus = execution.getVariable("approve_status");
        Object recipient = execution.getVariable("initiator_id");
        if (approveStatus != null && recipient != null) {
            log.info("STATUS: {}, RECIPIENT: {}", approveStatus, recipient);
            switch (StatusApprove.valueOf(String.valueOf(approveStatus))) {
                case APPROVED:
                    smtpService.sendMessage(String.valueOf(recipient), this.generateMessage(variables, MESSAGE_APPROVED_CLIENT), TITLE_APPROVED);
                    break;
                case RETURNED:
                    smtpService.sendMessage(String.valueOf(recipient), this.generateMessage(variables, MESSAGE_RETURNED_CLIENT), TITLE_RETURNED);
                    break;
                case REJECTED:
                    smtpService.sendMessage(String.valueOf(recipient), this.generateMessage(variables, MESSAGE_REJECT_CLIENT), TITLE_REJECT);
                    break;
                default:
                    log.warn("UNKNOWN APPROVE STATUS: {}, {}", approveStatus, recipient);
            }
        } else {
            log.warn("RECIPIENT OR APPROVE STATUS IS NULL( status: {}, recipient: {} )", approveStatus, recipient);
        }
    }

    private String generateMessage(Map<String, Object> variables, String message) {
        if (variables != null && !variables.isEmpty()) {
            Object client_json = variables.get(CLIENT_ATTR);
            ComplexClientJson clientDto = jsonParserService.parseJsonToObject(String.valueOf(client_json), ComplexClientJson.class);
            if (clientDto == null) {
                clientDto = jsonParserService.convertMapToObject(client_json, ComplexClientJson.class);
            }
            if (clientDto != null) {
                ClientDto oldValue = clientDto.getOldValue();
                String withInn = message.replace("?1", oldValue.getInn());
                return withInn.replace("?2", oldValue.getName());
            }
        }
        return message;
    }
}
