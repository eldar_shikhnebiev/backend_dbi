package ru.bostonsd.clientmanagement.bpm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bostonsd.clientmanagement.bpm.model.CommentMessage;

import java.util.List;

/**
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * @date 18.12.2019
 */
@Repository
public interface CommentRepository extends JpaRepository<CommentMessage, Long> {

    List<CommentMessage> findByProcessId(String processId);

}
