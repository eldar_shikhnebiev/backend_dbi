package ru.bostonsd.clientmanagement.bpm.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @author asaburov on 12/5/2019
 */
@Data
@Builder
public class TaskDto implements Serializable {

    private String processId;
    private String processName;
    private String taskId;
    private String taskName;
    private String distributorInitiator;
    private Map<String, Object> variables;
}
