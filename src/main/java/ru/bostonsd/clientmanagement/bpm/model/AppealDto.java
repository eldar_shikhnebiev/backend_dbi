package ru.bostonsd.clientmanagement.bpm.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryEntity;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryField;
import ru.bostonsd.clientmanagement.history.models.enums.HistoryObjectType;

import java.io.Serializable;

/**
 * @author asaburov on 1/9/2020
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@HistoryEntity(type = HistoryObjectType.TICKET)
public class AppealDto implements Serializable {

    private Long distributorId;
    private String distributorName;
    @HistoryField(alias = "Сообщение")
    private String ticketMessage;
}
