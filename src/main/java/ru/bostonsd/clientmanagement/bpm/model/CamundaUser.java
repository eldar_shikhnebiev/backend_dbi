package ru.bostonsd.clientmanagement.bpm.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author asaburov on 12/9/2019
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CamundaUser implements Serializable {

    private String id;
    private String firstName;
    private String lastName;
    private String email;
}
