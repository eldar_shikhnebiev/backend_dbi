package ru.bostonsd.clientmanagement.bpm.model;

/**
 * @author asaburov on 12/5/2019
 *
 * Array of available statuses for creating client bussines client
 */
public enum StatusApprove {

    APPROVED, REJECTED, RETURNED
}
