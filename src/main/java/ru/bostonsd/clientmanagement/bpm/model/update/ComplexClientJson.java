package ru.bostonsd.clientmanagement.bpm.model.update;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientDto;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ComplexClientJson implements Serializable {

    private ClientDto oldValue;
    private ClientDto newValue;
}
