package ru.bostonsd.clientmanagement.bpm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Saburov Aleksey
 * @date 06.12.2019 13:12
 */

@Data
@Entity(name = "comments")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommentMessage implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    @Column(name="id")
    private Long id;

    @Column(name = "process_id")
    private String processId;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "date")
    private Long createDate;

    @Column(name = "message")
    @Type(type = "text")
    private String message;
}
