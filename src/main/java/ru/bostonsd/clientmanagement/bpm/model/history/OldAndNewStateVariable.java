package ru.bostonsd.clientmanagement.bpm.model.history;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OldAndNewStateVariable implements Serializable {

    private String processId;
    private String assignee;
    private String variableName;
    private String newState;
    private String oldState;
}
