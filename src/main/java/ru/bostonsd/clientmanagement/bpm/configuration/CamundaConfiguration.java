package ru.bostonsd.clientmanagement.bpm.configuration;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.ProcessEngineConfiguration;
import org.camunda.bpm.engine.spring.SpringProcessEngineConfiguration;
import org.camunda.bpm.spring.boot.starter.webapp.CamundaBpmWebappAutoConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.Arrays;


/**
 * @author achekulaev on 02.12.2019
 * <p>
 * Configuration main beans for Camunda BPM
 */
@Configuration
@Slf4j
public class CamundaConfiguration {

    @Autowired
    private DataSource dataSource;

    @Autowired
//    @Qualifier("bpmtm")
    private PlatformTransactionManager transactionManager;

    /**
     * Customization client engine for creation all necessary beans for working with Camunda PBM
     *
     * @param deploymentResources array of resource {@link Resource}
     * @return {@link SpringProcessEngineConfiguration}
     */
    @Bean
    public SpringProcessEngineConfiguration processEngineConfiguration(@Value("classpath:/static/*.bpmn") Resource[] deploymentResources) {

        SpringProcessEngineConfiguration config = new SpringProcessEngineConfiguration();
        log.info("Resources: {}", Arrays.toString(deploymentResources));
        config.setDataSource(dataSource);
        config.setTransactionManager(transactionManager);
        config.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);
        config.setHistory(ProcessEngineConfiguration.HISTORY_FULL);
        config.setJobExecutorActivate(true);
        config.setDeploymentResources(deploymentResources);
        config.setDatabaseType("postgres");
        config.setAuthorizationEnabled(true);
        config.setUserResourceWhitelistPattern("[a-zA-Z0-9_@.-]+");
        config.setGeneralResourceWhitelistPattern("[a-zA-Z0-9_@.]+|camunda-admin");
        return config;
    }

}
