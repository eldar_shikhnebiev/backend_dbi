package ru.bostonsd.clientmanagement.history.models.annatation;

import ru.bostonsd.clientmanagement.history.models.enums.HistoryObjectType;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface HistoryEntity {

    HistoryObjectType type();
}
