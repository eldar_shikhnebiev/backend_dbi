package ru.bostonsd.clientmanagement.history.models.enums;


import ru.bostonsd.clientmanagement.history.models.Ticket;

/**
 * Enumerator type Ticket for {@link Ticket}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 11, 2019
 */

public enum TicketType {
    CREATE_CLIENT,
    LINKED_CLIENT,
    REQUEST
}
