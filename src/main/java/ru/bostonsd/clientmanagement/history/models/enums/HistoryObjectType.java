package ru.bostonsd.clientmanagement.history.models.enums;

public enum HistoryObjectType {

    CLIENT, DISTRIBUTOR, TICKET, ATTACHMENT
}
