package ru.bostonsd.clientmanagement.history.models.dto;

import lombok.*;
import ru.bostonsd.clientmanagement.domain.models.Client;
import ru.bostonsd.clientmanagement.domain.models.Distributor;
import ru.bostonsd.clientmanagement.history.models.Ticket;

/**
 * DTO for Entity Ticket, short form
 * Part of  fields are analogous to fields in the {@link Ticket}
 * For the client the data is obtained by the client ID from data store {@link Client}
 * Name of distributor is obtained by the client ID for data store {@link Distributor}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 12, 2019
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TicketDtoShort {

    private Long id;

    private Long processId;

    private String processName;

    private Long taskId;

    private String taskName;

    private String distributorInitiator;

    private String initiatorId;

    private Long startProcessDate;

}
