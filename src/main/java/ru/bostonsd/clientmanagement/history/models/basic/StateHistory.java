package ru.bostonsd.clientmanagement.history.models.basic;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

/**
 * Model for ticket state history
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * @date 19.12.2019
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "state_history")
@Entity
public class StateHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    @Column(name="id")
    private Long id;

    @Column(name="process_id")
    private String processId;

    @Column(name="field_name")
    private String fieldName;

    @Column(name="old_value")
    private String oldValue;

    @Column(name="new_value")
    private String newValue;

    @Column(name="user_name")
    private String userName;

    @Column(name="full_name")
    private String fullName;

    @Column(name="create_date")
    private Long date;

}
