package ru.bostonsd.clientmanagement.history.models;

import lombok.*;
import ru.bostonsd.clientmanagement.domain.models.File;
import ru.bostonsd.clientmanagement.history.models.enums.TicketStatus;
import ru.bostonsd.clientmanagement.history.models.enums.TicketType;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Ticket domain model
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 11, 2019
 * Updated on December 24, 2019 | vkondaurov
 */


@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ticket")
@Entity
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private Long id;

    /** date of registration in the Camunda */
    @Column(name="start_process_date")
    @ToString.Exclude
    private Date startProcessDate;

    @Column(name = "process_id")
    private Long processId;

    @Column(name = "process_name")
    private String processName;

    @Column(name = "distributor_initiator")
    private String distributorInitiator;

    @Column(name = "initiator_id")
    private String initiatorId;

    @Column(name = "status_flow")
    private String statusFlow;

    @Column(name = "variable", columnDefinition="TEXT")
    private String variable;
}
