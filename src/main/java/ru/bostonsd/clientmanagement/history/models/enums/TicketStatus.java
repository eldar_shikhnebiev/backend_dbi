package ru.bostonsd.clientmanagement.history.models.enums;

import ru.bostonsd.clientmanagement.history.models.Ticket;

/**
 * Enumerator status Ticket for {@link Ticket}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 11, 2019
 */

public enum TicketStatus {
    APPROVAL,
    REJECT
}
