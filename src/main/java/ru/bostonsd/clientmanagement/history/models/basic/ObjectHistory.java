package ru.bostonsd.clientmanagement.history.models.basic;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "object_history")
@Entity
public class ObjectHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    @Column(name="id")
    private Long id;

    @Column(name="object_type")
    private String objectType;

    @Column(name = "object_id")
    private String objectId;

    @Column(name="field_name")
    private String fieldName;

    @Column(name="old_value")
    private String oldValue;

    @Column(name="new_value")
    private String newValue;

    @Column(name="user_name")
    private String userName;

    @Column(name="full_name")
    private String fullName;

    @Column(name="create_date")
    private Long date;
}
