package ru.bostonsd.clientmanagement.history.models.dto;

import lombok.*;
import ru.bostonsd.clientmanagement.domain.models.Client;
import ru.bostonsd.clientmanagement.domain.models.Distributor;
import ru.bostonsd.clientmanagement.history.models.Ticket;


import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Map;

/**
 * DTO for {@link Ticket} domain model
 *
 * Part of  fields are analogous to fields in the {@link Ticket}
 * For the client the data is obtained by the client ID from data store {@link Client}
 * Name of distributor is obtained by the client ID for data store {@link Distributor}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 12, 2019
 * Updated on December 24, 2019 | vkondaurov
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TicketDto {

    private Long id;

    private Long processId;

    private String processName;

    private Long taskId;

    private String taskName;

    private String distributorInitiator;

    private Map<String, Object> variables;
}
