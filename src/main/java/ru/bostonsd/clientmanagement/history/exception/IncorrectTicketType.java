package ru.bostonsd.clientmanagement.history.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.PrintWriter;
import java.io.StringWriter;


/**
 * Exception for Transformation Service Ticket Entity
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * @date 05.12.2019
 *
 * */

@Slf4j
@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Incorrect Type Ticket")
public class IncorrectTicketType extends RuntimeException{
    public IncorrectTicketType(String message) {
        super(message);
        printLog();
    }

    public void printLog () {
        StringWriter trace = new StringWriter();
        printStackTrace(new PrintWriter(trace));
        log.error(trace.toString());
    }
}