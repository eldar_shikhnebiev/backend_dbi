package ru.bostonsd.clientmanagement.history.service;

import ru.bostonsd.clientmanagement.history.models.basic.ObjectHistory;
import ru.bostonsd.clientmanagement.history.models.enums.HistoryObjectType;

import java.util.List;

public interface ObjectHistoryService {

    List<ObjectHistory> getByTypeAndObjectId(HistoryObjectType type, String objectId);

    boolean saveAll(List<ObjectHistory> historyList);

    boolean save(ObjectHistory history);

    boolean delete(ObjectHistory history);
}
