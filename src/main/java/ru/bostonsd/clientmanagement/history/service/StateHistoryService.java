package ru.bostonsd.clientmanagement.history.service;

import ru.bostonsd.clientmanagement.history.models.basic.StateHistory;

import java.util.List;

/**
 * Service for manage StateHistory entity
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * @date 19.12.2019
 */
public interface StateHistoryService {

    List<StateHistory> getByProcessId(String processId);

    boolean save(StateHistory history);

    boolean saveAll(List<StateHistory> histories);

    boolean delete(StateHistory history);

}
