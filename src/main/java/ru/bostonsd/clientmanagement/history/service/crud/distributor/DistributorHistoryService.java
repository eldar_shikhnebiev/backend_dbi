package ru.bostonsd.clientmanagement.history.service.crud.distributor;

import ru.bostonsd.clientmanagement.domain.models.dto.DistributorDto;

public interface DistributorHistoryService {

    DistributorDto updateClientAndCreateHistory(DistributorDto distributors, String jwt);
}
