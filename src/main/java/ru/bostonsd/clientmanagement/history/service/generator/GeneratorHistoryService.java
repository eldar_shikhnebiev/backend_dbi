package ru.bostonsd.clientmanagement.history.service.generator;

import ru.bostonsd.clientmanagement.history.models.basic.ObjectHistory;
import ru.bostonsd.clientmanagement.history.models.basic.StateHistory;

import java.util.List;

public interface GeneratorHistoryService {

    List<StateHistory> generateStateHistory(Object newObj, Object oldObj, String processId, String username);

    List<ObjectHistory> generateObjectHistory(Object newObj, Object oldObj, String username);

    ObjectHistory createAddedOrDeleteValue(Object newObj, Object oldObj, String username, Object id, String type);

    StateHistory createAddedOrDeleteState(Object newObj, Object oldObj, String username, String processId);
}
