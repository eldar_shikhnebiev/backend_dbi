package ru.bostonsd.clientmanagement.history.service.crud.distributor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.domain.models.Distributor;
import ru.bostonsd.clientmanagement.domain.models.dto.DistributorDto;
import ru.bostonsd.clientmanagement.domain.service.DistributorService;
import ru.bostonsd.clientmanagement.domain.service.TransformationService;
import ru.bostonsd.clientmanagement.history.models.basic.ObjectHistory;
import ru.bostonsd.clientmanagement.history.service.ObjectHistoryService;
import ru.bostonsd.clientmanagement.history.service.generator.GeneratorHistoryService;
import ru.bostonsd.clientmanagement.security.service.JwtTokenUtil;

import java.util.List;

@Service
@Slf4j
public class DistributorHistoryServiceImpl implements DistributorHistoryService{

    @Autowired
    private DistributorService distributorService;

    @Autowired
    private GeneratorHistoryService generatorHistoryService;

    @Autowired
    private TransformationService transformationService;

    @Autowired
    private ObjectHistoryService objectHistoryService;

    @Override
    public DistributorDto updateClientAndCreateHistory(DistributorDto distributors, String jwt) {
        String username = JwtTokenUtil.getUsernameFromToken(jwt);
        if(distributors != null && username != null){
            Distributor existedDistributor = distributorService.getDistributorById(distributors.getId());
            DistributorDto oldDistributor = transformationService.transformationFromDistributor(existedDistributor);
            DistributorDto newDistributor = distributorService.update(distributors);
            List<ObjectHistory> objectHistories = generatorHistoryService.generateObjectHistory(newDistributor, oldDistributor, username);
            boolean result = objectHistoryService.saveAll(objectHistories);
            log.info("old client is null: {}, new old is null: {}, username: {}, result update distributor: {}",
                    oldDistributor == null, newDistributor == null, username, result);
            return result ? newDistributor : null;
        }
        return null;
    }
}
