package ru.bostonsd.clientmanagement.history.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.history.models.basic.StateHistory;
import ru.bostonsd.clientmanagement.history.repository.StateHistoryRepository;
import ru.bostonsd.clientmanagement.history.service.StateHistoryService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * @date 19.12.2019
 */
@Service
public class StateHistoryServiceImpl implements StateHistoryService {

    @Autowired
    private StateHistoryRepository repository;

    @Override
    public List<StateHistory> getByProcessId(String processId) {
        if (processId != null && !processId.trim().isEmpty()) {
            return repository.findByProcessIdOrderByDateDesc(processId);

        }
        return new ArrayList<>();
    }

    @Override
    public boolean save(StateHistory history) {
        if (history != null) {
            StateHistory save = repository.save(history);
            return save != null;
        }
        return false;
    }

    @Override
    public boolean saveAll(List<StateHistory> histories) {
        if (histories != null && !histories.isEmpty()) {
            List<StateHistory> stateHistories = repository.saveAll(histories);
            return !stateHistories.isEmpty();
        }
        return false;
    }

    @Override
    public boolean delete(StateHistory history) {
        if (history != null) {
            repository.delete(history);
            return true;
        }
        return false;
    }
}
