package ru.bostonsd.clientmanagement.history.service.generator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.history.models.annatation.*;
import ru.bostonsd.clientmanagement.history.models.basic.ObjectHistory;
import ru.bostonsd.clientmanagement.history.models.basic.StateHistory;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUser;
import ru.bostonsd.clientmanagement.security.service.UserService;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class GeneratorHistoryServiceImpl implements GeneratorHistoryService {

    @Autowired
    private UserService userService;

    @Override
    public List<StateHistory> generateStateHistory(Object newObj, Object oldObj, String processId, String username) {
        List<StateHistory> result = new ArrayList<>();
        if (newObj != null && oldObj != null
                && Objects.equals(newObj.getClass(), oldObj.getClass())
                && newObj.getClass().isAnnotationPresent(HistoryEntity.class)) {

            SCHDUser user = userService.getByUserName(username);

            Field[] declaredFields = newObj.getClass().getDeclaredFields();
            List<Field> innerObjects = Arrays.stream(declaredFields)
                    .filter(f -> f.isAnnotationPresent(HistoryInnerClass.class)).collect(Collectors.toList());
            List<Field> listFields = Arrays.stream(declaredFields).filter(f -> f.isAnnotationPresent(HistoryList.class)).collect(Collectors.toList());

            if (!innerObjects.isEmpty()) {
                for (Field innerClass : innerObjects) {
                    try {
                        innerClass.setAccessible(true);
                        List<StateHistory> stateHistories = this.generateStateHistory(innerClass.get(newObj), innerClass.get(oldObj), processId, username);
                        if (stateHistories != null && !stateHistories.isEmpty()) {
                            result.addAll(stateHistories);
                        }
                        innerClass.setAccessible(false);
                    } catch (IllegalAccessException e) {
                        log.error("GETTING VALUE OF INNER CLASS ERROR, MESSAGE : {}", e.getMessage());
                    }
                }
            }

            if (!listFields.isEmpty()) {
                for (Field field : listFields) {
                    field.setAccessible(true);
                    try {
                        List newValues = (List) field.get(newObj);
                        List oldValues = (List) field.get(oldObj);
                        if (newValues != null && !newValues.isEmpty()
                                && oldValues != null && !oldValues.isEmpty()) {

                            Object firstNewValue = newValues.get(0);
                            Object firstOldValue = oldValues.get(0);
                            if (firstNewValue != null && firstOldValue != null
                                    && Objects.equals(firstNewValue.getClass(), firstOldValue.getClass())
                                    && firstNewValue.getClass().isAnnotationPresent(HistoryEntity.class)) {

                                Field[] innerDeclaredFields = firstNewValue.getClass().getDeclaredFields();
                                Field fieldId = Arrays.stream(innerDeclaredFields)
                                        .filter(o -> o.isAnnotationPresent(HistoryId.class))
                                        .findFirst().orElse(null);
                                if (fieldId != null) {
                                    fieldId.setAccessible(true);

                                    //Searching new objects or updated
                                    for (Object newVal : newValues) {
                                        Object newId = fieldId.get(newVal);
                                        Object oldVal = oldValues.stream().filter(o -> {
                                            Object oldId = null;
                                            try {
                                                oldId = fieldId.get(o);
                                            } catch (IllegalAccessException e) {
                                                e.printStackTrace();
                                            }
                                            return oldId != null && Objects.equals(newId, oldId);
                                        }).findFirst().orElse(null);

                                        if (oldVal != null) {
                                            List<StateHistory> stateHistories = this.generateStateHistory(newVal, oldVal, username, processId);
                                            if (stateHistories != null && !stateHistories.isEmpty()) {
                                                result.addAll(stateHistories);
                                            }
                                        } else {
                                            StateHistory history = this.createAddedOrDeleteState(newVal, oldVal, username, processId);
                                            if(history != null){
                                                result.add(history);
                                            }
                                        }
                                    }
                                    for (Object oldVal : oldValues) {
                                        Object oldId = fieldId.get(oldVal);
                                        Object newVal = oldValues.stream().filter(o -> {
                                            Object newId = null;
                                            try {
                                                newId = fieldId.get(o);
                                            } catch (IllegalAccessException e) {
                                                e.printStackTrace();
                                            }
                                            return newId != null && Objects.equals(newId, oldId);
                                        }).findFirst().orElse(null);

                                        if (newVal == null) {
                                            StateHistory history = this.createAddedOrDeleteState(newVal, oldVal, username, processId);
                                            if(history != null){
                                                result.add(history);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    } catch (IllegalAccessException e) {
                        log.error("Getting list value error, message: {}", e.getMessage());
                    } catch (ClassCastException ex) {
                        log.error("Cast value to collection error, message:{}", ex.getMessage());
                    }
                }
            }

            for (Field currentField : declaredFields) {
                if (currentField.isAnnotationPresent(HistoryField.class)) {
                    try {
                        currentField.setAccessible(true);
                        Object newValue = currentField.get(newObj);
                        Object oldValue = currentField.get(oldObj);

                        if (!Objects.equals(newValue, oldValue)) {
                            HistoryField historyField = currentField.getAnnotation(HistoryField.class);
                            log.info("Old value: {}, new value: {}, field name: {}", oldValue, newValue, historyField.alias());

                            StateHistory stateHistory = StateHistory.builder()
                                    .fieldName(historyField.alias())
                                    .userName(username)
                                    .fullName(user != null ? user.getFullName() : null)
                                    .processId(processId)
                                    .newValue(newValue != null ? String.valueOf(newValue) : null)
                                    .oldValue(oldValue != null ? String.valueOf(oldValue) : null)
                                    .date(new Date().getTime())

                                    .build();
                            result.add(stateHistory);
                        }
                        currentField.setAccessible(false);

                    } catch (IllegalAccessException e) {
                        log.error("GETTING VALUE FROM FIELD ERROR, MESSAGE : {}", e.getMessage());
                    }
                }
            }
        }
        return result;
    }

    @Override
    public List<ObjectHistory> generateObjectHistory(Object newObj, Object oldObj, String username) {
        List<ObjectHistory> result = new ArrayList<>();
        if (newObj != null && oldObj != null
                && Objects.equals(newObj.getClass(), oldObj.getClass())
                && newObj.getClass().isAnnotationPresent(HistoryEntity.class)) {

            SCHDUser user = userService.getByUserName(username);

            Field[] declaredFields = newObj.getClass().getDeclaredFields();
            List<Field> listFields = Arrays.stream(declaredFields).filter(f -> f.isAnnotationPresent(HistoryList.class)).collect(Collectors.toList());
            List<Field> innerObjects = Arrays.stream(declaredFields)
                    .filter(f -> f.isAnnotationPresent(HistoryInnerClass.class)).collect(Collectors.toList());

            Field idField = Arrays.stream(declaredFields)
                    .filter(f -> f.isAnnotationPresent(HistoryId.class))
                    .findFirst().orElse(null);
            Object id = null;
            if (idField != null) {
                try {
                    idField.setAccessible(true);
                    id = idField.get(oldObj);
                    idField.setAccessible(false);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            HistoryEntity historyEntity = newObj.getClass().getAnnotation(HistoryEntity.class);

            if (!innerObjects.isEmpty()) {
                for (Field innerClass : innerObjects) {
                    try {
                        innerClass.setAccessible(true);
                        List<ObjectHistory> objectHistories = this.generateObjectHistory(innerClass.get(newObj), innerClass.get(oldObj), username);
                        if (objectHistories != null && !objectHistories.isEmpty()) {
                            result.addAll(objectHistories);
                        }
                        innerClass.setAccessible(false);
                    } catch (IllegalAccessException e) {
                        log.error("GETTING VALUE OF INNER CLASS ERROR, MESSAGE : {}", e.getMessage());
                    }
                }
            }
            if (!listFields.isEmpty()) {
                for (Field field : listFields) {
                    field.setAccessible(true);
                    try {
                        List newValues = (List) field.get(newObj);
                        List oldValues = (List) field.get(oldObj);
                        if (newValues != null && !newValues.isEmpty()
                                && oldValues != null && !oldValues.isEmpty()) {

                            Object firstNewValue = newValues.get(0);
                            Object firstOldValue = oldValues.get(0);
                            if (firstNewValue != null && firstOldValue != null
                                    && Objects.equals(firstNewValue.getClass(), firstOldValue.getClass())
                                    && firstNewValue.getClass().isAnnotationPresent(HistoryEntity.class)) {

                                Field[] innerDeclaredFields = firstNewValue.getClass().getDeclaredFields();
                                Field fieldId = Arrays.stream(innerDeclaredFields)
                                        .filter(o -> o.isAnnotationPresent(HistoryId.class))
                                        .findFirst().orElse(null);
                                if (fieldId != null) {
                                    fieldId.setAccessible(true);

                                    //Searching new objects or updated
                                    for (Object newVal : newValues) {
                                        Object newId = fieldId.get(newVal);
                                        Object oldVal = oldValues.stream().filter(o -> {
                                            Object oldId = null;
                                            try {
                                                oldId = fieldId.get(o);
                                            } catch (IllegalAccessException e) {
                                                e.printStackTrace();
                                            }
                                            return oldId != null && Objects.equals(newId, oldId);
                                        }).findFirst().orElse(null);

                                        if (oldVal != null) {
                                            List<ObjectHistory> objectHistories = this.generateObjectHistory(newVal, oldVal, username);
                                            if (objectHistories != null && !objectHistories.isEmpty()) {
                                                result.addAll(objectHistories);
                                            }
                                        } else {
                                            ObjectHistory history = this.createAddedOrDeleteValue(newVal, oldVal, username, id, historyEntity.type().name());
                                            if(history != null){
                                                result.add(history);
                                            }
                                        }
                                    }
                                    for (Object oldVal : oldValues) {
                                        Object oldId = fieldId.get(oldVal);
                                        Object newVal = oldValues.stream().filter(o -> {
                                            Object newId = null;
                                            try {
                                                newId = fieldId.get(o);
                                            } catch (IllegalAccessException e) {
                                                e.printStackTrace();
                                            }
                                            return newId != null && Objects.equals(newId, oldId);
                                        }).findFirst().orElse(null);

                                        if (newVal == null) {
                                            ObjectHistory history = this.createAddedOrDeleteValue(newVal, oldVal, username, id, historyEntity.type().name());
                                            if(history != null){
                                                result.add(history);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    } catch (IllegalAccessException e) {
                        log.error("Getting list value error, message: {}", e.getMessage());
                    } catch (ClassCastException ex) {
                        log.error("Cast value to collection error, message:{}", ex.getMessage());
                    }
                }
            }

            for (Field currentField : declaredFields) {
                if (currentField.isAnnotationPresent(HistoryField.class)) {
                    try {
                        currentField.setAccessible(true);
                        Object newValue = currentField.get(newObj);
                        Object oldValue = currentField.get(oldObj);

                        if (!Objects.equals(newValue, oldValue)) {
                            HistoryField historyField = currentField.getAnnotation(HistoryField.class);

                            log.info("Old value: {}, new value: {}, field name: {}", oldValue, newValue, historyField.alias());
                            ObjectHistory objectHistory = ObjectHistory.builder()
                                    .objectId(id != null ? String.valueOf(id) : null)
                                    .objectType(historyEntity.type().name())
                                    .fieldName(historyField.alias())
                                    .userName(username)
                                    .fullName(user != null ? user.getFullName() : null)
                                    .oldValue(oldValue != null ? String.valueOf(oldValue) : null)
                                    .newValue(newValue != null ? String.valueOf(newValue) : null)
                                    .date(new Date().getTime())
                                    .build();
                            result.add(objectHistory);
                        }
                        currentField.setAccessible(false);

                    } catch (IllegalAccessException e) {
                        log.error("GETTING VALUE FROM FIELD ERROR, MESSAGE : {}", e.getMessage());
                    }
                }
            }
        }
        return result;
    }

    @Override
    public ObjectHistory createAddedOrDeleteValue(Object newObj, Object oldObj, String username, Object id, String type) {
        ObjectHistory result = null;
        SCHDUser user = userService.getByUserName(username);
        if (user != null && newObj != null && newObj.getClass().isAnnotationPresent(HistoryEntity.class)) {
            Field[] declaredFields = newObj.getClass().getDeclaredFields();
            Field fieldWithNewValue = Arrays.stream(declaredFields)
                    .filter(f -> f.isAnnotationPresent(HistoryValueForList.class))
                    .findFirst().orElse(null);
            if (fieldWithNewValue != null) {
                try {
                    fieldWithNewValue.setAccessible(true);
                    Object newVal = fieldWithNewValue.get(newObj);
                    HistoryField alais = fieldWithNewValue.getAnnotation(HistoryField.class);

                    result = ObjectHistory.builder()
                            .objectId(id != null ? String.valueOf(id) : null)
                            .objectType(type)
                            .fieldName(alais != null ? alais.alias() : null)
                            .userName(user.getUsername())
                            .fullName(user.getFullName())
                            .newValue(newVal != null ? String.valueOf(newVal) : null)
                            .date(new Date().getTime())
                            .build();
                    fieldWithNewValue.setAccessible(false);
                } catch (IllegalAccessException e) {
                    log.error("Getting value error, message: {}", e.getMessage());
                }
            }

        } else if (user != null && oldObj != null && oldObj.getClass().isAnnotationPresent(HistoryEntity.class)) {
            Field[] declaredFields = oldObj.getClass().getDeclaredFields();
            Field fieldWithOldValue = Arrays.stream(declaredFields)
                    .filter(f -> f.isAnnotationPresent(HistoryValueForList.class))
                    .findFirst().orElse(null);
            if (fieldWithOldValue != null) {
                try {
                    fieldWithOldValue.setAccessible(true);
                    Object oldVal = fieldWithOldValue.get(oldObj);
                    HistoryField alais = fieldWithOldValue.getAnnotation(HistoryField.class);

                    result = ObjectHistory.builder()
                            .objectId(id != null ? String.valueOf(id) : null)
                            .objectType(type)
                            .fieldName(alais != null ? alais.alias() : null)
                            .userName(user.getUsername())
                            .fullName(user.getFullName())
                            .oldValue(oldVal != null ? String.valueOf(oldVal) : null)
                            .date(new Date().getTime())
                            .build();
                    fieldWithOldValue.setAccessible(false);
                } catch (IllegalAccessException e) {
                    log.error("Getting value error, message: {}", e.getMessage());
                }
            }
        }
        return result;
    }

    @Override
    public StateHistory createAddedOrDeleteState(Object newObj, Object oldObj, String username, String processId) {
        StateHistory result = null;
        SCHDUser user = userService.getByUserName(username);
        if (user != null && newObj != null && newObj.getClass().isAnnotationPresent(HistoryEntity.class)) {
            Field[] declaredFields = newObj.getClass().getDeclaredFields();
            Field fieldWithNewValue = Arrays.stream(declaredFields)
                    .filter(f -> f.isAnnotationPresent(HistoryValueForList.class))
                    .findFirst().orElse(null);
            if (fieldWithNewValue != null) {
                try {
                    fieldWithNewValue.setAccessible(true);
                    Object newVal = fieldWithNewValue.get(newObj);
                    HistoryField alais = fieldWithNewValue.getAnnotation(HistoryField.class);

                    result = StateHistory.builder()
                            .fieldName(alais.alias())
                            .userName(user.getUsername())
                            .fullName(user.getFullName())
                            .processId(processId)
                            .newValue(newVal != null ? String.valueOf(newVal) : null)
                            .date(new Date().getTime())
                            .build();
                    fieldWithNewValue.setAccessible(false);
                } catch (IllegalAccessException e) {
                    log.error("Getting value error, message: {}", e.getMessage());
                }
            }

        } else if (user != null && oldObj != null && oldObj.getClass().isAnnotationPresent(HistoryEntity.class)) {
            Field[] declaredFields = oldObj.getClass().getDeclaredFields();
            Field fieldWithOldValue = Arrays.stream(declaredFields)
                    .filter(f -> f.isAnnotationPresent(HistoryValueForList.class))
                    .findFirst().orElse(null);
            if (fieldWithOldValue != null) {
                try {
                    fieldWithOldValue.setAccessible(true);
                    Object oldVal = fieldWithOldValue.get(oldObj);
                    HistoryField alais = fieldWithOldValue.getAnnotation(HistoryField.class);

                    result = StateHistory.builder()
                            .fieldName(alais.alias())
                            .userName(user.getUsername())
                            .fullName(user.getFullName())
                            .processId(processId)
                            .oldValue(oldVal != null ? String.valueOf(oldVal) : null)
                            .date(new Date().getTime())
                            .build();
                    fieldWithOldValue.setAccessible(false);
                } catch (IllegalAccessException e) {
                    log.error("Getting value error, message: {}", e.getMessage());
                }
            }
        }
        return result;
    }
}
