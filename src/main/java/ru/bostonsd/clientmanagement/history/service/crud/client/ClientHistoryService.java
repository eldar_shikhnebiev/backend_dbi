package ru.bostonsd.clientmanagement.history.service.crud.client;

import ru.bostonsd.clientmanagement.domain.models.dto.ClientDto;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientsUpdateTypeDto;

public interface ClientHistoryService {

    ClientDto updateClientAndCreateHistory(ClientDto client, String jwt);

    boolean updateClientAndCreateHistory(ClientsUpdateTypeDto clients, String jwt);
}
