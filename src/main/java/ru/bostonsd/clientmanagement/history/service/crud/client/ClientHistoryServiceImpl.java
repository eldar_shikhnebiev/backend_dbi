package ru.bostonsd.clientmanagement.history.service.crud.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.domain.models.Client;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientDto;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientsUpdateTypeDto;
import ru.bostonsd.clientmanagement.domain.service.ClientService;
import ru.bostonsd.clientmanagement.domain.service.TransformationService;
import ru.bostonsd.clientmanagement.history.models.basic.ObjectHistory;
import ru.bostonsd.clientmanagement.history.service.ObjectHistoryService;
import ru.bostonsd.clientmanagement.history.service.generator.GeneratorHistoryService;
import ru.bostonsd.clientmanagement.security.service.JwtTokenUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class ClientHistoryServiceImpl implements ClientHistoryService {

    @Autowired
    private ObjectHistoryService historyService;

    @Autowired
    private GeneratorHistoryService generatorHistoryService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private TransformationService transformationService;

    @Override
    public ClientDto updateClientAndCreateHistory(ClientDto client, String jwt) {
        String username = JwtTokenUtil.getUsernameFromToken(jwt);
        if (client != null && username != null) {
            Client clientExist = clientService.getClientById(client.getId());
            if (clientExist != null) {
                ClientDto oldClient = transformationService.transformationFromClient(clientExist);
                ClientDto newClient = clientService.updateWithSendInfo(client, clientExist);
                List<ObjectHistory> objectHistories = generatorHistoryService.generateObjectHistory(newClient, oldClient, username);
                boolean result = historyService.saveAll(objectHistories);
                log.info("old client is null: {}, new old is null: {}, username: {}, result update client history: {}",
                        oldClient == null, newClient == null, username, result);
                return newClient;
            }
        }
        return null;
    }

    @Override
    public boolean updateClientAndCreateHistory(ClientsUpdateTypeDto clients, String jwt) {
        String username = JwtTokenUtil.getUsernameFromToken(jwt);
        boolean result = false;
        if (clients != null && username != null) {
            List<ClientDto> oldClients = clientService.getListByIdIn(clients.getClientsId());
            if (clients.getTypeClient() != null && !oldClients.isEmpty()) {
                List<ClientDto> updatedClients = clientService.updateListClientDtoByType(clients);
                List<ObjectHistory> objectHistories = new ArrayList<>();
                for (ClientDto oldClient : oldClients) {
                    Optional<ClientDto> newClient = updatedClients.stream().filter(c -> c.getId().equals(oldClient.getId())).findFirst();
                    if (newClient.isPresent()) {
                        List<ObjectHistory> currentHistiry = generatorHistoryService.generateObjectHistory(newClient.get(), oldClient, username);
                        if(currentHistiry != null && !currentHistiry.isEmpty()) objectHistories.addAll(currentHistiry);
                    }
                }
                if(!objectHistories.isEmpty()){
                    result = historyService.saveAll(objectHistories);
                    log.info("username: {}, result update client history: {}", username, result);
                }
            }
        }
        return result;
    }
}
