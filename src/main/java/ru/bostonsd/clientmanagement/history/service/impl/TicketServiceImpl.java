package ru.bostonsd.clientmanagement.history.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.history.exception.TicketException;
import ru.bostonsd.clientmanagement.history.models.Ticket;
import ru.bostonsd.clientmanagement.history.models.dto.TicketDto;
import ru.bostonsd.clientmanagement.history.models.dto.TicketDtoShort;
import ru.bostonsd.clientmanagement.history.repository.TicketRepository;
import ru.bostonsd.clientmanagement.history.service.TicketService;
import ru.bostonsd.clientmanagement.history.service.TicketTransformationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * Implementation for {@link TicketService}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 11, 2019
 * Updated on December 16, 2019 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 */

@Service
@Slf4j
public class TicketServiceImpl implements TicketService {

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private TicketTransformationService ticketTransformationService;


    /**
     * Implementation of the method defined in the interface {@link TicketService}
     * A list of all instances entity  {@link Ticket} in the data store is requested.
     * Checking that it is not empty and transforming all instances from the domain model
     * into a DTO model {@link TicketDto}.
     *
     * @return  list instances entity {@link TicketDto}, if data store not empty
     *          empty list, if the store does not contain data
     */
    @Override
    public List<TicketDto> getAllList() {
        List<Ticket> tickets = ticketRepository.findAll();
        return getTicketDto(tickets);
    }

    @Override
    public List<TicketDto> getPage(Pageable pageable) {
       Page<Ticket> tickets = ticketRepository.findAll(pageable);
       return getTicketDto(tickets.getContent());
    }

    /**
     * Implementation of the method defined in the interface {@link TicketService}
     * A list of all instances entity  {@link Ticket} in the data store is requested.
     * Checking that it is not empty and transforming all instances from the domain model
     * into a DTO model {@link TicketDtoShort}.
     *
     * @return  list instances entity {@link TicketDtoShort}, if data store not empty
     *          empty list, if the store does not contain data
     */
    @Override
    public List<TicketDtoShort> getShortFormAllList() {
        List<Ticket> tickets = ticketRepository.findAll();
        return getTicketDtoShort(tickets);
    }

    @Override
    public List<TicketDtoShort> getShortFormPage(Pageable pageable) {
        Page<Ticket> tickets = ticketRepository.findAll(pageable);
        return getTicketDtoShort(tickets.getContent());
    }

    /**
     * Implementation of the method defined in the interface {@link TicketService}
     * A list of instances entity {@link Ticket} in the data store
     * with the specified id distributor is requested.
     *
     * After receiving the data - checking that it is not empty
     * and transforming list of instances from the domain model into a DTO model {@link TicketDto}
     *
     * @param distributorInitiator - name of distributor for find  in data store.
     * @return  list instances entity {@link TicketDto}, if data store not empty
     *          empty list, if the store does not contain data
     */
    @Override
    public List<TicketDto> getListByDistributorInitiator(String distributorInitiator) {
        List<Ticket> tickets = ticketRepository.findByDistributorInitiator(distributorInitiator);
        return getTicketDto(tickets);
    }

    @Override
    public List<TicketDto> getPageByDistributorInitiator(String distributorInitiator,
                                                         Pageable pageable) {
        Page<Ticket> tickets = ticketRepository.findByDistributorInitiator(distributorInitiator, pageable);
        return getTicketDto(tickets.getContent());
    }

    @Override
    public List<TicketDto> getListByProcessId(Long processId) {
        List<Ticket> tickets = ticketRepository.findByProcessId(processId);
        return getTicketDto(tickets);
    }

    @Override
    public List<TicketDto> getPageByProcessId(Long processId, Pageable pageable) {
        Page<Ticket> tickets = ticketRepository.findByProcessId(processId, pageable);
        return getTicketDto(tickets.getContent());
    }

    @Override
    public List<TicketDto> getListByProcessName(String processName) {
        List<Ticket> tickets = ticketRepository.findByProcessName(processName);
        return getTicketDto(tickets);
    }

    @Override
    public List<TicketDto> getPageByProcessName(String processName, Pageable pageable) {
        Page<Ticket> tickets = ticketRepository.findByProcessName(processName, pageable);
        return getTicketDto(tickets.getContent());
    }

    @Override
    public List<TicketDto> getListByInitiatorId(String initiatorId) {
        List<Ticket> tickets = ticketRepository.findByInitiatorId(initiatorId);
        return getTicketDto(tickets);
    }


    @Override
    public List<TicketDto> getPageDtoByInitiatorId(String initiatorId, Pageable pageable) {
        Page<Ticket> tickets = ticketRepository.findByInitiatorId(initiatorId, pageable);
        return getTicketDto(tickets.getContent());
    }

    private List<TicketDto> getTicketDto(List<Ticket> tickets) {
        if (tickets.isEmpty()) {
            return new ArrayList<>();
        } else {
            return tickets.stream()
                    .map(ticketTransformationService::transformationFromTicket)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Implementation of the method defined in the interface {@link TicketService}
     * A list of instances entity {@link Ticket} in the data store
     * with the specified id distributor is requested.
     *
     * After receiving the data - checking that it is not empty
     * and transforming list of instances from the domain model into a DTO model {@link TicketDtoShort}
     *
     * @param distributorInitiator - id distributor for find in data store.
     * @return  list instances entity {@link TicketDtoShort}, if data store not empty
     *          empty list, if the store does not contain data
     */
    @Override
    public List<TicketDtoShort> getListShortFormByDistributorInitiator(String distributorInitiator) {
        List<Ticket> tickets = ticketRepository.findByDistributorInitiator(distributorInitiator);
        return getTicketDtoShort(tickets);
    }

    @Override
    public List<TicketDtoShort> getPageShortFormByDistributorInitiator(String distributorInitiator,
                                                                       Pageable pageable) {
        Page<Ticket> tickets = ticketRepository.findByDistributorInitiator(distributorInitiator, pageable);
        return getTicketDtoShort(tickets.getContent());
    }

    private List<TicketDtoShort> getTicketDtoShort(List<Ticket> tickets) {
        if (tickets.isEmpty()) {
            return new ArrayList<>();
        } else {
            return tickets.stream()
                    .map(ticketTransformationService::transformationShortFromTicket)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Implementation of the method defined in the interface {@link TicketService}
     *
     * Finding instance of entity {@link Ticket} in the data store by given id.
     *
     * If not found in data store throw exception {@link TicketException}
     *
     * Checking that it is not empty and transforming the domain model into a DTO model {@link TicketDto}.
     *
     * @param id - id Ticket in datastore for find.
     * @return  instance entity {@link TicketDto}, if finding data store not empty.
     */
    @Override
    public TicketDto getTicketDtoById(Long id) {
        Optional<Ticket> ticketOptional= ticketRepository.findById(id);
        if (ticketOptional.isPresent()) {
            return ticketTransformationService.transformationFromTicket(ticketOptional.get());
        } else throw  new TicketException("Ticket with id " + id + " not found");
    }

    /**
     * Implementation of the method defined in the interface {@link TicketService}
     *
     * Saves given instance of entity {@link Ticket} in the data store.
     * Before saving:
     *    - the given entity instance is transformed from DTO to domain model
     *    - it checks that a ticket with the same id is missing from the repository
     *      If exist in data store throw exception {@link TicketException}
     *
     * After saving reverse transformation of a saved entity instance into a DTO.
     *
     * @param ticketDto - instance of entity {@link TicketDto} for saved.
     * @return  instance entity {@link TicketDto}, saved in data store.
     */
    @Override
    public TicketDto save(TicketDto ticketDto) {
        if (ticketDto !=null ) {
            Ticket ticket = ticketTransformationService.transformationFromTicketDto(ticketDto);
            if (ticket.getId() != null) {
                if (ticketRepository.existsById(ticket.getId())) {
                    throw new TicketException("Ticket with id " + ticket.getId() + " exist in DB");
                }
            }
            ticket = ticketRepository.save(ticket);

            return ticketTransformationService.transformationFromTicket(ticket);
        } else throw new TicketException("Ticket  is empty ");
    }

    /**
     * Implementation of the method defined in the interface {@link TicketService}
     *
     * Deletes instance of entity {@link Ticket} by id in the data store.
     * Before deleted:
     *    - finding instance entity {@link Ticket} by given id.
     *      If it was found, it is deleted to the data store and return true
     *      otherwise false
     *
     * @param id - id Ticket for deleted.
     * @return boolean answer from result of the delete operation
     */
    @Override
    public boolean delete(Long id) {
        Optional<Ticket> ticketOptional= ticketRepository.findById(id);
        if (ticketOptional.isPresent()) {
            ticketRepository.delete(ticketOptional.get());
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void deleteAll(){
        ticketRepository.deleteAll();
        log.info("All snapshots business processes were deleted");
    }

}
