package ru.bostonsd.clientmanagement.history.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.history.models.basic.ObjectHistory;
import ru.bostonsd.clientmanagement.history.models.enums.HistoryObjectType;
import ru.bostonsd.clientmanagement.history.repository.ObjectHistoryRepository;
import ru.bostonsd.clientmanagement.history.service.ObjectHistoryService;

import java.util.ArrayList;
import java.util.List;

@Service
public class ObjectHistoryServiceImpl implements ObjectHistoryService {

    @Autowired
    private ObjectHistoryRepository repository;

    @Override
    public List<ObjectHistory> getByTypeAndObjectId(HistoryObjectType type, String objectId) {
        if (type != null && objectId != null) {
            return repository.findByObjectTypeAndObjectId(type.name(), objectId);
        }
        return new ArrayList<>();
    }

    @Override
    public boolean saveAll(List<ObjectHistory> historyList) {
        if (historyList != null && !historyList.isEmpty()) {
            List<ObjectHistory> objectHistories = repository.saveAll(historyList);
            return !objectHistories.isEmpty();
        }
        return false;
    }

    @Override
    public boolean save(ObjectHistory history) {
        if (history != null) {
            ObjectHistory save = repository.save(history);
            return save != null;
        }
        return false;
    }

    @Override
    public boolean delete(ObjectHistory history) {
        if(history != null){
            repository.delete(history);
            return true;
        }
        return false;
    }
}
