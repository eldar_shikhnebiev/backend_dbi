package ru.bostonsd.clientmanagement.history.service;

import org.springframework.data.domain.Pageable;
import ru.bostonsd.clientmanagement.history.models.Ticket;
import ru.bostonsd.clientmanagement.history.models.dto.TicketDto;
import ru.bostonsd.clientmanagement.history.models.dto.TicketDtoShort;

import java.util.List;

/**
 * Service for managing the entity {@link Ticket}
 * using the Dto format {@link TicketDto}, {@link TicketDtoShort}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 12, 2019
 * Updated on December 16, 2019 | vkondaurov
 */

public interface TicketService {

    /**
     * List all instances of the entity {@link Ticket} in format DTO {@link TicketDto}
     *
     * @return list instances current entity
     */
    List<TicketDto> getAllList();
    List<TicketDto> getPage(Pageable pageable);

    /**
     * List all instances of the entity {@link Ticket} in format DTO {@link TicketDtoShort}
     *
     * @return list instances current entity
     */
    List<TicketDtoShort> getShortFormAllList();
    List<TicketDtoShort> getShortFormPage(Pageable pageable);

    /**
     * List instances of the entity {@link Ticket} in format DTO {@link TicketDto}
     * with the given distributor id
     *
     * @param distributorInitiator - must not be {@literal null}
     * @return list instances current entity
     */
    List<TicketDto> getListByDistributorInitiator(String distributorInitiator);
    List<TicketDtoShort> getListShortFormByDistributorInitiator(String distributorInitiator);

    List<TicketDto> getPageByDistributorInitiator(String distributorInitiator, Pageable pageable);
    List<TicketDtoShort> getPageShortFormByDistributorInitiator(String distributorInitiator, Pageable pageable);

    List<TicketDto> getListByProcessId(Long processId);
    List<TicketDto> getPageByProcessId(Long processId, Pageable pageable);


    List<TicketDto> getListByProcessName(String processName);
    List<TicketDto> getPageByProcessName(String processName, Pageable pageable);

    List<TicketDto> getListByInitiatorId(String initiatorId);
    List<TicketDto> getPageDtoByInitiatorId(String initiatorId, Pageable pageable);


    /**
     * Retrieves an entity {@link Ticket} by the given id of ticket
     *
     * @param id - must not be {@literal null}
     * @return the current entity with the given id or {@literal null} if none found
     */
    TicketDto getTicketDtoById(Long id);


    /**
     * Saves a given entity. Use the returned instance entity {@link Ticket} in format DTO {@link TicketDto}
     *
     * @param ticketDto - must not be {@literal null}.
     * @return the saved entity; will never be {@literal null}.
     */
    TicketDto save(TicketDto ticketDto);

    /**
     * Deletes the entity {@link Ticket}  with the given id .
     *
     * @param id must not be {@literal null}.
     * @return  {@literal true} if the entity with given name deleted in data store.
     *          {@literal false} if the entity with given name not found in data store.
     */
    boolean delete(Long id);

    void deleteAll();

}
