package ru.bostonsd.clientmanagement.history.service;

import org.camunda.bpm.engine.task.Task;
import ru.bostonsd.clientmanagement.bpm.model.TaskDto;
import ru.bostonsd.clientmanagement.history.models.Ticket;
import ru.bostonsd.clientmanagement.history.models.dto.TicketDto;
import ru.bostonsd.clientmanagement.history.models.dto.TicketDtoShort;

/**
 * Service for Transformation Ticket entity to/from DTO inside services entity
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 12, 2019
 */

public interface TicketTransformationService {

    /** Retrieves an entity {@link Ticket} trans formatied from {@link TicketDto}
     *
     * @param ticketDto - must not be {@literal null}
     * @return the current entity;
     */
    Ticket transformationFromTicketDto (TicketDto ticketDto);

    /** Retrieves an entity {@link TicketDto} trans formatied from {@link Ticket}
     *
     * @param ticket - must not be {@literal null}
     * @return the current entity;
     */
    TicketDto transformationFromTicket (Ticket ticket);

    TaskDto transformationTaskFromTicketDto (TicketDto ticketDto);

    TicketDto transformationTicketFromTaskDto (TaskDto taskDto);

    /** Retrieves an entity {@link TicketDtoShort} trans formatied from {@link Ticket}
     *
     * @param ticket - must not be {@literal null}
     * @return the current entity;
     */
    TicketDtoShort transformationShortFromTicket (Ticket ticket);
}
