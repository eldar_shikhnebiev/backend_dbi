package ru.bostonsd.clientmanagement.history.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.bpm.model.TaskDto;
import ru.bostonsd.clientmanagement.history.models.Ticket;
import ru.bostonsd.clientmanagement.history.models.dto.TicketDto;
import ru.bostonsd.clientmanagement.history.models.dto.TicketDtoShort;
import ru.bostonsd.clientmanagement.history.repository.TicketRepository;
import ru.bostonsd.clientmanagement.history.service.TicketTransformationService;

import javax.validation.Valid;
import java.util.Date;
import java.util.Map;

/**
 * Implementation for Transformation Service for Ticket Entiry
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * @date 11.12.2019
 */
@Slf4j
@Service
@PropertySource("classpath:tickettransformation.properties")
public class TicketTransformationServiceImpl implements TicketTransformationService {

    @Autowired
    private TicketRepository ticketRepository;

    @Value("${ticket.variables.start.process.date}")
    private String startProcessDate;

    @Value("${ticket.variables.initiator.id}")
    private String initiatorId;

    @Value("${ticket.variables.status.flow}")
    private String statusFlow;

    @Autowired
    Environment environment;

    @Override
    public Ticket transformationFromTicketDto(TicketDto ticketDto) {

        if (ticketDto != null) {
            Ticket ticket = Ticket.builder()
                    .id(ticketDto.getId())
                    .processId(ticketDto.getProcessId())
                    .processName(ticketDto.getProcessName())
                    .distributorInitiator(ticketDto.getDistributorInitiator())
                    .build();

            if (ticketDto.getVariables() != null && !ticketDto.getVariables().isEmpty()) {
                Object dateString = ticketDto.getVariables().get(startProcessDate);
                ticketDto.getVariables().remove(startProcessDate);
                if (dateString != null) {
                    Long dateLong = Long.valueOf(dateString.toString());
                    Date date = new Date(dateLong);
                    ticket.setStartProcessDate(date);
                }

                ticket.setInitiatorId(ticketDto.getVariables().get(initiatorId).toString());
                ticketDto.getVariables().remove(initiatorId);
                ticket.setStatusFlow(ticketDto.getVariables().get(statusFlow).toString());
                ticketDto.getVariables().remove(statusFlow);
                try {
                    String json = new ObjectMapper().writeValueAsString(ticketDto.getVariables());
                    ticket.setVariable(json);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
            return ticket;
        } else {
            return null;
        }
    }

    @Override
    public TicketDto transformationFromTicket(Ticket ticket) {
        if (ticket != null) {
            TicketDto ticketDto = TicketDto.builder()
                    .id(ticket.getId())
                    .processId(ticket.getProcessId())
                    .processName(ticket.getProcessName())
                    .distributorInitiator(ticket.getDistributorInitiator())
                    .build();
            try {
                Map variable = new ObjectMapper().readValue(ticket.getVariable(), Map.class);
                variable.put(startProcessDate, (ticket.getStartProcessDate()!=null ?ticket.getStartProcessDate().getTime() : null));
                variable.put(initiatorId, ticket.getInitiatorId());
                variable.put(statusFlow, ticket.getStatusFlow());
                ticketDto.setVariables(variable);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return ticketDto;
        }
        return null;

    }

    @Override
    public TaskDto transformationTaskFromTicketDto(TicketDto ticketDto) {
        if (ticketDto != null) {
            return TaskDto.builder()
                    .processId(ticketDto.getProcessId()!= null ? ticketDto.getProcessId().toString() : null)
                    .processName(ticketDto.getProcessName())
                    .taskId(ticketDto.getTaskId()!= null ? ticketDto.getTaskId().toString() : null)
                    .taskName(ticketDto.getTaskName())
                    .distributorInitiator(ticketDto.getDistributorInitiator())
                    .variables(ticketDto.getVariables())
                    .build();
        } else return null;
    }

    @Override
    public TicketDto transformationTicketFromTaskDto(TaskDto taskDto) {
        if (taskDto != null) {
            TicketDto ticketDto =  TicketDto.builder()
                    .processName(taskDto.getProcessName())
                    .distributorInitiator(taskDto.getDistributorInitiator())
                    .taskName(taskDto.getTaskName())
                    .variables(taskDto.getVariables())
                    .build();
            try {
                Long id = Long.valueOf(taskDto.getProcessId());
                ticketDto.setProcessId(id);
            } catch (NumberFormatException e) {
                log.info("ERROR transformation String -> Long. Incorrect format field ProcessId = {} ",  taskDto.getProcessId());
            }

            try {
                Long id =Long.valueOf(taskDto.getTaskId());
                ticketDto.setTaskId(id);
            } catch (NumberFormatException e) {
                log.info("ERROR transformation String -> Long. Incorrect format field TaskId {} ",  taskDto.getTaskId());
            }
            return  ticketDto;
        } else {
            return null;
        }
    }

    @Override
    public TicketDtoShort transformationShortFromTicket(Ticket ticket) {
        if (ticket != null) {
            return TicketDtoShort.builder()
                    .id(ticket.getId())
                    .processId(ticket.getProcessId())
                    .processName(ticket.getProcessName())
                    .distributorInitiator(ticket.getDistributorInitiator())
                    .initiatorId(ticket.getInitiatorId())
                    .startProcessDate(ticket.getStartProcessDate() !=null ?ticket.getStartProcessDate().getTime() : null)
                    .build();
        } else {
            return null;
        }
    }
}
