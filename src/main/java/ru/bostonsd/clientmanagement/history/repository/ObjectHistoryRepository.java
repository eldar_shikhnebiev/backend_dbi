package ru.bostonsd.clientmanagement.history.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bostonsd.clientmanagement.history.models.basic.ObjectHistory;

import java.util.List;

@Repository
public interface ObjectHistoryRepository extends JpaRepository<ObjectHistory, Long> {

    List<ObjectHistory> findByObjectTypeAndObjectId(String objectType, String objectId);
}
