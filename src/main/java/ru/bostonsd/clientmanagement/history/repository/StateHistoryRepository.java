package ru.bostonsd.clientmanagement.history.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bostonsd.clientmanagement.history.models.basic.StateHistory;

import java.util.List;

/**
 * Repository for StateHistory
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * @date 19.12.2019
 */
@Repository
public interface StateHistoryRepository extends JpaRepository<StateHistory, Long> {

    List<StateHistory> findByProcessIdOrderByDateDesc(String processId);
}
