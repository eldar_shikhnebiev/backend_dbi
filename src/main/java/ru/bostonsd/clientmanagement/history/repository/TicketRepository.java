package ru.bostonsd.clientmanagement.history.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bostonsd.clientmanagement.history.models.Ticket;

import java.util.List;

/**
 * Repository for {@link Ticket} domain model
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 16, 2019
 */
@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

    List<Ticket> findByProcessId(Long processId);
    Page<Ticket> findByProcessId(Long processId, Pageable pageable);

    List<Ticket> findByProcessName(String processName);
    Page<Ticket> findByProcessName(String processName, Pageable pageable);

    /**  Returns list instances of the entity {@link Ticket}
     *   that are associated with the given distributor id.
     *
     * @param distributorInitiator - name field of instance {@link Ticket} in DB. must not be {@literal null}
     * @return list entity with the given distributor id or {@literal null} if none found
     */
    List<Ticket> findByDistributorInitiator(String distributorInitiator);
    Page<Ticket> findByDistributorInitiator(String distributorInitiator, Pageable pageable);

    List<Ticket> findByInitiatorId(String initiatorId);
    Page<Ticket> findByInitiatorId(String initiatorId, Pageable pageable);

}
