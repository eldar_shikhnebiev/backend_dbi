package ru.bostonsd.clientmanagement.sellout.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * ClientAnnualSales domain model
 * Used for connection OracleDb view DW_CLIENT_ANNUAL_SALES_V
 * and specify the sales volume
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on February 14, 2020
 */

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "DW_CLIENT_ANNUAL_SALES_V")
@IdClass(ClientAnnualSalesPK.class)
public class ClientAnnualSales {

    @Id
    @Column(name="INN")
    private String innClient;

    @Id
    @Column(name="PARTNER_NAME")
    private String namePartner;

    @Id
    @Column(name="YEAR")
    private Integer yearSales;

    @Column(name="PURCHASE_AMOUNT")
    private BigDecimal amount;
}
