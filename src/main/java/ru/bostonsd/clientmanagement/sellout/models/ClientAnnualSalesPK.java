package ru.bostonsd.clientmanagement.sellout.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;


/**
 * ClientAnnualSalesKey domain model for {@link ClientAnnualSales}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on February 17, 2020
 */

@Data
@Embeddable
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientAnnualSalesPK implements Serializable {

    private String innClient;

    private String namePartner;

    private Integer yearSales;
}
