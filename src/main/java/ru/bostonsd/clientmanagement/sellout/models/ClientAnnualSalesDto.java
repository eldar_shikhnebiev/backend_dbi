package ru.bostonsd.clientmanagement.sellout.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientAnnualSalesDto {

    private String namePartner;

    private Map<Integer, Long> saleYears;

}
