package ru.bostonsd.clientmanagement.sellout.service.impl;

import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.datasource.OracleDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientWithOnlySellOutSalesVolumeDto;
import ru.bostonsd.clientmanagement.domain.models.dto.SellOutSalesVolumeClientDto;
import ru.bostonsd.clientmanagement.sellout.exception.SellOutException;
import ru.bostonsd.clientmanagement.sellout.models.ClientAnnualSales;
import ru.bostonsd.clientmanagement.sellout.models.ClientAnnualSalesDto;
import ru.bostonsd.clientmanagement.sellout.repository.ClientAnnualSalesRepository;
import ru.bostonsd.clientmanagement.sellout.service.ClientAnnualSalesService;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.Year;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation for {@link ClientAnnualSalesService}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * <p>
 * Created on February 17, 2020
 */

@Slf4j
@Service
public class ClientAnnualSalesServiceImpl implements ClientAnnualSalesService {

    @Autowired
    private ClientAnnualSalesRepository clientAnnualSalesRepository;

    @Autowired
    @Qualifier("dataSourceOracle")
    private DataSource dataSourceOracle;

    /**
     * Implementation of the method defined in the interface {@link ClientAnnualSalesService}
     * - Splitting the incoming list of keys into partitions of 1000 items.
     * - Query for each part of the list from SellOut.
     * - Checking that it is not empty and converting to the output data type.
     * - Adding the resulting partition to the shared Map
     *
     * @param innClientPlusPartnerId - list of unique INN and Partner_Id values
     * @param years            - list of codes for which sales data is needed
     * @return map with inn Client  of key and values Map - years and amount
     */


    @Override
    public Map<String, Map<Integer, Long>> getSalesByListInnPlusPartnerIdAndListYears(List<String> innClientPlusPartnerId, List<Integer> years) {
        Map<String, Map<Integer, Long>> collectAll = new HashMap<>();
        Map<String, Map<Integer, Long>> collect;
        List<List<String>> subListsInnPlusPartnerId = Lists.partition(innClientPlusPartnerId, 1000);
        List<ClientAnnualSales> listInnPlusPartnerId;
        for (List<String> subList : subListsInnPlusPartnerId) {
            listInnPlusPartnerId = clientAnnualSalesRepository.findByListInnPlusPartnerId(subList, years);
            if (!listInnPlusPartnerId.isEmpty()) {
                collect = listInnPlusPartnerId.stream()
                        .collect(Collectors.groupingBy(ClientAnnualSales::getInnClient,
                                Collectors.toMap(ClientAnnualSales::getYearSales, e -> e.getAmount().longValue())));
                collectAll.putAll(collect);
            }
        }
        return collectAll;
    }

    /**
     * Implementation of the method defined in the interface {@link ClientAnnualSalesService}
     * - Splitting the incoming list of keys into partitions of 1000 items.
     * - Query for each part of the list from SellOut.
     * - Checking that it is not empty and converting to the output data type.
     * - Adding the resulting partition to the shared Map
     *
     * @param innClient - list of unique INN values
     * @param years     - list of codes for which sales data is needed
     * @return list instance of entity {@link ClientAnnualSalesDto} in Map format
     */
    @Override
    public Map<String, List<ClientAnnualSalesDto>> getSalesByListInnClientAndListYears(List<String> innClient, List<Integer> years) {
        Map<String, List<ClientAnnualSalesDto>> innClientAndLIstPartnerWithSalesAll = new HashMap<>();
        Map<String, List<ClientAnnualSalesDto>> innClientAndLIstPartnerWithSalesPart;
        List<List<String>> listPartsInnClient = Lists.partition(innClient, 1000);
        List<ClientAnnualSales> listInnClient;
        Map<String, Map<Integer, Long>> mapPartnerNameSalesVolume;
        List<ClientAnnualSalesDto> clientAnnualSalesDtoSet;

        for (List<String> partInnClient : listPartsInnClient) {
            listInnClient = clientAnnualSalesRepository.findByListInnClient(partInnClient, years);
            if (!listInnClient.isEmpty()) {
                innClientAndLIstPartnerWithSalesPart = new HashMap<>();
                for (String inn: partInnClient) {
                    mapPartnerNameSalesVolume = listInnClient.stream()
                            .filter(sales -> sales.getInnClient().equals(inn) && sales.getNamePartner() != null)
                            .collect(Collectors.groupingBy(ClientAnnualSales::getNamePartner,
                                    Collectors.toMap(ClientAnnualSales::getYearSales,
                                                            sales -> sales.getAmount().longValue(), (sales1, sales2) -> sales1)));

                    if (!mapPartnerNameSalesVolume.isEmpty()) {
                        clientAnnualSalesDtoSet = new ArrayList<>();
                        for (Map.Entry<String, Map<Integer, Long>> entry : mapPartnerNameSalesVolume.entrySet()) {
                            clientAnnualSalesDtoSet.add(ClientAnnualSalesDto.builder()
                                    .namePartner(entry.getKey())
                                    .saleYears(entry.getValue())
                                    .build());
                        }
                        innClientAndLIstPartnerWithSalesPart.put(inn, clientAnnualSalesDtoSet);
                    }
                }
                innClientAndLIstPartnerWithSalesAll.putAll(innClientAndLIstPartnerWithSalesPart);
            }
        }
        return innClientAndLIstPartnerWithSalesAll;
    }

    @Override
    public ClientWithOnlySellOutSalesVolumeDto getClientSalesByInn(String inn) {

        if (!checkConnection()) throw new SellOutException("База SellOut на данный момент недоступна");

        Integer firstYear =  Year.now().minusYears(1).getValue();
        Integer secondYear =  Year.now().getValue();
        List<Integer> years = Arrays.asList(firstYear, secondYear);

        List<ClientAnnualSales> listInnClient = clientAnnualSalesRepository.findByInnClient(inn, years);

        if (listInnClient.isEmpty()) throw new SellOutException("Клиент не найдет в базе SellOut");

        Map<String, Map<Integer, Long>> mapPartnersWithAnnualSales = listInnClient.stream()
                .filter(sales -> sales.getNamePartner() != null)
                .collect(Collectors.groupingBy(ClientAnnualSales::getNamePartner,
                        Collectors.toMap(ClientAnnualSales::getYearSales,
                                sales -> sales.getAmount().longValue(), (sales1, sales2) -> sales1)));


        Set<SellOutSalesVolumeClientDto> sell = new HashSet<>();
        SellOutSalesVolumeClientDto sellOutSalesVolumeClientDto;
        for (Map.Entry<String, Map<Integer, Long>> partnerWithSales : mapPartnersWithAnnualSales.entrySet()) {
            sellOutSalesVolumeClientDto = new SellOutSalesVolumeClientDto();
            sellOutSalesVolumeClientDto.setNamePartner(partnerWithSales.getKey());
            Long saleYear = partnerWithSales.getValue().get(firstYear);
            if (saleYear != null) {
                sellOutSalesVolumeClientDto.setVolumeFirstYear(saleYear);
            }
            saleYear = partnerWithSales.getValue().get(secondYear);
            if (saleYear != null) {
                sellOutSalesVolumeClientDto.setVolumeSecondYear(saleYear);
            }
            sell.add(sellOutSalesVolumeClientDto);
        }


        return ClientWithOnlySellOutSalesVolumeDto
                .builder()
                .inn(inn)
                .sellOutSalesVolume(sell)
                .build();
    }

    private boolean checkConnection() {
        boolean isValid = false;
        try {
            Connection connection = dataSourceOracle.getConnection();
            isValid =  connection.isValid(1);
        } catch (SQLException e) {
            log.error("Validate connection error: {}", e.getMessage());
        }
        return isValid;
    }
}
