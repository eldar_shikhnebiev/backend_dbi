package ru.bostonsd.clientmanagement.sellout.service;

import ru.bostonsd.clientmanagement.domain.models.dto.ClientWithOnlySellOutSalesVolumeDto;
import ru.bostonsd.clientmanagement.sellout.models.ClientAnnualSales;
import ru.bostonsd.clientmanagement.sellout.models.ClientAnnualSalesDto;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Service for managing the entity {@link ClientAnnualSales}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on February 17, 2020
 */

public interface ClientAnnualSalesService {

    /**
     *  According to the specified list of keys (INN plus Partner ID) and ready,
     *  generates a Map key (INN plus Partner ID) and a value-internal Map (key-year, value-sales volume)
     *
     * @param innPlusPartnerId - list of unique INN values plus the Partner ID
     * @param years - list of codes for which sales data is needed
     * @return - data found by the specified parameters in the form of Map
     */

    Map<String, Map<Integer, Long>> getSalesByListInnPlusPartnerIdAndListYears(List<String> innPlusPartnerId, List<Integer> years);

    Map<String, List<ClientAnnualSalesDto>> getSalesByListInnClientAndListYears(List<String> innClient, List<Integer> years);

    ClientWithOnlySellOutSalesVolumeDto getClientSalesByInn(String innClient);

}
