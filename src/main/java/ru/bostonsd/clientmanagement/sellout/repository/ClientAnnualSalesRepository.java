package ru.bostonsd.clientmanagement.sellout.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.bostonsd.clientmanagement.sellout.models.ClientAnnualSales;

import java.util.List;

/**
 * Repository for {@link ClientAnnualSales} domain model
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on February 18, 2020
 */

@Repository
public interface ClientAnnualSalesRepository extends JpaRepository<ClientAnnualSales, Long> {

    @Query(value = "select v.INN, v.PARTNER_ID, v.YEAR, v.PURCHASE_AMOUNT  " +
                    "from dm.dw_client_annual_sales_v v " +
                    "WHERE v.inn || v.partner_id in :innPlusPartnerId and v.YEAR in :years",
            nativeQuery = true)
    List<ClientAnnualSales> findByListInnPlusPartnerId (@Param("innPlusPartnerId") List<String> innPlusPartnerId,
                                                        @Param("years")List<Integer> years);

    @Query(value = "select v.INN, v.PARTNER_NAME, v.YEAR, v.PURCHASE_AMOUNT  " +
            "from dm.dw_client_annual_sales_v v " +
            "WHERE v.inn  in :innClient and v.YEAR in :years",
            nativeQuery = true)
    List<ClientAnnualSales> findByListInnClient (@Param("innClient") List<String> innClient,
                                                        @Param("years")List<Integer> years);


    @Query(value = "select v.INN, v.PARTNER_NAME, v.YEAR, v.PURCHASE_AMOUNT  " +
            "from dm.dw_client_annual_sales_v v " +
            "WHERE v.inn  = :innClient and v.YEAR in :years",
            nativeQuery = true)
    List<ClientAnnualSales> findByInnClient (@Param("innClient") String innClient,
                                                 @Param("years")List<Integer> years);
}
