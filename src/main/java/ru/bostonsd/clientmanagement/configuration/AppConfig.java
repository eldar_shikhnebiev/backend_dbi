package ru.bostonsd.clientmanagement.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.catalina.Context;
import org.apache.tomcat.util.scan.StandardJarScanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Configurations that apply to the entire application
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 5, 2019
 */

@Configuration
@EnableTransactionManagement
@EnableAsync
@EnableScheduling
@EnableJpaRepositories(
        entityManagerFactoryRef = "entityManagerFactory",
        transactionManagerRef = "transactionManager",
        basePackages = {"ru.bostonsd.clientmanagement.bpm.repository",
                "ru.bostonsd.clientmanagement.domain.repository",
                "ru.bostonsd.clientmanagement.history.repository",
                "ru.bostonsd.clientmanagement.security.repository",
                "ru.bostonsd.clientmanagement.util.jsonparser.importData.repository"})
public class AppConfig {

    @Autowired
    private Environment environment;

    /**
     * Customization Data Source
     * the storage connection parameters are extracted from the application.properties
     *
     * @return {@link DataSource}
     */

    @Bean
    @Primary
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getProperty("spring.datasource.driver-class-name"));
        dataSource.setUrl(environment.getProperty("spring.datasource.url"));
        dataSource.setUsername(environment.getProperty("spring.datasource.username"));
        dataSource.setPassword(environment.getProperty("spring.datasource.password"));
        return dataSource;
    }

    @Bean("postgres")
    @Primary
    public EntityManagerFactoryBuilder entityManagerFactoryBuilder() {
        return new EntityManagerFactoryBuilder(new HibernateJpaVendorAdapter(), new HashMap<>(), null);
    }

    /**
     * Customization Entity Manager Factory with Data Source and parameters are extracted from the application.properties
     *
     * @param builder instance of entity {@link EntityManagerFactoryBuilder} for Entity Manager Factory
     * @return configured instance  of entity {@link LocalContainerEntityManagerFactoryBean}
     */
    @Bean(name = "entityManagerFactory")
    @Primary
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@Qualifier("postgres") EntityManagerFactoryBuilder builder) {
        Map<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", environment.getProperty("spring.jpa.hibernate.ddl-auto"));
        properties.put("hibernate.dialect", environment.getProperty("spring.jpa.properties.hibernate.dialect"));
        return builder
                .dataSource(dataSource())
                .packages("ru.bostonsd.clientmanagement.bpm.model",
                        "ru.bostonsd.clientmanagement.domain.models",
                        "ru.bostonsd.clientmanagement.history.models",
                        "ru.bostonsd.clientmanagement.security.model",
                        "ru.bostonsd.clientmanagement.util.jsonparser.importData.model")
                .properties(properties)
                .build();
    }

    /**
     * Customization Transaction Manager with using Entity Manager Factory and Data Source
     *
     * @param entityManagerFactory - instance of entity {@link EntityManagerFactory} for Transaction Manager
     * @return configured instance  of entity {@link PlatformTransactionManager}
     */
    @Bean(name = "transactionManager")
    @Primary
    public PlatformTransactionManager dbTransactionManager(@Qualifier("entityManagerFactory")
                                                                   EntityManagerFactory entityManagerFactory) {


        JpaTransactionManager transactionManager = new JpaTransactionManager(entityManagerFactory);
        transactionManager.setDataSource(dataSource());
        return transactionManager;
    }


    /**
     * Customization transaction manager with {@link DataSource}
     *
     * @return {@link PlatformTransactionManager}
     */
//    @Bean(name = "bpmtm")
//    public PlatformTransactionManager initTransactionManager() {
//        DataSource dataSource = this.dataSource();
//        DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager(dataSource);
//        dataSourceTransactionManager.setGlobalRollbackOnParticipationFailure(false);
//        return dataSourceTransactionManager;
//    }

    /**
     * Initializing RestTemplate using a configured JSON Converter to Http-request
     *
     * @return initialized entity instance {@link RestTemplate}
     */
    @Bean
    public RestTemplate initRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(mappingJacksonHttpMessageConverter());
        return restTemplate;
    }

    /**
     * Configuring the json to Http message Converter
     *
     * @return initialized entity instance {@link HttpMessageConverter}
     */
    @Bean
    public HttpMessageConverter mappingJacksonHttpMessageConverter() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(defaultMapper());
        return converter;
    }

    /**
     * Initializing the default JSON handler  {@link ObjectMapper}
     *
     * @return initialized entity instance {@link ObjectMapper}
     */
    @Bean
    @Primary
    public ObjectMapper defaultMapper() {
        return new ObjectMapper();
    }

    @Bean(name = "threadPoolTaskExecutor")
    public Executor threadPoolTaskExecutor() {
        return Executors.newFixedThreadPool(10);
    }

    @Bean
    public ViewResolver internalResourceViewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/WEB-INF/pages/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

    /**
     * Resolve conflict spring boot web, include tomcat core and tomcat embeded for jsp
     * https://stackoverflow.com/questions/43264890/after-upgrade-from-spring-boot-1-2-to-1-5-2-filenotfoundexception-during-tomcat/43280452#43280452
     *
     * @return
     */
    @Bean
    public TomcatServletWebServerFactory tomcatFactory() {
        return new TomcatServletWebServerFactory() {
            @Override
            protected void postProcessContext(Context context) {
                ((StandardJarScanner) context.getJarScanner()).setScanManifest(false);
            }
        };
    }

}

