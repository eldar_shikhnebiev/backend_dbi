package ru.bostonsd.clientmanagement.configuration;

import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.pool.OracleDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "entityManagerFactoryOracle",
        transactionManagerRef = "transactionManagerOracle",
        basePackages = {"ru.bostonsd.clientmanagement.sellout"})
public class OracleConfig {

    @Autowired
    private Environment environment;

    @Bean("dataSourceOracle")
    public DataSource dataSourceOracle()  {
        OracleDataSource dataSource = null;
        try {
             dataSource = new OracleDataSource();
             dataSource.setURL(environment.getProperty("spring.datasource.oracle.url"));
             dataSource.setUser(environment.getProperty("spring.datasource.oracle.username"));
             dataSource.setPassword(environment.getProperty("spring.datasource.oracle.password"));
             dataSource.setImplicitCachingEnabled(true);
             dataSource.setFastConnectionFailoverEnabled(true);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return dataSource;
    }

    @Bean("oracle")
    public EntityManagerFactoryBuilder entityManagerFactoryBuilderOracle() {
        return new EntityManagerFactoryBuilder(new HibernateJpaVendorAdapter(), new HashMap<>(), null);
    }

    @Bean(name = "entityManagerFactoryOracle")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryOracle(@Qualifier("oracle") EntityManagerFactoryBuilder builder,
                                                                             @Qualifier("dataSourceOracle") DataSource dataSourceOracle) {
        Map<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", environment.getProperty("spring.jpa.oracle.hibernate.ddl-auto"));
        properties.put("hibernate.dialect", environment.getProperty("spring.jpa.oracle.properties.hibernate.dialect"));
        return builder
                .dataSource(dataSourceOracle)
                .packages("ru.bostonsd.clientmanagement.sellout.models")
                .properties(properties)
                .build();
    }

    @Bean(name = "transactionManagerOracle")
    public PlatformTransactionManager dbOracleTransactionManager(@Qualifier("entityManagerFactoryOracle")
                                                                         EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager(entityManagerFactory);
        transactionManager.setDataSource(dataSourceOracle());
        return transactionManager;
    }
}
