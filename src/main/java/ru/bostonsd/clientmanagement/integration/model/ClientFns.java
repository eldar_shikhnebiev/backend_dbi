package ru.bostonsd.clientmanagement.integration.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * DTO object for transformation data from API DaData.ru
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 4, 2019
 */
@Data
@AllArgsConstructor
@Builder
public class ClientFns {

    /** taxpayer identification number */
    private String inn;

    private String name;

    private String phone;

    private String email;

    private String address;

    private String site;
}
