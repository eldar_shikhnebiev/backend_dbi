package ru.bostonsd.clientmanagement.integration.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Body model for request to API DaData.ru
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 4, 2019
 */

@Data
@AllArgsConstructor
@Builder
public class FnsQuery {
    private String query;
}
