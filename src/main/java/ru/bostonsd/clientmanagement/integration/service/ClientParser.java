package ru.bostonsd.clientmanagement.integration.service;

import ru.bostonsd.clientmanagement.integration.model.ClientFns;

/**
 * Parsing service response data from DaData.ru
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 5, 2019
 */
public interface ClientParser {
    /**
     * Parsing json with client data into an instance of the ClientFns entity
     *
     * @param json - response in JSON format received from DaData.ru
     * @return - an instance of the CLientFNS entity parsed from a given JSOn object
     */
    ClientFns parse(String json);
}
