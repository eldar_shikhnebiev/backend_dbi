package ru.bostonsd.clientmanagement.integration.service;

import ru.bostonsd.clientmanagement.integration.model.ClientFns;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Integration service for DaData.ru
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * @date 05.12.2019
 */
public interface SearchClientIntegration {
    /**
     * search client data on DaData.ru by the specified inn.
     *
     * @param inn -  taxpayer identification number for search
     * @return - result of search by inn
     */
    String getEntityJsonByInn(String inn);

    Map<String, ClientFns> checkByInn(Collection<String> inns);
}
