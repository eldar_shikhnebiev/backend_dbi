package ru.bostonsd.clientmanagement.integration.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.integration.model.ClientFns;
import ru.bostonsd.clientmanagement.integration.service.ClientParser;

/**
 * Implementation for Parser Service
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 4, 2019
 */

@Service
@Slf4j
public class ClientParserService implements ClientParser {

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Implementation of the method defined in the interface {@link ClientParser}
     *
     * Transformation data from DaData.ru to ClientFns
     *  The transformation process
     *  - extracts data from the specified string to the json node
     *  - the JSON node data is passed by key to the fields of the ClientFns entity instance created using the Builder
     *
     * @param json - response in JSON format received from DaData.ru
     * @return -
     */
    public ClientFns parse(String json) {
        try {
            JsonNode client = objectMapper.readTree(json).get("suggestions").get(0);
            if (client != null) {
                return ClientFns.builder()
                        .inn(client.get("data").get("inn") != null ? client.get("data").get("inn").asText() : null)
                        .name(client.get("value") != null ? client.get("value").asText() : null)
                        .phone(client.get("data").get("phones") != null
                                && !client.get("data").get("phones").asText().equals("null") ? client.get("data").get("phones").asText() : null)
                        .address(client.get("data").get("address").get("value") != null
                                && !client.get("data").get("address").get("value").asText().equals("null") ? client.get("data").get("address").get("value").asText() : null)
                        .email(client.get("data").get("emails") != null
                                && !client.get("data").get("emails").asText().equals("null") ? client.get("data").get("emails").asText() : null)
                        .build();
            }
        } catch (JsonProcessingException e) {
            log.error("JSON PARSE ERROR: {}", e.getMessage());
        }
        return null;
    }
}
