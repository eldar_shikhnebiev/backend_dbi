package ru.bostonsd.clientmanagement.integration.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.bostonsd.clientmanagement.integration.model.ClientFns;
import ru.bostonsd.clientmanagement.integration.model.FnsQuery;
import ru.bostonsd.clientmanagement.integration.service.SearchClientIntegration;

import java.io.IOException;
import java.net.ConnectException;
import java.util.*;

/**
 * Implementation for Search Client Integration Service
 *
 * This service send request to API DaData.ru and get data about organization
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 4, 2019
 */

@Service
public class SearchClientIntegrationService implements SearchClientIntegration {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ClientParserService clientParserService;

    /**
     * the url where client requests are sent. Data is extracted from the application.properties
     */
    @Value("${dadata.api.search.url.inn}")
    private String FNS_URL_SEARCH_BY_INN;

    /**
     * token to connect to Data.ru
     */
    @Value("${dadata.api.token}")
    private String apiKey;

    private final String RESPONSE_NULL = "{\"suggestions\":[]}";

    /**
     * Getting information about an organization from DaData.ru by given INN
     *
     * An Http request of the POST type is created to receive data
     * with the header fields-the types of sent and received context, the token for authorization.
     *
     * The body of the request specifies the tin For the search
     *
     * @param inn -  taxpayer identification number for search
     * @return -result search in JSON format type String
     */
    public String getEntityJsonByInn(String inn) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", "Token " + apiKey);
        FnsQuery query = FnsQuery.builder().query(inn).build();
        HttpEntity<FnsQuery> entity = new HttpEntity<>(query, headers);
        ResponseEntity<String> exchange = restTemplate.exchange(FNS_URL_SEARCH_BY_INN, HttpMethod.POST, entity, String.class);
        return exchange.getBody();
    }

    @Override
    public Map<String, ClientFns> checkByInn(Collection<String> inns) {
        Map<String, ClientFns> resultCheck = new HashMap<>();
        String entityJson;
        try{
            for (String inn: inns) {
                entityJson = this.getEntityJsonByInn(inn);
                if (entityJson.equals(RESPONSE_NULL)) {
                    resultCheck.put(inn, null);
                } else {
                    resultCheck.put(inn, clientParserService.parse(entityJson));
                }
            }
        } catch (RuntimeException e) {
            e.getMessage();
        }
        return resultCheck;
    }
}
