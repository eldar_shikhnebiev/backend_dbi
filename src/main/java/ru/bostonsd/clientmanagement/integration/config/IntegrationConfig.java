package ru.bostonsd.clientmanagement.integration.config;

import org.springframework.context.annotation.Configuration;

/**
 * Configuration for integration component
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 4, 2019
 */

@Configuration
public class IntegrationConfig {

}
