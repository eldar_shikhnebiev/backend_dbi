package ru.bostonsd.clientmanagement.security.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUser;

import java.util.List;

@Repository
public interface SCHDUserRepository extends JpaRepository<SCHDUser, Long> {

    SCHDUser findByUsername(String username);
    List<SCHDUser> findByFullName(String fullName, Pageable pageable);;

    List<SCHDUser> findByDistributorId(Long distributorId, Pageable pageable);
    List<SCHDUser> findByDistributorIdIn(List<Long> distributorIds);

    List<SCHDUser> findByRoleRoleName(String roleName, Pageable pageable);
    List<SCHDUser> findAllByRoleRoleName(String roleName);

    List<SCHDUser> findByIsActive(Boolean isActive);

}
