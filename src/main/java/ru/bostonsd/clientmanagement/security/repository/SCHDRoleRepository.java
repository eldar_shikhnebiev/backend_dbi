package ru.bostonsd.clientmanagement.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bostonsd.clientmanagement.security.model.database.SCHDRole;

@Repository
public interface SCHDRoleRepository extends JpaRepository<SCHDRole, Long> {

    SCHDRole findByRoleName(String roleName);
}
