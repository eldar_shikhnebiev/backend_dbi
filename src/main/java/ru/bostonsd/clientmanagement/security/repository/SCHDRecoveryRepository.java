package ru.bostonsd.clientmanagement.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUserPasswordRecovery;

/**
 * Repository for {@link SCHDUserPasswordRecovery} domain model
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on February 5, 2020
 * Updated on February 7, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 */


@Repository
public interface SCHDRecoveryRepository extends JpaRepository<SCHDUserPasswordRecovery, String> {
        SCHDUserPasswordRecovery findByUuidCode(String uuidCode);

        @Modifying
        @Query("DELETE from SCHDUserPasswordRecovery a where a.dateRegistration <= :earlyDate")
        int deleteAllWithDateRegistrationBefore(@Param("earlyDate") Long earlyDate);
}

