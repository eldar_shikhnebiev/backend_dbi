package ru.bostonsd.clientmanagement.security.config.jwt;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;
import ru.bostonsd.clientmanagement.security.service.AuthenticationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The Logout handler.
 * Logs out a user out by a jwt token from the Authorization request header.
 *
 * @author Sergey Petryaev (spetryaev@bostonsd.ru)
 * Created on December 4, 2019
 */
@Component
@Slf4j
public class JwtLogoutHandler implements LogoutHandler {

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        boolean isLoggedOut = authenticationService.logoutByTokenRequest(request, response, authentication);
        if(!isLoggedOut){
            try {
                log.info("Unauthorized attempt to sign out.");
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
