package ru.bostonsd.clientmanagement.security.config.jwt;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.bostonsd.clientmanagement.security.exception.CustomSecurityException;
import ru.bostonsd.clientmanagement.security.service.AuthenticationService;
import ru.bostonsd.clientmanagement.security.service.JwtTokenUtil;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The JwtTokenFilter is the main element in the JWT-based authentication.
 * <p> Since the filter provides authentication, it is called
 * <b>before</b> the Spring's Security UsernamePasswordAuthenticationFilter </p>
 *
 * @author Sergey Petryaev (spetryaev@bostonsd.ru)
 */
@Slf4j
@Component
public class JwtTokenFilter extends OncePerRequestFilter {

    @Autowired
    private AuthenticationService authenticationService;

    /*
     * Here's a list of uri addresses ignored by WebSecurityConfig class.
     * All the requests by there URIs still go trough the filter chain,
     * but they don't need authentication to get in.
     */
    private static String[] ignoredByWebConfigURIAddresses;

    static {
        ignoredByWebConfigURIAddresses = new String[]{
                /*   etc   */    "/csrf",
                /* camunda */    "/api", "/app", "/lib", "/camunda-welcome",
                /* swagger */    "/v2", "/swagger-", "/configuration", "/webjars", "/public"};
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        /* We don't want these requests to be spamming log messages.*/
        if (StringUtils.startsWithAny(request.getRequestURI(), ignoredByWebConfigURIAddresses)) {
            // Do nothing.
        } else {
            log.info(request.getMethod() + " request to " + request.getRequestURL().toString());
        }

        String token = JwtTokenUtil.extractTokenFromRequest(
                request.getHeader("Authorization")
        );
        try {
            authenticationService.authenticateByTokenRequest(token);
        } catch (CustomSecurityException ex) {
            // This is very important, since it guarantees the user is not authenticated at all.
            SecurityContextHolder.clearContext();
            log.error("UserName: {} . Error - {}", JwtTokenUtil.getUsernameFromToken(token), ex.getMessage());
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }

        // Continue going through the filter chain even if a user is not authenticated at all.
        filterChain.doFilter(request, response);
    }

}
