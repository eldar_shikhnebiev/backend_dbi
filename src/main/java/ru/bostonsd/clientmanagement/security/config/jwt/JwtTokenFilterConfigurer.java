package ru.bostonsd.clientmanagement.security.config.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

/**
 * This is a http setting class for the JwtTokenFilter.
 * This setting is applied to the main one as an extension.
 *
 * @author Sergey Petryaev (spetryaev@bostonsd.ru)
 */
@Component
public class JwtTokenFilterConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

  @Autowired
  private JwtTokenFilter customFilter;

  @Override
  public void configure(HttpSecurity http) throws Exception {
    http.addFilterBefore(customFilter, UsernamePasswordAuthenticationFilter.class);
  }

}
