package ru.bostonsd.clientmanagement.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import ru.bostonsd.clientmanagement.security.config.jwt.JwtAuthenticationEntryPoint;
import ru.bostonsd.clientmanagement.security.config.jwt.JwtLogoutHandler;
import ru.bostonsd.clientmanagement.security.config.jwt.JwtTokenFilterConfigurer;
import ru.bostonsd.clientmanagement.security.service.impl.SCHDUserDetailsService;

/**
 * The main security configuration class.
 *
 * @author Sergey Petryaev (spetryaev@bostonsd.ru)
 * Created on December 3, 2019
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Autowired
    private SCHDUserDetailsService SCHDUserDetailsService;

    @Autowired
    private JwtTokenFilterConfigurer jwtTokenFilterConfigurer;

    @Autowired
    private JwtLogoutHandler jwtLogoutHandler;

    /**
     * CORS Configuration
     */
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH", "OPTIONS");
            }
        };
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        // Allow swagger to be accessed without authentication
        // But it still goes through filters
        web.ignoring()
                .antMatchers("/resources/**")
                // Swagger
                .antMatchers("/v2/api-docs")
                .antMatchers("/swagger-resources/**")
                .antMatchers("/swagger-ui.html")
                .antMatchers("/configuration/**")
                .antMatchers("/webjars/**")
                .antMatchers("/public")
                .antMatchers("/webapp/**")
                .antMatchers("/WEB-INF/pages/**")
                .antMatchers("/recovery/**");
    }

    /**
     * MAIN SECURITY CONFIGURATION
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Disable CSRF (cross site request forgery)
        http.csrf().disable();

        // No session will be created or used by spring security
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // Entry points
        http.authorizeRequests()
                // etc
                .antMatchers("/").permitAll()
                .antMatchers("/csrf").permitAll()

                // auth endpoints
                .antMatchers(HttpMethod.POST, "/auth/signin").permitAll()
                .antMatchers(HttpMethod.POST, "/auth/signup").permitAll()

                // Camunda's UI and API
                .antMatchers("/camunda-welcome/**").permitAll()
                .antMatchers("/app/**", "/lib/**", "/api/**").permitAll()
                //JSP Recovery Password
                .antMatchers("/recovery/**").permitAll()
                .antMatchers("/css/**", "/js/**").permitAll()
                // BY ROLE
                /*.antMatchers(HttpMethod.POST, "users/*").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "users/*").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"/users/full/*").hasRole("ADMIN")*/

                // DISABLE SECURITY - disables authentication requirement
//                .antMatchers("/**").permitAll()
                .anyRequest().authenticated();

        // Logout handling
        http.logout()
                .deleteCookies("JSESSIONID", "remove")
                .logoutUrl("/auth/signout")
                .addLogoutHandler(jwtLogoutHandler)
                .logoutSuccessHandler((new HttpStatusReturningLogoutSuccessHandler(HttpStatus.OK))) // WITHOUT REDIRECT
                .permitAll();
        // Exception handling
        // If an unauthorized user tries to access a resource
        http.exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint);

        // Apply JWT http config
        http.apply(jwtTokenFilterConfigurer);

        // CORS
        http.cors();
    }

    /*
     * AUTHENTICATION MANAGER CONFIGURATION
     */

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * Configuration of AuthenticationManager so that it knows from where to load
     * a user for matching credentials
     * <p>
     * It uses BCryptPasswordEncoder
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(SCHDUserDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
