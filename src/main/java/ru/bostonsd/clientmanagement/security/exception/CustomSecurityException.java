package ru.bostonsd.clientmanagement.security.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.PrintWriter;
import java.io.StringWriter;

@Slf4j
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Bad Request for DB User ")
public class CustomSecurityException extends RuntimeException {

  public CustomSecurityException(String msg) {
    super(msg);
    printLog();
  }

  public void printLog () {
    StringWriter trace = new StringWriter();
    printStackTrace(new PrintWriter(trace));
    log.error(trace.toString());
  }

}
