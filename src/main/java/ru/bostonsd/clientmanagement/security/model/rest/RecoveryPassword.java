package ru.bostonsd.clientmanagement.security.model.rest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Model DTO for recovery password
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on February 7, 2020
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RecoveryPassword {
    private String password;
    private String passwordConfirm;
    private String code;
}
