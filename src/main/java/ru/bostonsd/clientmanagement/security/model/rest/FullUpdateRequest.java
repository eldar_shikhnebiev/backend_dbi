package ru.bostonsd.clientmanagement.security.model.rest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.bostonsd.clientmanagement.security.model.database.RoleEnum;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FullUpdateRequest{
    private String fullName;
    private String password;
    private Long distributorId;
    private RoleEnum role;
    private Boolean isActive;
}
