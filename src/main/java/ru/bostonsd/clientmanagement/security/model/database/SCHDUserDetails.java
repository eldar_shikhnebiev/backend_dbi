package ru.bostonsd.clientmanagement.security.model.database;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * The UserDetails implementation which is used for Spring Security Authorization
 *
 * @author Sergey Petryaev (spetryaev@bostonsd.ru)
 * Created on December 3, 2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SCHDUserDetails implements UserDetails {

    private Long id;
    private String username;
    private String fullName;
    private Long distributorId;
    private String distributorName;

    private String password;
    private boolean isActive;
    private Collection<? extends GrantedAuthority> authorities;

    public static SCHDUserDetails create(SCHDUser user){
        List<GrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority(user.getRole().getRoleName()));
        return SCHDUserDetails.builder()
                .id(user.getId())
                .username(user.getUsername())
                .fullName(user.getFullName())
                .distributorId(user.getDistributorId())
                .distributorName(user.getDistributorName())
                .password(user.getPassword())
                .isActive(user.getIsActive())
                .authorities(authorities)
                .build();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isActive;
    }

    public Long getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }
}
