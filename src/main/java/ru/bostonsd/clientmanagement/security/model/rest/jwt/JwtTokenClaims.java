package ru.bostonsd.clientmanagement.security.model.rest.jwt;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUser;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUserDetails;

/**
 * The JwtTokenClaims is used for packaging a payload of a jwt token
 *
 * @author Sergey Petryaev (spetryaev@bostonsd.ru)
 * Created on December 3, 2019
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JwtTokenClaims {

    private Long sub;
    private String username;
    private String roleName;
    private String fullName;
    private Long distributorId;
    private String distributorName;

    public JwtTokenClaims(SCHDUserDetails userDetails) {
        this.sub = userDetails.getId();
        this.username = userDetails.getUsername();
        this.roleName = userDetails.getAuthorities().iterator().next().getAuthority(); // each user has only one role
        this.fullName = userDetails.getFullName();
        this.distributorId = userDetails.getDistributorId();
        this.distributorName = userDetails.getDistributorName();
    }

    public JwtTokenClaims(SCHDUser user) {
        this.sub = user.getId();
        this.username = user.getUsername();
        this.roleName = user.getRole().getRoleName(); // each user has only one role
        this.distributorId = user.getDistributorId();
        this.distributorName = user.getDistributorName();
    }

}
