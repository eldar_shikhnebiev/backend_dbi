package ru.bostonsd.clientmanagement.security.model.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for managing access to methods based on user roles
 * To use the annotation correctly you must specify the content argument user role
 * in the parameters of the annotated method
 * For example - jwt token.
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on March 2, 2020
 *
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateRole {
    String[] roles();
}
