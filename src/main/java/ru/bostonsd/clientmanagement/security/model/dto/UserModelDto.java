package ru.bostonsd.clientmanagement.security.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUser;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Aleksei Saburov (asaburov@bostonsd.ru)
 * Created on December 23, 2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserModelDto implements Serializable {

    private Long id;
    private String username;
    private String fullName;
    private Boolean isActive;
    private Long distributorId;
    private String distributorName;
    private String roleName;
    private Date createdDate;
    private Date lastModifiedDate;

    public UserModelDto(SCHDUser schdUser){
        this.id = schdUser.getId();
        this.username = schdUser.getUsername();
        this.fullName = schdUser.getFullName();
        this.isActive = schdUser.getIsActive();
        this.distributorId = schdUser.getDistributorId();
        this.distributorName = schdUser.getDistributorName();
        this.roleName = schdUser.getRole().getRoleName();
        this.createdDate = schdUser.getCreatedDate();
        this.lastModifiedDate = schdUser.getLastModifiedDate();
    }
}
