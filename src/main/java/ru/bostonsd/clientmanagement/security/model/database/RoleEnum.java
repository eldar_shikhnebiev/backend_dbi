package ru.bostonsd.clientmanagement.security.model.database;

/**
 * User Roles
 */
public enum RoleEnum {
    ROLE_ADMIN, ROLE_DISTRIBUTOR, ROLE_GOLDEN_DEALER
}
