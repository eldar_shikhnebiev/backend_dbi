package ru.bostonsd.clientmanagement.security.model.database;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *  UserPasswordRecovery domain model
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on February 5, 2020
 *
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "user_recovery")
public class SCHDUserPasswordRecovery {

    @Id
    @Column(name="username")
    private String username;

    @Column(name = "code")
    private String uuidCode;

    @Column(name = "create_date")
    private Long dateRegistration;
}
