package ru.bostonsd.clientmanagement.security.model.rest;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 19, 2019
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BasicUpdateRequest {
    private String fullName;
    private String oldPassword;
    private String newPassword;
}
