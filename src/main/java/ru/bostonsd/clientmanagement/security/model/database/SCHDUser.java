package ru.bostonsd.clientmanagement.security.model.database;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * The User model.
 *
 * @author Sergey Petryaev (spetryaev@bostonsd.ru)
 * Created on December 3, 2019
 */
@Data
@Entity
@Table(name = "users", schema = "public")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SCHDUser{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="username", nullable = false, unique = true)
    private String username;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @JsonIgnore
    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "active")
    private Boolean isActive;

    @Column(name = "distributor_id")
    private Long distributorId;

    @Column(name = "distributor_name")
    private String distributorName;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "role_id", nullable = false)
    private SCHDRole role;

    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "last_modified_date")
    private Date lastModifiedDate;



    public SCHDUser(SCHDUser user){
        this.id = user.id;
        this.username = user.username;
        this.fullName = user.fullName;
        this.password = user.password;
        this.isActive = user.isActive;
        this.distributorId = user.distributorId;
        this.distributorName = user.distributorName;
        this.role = user.role;
        this.createdDate = user.createdDate;
        this.lastModifiedDate = user.lastModifiedDate;
    }
}
