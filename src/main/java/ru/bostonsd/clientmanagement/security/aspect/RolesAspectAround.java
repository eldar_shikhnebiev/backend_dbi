package ru.bostonsd.clientmanagement.security.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ru.bostonsd.clientmanagement.domain.exception.AppException;
import ru.bostonsd.clientmanagement.domain.exception.ClientException;
import ru.bostonsd.clientmanagement.security.model.annotation.ValidateRole;
import ru.bostonsd.clientmanagement.security.model.rest.jwt.JwtTokenClaims;
import ru.bostonsd.clientmanagement.security.service.JwtTokenUtil;

import java.util.Arrays;
import java.util.Objects;


/**
 * Processing AspectJ getting access to a method marked with the annotation {@link ValidateRole}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on March 2, 2020
 * Updated on April 2, 2020 | vkondaurov
 */
@Slf4j
@Aspect
@Component
public class RolesAspectAround {

    @Around("execution(* ru.bostonsd.clientmanagement.web..*(..)) && @annotation(validateRole)")
    public ResponseEntity rolesAroundAdvice(ProceedingJoinPoint joinPoint, ValidateRole validateRole) {
        ResponseEntity result = new ResponseEntity(HttpStatus.FORBIDDEN);
        try {
            for (Object arg : joinPoint.getArgs()) {
                if (String.valueOf(arg).startsWith("Bearer ")) {
                    JwtTokenClaims userClaims = JwtTokenUtil.getUserClaims(String.valueOf(arg));
                    if (userClaims != null) {
                        boolean isValidRole = Arrays.stream(validateRole.roles())
                                .anyMatch(f -> Objects.equals(f, userClaims.getRoleName()));

                        result = isValidRole ? (ResponseEntity) joinPoint.proceed() : new ResponseEntity(HttpStatus.FORBIDDEN);
                    }
                    break;
                }
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
            if (e instanceof AppException) {
                result = new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
            }
        }
        return result;
    }
}
