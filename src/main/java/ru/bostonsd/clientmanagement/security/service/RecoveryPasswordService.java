package ru.bostonsd.clientmanagement.security.service;

import ru.bostonsd.clientmanagement.security.model.rest.RecoveryPassword;

/**
 * Service for managing processing recovery password
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on February 5, 2020
 */

public interface RecoveryPasswordService {

    boolean sendMail(String email, String requestUri);

    String recovery(String code);

    boolean updatePassword(RecoveryPassword password);
}
