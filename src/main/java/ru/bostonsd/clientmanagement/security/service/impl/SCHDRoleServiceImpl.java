package ru.bostonsd.clientmanagement.security.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.security.model.database.SCHDRole;
import ru.bostonsd.clientmanagement.security.repository.SCHDRoleRepository;
import ru.bostonsd.clientmanagement.security.service.RoleService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SCHDRoleServiceImpl implements RoleService {

    @Autowired
    private SCHDRoleRepository roleRepository;


    @Override
    public Optional<SCHDRole> getById(Long id) {
        if(id != null){
            return roleRepository.findById(id);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public SCHDRole getRoleByName(String roleName) {
        if (roleName != null && !roleName.trim().isEmpty()) {
            return roleRepository.findByRoleName(roleName);
        }
        return null;
    }

    @Override
    public List<String> getAllRoleNames() {
        List<SCHDRole> allRoles = roleRepository.findAll();
        List<String> result = new ArrayList<>();
        if (allRoles != null && !allRoles.isEmpty()) {
            result = allRoles.stream().map(SCHDRole::getRoleName).collect(Collectors.toList());
        }
        return result;
    }

    @Override
    public List<SCHDRole> getAll() {
        return roleRepository.findAll();
    }

    @Override
    public Long create(SCHDRole role) {
        if(role != null){
            roleRepository.save(role);
            return role.getId();
        }
        return null;
    }

    @Override
    public boolean update(SCHDRole role) throws Exception {
        if(role != null && role.getId() != null){
            roleRepository.save(role);
            return true;
        } else {
            throw new Exception("Role and role Id must not be null");
        }
    }

    @Override
    public boolean delete(Long id) {
        if(id != null){
            roleRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
