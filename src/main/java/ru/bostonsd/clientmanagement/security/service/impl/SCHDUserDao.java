package ru.bostonsd.clientmanagement.security.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.bpm.service.general.CamundaService;
import ru.bostonsd.clientmanagement.domain.models.Distributor;
import ru.bostonsd.clientmanagement.domain.repository.DistributorRepository;
import ru.bostonsd.clientmanagement.security.model.database.RoleEnum;
import ru.bostonsd.clientmanagement.security.model.database.SCHDRole;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUser;
import ru.bostonsd.clientmanagement.security.model.rest.BasicUpdateRequest;
import ru.bostonsd.clientmanagement.security.model.rest.FullUpdateRequest;
import ru.bostonsd.clientmanagement.security.model.rest.SignUpRequest;
import ru.bostonsd.clientmanagement.security.model.rest.jwt.JwtTokenClaims;
import ru.bostonsd.clientmanagement.security.repository.SCHDRoleRepository;
import ru.bostonsd.clientmanagement.security.repository.SCHDUserRepository;
import ru.bostonsd.clientmanagement.security.service.UserService;
import ru.bostonsd.clientmanagement.smtp.service.PreparatorSenderService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ru.bostonsd.clientmanagement.security.model.database.RoleEnum.*;

/**
 * Operations with users
 *
 * @author Sergey Petryaev (spetryaev@bostonsd.ru)
 * Created on December 12, 2019
 * Updated on March 1, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 *
 */
@Service
@Slf4j
public class SCHDUserDao implements UserService {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private SCHDUserRepository userRepository;

    @Autowired
    private SCHDRoleRepository roleRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private CamundaService camundaService;

    @Autowired
    private PreparatorSenderService preparatorSenderService;

    /**
     * READING OPERATIONS | END
     */
    @Override
    public Optional<SCHDUser> getById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public SCHDUser getByUserName(String userName) {
        return userRepository.findByUsername(userName);
    }

    @Override
    public List<SCHDUser> getByIsActive(Boolean isActive) {
        return userRepository.findByIsActive(isActive);
    }

    @Override
    public List<SCHDUser> getByDistributorId(Long distributorId, Integer page, Integer size) {
        if (distributorId != null) {
            Pageable pageable;
            if (page != null && size != null) {
                pageable = PageRequest.of(page, size);
            } else {
                pageable = PageRequest.of(0, 100);
            }
            return userRepository.findByDistributorId(distributorId, pageable);
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<SCHDUser> getAllUsersByDistributorIds(List<Long> distributorIds) {
        if (distributorIds != null && !distributorIds.isEmpty()) {
            return userRepository.findByDistributorIdIn(distributorIds);
        }
        return new ArrayList<>();
    }

    @Override
    public List<SCHDUser> getByRoleName(String roleName, Integer page, Integer size) {
        if (roleName != null && !roleName.isEmpty()) {
            Pageable pageable;
            if (page != null && size != null) {
                pageable = PageRequest.of(page, size);
            } else {
                pageable = PageRequest.of(0, 100);
            }
            return userRepository.findByRoleRoleName(roleName.toUpperCase(), pageable);
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<SCHDUser> getAllByRoleName(String roleName) {
        if (roleName != null && !roleName.isEmpty()) {
            return userRepository.findAllByRoleRoleName(roleName.toUpperCase());
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<SCHDUser> getAll() {
        return userRepository.findAll();
    }
    /**
     * READING OPERATIONS | END
     */

    /**
     * SAVING OPERATIONS | START
     */
    @Override
    public Long create(SignUpRequest signUpRequest) throws Exception {
        if (signUpRequest != null) {
            SCHDUser user = new SCHDUser();

            { /* USERNAME VALIDATION | START */
                String username = signUpRequest.getUsername();
                if (username != null) {
                    SCHDUser byUsername = userRepository.findByUsername(username);
                    if (byUsername != null) {
                        throw new Exception("User with such username exists already");
                    } else {
                        String regex = "^(.+)@(.+)$";
                        Pattern pattern = Pattern.compile(regex);
                        Matcher matcher = pattern.matcher(username);
                        if (matcher.matches()) {
                            user.setUsername(username);
                        } else {
                            throw new Exception("It's not an email address!");
                        }
                    }
                }
            } /* USERNAME VALIDATION | END */

            { /* FULL NAME VALIDATION | START */
                if (signUpRequest.getFullName() != null && !signUpRequest.getFullName().isEmpty()) {
                    user.setFullName(signUpRequest.getFullName());
                } else {
                    throw new Exception("Full name must not be null or empty");
                }
            } /* FULL NAME VALIDATION | END */

            { /* IS ACTIVE VALIDATION | START */
                if (signUpRequest.getIsActive() != null) {
                    user.setIsActive(signUpRequest.getIsActive());
                } else {
                    user.setIsActive(Boolean.FALSE);
                }
            } /* IS ACTIVE VALIDATION | END */

            { /* PASSWORD VALIDATION | START */
                String password = signUpRequest.getPassword();
                if (password != null && !password.isEmpty()) {
                    user.setPassword(encoder.encode(password));
                } else {
                    throw new Exception("Password must not be null or empty");
                }
            } /* PASSWORD VALIDATION | END */

            { /* ROLE VALIDATION | START */
                RoleEnum roleName = signUpRequest.getRole();
                if (roleName != null) {
                    SCHDRole role = roleRepository.findByRoleName(roleName.name());
                    if (role != null) {
                        user.setRole(role);
                    }
                } else {
                    throw new Exception("Role name must not be null or empty");
                }
            } /* ROLE VALIDATION | END */

            { /* DISTRIBUTOR VALIDATION | START */
                if (user.getRole().getRoleName().equals(ROLE_DISTRIBUTOR.name())) {
                    Long distributorId = signUpRequest.getDistributorId();
                    if (distributorId != null) {
                        // TODO discuss FNS check
                        Optional<Distributor> distributorOptional = distributorRepository.findById(distributorId);
                        if (distributorOptional.isPresent()) {
                            user.setDistributorId(distributorOptional.get().getId());
                            user.setDistributorName(distributorOptional.get().getName());
                        } else {
                            throw new Exception("Distributor is not found!");
                        }
                    } else {
                        throw new Exception("Distributor must not be null");
                    }
                }
            } /* DISTRIBUTOR VALIDATION | END */

            { /* FINAL ACTIONS */
                Date now = new Date();
                user.setCreatedDate(now);
                user.setLastModifiedDate(now);
                SCHDUser savedUser = userRepository.save(user);
                // CAMUNDA
                camundaService.createUserAndRole(new JwtTokenClaims(savedUser));
                // EMAIL NOTIFICATION
                preparatorSenderService.prepareSaveNewUser(signUpRequest);
                return savedUser.getId();
            }
        }
        return null;
    }

    /**
     * UPDATE OPERATIONS | START
     */
    @Override
    public boolean updateBasicUserData(Long userId, BasicUpdateRequest updateRequest) throws Exception {
        if (updateRequest != null && userId != null) {
            if (updateRequest.getFullName() == null || "".equals(updateRequest.getFullName())) {
                throw new Exception("Full name must not be null or empty.");
            }
            Optional<SCHDUser> user = userRepository.findById(userId);
            if (user.isPresent()) {
                SCHDUser oldUser = user.get();
                entityManager.detach(oldUser);
                SCHDUser result = new SCHDUser(oldUser);

                boolean validPassword;
                { /* PASSWORD VALIDATION | START */
                    if (updateRequest.getOldPassword() != null && !updateRequest.getOldPassword().isEmpty()
                            && updateRequest.getNewPassword() != null && !updateRequest.getNewPassword().isEmpty()) {
                        // match encrypted old passwords
                        validPassword = encoder.matches(updateRequest.getOldPassword(), oldUser.getPassword());
                    } else {
                        throw new Exception("Password(-s) must not be null or empty.");
                    }
                } /* PASSWORD VALIDATION | END */

                if (validPassword) {
                    result.setPassword(encoder.encode(updateRequest.getNewPassword()));
                    result.setFullName(updateRequest.getFullName());

                    { /* FINAL ACTIONS */
                        result.setLastModifiedDate(new Date());
                        userRepository.save(result);
                        // EMAIL NOTIFICATION
                        preparatorSenderService.prepareUpdateUsersPassword(oldUser, updateRequest);
                        return true;
                    }
                } else {
                    throw new Exception("Old password is incorrect. Unable to perform update.");
                }
            } else {
                throw new Exception("User is not found.");
            }
        } else {
            throw new Exception("Null data input. Unable to perform update.");
        }
    }

    @Override
    public boolean updateUserPassword(Long userId, BasicUpdateRequest updateRequest) throws Exception {
        if (updateRequest != null && userId != null) {
            Optional<SCHDUser> user = userRepository.findById(userId);
            if (user.isPresent()) {
                SCHDUser oldUser = user.get();
                entityManager.detach(oldUser);
                SCHDUser result = new SCHDUser(oldUser);
                    result.setPassword(encoder.encode(updateRequest.getNewPassword()));
                /* NEW PASSWORD VALIDATION */
                if (updateRequest.getNewPassword() != null && !updateRequest.getNewPassword().isEmpty()) {
                    /* FINAL ACTIONS */
                    result.setLastModifiedDate(new Date());
                    userRepository.save(result);
                    return true;
                } else {
                    throw new Exception("Password(-s) must not be null or empty.");
                }
            } else {
                throw new Exception("User is not found.");
            }
        } else {
            throw new Exception("Null data input. Unable to perform update.");
        }
    }

    @Override
    public boolean updateFullUserData(Long userId, FullUpdateRequest updateRequest) throws Exception {
        Optional<SCHDUser> user = userRepository.findById(userId);
        if (user.isPresent()) {
            SCHDUser oldUser = user.get();
            entityManager.detach(oldUser); // makes the old record not dependent of further updates
            SCHDUser result = new SCHDUser(oldUser);

            { /* FULL NAME UPDATE | START */
                if (updateRequest.getFullName() != null && !updateRequest.getFullName().isEmpty()) {
                    result.setFullName(updateRequest.getFullName());
                } else {
                    throw new Exception("Full name must be provided");
                }
            } /* FULL NAME UPDATE | END */

            { /* ROLE UPDATE | START */
                if (updateRequest.getRole() != null && !updateRequest.getRole().toString().isEmpty()) {
                    SCHDRole role;
                    role = roleRepository.findByRoleName(updateRequest.getRole().name());
                    if (role != null) {
                        result.setRole(role);
                    } else {
                        throw new Exception("There is no role with such name.");
                    }
                } else {
                    throw new Exception("Role name must be provided");
                }
            } /* ROLE UPDATE | END*/

            { /* DISTRIBUTOR INFO UPDATE | START */
                if (ROLE_ADMIN.name().equals(result.getRole().getRoleName())) {
                    // admin cannot be linked with a distributor
                    result.setDistributorId(null);
                    result.setDistributorName(null);
                }

                if (ROLE_GOLDEN_DEALER.name().equals(result.getRole().getRoleName())) {
                    // admin cannot be linked with a distributor
                    result.setDistributorId(null);
                    result.setDistributorName(null);
                }

                if (ROLE_DISTRIBUTOR.name().equals(result.getRole().getRoleName())) {
                    if (updateRequest.getDistributorId() == null) {
                        throw new Exception("Distributor ID must be provided");
                    }
                    Optional<Distributor> distributor = distributorRepository.findById(updateRequest.getDistributorId());
                    if (distributor.isPresent()) {
                        result.setDistributorId(distributor.get().getId());
                        result.setDistributorName(distributor.get().getName());
                    } else {
                        throw new Exception("There is no distributor with such id.");
                    }
                }
            } /* DISTRIBUTOR INFO UPDATE | END */

            { /* PASSWORD UPDATE | START */
                if (updateRequest.getPassword() != null && !updateRequest.getPassword().isEmpty()) {
                    result.setPassword(encoder.encode(updateRequest.getPassword()));
                } else {
                    // set old password if new is not provided
                    result.setPassword(oldUser.getPassword());
                }
            } /* PASSWORD UPDATE | END */

            { /* IS ACTIVE UPDATE | START */
                if (updateRequest.getIsActive() != null) {
                    result.setIsActive(updateRequest.getIsActive());
                } else {
                    result.setIsActive(Boolean.FALSE);
                }
            } /* IS ACTIVE UPDATE | END */

            { /* FINAL ACTIONS */
                result.setLastModifiedDate(new Date());
                userRepository.save(result);
                // EMAIL NOTIFICATION
                preparatorSenderService.prepareUpdateUsersPassword(oldUser, updateRequest);
                return true;
            }
        } else { // if user is not found
            throw new Exception("There is no user with such id.");
        }
    }
    /** UPDATE OPERATIONS | END */

    /**
     * DELETING OPERATIONS | START
     */
    @Override
    public boolean delete(Long userId) throws Exception {
        Optional<SCHDUser> user = userRepository.findById(userId);
        if (user.isPresent()) {
            userRepository.delete(user.get());
            return true;
        }
        throw new Exception("There is no user with such id");
    }

}
