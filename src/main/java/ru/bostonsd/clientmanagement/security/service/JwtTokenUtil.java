package ru.bostonsd.clientmanagement.security.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import ru.bostonsd.clientmanagement.security.exception.CustomSecurityException;
import ru.bostonsd.clientmanagement.security.model.rest.jwt.JwtTokenClaims;

import java.util.Date;

/**
 * The JwtTokenUtil is responsible for performing JWT operations like creation and validation.
 * Use this class if you want to extract data from a token
 *
 * @author Sergey Petryaev (spetryaev@bostonsd.ru)
 * Created on December 3, 2019
 */
@Slf4j
public class JwtTokenUtil {


    /**
     * Get user claims from a JWT token as a DTO
     *
     * @param token jwt token
     * @return user info
     */
    public static JwtTokenClaims getUserClaims(String token){
        if(token != null){
            try{
                String jwt = extractTokenFromRequest(token);
                Claims claims = getClaimsFromToken(jwt);
                return JwtTokenClaims.builder()
                        .sub(claims.get("sub", Long.class))
                        .username(claims.get("username", String.class))
                        .roleName(claims.get("roleName", String.class))
                        .fullName(claims.get("fullName", String.class))
                        .distributorId(claims.get("distributorId", Long.class))
                        .distributorName(claims.get("distributorName", String.class))
                        .build();
            } catch (Exception e){
                log.error(e.getMessage());
            }
        }
        return null;
    }

    /**
     * Retrieve a username from a jwt token
     *
     * @param token must not be null
     * @return a username related to the token
     */
    public static String getUsernameFromToken(String token) {
        if (token != null) {
            String jwt = extractTokenFromRequest(token);
            return getClaimsFromToken(jwt).get("username", String.class);
        } else {
            throw new IllegalArgumentException("Unable to get a token");
        }
    }

    /**
     * Retrieve a role from a jwt token
     *
     * @param token must not be null
     * @return a username related to the token
     */
    public static String getRoleFromToken(String token) {
        if (token != null) {
            String jwt = extractTokenFromRequest(token);
            return getClaimsFromToken(jwt).get("roleName", String.class);
        } else {
            throw new IllegalArgumentException("Unable to get a token");
        }
    }

    /**
     * Retrieve a distributor id from a jwt token
     *
     * @param token must not be null
     * @return a username related to the token
     */
    public static Long getDistributorId(String token) {
        if (token != null) {
            String jwt = extractTokenFromRequest(token);
            return getClaimsFromToken(jwt).get("distributorId", Long.class);
        } else {
            throw new IllegalArgumentException("Unable to get a token");
        }
    }

    /**
     * Retrieve an expiration date from a jwt token
     *
     * @param token must not be null
     * @return an expiration date of the token
     */
    public static Date getExpirationDateFromToken(String token) {
        if (token != null) {
            String jwt = extractTokenFromRequest(token);
            return getClaimsFromToken(jwt).get("exp", Date.class);
        } else {
            throw new IllegalArgumentException("Unable to get a token");
        }
    }


    /**
     * retrieve information from a token
     *
     * @param token must not be null
     * @return all claims contained in the token
     */
    private static Claims getClaimsFromToken(String token) {
        if (token != null) {
            try{
                return Jwts.parser().setSigningKey(JwtTokenService.secret).parseClaimsJws(token).getBody();
            } catch (Exception e){
                throw new CustomSecurityException("Message: " + e.getMessage());
            }
        } else {
           return null;
        }
    }

    /**
     * Extract a token from the Authorization request header
     *
     * @param jwt token
     * @return token without the "Bearer " prefix
     */
    public static String extractTokenFromRequest(String jwt) {
        if (jwt != null && jwt.startsWith("Bearer ")) {
            return jwt.substring(7);
        }
        return jwt;
    }

}
