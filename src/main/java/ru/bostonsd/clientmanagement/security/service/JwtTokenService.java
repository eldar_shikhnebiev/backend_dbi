package ru.bostonsd.clientmanagement.security.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.security.exception.CustomSecurityException;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUserDetails;
import ru.bostonsd.clientmanagement.security.model.rest.jwt.JwtTokenClaims;
import ru.bostonsd.clientmanagement.security.service.impl.SCHDUserDetailsService;

import java.util.Date;
import java.util.Map;

/**
 * The JwtTokenService provides operations related to JWT
 *
 * @author Sergey Petryaev (spetryaev@bostonsd.ru)
 * Created on December 3, 2019
 */
@Service
@Slf4j
public class JwtTokenService {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private SCHDUserDetailsService userDetailsService;

    private static Long tokenExpireLength; // in milliseconds

    static String secret;

    @Value("${security.jwt.token.secret-key}")
    private void setSecret(String secret) {
        JwtTokenService.secret = secret;
    }

    @Value("${security.jwt.token.expire-length:3600000}")
    private void setTokenExpireLength(Long tokenExpireLength) {
        JwtTokenService.tokenExpireLength = tokenExpireLength;
    }

    /**
     * Generate a JWT token.
     *
     * @param userDetails must not be null
     * @return a token
     */
    public String generateToken(SCHDUserDetails userDetails) {
        if (userDetails != null) {
            return Jwts.builder()
                    .setHeaderParam("typ", "JWT")
                    .setClaims(objectMapper.convertValue(new JwtTokenClaims(userDetails), Map.class))
                    .setIssuedAt(new Date(System.currentTimeMillis()))
                    .setExpiration(new Date(System.currentTimeMillis() + tokenExpireLength))
                    .signWith(SignatureAlgorithm.HS512, secret).compact();
        } else {
            return "INVALID_USER_DETAILS";
        }
    }

    /**
     * Validate a JWT token structure.
     *
     * @param token must not be null
     * @return true if token is valid
     */
    public Boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
            return true;
        } catch (Exception e) {
            //log.error("Token parsing error: " + e.getMessage());
            return false;
        }
    }

    /*
     * Check if a token has been expired.
     */
    public Boolean isTokenExpired(String token) {
        if (token != null) {
            final Date exp = JwtTokenUtil.getExpirationDateFromToken(token);
            return exp.before(new Date());
        } else {
            throw new IllegalArgumentException("Unable to get a token");
        }
    }

    /*
     * Get an authentication object by a token.
     */
    public Authentication getAuthenticationByToken(String token) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(JwtTokenUtil.getUsernameFromToken(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

}
