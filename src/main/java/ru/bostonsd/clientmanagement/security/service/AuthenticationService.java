package ru.bostonsd.clientmanagement.security.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.bpm.service.general.CamundaService;
import ru.bostonsd.clientmanagement.security.exception.CustomSecurityException;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUserDetails;
import ru.bostonsd.clientmanagement.security.service.impl.SCHDUserDetailsService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * The AuthenticationService provides log-in/log-out operations.
 *
 * @author Sergey Petryaev (spetryaev@bostonsd.ru)
 * Created on December 3, 2019
 */
@Service
@Slf4j
public class AuthenticationService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Autowired
    private SCHDUserDetailsService userDetailsService;

    @Autowired
    private CamundaService camundaService;

    /**
     * THIS IS A KEY ELEMENT OF THE AUTHENTICATION - DO NOT REMOVE OR CHANGE ACCESS MODIFIER!!!
     * This map contains authenticated users and their tokens
     * Therefore, there's could be only one session of only one user with only one token at the same time
     */
    public static Map<String, String> authenticatedUsers = new HashMap<>();

    /**
     * Authenticate a user by his/her credentials - username and password
     * <p>
     * If the Authentication Manager authenticated the user successfully then JwtTokenService generates an
     * access token and stores it in the authenticatedUsers map.
     *
     * @param username must be filled
     * @param password must be filled
     * @return a JWT Token with packaged info about a user
     * @throws Exception if a user is disabled or bad credentials were provided
     */
    public String authenticateByCredentials(String username, String password) throws Exception {
        try {
            log.error("User for authenticated :" + username);
            Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            log.info("Authenticated is credentials: " + authenticate.isAuthenticated());
            if (authenticate.isAuthenticated()) {
                String token = jwtTokenService.generateToken((SCHDUserDetails) authenticate.getPrincipal());
                authenticatedUsers.put(username, token);
                /* CAMUNDA */
                camundaService.createUserAndRole(JwtTokenUtil.getUserClaims(token));
                return token;
            } else {
                return null;
            }
        } catch (DisabledException e) {
            log.info("User is disabled: " + username);
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            log.info("Invalid credentials: " + username);
            throw new Exception("INVALID_CREDENTIALS " + username, e);
        }
    }

    /**
     * Authenticate by a request with a JWT token in the Authorization header
     * <p>
     * This method verifies validity of the token and then checks presence of this token in the authenticatedUsers map
     */
    public void authenticateByTokenRequest(String token) {
        if(token != null){
            String username = JwtTokenUtil.getUsernameFromToken(token);
            Authentication auth = null;
            if(AuthenticationService.authenticatedUsers.containsKey(username)){
                String storedToken = AuthenticationService.authenticatedUsers.get(username);
                if (jwtTokenService.validateToken(token)) {
                    if(token.equals(storedToken)){
                        if(!jwtTokenService.isTokenExpired(token)){
                            auth = jwtTokenService.getAuthenticationByToken(token);
                        }
                    } else {
                        throw new CustomSecurityException("Provided and stored tokens do not match. " +
                                "Perhaps someone logged in by your credentials");
                    }
                }
            }
            if(auth != null){
                if(auth.isAuthenticated()){
                    SecurityContextHolder.getContext().setAuthentication(auth);
                    //log.info("Authenticated by a token request: " + username);
                }
            }
        } // else do nothing
    }

    /**
     * Logs out a user out of the system (security context and jwt token storage)
     */
    public boolean logoutByTokenRequest(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        String token = JwtTokenUtil.extractTokenFromRequest(
                request.getHeader("Authorization")
        );
        if (token != null) {
            String username = JwtTokenUtil.getUsernameFromToken(token);
            if (token.equals(authenticatedUsers.get(username))) {
                authenticatedUsers.remove(username);
                if (authentication != null) {
                    new SecurityContextLogoutHandler().logout(request, response, authentication);
                }
                //log.info("Logged out: " + username);
                return true;
            } else {
                //log.info("Provided and stored tokens don't match");
            }
        } else {
            //log.warn("JWT Token is missing or doesn't start with a Bearer String.");
        }
        return false;
    }

}
