package ru.bostonsd.clientmanagement.security.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bostonsd.clientmanagement.security.exception.CustomSecurityException;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUser;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUserPasswordRecovery;
import ru.bostonsd.clientmanagement.security.model.rest.BasicUpdateRequest;
import ru.bostonsd.clientmanagement.security.model.rest.RecoveryPassword;
import ru.bostonsd.clientmanagement.security.repository.SCHDRecoveryRepository;
import ru.bostonsd.clientmanagement.security.service.RecoveryPasswordService;
import ru.bostonsd.clientmanagement.smtp.service.SmtpService;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * Implementation for {@link RecoveryPasswordService}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on February 5, 2020
 * Updated on February 7, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 */

@Slf4j
@Service
public class RecoveryPasswordServiceImpl implements RecoveryPasswordService {

    @Autowired
    private SCHDUserDao userDao;

    @Autowired
    private SCHDRecoveryRepository recoveryRepository;

    @Autowired
    private SmtpService smtpService;

    @Value("${recovery.time}")
    private Long expirationDateCodeMail;

    @Override
    public boolean sendMail(String email, String uriCheck) {
        final String FORMAT_MAIL_BODY =  "<a href='%s?code=%s'>Ссылка на страницу.</a><br>" +
                "Активна в течение 24 часов с момента отправки.";
        final String MAIL_SUBJECT = "Schneider electric. Восставление пароля";

        SCHDUser user = userDao.getByUserName(email);
        if (user == null) {
            throw new CustomSecurityException("Пользователь " + email + " не зарегистрирован в приложении");
        }
        String codeRecovery = UUID.randomUUID().toString();
        SCHDUserPasswordRecovery userPasswordRecovery = SCHDUserPasswordRecovery.builder()
                                                                .username(email)
                                                                .uuidCode(codeRecovery)
                                                                .dateRegistration(new Date().getTime())
                                                                .build();
        recoveryRepository.save(userPasswordRecovery);
        String bodyMail = String.format(FORMAT_MAIL_BODY, uriCheck, codeRecovery);
        smtpService.sendMessage(email, bodyMail, MAIL_SUBJECT);
        return true;
    }

    @Override
    public String recovery(String code) {
        SCHDUserPasswordRecovery userPasswordRecovery = recoveryRepository.findByUuidCode(code);
        if(userPasswordRecovery != null) {
            Long dateNow = new Date().getTime();
            long deltaTime = dateNow - userPasswordRecovery.getDateRegistration();
            if (deltaTime < expirationDateCodeMail ) {
                String codeRecoveryNew = UUID.randomUUID().toString();
                userPasswordRecovery.setUuidCode(codeRecoveryNew);
                recoveryRepository.save(userPasswordRecovery);
                return codeRecoveryNew;
            } else {
                log.warn("Time for recovery password over " + deltaTime );
            }
        }  else {
            log.warn("Not found");
        }
        return null;
    }

    @Override
    public boolean updatePassword(RecoveryPassword password) {
        if (Objects.equals(password.getPassword(), password.getPasswordConfirm())) {
            SCHDUserPasswordRecovery userPasswordRecovery = recoveryRepository.findByUuidCode(password.getCode());
            if (userPasswordRecovery != null) {
                recoveryRepository.delete(userPasswordRecovery);
                Long dateNow = new Date().getTime();
                long deltaTime = dateNow - userPasswordRecovery.getDateRegistration();
                log.info("Delta time seconds " + deltaTime / 1000);
                if (deltaTime < expirationDateCodeMail) {
                    SCHDUser user = userDao.getByUserName(userPasswordRecovery.getUsername());
                    BasicUpdateRequest basicUpdateRequest = BasicUpdateRequest.builder()
                            .fullName(user.getFullName())
                            .oldPassword(null)
                            .newPassword(password.getPassword())
                            .build();
                    try {
                        return userDao.updateUserPassword(user.getId(), basicUpdateRequest);
                    } catch (Exception e) {
                        e.printStackTrace();
                        throw new CustomSecurityException(e.getMessage());
                    }
                } else {
                    throw new CustomSecurityException("Время для восстановления пароля истекло");
                }
            } else {
                throw new CustomSecurityException("Присланные для восстановления пароля данные не корректны");
            }
        } else {
            throw new CustomSecurityException("Введенные пароли не совпадают");
        }
    }

    @Scheduled(cron = "0 0 1 * * *")
    @Transactional
    public void deleteOldRecoveryData() {
        log.info("Scheduled task by delete old records of recovery password");
        log.info("-----------START----------------");
        Date dateNow = new Date();
        Long dateDelete = dateNow.getTime() - expirationDateCodeMail;
        int result = recoveryRepository.deleteAllWithDateRegistrationBefore(dateDelete);
        if (result == 0 ) {
            log.info("Records in DB for delete not found");
        } else {
            log.info("Found and deleted " + result + " records in DB");
        }
        log.info("-----------FINISH----------------");
    }
}
