package ru.bostonsd.clientmanagement.security.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.domain.repository.DistributorRepository;
import ru.bostonsd.clientmanagement.security.model.database.RoleEnum;
import ru.bostonsd.clientmanagement.security.model.database.SCHDRole;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUser;
import ru.bostonsd.clientmanagement.security.model.database.SCHDUserDetails;
import ru.bostonsd.clientmanagement.security.repository.SCHDRoleRepository;
import ru.bostonsd.clientmanagement.security.repository.SCHDUserRepository;

import javax.annotation.PostConstruct;
import java.util.Date;

/**
 * The JwtUserDetailsService provides UserDetails of a user.
 *
 * @author Sergey Petryaev (spetryaev@bostonsd.ru)
 * Created on December 3, 2019
 * Updated on December 12, 2019 | spetryaev
 */
@Service
@Slf4j
public class SCHDUserDetailsService implements UserDetailsService {

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private SCHDRoleRepository roleRepository;

    @Autowired
    private SCHDUserRepository userRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SCHDUser user = userRepository.findByUsername(username);
        if (user != null){
            return SCHDUserDetails.create(user);
        } else {
            log.info("User with username \"" + username + "\" is not found");
            throw new UsernameNotFoundException("User with username \"" + username + "\" is not found");
        }
    }

    /*
    INITIAL DATA
     */
    @PostConstruct
    private void initRolesAndUsers(){
        createRolesAndAdminUser();
    }

    private void createRolesAndAdminUser(){
        // admin role
        SCHDRole adminRole = roleRepository.findByRoleName(RoleEnum.ROLE_ADMIN.name());
        if(adminRole == null){
            adminRole = roleRepository.save(new SCHDRole(null, RoleEnum.ROLE_ADMIN.name(), "Administrator of the system"));
        }
        // distributor role
        SCHDRole distributorRole = roleRepository.findByRoleName(RoleEnum.ROLE_DISTRIBUTOR.name());
        if(distributorRole == null){
            roleRepository.save(new SCHDRole(null, RoleEnum.ROLE_DISTRIBUTOR.name(), "Distributor of the system"));
        }

        // admin user
        SCHDUser adminUser = userRepository.findByUsername("admin@example.com");
        if(adminUser == null){
            adminUser = new SCHDUser();
            adminUser.setUsername("admin@example.com");
            adminUser.setPassword(encoder.encode("admin"));
            adminUser.setFullName("Администратор");
            adminUser.setIsActive(true);
            adminUser.setRole(adminRole);
            Date now = new Date();
            adminUser.setCreatedDate(now);
            adminUser.setLastModifiedDate(now);
            SCHDUser savedAdmin = userRepository.save(adminUser);
            if(savedAdmin != null && savedAdmin.getId() != null){
                log.info("Created mock user with role ROLE_ADMIN");
            }
        }
    }


}
