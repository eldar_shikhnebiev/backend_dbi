package ru.bostonsd.clientmanagement.security.service;

import ru.bostonsd.clientmanagement.security.model.database.SCHDRole;

import java.util.List;
import java.util.Optional;

public interface RoleService {

    Optional<SCHDRole> getById(Long id);

    SCHDRole getRoleByName(String roleName);

    List<String> getAllRoleNames();

    List<SCHDRole> getAll();

    Long create(SCHDRole role);

    boolean update(SCHDRole role) throws Exception;

    boolean delete(Long id);
}
