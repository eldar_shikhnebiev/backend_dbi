package ru.bostonsd.clientmanagement.security.service;

import ru.bostonsd.clientmanagement.security.model.database.SCHDUser;
import ru.bostonsd.clientmanagement.security.model.rest.BasicUpdateRequest;
import ru.bostonsd.clientmanagement.security.model.rest.FullUpdateRequest;
import ru.bostonsd.clientmanagement.security.model.rest.SignUpRequest;

import java.util.List;
import java.util.Optional;

/**
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on  December 11, 2019
 */
public interface UserService {

    Optional<SCHDUser> getById(Long id);

    SCHDUser getByUserName(String userName);

    List<SCHDUser> getByIsActive(Boolean isActive);

    List<SCHDUser> getByDistributorId(Long distributorId, Integer page, Integer size);

    List<SCHDUser> getAllUsersByDistributorIds(List<Long> distributorIds);

    List<SCHDUser> getByRoleName(String roleName, Integer page, Integer size);

    List<SCHDUser> getAllByRoleName(String roleName);

    List<SCHDUser> getAll();

    Long create(SignUpRequest signUpRequest) throws Exception;

    boolean updateBasicUserData(Long userId, BasicUpdateRequest basicUpdateRequest) throws Exception;

    boolean updateFullUserData(Long userId, FullUpdateRequest updateRequest) throws Exception;

    boolean updateUserPassword(Long userId, BasicUpdateRequest updateRequest) throws Exception;

    boolean delete(Long userId) throws Exception;
}
