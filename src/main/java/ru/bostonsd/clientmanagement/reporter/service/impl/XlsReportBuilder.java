package ru.bostonsd.clientmanagement.reporter.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.domain.models.dto.*;
import ru.bostonsd.clientmanagement.domain.service.ClientService;
import ru.bostonsd.clientmanagement.domain.service.DiscountCollectionService;
import ru.bostonsd.clientmanagement.domain.service.DistributorService;
import ru.bostonsd.clientmanagement.integration.model.ClientFns;
import ru.bostonsd.clientmanagement.integration.service.SearchClientIntegration;
import ru.bostonsd.clientmanagement.reporter.model.AggregateReportEntity;
import ru.bostonsd.clientmanagement.reporter.service.ReportBuilder;
import ru.bostonsd.clientmanagement.security.model.database.RoleEnum;
import ru.bostonsd.clientmanagement.security.service.JwtTokenUtil;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation for {@link ReportBuilder}
 * <p>
 * Service creating a reports in xls format.
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 10, 2019
 * Updated on March 4, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 */
@Service
@Slf4j
@PropertySource("classpath:reporter.properties")
public class XlsReportBuilder implements ReportBuilder {

    @Autowired
    private ClientService clientService;

    @Autowired
    private DistributorService distributorService;

    @Autowired
    private DiscountCollectionService discountCollectionService;

    @Autowired
    private SearchClientIntegration searchClientIntegration;

    @Autowired
    private Environment env;

    private final String COLUMN_NAME_PREFIX = "report.name.for.column.";

    private final String ROLE_NAME_PATH = "report.column.%s.name";

    private final String ROLE_SIZE_PATH = "report.column.%s.size";

    //limit of time for requests to DaData.ru - 1 hour
    private final long timeLimitRequest = 3600000L;

    /**
     * Creating a report on clients and distributors in xls
     * format based on the role of the authorized user
     * <p>
     * If the role from enum entity {@link RoleEnum} is
     * - ROLE_DISTRIBUTOR - data for the report is selected for clients who are associated
     * with the distributor from which the user is authorized
     * - ROLE_ADMIN, data for all clients is added to the report
     *
     * @param jwt -  token received by an authorized user
     * @return xls file in byte array with data about clients and distributors
     */
    @Override
    public ByteArrayInputStream buildReport(String jwt) {
        String role = JwtTokenUtil.getRoleFromToken(jwt);
        String roleName = String.format(ROLE_NAME_PATH, role);
        String roleSize = String.format(ROLE_SIZE_PATH, role);

        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Clients");

        String columnsNumberString = env.getProperty(roleName);
        String columnsSizeString = env.getProperty(roleSize);

        String[] columnsNumberStringArray = columnsNumberString.split(",");
        List<Integer> columnsSize = Arrays.asList(columnsSizeString.split(",")).stream()
                .map(e -> Integer.valueOf(e)).collect(Collectors.toList());


        for (int i = 0; i < columnsSize.size(); i++) {
            sheet.setColumnWidth(i, columnsSize.get(i));
        }

        Row header = sheet.createRow(0);

        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        headerStyle.setFont(font);

        // Add cells
        createRowHeader(header, headerStyle, columnsNumberStringArray);

        //Add data

        AggregateReportEntity aggregateReportEntity = new AggregateReportEntity();
        aggregateReportEntity.setCollections(discountCollectionService.getAllList());
        if (RoleEnum.ROLE_DISTRIBUTOR.name().equals(role)) {
            Long distributorId = JwtTokenUtil.getDistributorId(jwt);
            aggregateReportEntity.setClients(clientService.getListByDistributorId(distributorId, true));
            DistributorDto distributorDto = distributorService.getDistributorDtoById(distributorId);
            aggregateReportEntity.setDistributors(Collections.singletonMap(distributorDto.getId(), distributorDto));
            writeValueDistributor(aggregateReportEntity, workbook, sheet);
        } else if (RoleEnum.ROLE_ADMIN.name().equals(role)) {
            aggregateReportEntity.setClients(clientService.getAllList());
            List<DistributorDto> distributorDtoList = distributorService.getAllList();
            Map<Long, DistributorDto> distributorDtoMap = distributorDtoList.stream()
                    .collect(Collectors.toMap(DistributorDto::getId, distributorDto -> distributorDto));
            aggregateReportEntity.setDistributors(distributorDtoMap);
            writeValueTotal(aggregateReportEntity, workbook, sheet);
        }

        ByteArrayOutputStream outputStream;
        try {
            outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            workbook.close();
            return new ByteArrayInputStream(outputStream.toByteArray());
        } catch (Exception e) {
            log.error("FILE BUILD ERROR: {}", e.getMessage());
        }
        return null;
    }

    /**
     * Write an instance of an class {@link AggregateReportEntity} to a specified workbook and worksheet
     * - data of distributor
     * - data of client linked with distibutor
     * - data collection
     *
     * @param aggEntity - instance of class {@link AggregateReportEntity} for write to sheet workbook
     * @param workbook  - instance of entity {@link Workbook} for write data
     * @param sheet     -  instance of entity {@link Sheet} in workbook for write data
     */
    private void writeValueTotal(AggregateReportEntity aggEntity, Workbook workbook, Sheet sheet) {
        CellStyle style = workbook.createCellStyle();
        style.setWrapText(true);

        int clientCounter = 0;
        int rowCounter = 1;

        for (int i = 0; i < aggEntity.getClients().size(); i++) {
            for (SalesVolumeClientDto distributorId : aggEntity.getClients().get(clientCounter).getSalesVolume()) {

                DistributorDto distributor = aggEntity.getDistributors().get(distributorId.getIdDistributor());
                ClientDto client = aggEntity.getClients().get(clientCounter);
                if (client.getClientLinks().isEmpty()) {
                    createRowValueTotal(aggEntity, sheet, style, rowCounter, distributor, client, null);
                    rowCounter++;
                } else {
                    for (ClientLinkedDto clientLink : client.getClientLinks()) {
                        createRowValueTotal(aggEntity, sheet, style, rowCounter, distributor, client, clientLink);
                        rowCounter++;
                    }
                }
            }
            clientCounter++;
        }
    }

    /**
     * Write an instance of an class {@link AggregateReportEntity} to a specified workbook and worksheet
     * for role Distributor
     * - data of client linked with distibutor
     * - data collection
     *
     * @param aggEntity - instance of class {@link AggregateReportEntity} for write to sheet workbook
     * @param workbook  - instance of entity {@link Workbook} for write data
     * @param sheet     -  instance of entity {@link Sheet} in workbook for write data
     */

    private void writeValueDistributor(AggregateReportEntity aggEntity, Workbook workbook, Sheet sheet) {
        CellStyle style = workbook.createCellStyle();
        style.setWrapText(true);

        int clientCounter = 0;
        int rowCounter = 1;

        for (int i = 0; i < aggEntity.getClients().size(); i++) {
            for (SalesVolumeClientDto distributorId : aggEntity.getClients().get(clientCounter).getSalesVolume()) {
                
                Row row = sheet.createRow(rowCounter);
                DistributorDto distributor = aggEntity.getDistributors().get(distributorId.getIdDistributor());
                ClientDto client = aggEntity.getClients().get(clientCounter);

                Cell cell = row.createCell(0);
                cell.setCellValue(distributor.getName());
                cell.setCellStyle(style);

                cell = row.createCell(1);
                cell.setCellValue(distributor.getInn());
                cell.setCellStyle(style);

                cell = row.createCell(2);
                cell.setCellValue(client.getName());
                cell.setCellStyle(style);

                cell = row.createCell(3);
                cell.setCellValue(client.getInn());
                cell.setCellStyle(style);

                cell = row.createCell(4);
                cell.setCellValue(this.getTextActive(client.isActive()));
                cell.setCellStyle(style);

                cell = row.createCell(5);
                cell.setCellValue(client.getAddress());
                cell.setCellStyle(style);

                cell = row.createCell(6);
                cell.setCellValue(client.getSite());
                cell.setCellStyle(style);

                cell = row.createCell(7);
                cell.setCellValue(client.getClientTypeString());
                cell.setCellStyle(style);

                cell = row.createCell(8);
                cell.setCellValue(client.getClientClassificationString());
                cell.setCellStyle(style);

                String namePropertyCurrent;
                for (int c = 9; c < 17; c++) {
                    cell = row.createCell(c);
                    namePropertyCurrent = COLUMN_NAME_PREFIX + (c + 5);
                    cell.setCellValue(getSellByName(aggEntity.getCollections(),
                            env.getProperty(namePropertyCurrent),
                            client.getClientTypeString()));
                    cell.setCellStyle(style);
                }
                rowCounter++;
            }
            clientCounter++;
        }
    }

    private void createRowValueTotal(AggregateReportEntity aggEntity, Sheet sheet, CellStyle style, int rowCounter, DistributorDto distributor, ClientDto client, ClientLinkedDto clientLink) {
        Row row = sheet.createRow(rowCounter);

        Cell cell = row.createCell(0);
        createFirstColumn(style, distributor, client, row, cell);

        if (clientLink != null) {
            cell = row.createCell(9);
            cell.setCellValue(clientLink.getInn());
            cell.setCellStyle(style);

            cell = row.createCell(10);
            cell.setCellValue(clientLink.getName());
            cell.setCellStyle(style);
        }

        cell = row.createCell(11);
        cell.setCellValue(getTextDate(client.getDate()));
        cell.setCellStyle(style);

        cell = row.createCell(12);
        cell.setCellValue(client.getClientTypeString());
        cell.setCellStyle(style);

        cell = row.createCell(13);
        cell.setCellValue(client.getClientClassificationString());
        cell.setCellStyle(style);

        String namePropertyCurrent;
        for (int c = 14; c < 22; c++) {
            cell = row.createCell(c);
            namePropertyCurrent = COLUMN_NAME_PREFIX + (c);
            cell.setCellValue(getSellByName(aggEntity.getCollections(),
                    env.getProperty(namePropertyCurrent),
                    client.getClientTypeString()));
            cell.setCellStyle(style);
        }
    }

    private void createFirstColumn(CellStyle style, DistributorDto distributor, ClientDto client, Row row, Cell cell) {
        cell.setCellValue(distributor.getName());
        cell.setCellStyle(style);

        cell = row.createCell(1);
        cell.setCellValue(distributor.getInn());
        cell.setCellStyle(style);

        cell = row.createCell(2);
        cell.setCellValue(client.getName());
        cell.setCellStyle(style);

        cell = row.createCell(3);
        cell.setCellValue(client.getInn());
        cell.setCellStyle(style);

        cell = row.createCell(4);
        cell.setCellValue(this.getTextActive(client.isActive()));
        cell.setCellStyle(style);

        cell = row.createCell(5);
        cell.setCellValue(client.getEmail());
        cell.setCellStyle(style);

        cell = row.createCell(6);
        cell.setCellValue(client.getAddress());
        cell.setCellStyle(style);

        cell = row.createCell(7);
        cell.setCellValue(client.getSite());
        cell.setCellStyle(style);

        cell = row.createCell(8);
        cell.setCellValue(client.getPhone());
        cell.setCellStyle(style);
    }

    /**
     * Creating a header in an Excel table
     *
     * @param rowHeader       - the line to which the title is added
     * @param cellStyleHeader - specified header style
     */
    private void createRowHeader(Row rowHeader, CellStyle cellStyleHeader, String[] columnsNumber) {
        String columnName;
        for (int i = 0; i < columnsNumber.length; i++) {
            Cell headerCell = rowHeader.createCell(i);
            columnName = COLUMN_NAME_PREFIX.concat(String.valueOf(columnsNumber[i]));
            headerCell.setCellValue(env.getProperty(columnName));
            headerCell.setCellStyle(cellStyleHeader);
        }
    }

    /**
     * Converting a date in long format to string format with a specified pattern
     *
     * @param date - date in long format
     * @return date in string format with the specified pattern
     */
    private String getTextDate(Long date) {
        Date d = new Date(date);
        SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yyyy");
        return df2.format(d);
    }

    /**
     * Converting value in boolean format to string format
     *
     * @param isActive - active in boolean format
     * @return value in string format
     */
    private String getTextActive(Boolean isActive) {
        if (isActive) {
            return "Активен";
        } else {
            return "Деактивирован";
        }
    }

    /**
     * Search the collections list by collection name and client type discount size
     *
     * @param collections      - a collection дшые containing discount data
     * @param collectionName   - the name of the collection to search
     * @param clientTypeString - client type to search for discount size
     * @return - discount size. If not found, returned zero
     */
    private double getSellByName(List<DiscountCollectionDto> collections, String collectionName, String clientTypeString) {
        for (DiscountCollectionDto collectionDto : collections) {
            if (collectionDto.getName().equals(collectionName)) {
                switch (clientTypeString) {
                    case "Бронзовый":
                        return collectionDto.getBronze();
                    case "Серебряный":
                        return collectionDto.getSilver();
                    case "Золотой":
                        return collectionDto.getGold();
                }
            }
        }
        return 0;
    }

    /**
     * Creates a report of result checking clients from the repository in FTS.
     * - given list of all instance of entity {@link ClientShortDto}
     * - extract set of inn of client from given list
     * - sending requests in batches to search for customer data in the FTS database.
     * - creating a report with the received data.
     *
     * @return xls file in byte array with data about clients and distributors
     */
    @Override
    public ByteArrayInputStream buildCheckReport() {
        log.info("START");
        log.info("GETTING ALL CLIENT FROM DB");

        List<ClientShortDto> listClient = clientService.getShortFormAllList();
        List<String> innClientsList = listClient.stream()
                .map(ClientShortDto::getInn)
                .distinct()
                .collect(Collectors.toList());


        log.info("REQUEST CHECKING OF CLIENT IN FNS");
        Map<String, ClientFns> clientFnsMap = new HashMap<>();
        long time = System.currentTimeMillis();

        long maxTime = time + timeLimitRequest;
        while (!innClientsList.isEmpty() && time < maxTime) {
            Set<String> innClientSetForSearch = innClientsList.stream().limit(500).collect(Collectors.toSet());

            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("Sends a data packet of " + innClientSetForSearch.size() + "  inn  from the remaining " + innClientsList.size());
            Map<String, ClientFns> resultClientFnsMap = searchClientIntegration.checkByInn(innClientSetForSearch);

            log.info("Result packet of data. Size  - " + resultClientFnsMap.size());
            innClientsList = innClientsList.stream().filter(c -> !resultClientFnsMap.containsKey(c)).collect(Collectors.toList());

            clientFnsMap.putAll(resultClientFnsMap);
            time = System.currentTimeMillis();
        }

        log.info("CREATE REPORT");

        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Клиентов нет в базе ФНС");

        String roleName = String.format(ROLE_NAME_PATH, "CHECK.FNS");
        String roleSize = String.format(ROLE_SIZE_PATH, "CHECK.FNS");

        String columnsNumberString = env.getProperty(roleName);
        String columnsSizeString = env.getProperty(roleSize);

        String[] columnsNumberStringArray = columnsNumberString.split(",");
        List<Integer> columnsSize = Arrays.asList(columnsSizeString.split(",")).stream()
                .map(e -> Integer.valueOf(e)).collect(Collectors.toList());


        for (int i = 0; i < columnsSize.size(); i++) {
            sheet.setColumnWidth(i, columnsSize.get(i));
        }
        int rowCounter = 0;
        Row header = sheet.createRow(rowCounter++);

        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 16);
        font.setBold(true);
        headerStyle.setFont(font);

        // Add cells
        createRowHeader(header, headerStyle, columnsNumberStringArray);

        CellStyle style = workbook.createCellStyle();
        style.setWrapText(true);
        Row row;
        Cell cell;
        Boolean check;
        ClientFns clientFns;
        for (ClientShortDto client : listClient) {

            row = sheet.createRow(rowCounter++);

            cell = row.createCell(0);
            cell.setCellValue(client.getName());
            cell.setCellStyle(style);

            cell = row.createCell(1);
            cell.setCellValue(client.getInn());
            cell.setCellStyle(style);

            if (clientFnsMap.containsKey(client.getInn())) {
                clientFns = clientFnsMap.get(client.getInn());
                check = clientFns != null;

                if (check) {
                    String nameClientFns = clientFns.getName();
                    if (!nameClientFns.equals(client.getName())) {
                        cell = row.createCell(3);
                        cell.setCellValue(nameClientFns);
                        cell.setCellStyle(style);
                    }
                }
            } else {
                check = null;
            }
            cell = row.createCell(2);
            cell.setCellValue(getTextCheck(check));
            cell.setCellStyle(style);
        }

        AutoFilter autoFilter = sheet.setAutoFilter(new CellRangeAddress(0, rowCounter - 1, 0, columnsSize.size() - 1));

        log.info("REPORT DONE");

        ByteArrayOutputStream outputStream;
        try {
            outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            workbook.close();
            log.info("END");
            return new ByteArrayInputStream(outputStream.toByteArray());
        } catch (Exception e) {
            log.error("FILE BUILD ERROR: {}", e.getMessage());
        }
        return null;
    }

    /**
     * Converting a value in boolean format to string format
     *
     * @param check - active in boolean format
     * @return value in string format
     */
    private String getTextCheck(Boolean check) {
        if (check == null) {
            return "Не проверялся";
        }
        if (check) {
            return "Да";
        } else {
            return "Нет";
        }
    }
}
