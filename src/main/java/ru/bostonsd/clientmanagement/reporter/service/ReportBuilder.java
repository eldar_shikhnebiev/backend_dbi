package ru.bostonsd.clientmanagement.reporter.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;

/**
 * Service for creating a report with data from the storage
 * according to the role assigned to the authorized user using the JWT token
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 10, 2019
 */
public interface ReportBuilder {
    /**
     * Creates a report with data about client and distributors from the repository,
     * according to the issued user role during authorization using the jwt token
     * @param jwt -  token received by an authorized user
     * @return report in byte array format
     */
    ByteArrayInputStream buildReport(String jwt);

    /**
     * Creates a report of result checking clients from the repository in DaData.ru.
     * @return report in byte array format
     */
    ByteArrayInputStream buildCheckReport();
}
