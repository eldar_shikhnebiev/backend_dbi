package ru.bostonsd.clientmanagement.reporter.model;

import lombok.Data;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientDto;
import ru.bostonsd.clientmanagement.domain.models.dto.DiscountCollectionDto;
import ru.bostonsd.clientmanagement.domain.models.dto.DistributorDto;

import java.util.List;
import java.util.Map;

/**
 * Model for uploading from data store to report file
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 10, 2019
 * Updated on March 4, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 */
@Data
public class AggregateReportEntity {
    private List<ClientDto> clients;
    private Map<Long, DistributorDto> distributors;
    private List<DiscountCollectionDto> collections;
}
