package ru.bostonsd.clientmanagement.domain.models.dto;

import lombok.*;

/**
 * DTO for Entity ClientClassification for Update
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * @date 02.12.2019
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientClassificationDtoUpdate {

    @NonNull
    private Long id;

    @NonNull
    private String name;

    private boolean isActive;

}
