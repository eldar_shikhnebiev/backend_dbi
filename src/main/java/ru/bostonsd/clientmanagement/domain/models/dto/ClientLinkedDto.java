package ru.bostonsd.clientmanagement.domain.models.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.bostonsd.clientmanagement.domain.models.Client;

import java.io.Serializable;

/**
 * DTO for {@link Client} domain model, linked form for many to many
 * All fields are analogous to fields in the {@link Client}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on March 31, 2020
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientLinkedDto implements Serializable {

    private Long id;

    private String name;

    private String inn;

}
