package ru.bostonsd.clientmanagement.domain.models.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import ru.bostonsd.clientmanagement.domain.models.SalesVolume;

/**
 * DTO for {@link SalesVolume} domain model
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 9, 2019
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties
public class SalesVolumeDto {

    @NonNull
    private Long idClient;

    @NonNull
    private Long idDistributor;

    private String nameDistributor;

}
