package ru.bostonsd.clientmanagement.domain.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 *  File domain model
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 5, 2019
 * Updated on December 10, 2019 | dyarovoy
 */

@Entity
@Table(name = "file")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class File {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    /** name of file */
    @Column(name = "name")
    private String name;

    /** type of file in http header content type*/
    @Column(name = "type")
    private String type;

    /** contents of the file in bytes */
    @Column(name = "value")
    @Lob
    private byte[] value;
}
