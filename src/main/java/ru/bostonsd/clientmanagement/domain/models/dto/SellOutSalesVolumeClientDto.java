package ru.bostonsd.clientmanagement.domain.models.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import ru.bostonsd.clientmanagement.domain.models.SalesVolume;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryEntity;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryField;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryId;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryValueForList;
import ru.bostonsd.clientmanagement.history.models.enums.HistoryObjectType;

/**
 * DTO for {@link SellOutSalesVolumeClientDto} domain model
 * Designed for {@link ClientDto}

 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on May 6, 2020
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SellOutSalesVolumeClientDto {

    private String namePartner;

    private long volumeFirstYear;

    private long volumeSecondYear;
}
