package ru.bostonsd.clientmanagement.domain.models.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.bostonsd.clientmanagement.domain.models.Client;

import java.util.Set;

/**
 * DTO for {@link Client} domain model, inn with sellout sales volume
 * All fields are analogous to fields in the {@link Client}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on May 21, 2020
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientWithOnlySellOutSalesVolumeDto {

    private String inn;

    private Set<SellOutSalesVolumeClientDto> sellOutSalesVolume;

}
