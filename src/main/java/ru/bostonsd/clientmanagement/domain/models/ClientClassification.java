package ru.bostonsd.clientmanagement.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 *  ClientClassification domain model for {@link Client}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 2, 2019
 * Updated on December 17, 2019 | vkondaurov
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "client_classification")
public class ClientClassification {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /** name class client */
    @Column(name="name", unique = true)
    private String name;

    /** date of registration in the database */
    @Column(name="registration_date")
    @Builder.Default
    private Date dateRegistration = new Date();

    /** availability for class use*/
    @Column(name="is_active")
    private boolean isActive;

    @Override
    public String toString() {
        return "ClientClassification{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dateRegistration=" + dateRegistration +
                ", isActive=" + isActive +
                '}';
    }
}
