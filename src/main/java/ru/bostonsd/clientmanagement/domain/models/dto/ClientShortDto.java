package ru.bostonsd.clientmanagement.domain.models.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.bostonsd.clientmanagement.domain.models.Client;

/**
 * DTO for {@link Client} domain model, short form
 * All fields are analogous to fields in the {@link Client}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 9, 2019
 * Updated on December 12, 2019 | vkondaurov
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientShortDto {

    private Long id;

    private String name;

    private String inn;

    private Long date;

    private String clientTypeString;

    private String clientClassificationString;

    private boolean isActive;
}
