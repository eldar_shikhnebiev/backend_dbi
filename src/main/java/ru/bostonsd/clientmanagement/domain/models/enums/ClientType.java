package ru.bostonsd.clientmanagement.domain.models.enums;

import lombok.NoArgsConstructor;
import ru.bostonsd.clientmanagement.domain.exception.IncorrectClientType;
import ru.bostonsd.clientmanagement.domain.models.Client;

/**
 * Enumerator type for {@link Client}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 2, 2019
 * Updated on December 17, 2019 | vkondaurov
 */


@NoArgsConstructor
public enum ClientType {
    GOLD,
    SILVER,
    BRONZE;

    public static ClientType getByString(String type){
        switch (type){
            case "Бронзовый" : return BRONZE;
            case "Серебряный" : return SILVER;
            case "Золотой" : return GOLD;
            default:         throw new IncorrectClientType("the value "
                    + type + "has no match " +
                    "in Enum " +  ClientType.class.getSimpleName());
        }
    }

    public static String getByType(ClientType type){
        switch (type){
            case BRONZE: return "Бронзовый";
            case SILVER  : return "Серебряный";
            case GOLD  : return "Золотой";
            default: return null;
        }
    }
}
