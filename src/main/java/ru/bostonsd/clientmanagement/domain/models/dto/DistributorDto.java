package ru.bostonsd.clientmanagement.domain.models.dto;

import lombok.*;
import ru.bostonsd.clientmanagement.domain.models.Distributor;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryEntity;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryField;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryId;
import ru.bostonsd.clientmanagement.history.models.enums.HistoryObjectType;

/**
 * DTO for {@link Distributor} domain model
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 2, 2019
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@HistoryEntity(type = HistoryObjectType.DISTRIBUTOR)
public class DistributorDto {

    @HistoryId
    private Long id;

    @NonNull
    @HistoryField(alias = "Имя")
    private String name;

    @HistoryField(alias = "Идентификатор Партнера")
    private Long partnerId;

    @HistoryField(alias = "Дата регистрации")
    private Long date;

    @NonNull
    @HistoryField(alias = "ИНН")
    private String inn;

    @HistoryField(alias = "Почта")
    private String email;

    @HistoryField(alias = "Мобильный телефон")
    private String mobilePhone;

    @HistoryField(alias = "Телефон")
    private String phone;

    @HistoryField(alias = "Личный контакт")
    private String contactPerson;

    @HistoryField(alias = "Адрес")
    private String address;

    @HistoryField(alias = "Описание")
    private String description;
}
