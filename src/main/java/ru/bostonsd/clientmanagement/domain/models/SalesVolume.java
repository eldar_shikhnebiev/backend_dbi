package ru.bostonsd.clientmanagement.domain.models;

import lombok.*;

import javax.persistence.*;

/**
 * SalesVolume domain model
 * Used for linked Entity {@link Client} and {@link Distributor}
 * and specify the sales volume
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 9, 2019
 * Updated on December 17, 2019 | vkondaurov
 */

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sales_volume")
public class SalesVolume {

    /** composite primary key from Client.id and Distributor.id*/
    @EmbeddedId
    @Column(unique = true)
    private ClientDistributorKey id;

    /** Entity for linked {@link Client} */
    @ManyToOne
    @MapsId("client_id")
    @JoinColumn(name = "client_id")
    private Client client;

    /** Entity for linked {@link Distributor} */
    @ManyToOne
    @MapsId("distributor_id")
    @JoinColumn(name = "distributor_id")
    private Distributor distributor;

    @Override
    public String toString() {
        return "SalesVolume{" +
                "id=" + id +
                ", client= " + (client != null ? client.toStringShort() : null) +
                ", distributor= " + (distributor != null ? distributor.toStringShort() : null) +
                '}';
    }
}
