package ru.bostonsd.clientmanagement.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;


/**
 * Sell Out SalesVolume domain model
 * Used specify the sales volume
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on May 6, 2019
 */

@Data
@Builder
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class SellOutSalesVolume {

    @Column(name="partner_name", nullable = false)
    private String partnerName;

    @Column(name="volume_first_year")
    private long volumeFirstYear;

    @Column(name="volume_second_year")
    private long volumeSecondYear;

    @Override
    public String toString() {
        return "SellOutSalesVolume{" +
                "partnerName='" + partnerName + '\'' +
                ", volumeFirstYear=" + volumeFirstYear +
                ", volumeSecondYear=" + volumeSecondYear +
                '}';
    }
}
