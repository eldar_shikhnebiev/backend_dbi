package ru.bostonsd.clientmanagement.domain.models.dto;

import lombok.*;
import ru.bostonsd.clientmanagement.domain.models.DiscountCollection;

/**
 * DTO for {@link DiscountCollection} domain model
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 2, 2019
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DiscountCollectionDto {

    @NonNull
    private String name;

    private Long date;

    private boolean isActive;

    @NonNull
    private Double bronze;

    @NonNull
    private Double silver;

    @NonNull
    private Double gold;

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
