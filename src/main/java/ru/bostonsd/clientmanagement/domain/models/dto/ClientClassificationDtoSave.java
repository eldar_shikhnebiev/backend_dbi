package ru.bostonsd.clientmanagement.domain.models.dto;

import lombok.*;

/**
 * DTO for Entity ClientClassification for Save
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * @date 02.12.2019
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientClassificationDtoSave {

    @NonNull
    private String name;

    private boolean isActive;

}
