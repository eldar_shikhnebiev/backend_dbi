package ru.bostonsd.clientmanagement.domain.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientsUpdateTypeDto {
    @NotNull
    private String typeClient;
    @NotNull
    private ArrayList<Long> clientsId;
}
