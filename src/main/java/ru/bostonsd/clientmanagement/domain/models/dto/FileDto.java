package ru.bostonsd.clientmanagement.domain.models.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.bostonsd.clientmanagement.domain.models.File;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryEntity;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryField;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryId;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryValueForList;
import ru.bostonsd.clientmanagement.history.models.enums.HistoryObjectType;

/**
 * DTO for {@link File} domain model
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 5, 2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@HistoryEntity(type = HistoryObjectType.ATTACHMENT)
public class FileDto {

    @HistoryId
    private Long id;
    @HistoryField(alias = "Название")
    @HistoryValueForList
    private String name;
    @HistoryField(alias = "Тип")
    private String type;
}
