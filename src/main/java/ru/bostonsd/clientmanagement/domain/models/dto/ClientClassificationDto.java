package ru.bostonsd.clientmanagement.domain.models.dto;

import lombok.*;
import ru.bostonsd.clientmanagement.domain.models.ClientClassification;

/**
 * DTO for {@link ClientClassification} domain model
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 2, 2019
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientClassificationDto {

    private Long id;

    @NonNull
    private String name;

    private Long date;

    private boolean isActive;

}
