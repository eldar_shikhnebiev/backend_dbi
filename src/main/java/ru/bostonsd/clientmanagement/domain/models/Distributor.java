package ru.bostonsd.clientmanagement.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Distributor domain model
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 2, 2019
 * Updated on December 17, 2019 | vkondaurov
 */

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "distributor")
public class Distributor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private Long id;

    @Column(name="name")
    private String name;

    /** taxpayer identification number */
    @Column(name="inn")
    private String inn;

    /** internal partner identification number */
    @Column(name="partner_id")
    private Long partnerId;

    /** date of registration in the database */
    @Column(name="registration_date")
    @Builder.Default
    private Date dateRegistration = new Date();

    @Column(name="email")
    private String email;

    @Column(name="mobile_phone")
    private String mobilePhone;

    @Column(name="phone")
    private String phone;

    /** Distributor contact person */
    @Column(name="contact_person")
    private String contactPerson;

    @Column(name="address")
    private String address;

    @Column(name="description")
    private String description;

    @Override
    public String toString() {
        return "Distributor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", inn='" + inn + '\'' +
                ", partnerId='" + partnerId + '\'' +
                ", dateRegistration=" + dateRegistration +
                ", email='" + email + '\'' +
                ", mobilePhone='" + mobilePhone + '\'' +
                ", phone='" + phone + '\'' +
                ", contactPerson='" + contactPerson + '\'' +
                ", address='" + address + '\'' +
                ", description='" + description + '\'' +
                '}';
    }


    public String toStringShort() {
        return "Distributor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", inn='" + inn + '\'' +
                ", partnerId='" + partnerId + '\'' +
                '}';
    }
}
