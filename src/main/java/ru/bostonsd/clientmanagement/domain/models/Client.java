package ru.bostonsd.clientmanagement.domain.models;

import lombok.*;
import org.springframework.cache.annotation.Cacheable;
import ru.bostonsd.clientmanagement.domain.models.enums.ClientType;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *  Client domain model
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 2, 2019
 * Updated on May 6, 2020 | vkondaurov
 *
 */


@Getter
@Setter
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "client")
public class Client  {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private Long id;


    @Column(name="name")
    private String name;

    /** date of registration in the database */
    @Column(name="registration_date")
    @ToString.Exclude
    @Builder.Default
    private Date dateRegistration = new Date();

    /** taxpayer identification number */
    @Column(name="inn")
    private String inn;

    @Column(name="phone")
    private String phone;

    @Column(name="email")
    private String email;

    @Column(name="address")
    private String address;

    /** WebSite*/
    @Column(name="site")
    private String site;

    /** Type Client from Enumerator ClientType {@link ClientType} */
    @Column(name="name_discount_type")
    @Enumerated(EnumType.STRING)
    private ClientType clientType;

    /** availability for class use*/
    @Column(name="is_active")
    @Builder.Default
    private boolean isActive = true;

    /** Entity for linked current Entity with {@link Distributor}  */
    @OneToMany(mappedBy = "client")
    private List<SalesVolume> salesVolumes;

    /** Entity for sales volume from SellOut */
    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "sellout_sales_volume")
    private Set<SellOutSalesVolume> sellOutSalesVolumes;

    /** Class client in Classification */
    @ManyToOne
    @JoinColumn(name = "client_classification_id")
    private ClientClassification clientClassification;

    @ManyToMany
    @JoinTable(name="clients_link",
            joinColumns=@JoinColumn(name="client_pk", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="link_pk", referencedColumnName="id"))
    private Set<Client> clientChildren;

    @ManyToMany(mappedBy="clientChildren")
    private Set<Client> clientParents;

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dateRegistration=" + dateRegistration +
                ", inn='" + inn + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", site='" + site + '\'' +
                ", clientType=" + clientType +
                ", clientClassificationString=" + clientClassification +
                ", isActive=" + isActive +
                ", salesVolumes=" + salesVolumes +
                ", sellOutSalesVolumes=" + sellOutSalesVolumes +
                ", clientChildren = " + (clientChildren == null ? null :  toStringClientCollection(clientChildren)) +
                ", clientParents = " + (clientParents == null ? null :  toStringClientCollection(clientParents)) +
                '}';
    }

    public String toStringShort() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", inn='" + inn + '\'' +
                '}';
    }

    private String toStringClientCollection(Set<Client> client) {
        return client.stream().map(Client::toStringShort).collect(Collectors.joining(",")) ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;

        Client client = (Client) o;

        if (isActive() != client.isActive()) return false;
        if (getId() != null ? !getId().equals(client.getId()) : client.getId() != null) return false;
        if (!getName().equals(client.getName())) return false;
        if (getDateRegistration() != null ? !getDateRegistration().equals(client.getDateRegistration()) : client.getDateRegistration() != null)
            return false;
        if (!getInn().equals(client.getInn())) return false;
        if (getPhone() != null ? !getPhone().equals(client.getPhone()) : client.getPhone() != null) return false;
        if (getEmail() != null ? !getEmail().equals(client.getEmail()) : client.getEmail() != null) return false;
        if (getAddress() != null ? !getAddress().equals(client.getAddress()) : client.getAddress() != null)
            return false;
        if (getSite() != null ? !getSite().equals(client.getSite()) : client.getSite() != null) return false;
        if (getClientType() != client.getClientType()) return false;
        if (getSalesVolumes() != null ? !getSalesVolumes().equals(client.getSalesVolumes()) : client.getSalesVolumes() != null)
            return false;
        if (getSellOutSalesVolumes() != null ? !getSellOutSalesVolumes().equals(client.getSellOutSalesVolumes()) : client.getSellOutSalesVolumes() != null)
            return false;
        return getClientClassification() != null ? getClientClassification().equals(client.getClientClassification()) : client.getClientClassification() == null;

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + getName().hashCode();
        result = 31 * result + (getDateRegistration() != null ? getDateRegistration().hashCode() : 0);
        result = 31 * result + getInn().hashCode();
        result = 31 * result + (getPhone() != null ? getPhone().hashCode() : 0);
        result = 31 * result + (getEmail() != null ? getEmail().hashCode() : 0);
        result = 31 * result + (getAddress() != null ? getAddress().hashCode() : 0);
        result = 31 * result + (getSite() != null ? getSite().hashCode() : 0);
        result = 31 * result + (getClientType() != null ? getClientType().hashCode() : 0);
        result = 31 * result + (isActive() ? 1 : 0);
        result = 31 * result + (getSalesVolumes() != null ? getSalesVolumes().hashCode() : 0);
        result = 31 * result + (getSellOutSalesVolumes() != null ? getSellOutSalesVolumes().hashCode() : 0);
        result = 31 * result + (getClientClassification() != null ? getClientClassification().hashCode() : 0);
        return result;
    }
}
