package ru.bostonsd.clientmanagement.domain.models.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import ru.bostonsd.clientmanagement.domain.models.SalesVolume;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryEntity;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryField;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryId;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryValueForList;
import ru.bostonsd.clientmanagement.history.models.enums.HistoryObjectType;

import java.time.LocalDate;

/**
 * DTO for {@link SalesVolume} domain model
 * Designed for {@link ClientDto}

 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 9, 2019
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties
@HistoryEntity(type = HistoryObjectType.DISTRIBUTOR)
public class SalesVolumeClientDto {

    @NonNull
    @HistoryId
    private Long idDistributor;

    @HistoryField(alias = "Имя дистрибьютора")
    @HistoryValueForList
    private String nameDistributor;
}
