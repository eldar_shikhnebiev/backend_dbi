package ru.bostonsd.clientmanagement.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * DiscountCollection domain model
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 2, 2019
 * Updated on December 17, 2019 | vkondaurov
 */

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "discount_collection")
public class DiscountCollection implements Serializable, Cloneable {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="name")
    private String name;

    /** date of registration in the database */
    @Column(name="registration_date")
    @Builder.Default
    private Date dateRegistration = new Date();

    /** availability for collection use */
    @Column(name="is_active")
    private boolean isActive;

    /** Coefficients (1-discount) for clients {@link Client}  with the specified types in the field name */
    @Column(name="bronze")
    private Double bronze;

    @Column(name="silver")
    private Double silver;

    @Column(name="gold")
    private Double gold;

    @Override
    public String toString() {
        return "DiscountCollection{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dateRegistration=" + dateRegistration +
                ", isActive=" + isActive +
                ", bronze=" + bronze +
                ", silver=" + silver +
                ", gold=" + gold +
                '}';
    }
}
