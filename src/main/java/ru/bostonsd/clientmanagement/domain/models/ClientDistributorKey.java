package ru.bostonsd.clientmanagement.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;


/**
 * ClientDistributorKey domain model for {@link SalesVolume}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 12, 2019
 * Updated on December 18, 2019 | dyarovoy
 */

@Data
@Embeddable
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientDistributorKey implements Serializable {

    @Column(name = "client_id")
    private Long clientId;

    @Column(name = "distributor_id")
    private Long distributorId;

    @Override
    public int hashCode() {
        int result = 17;
        result = 37 * result + (int)'c' + (int)(clientId - (clientId >>> 32));
        result = 37 * result + (int)'d' + (int)(distributorId - (distributorId >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        ClientDistributorKey  clientDistributorKey = (ClientDistributorKey) obj;
        if ((clientDistributorKey.clientId.equals(this.clientId)) &&
                (clientDistributorKey.distributorId.equals(this.distributorId))) return true;
        else return false;
    }
}
