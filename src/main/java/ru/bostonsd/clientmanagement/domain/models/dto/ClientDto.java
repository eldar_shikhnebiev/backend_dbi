package ru.bostonsd.clientmanagement.domain.models.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;
import ru.bostonsd.clientmanagement.domain.models.Client;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryEntity;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryField;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryId;
import ru.bostonsd.clientmanagement.history.models.annatation.HistoryList;
import ru.bostonsd.clientmanagement.history.models.enums.HistoryObjectType;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * DTO for {@link Client} domain model
 * All fields are analogous to fields in the {@link Client}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 2, 2019
 * Updated on April 9, 2020 | vkondaurov
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@HistoryEntity(type = HistoryObjectType.CLIENT)
public class ClientDto implements Serializable, Cloneable {

    @HistoryId
    private Long id;

    @NonNull
    @HistoryField(alias = "Имя")
    private String name;

    @NonNull
    @HistoryField(alias = "ИНН")
    private String inn;

    @HistoryField(alias = "Дата регистрации")
    private Long date;

    @HistoryField(alias = "Телефон")
    private String phone;

    @HistoryField(alias = "Почта")
    private String email;

    @HistoryField(alias = "Адрес")
    private String address;

    @HistoryField(alias = "Сайт")
    private String site;

    //* Type client for list Types
    @HistoryField(alias = "Тип")
    private String clientTypeString;

    //* name on base Classification Client
    @HistoryField(alias = "Классификатор")
    private String clientClassificationString;

    @HistoryField(alias = "Статус клиента")
    private boolean isActive;

    @HistoryList
    private List<SalesVolumeClientDto> salesVolume;

    private Set<SellOutSalesVolumeClientDto> sellOutSalesVolume;

    @HistoryList
    private List<ClientLinkedDto> clientLinks;

    @Override
    public Object clone() throws CloneNotSupportedException {
        ObjectMapper mapper = new ObjectMapper();
        Object result = this;
        try {
            String json = mapper.writeValueAsString(this);
            result = mapper.readValue(json, this.getClass());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean addSalesVolumeClientDto(SalesVolumeClientDto salesVolumeClientDto) {
        if (salesVolumeClientDto== null) return false;
        Long id = salesVolumeClientDto.getIdDistributor();
        boolean noneMatchById = this.getSalesVolume().stream().noneMatch(c -> c.getIdDistributor().equals(id));
        if (noneMatchById) {
            this.salesVolume.add(salesVolumeClientDto);
            return true;
        } else {
            return false;
        }
    }

    public void addListSalesVolumeClientDto(List<SalesVolumeClientDto> salesVolumeClientDto) {
        if (salesVolumeClientDto == null) return;
        salesVolumeClientDto.forEach(this::addSalesVolumeClientDto);
    }


    public boolean addClientLinkedDto(ClientLinkedDto clientLinkedDto) {
        if (clientLinkedDto == null) return false;
        Long id = clientLinkedDto.getId();
        boolean noneMatchById = this.getClientLinks().stream().noneMatch(c -> c.getId().equals(id));
        if (noneMatchById) {
            this.clientLinks.add(clientLinkedDto);
            return true;
        } else {
            return false;
        }
    }

    public void addListClientLinkedDto(List<ClientLinkedDto> clientLinkedDtos) {
        if (clientLinkedDtos == null) return;
        clientLinkedDtos.forEach(this::addClientLinkedDto);
    }
}
