package ru.bostonsd.clientmanagement.domain.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.bostonsd.clientmanagement.domain.models.Client;
import ru.bostonsd.clientmanagement.domain.models.Distributor;

import java.util.List;


/**
 * Repository for {@link Client} domain model
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 2, 2019
 * Updated on December 17, 2019 | vkondaurov
 */

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    /**
     * Retrieves list of entities by given status
     *
     * @param isActive - must be true/false;
     * @return the list of entities with the given ids or {@literal null} if none found
     */
    List<Client> findByIsActive(Boolean isActive);

    /**
     * Retrieves page of entities by given status
     *
     * @param isActive - must be true/false;
     * @param pageable - instance entity {@link Pageable}
     * @return the page of entities with the given ids or {@literal null} if none found
     */
    Page<Client> findByIsActive(Boolean isActive, Pageable pageable);

    /**
     * Retrieves list of entities by them ids of client
     *
     * @param id - must not be {@literal null}
     * @return the list of entities with the given ids or {@literal null} if none found
     */

    List<Client> findByIdIn(List<Long> id);

    List<Client> findByInnIn(List<String> Inn);

    /**
     * Retrieves an entity by its id and status isActive of client
     *
     * @param id - must not be {@literal null}
     * @param isActive - must be true/false;
     * @return the entity with the given name or {@literal null} if none found
     */

    Client findByIdAndIsActive(Long id, Boolean isActive);

    /**
     * Retrieves an entity by its inn of client
     *
     * @param inn - must not be {@literal null}
     * @return the entity with the given inn or {@literal null} if none found
     */
    Client findByInn(String inn);

    /**
     * Retrieves an entity by its inn of client and isActive
     *
     * @param inn - must not be {@literal null}
     * @param isActive - must be true/false;
     * @return the entity with the given inn and isActive or {@literal null} if none found
     */
    Client findByInnAndIsActive(String inn, Boolean isActive);


    @Query("Select c from Client c  WHERE c.id = ?1")
    Client findClientById(Long id);

    /**
     * Checks whether the data store contains elements that match the given inn of client.
     * @param inn- must not be {@literal null}
     * @return {@literal true} if the data store contains elements that match the given inn.
     */
    boolean existsByInn(String inn);

    /**  Returns list instances of the type {@link Client}
     *   that are associated with the given distributor.
     *
     *   Use the hql query to access the related sales volume entity,
     *   which contains data about the entity {@link Distributor}
     *
     * @param distributor - instance entity {@link Distributor}
     * @return list entity with the given distributor or {@literal null} if none found
     */
    @Query("Select c from Client c inner join c.salesVolumes d WHERE d.distributor = ?1")
    List<Client> findByDistributorId(Distributor distributor);

    /**  Returns list instances of the type {@link Client}
     *   that are associated with the given distributor and active status
     *
     *   Use the hql query to access the related sales volume entity,
     *   which contains data about the entity {@link Distributor}
     *
     * @param distributor - instance entity {@link Distributor}
     * @param isActive - must be true/false;
     * @return list entity with the given distributor abd status or {@literal null} if none found
     */

    @Query("Select c from Client c inner join c.salesVolumes d WHERE d.distributor = ?1 AND c.isActive = ?2")
    List<Client> findByDistributorIdAndActive(Distributor distributor, boolean isActive);

    /**  Returns page ogf list instances of the type {@link Client}
     *   that are associated with the given distributor and pageable
     *
     *   Use the hql query to access the related sales volume entity,
     *   which contains data about the entity {@link Distributor}
     *
     * @param distributor - instance entity {@link Distributor}
     * @param pageable - instance entity {@link Pageable}
     * @return page entity with the given distributor or {@literal null} if none found
     */
    @Query("Select c from Client c inner join c.salesVolumes d WHERE d.distributor = ?1")
    Page<Client> findByDistributorId(Distributor distributor, Pageable pageable);

    /**  Returns page ogf list instances of the type {@link Client}
     *   that are associated with the given distributor, active status and pageable
     *
     *   Use the hql query to access the related sales volume entity,
     *   which contains data about the entity {@link Distributor}
     *
     * @param distributor - instance entity {@link Distributor}
     * @param pageable - instance entity {@link Pageable}
     * @return page entity with the given distributor and given status or {@literal null} if none found
     */
    @Query("Select c from Client c inner join c.salesVolumes d WHERE d.distributor = ?1 AND c.isActive = ?2")
    Page<Client> findByDistributorIdAndActive(Distributor distributor, boolean isActive, Pageable pageable);

    @Query(value = "Select link_pk as id FROM clients_link WHERE client_pk = :id " +
            "union " +
            "Select client_pk as id FROM clients_link WHERE link_pk = :id",
            nativeQuery = true)
    List<Long> getClientAllLinks(@Param("id") Long id);

    /**
     * Deleting link of client in table "clients_link" (stores  link between clients)
     * by given id
     *
     * @param id - id of client for deleted link with other clients
     * @return
     */
    @Modifying
    @Query(value = "DELETE FROM clients_link WHERE client_pk = :id or link_pk = :id",
            nativeQuery = true)
    @Transactional
    Integer deleteClientAllLinks(@Param("id") Long id);
}
