package ru.bostonsd.clientmanagement.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bostonsd.clientmanagement.domain.models.Distributor;


/**
 * Repository for {@link Distributor} domain model
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 2, 2019
 */

@Repository
public interface DistributorRepository extends JpaRepository<Distributor, Long> {

    /**
     * Retrieves an entity by its name of distributor
     *
     * @param name - must not be {@literal null}
     * @return the entity with the given name or {@literal null} if none found
     */
    Distributor findByName(String name);

    /**
     * Retrieves an entity by its inn of distributor
     *
     * @param inn - must not be {@literal null}
     * @return the entity with the given inn or {@literal null} if none found
     */
    Distributor findByInn(String inn);

    /**
     * Retrieves an entity by its inn and name of distributor
     *
     * @param inn - must not be {@literal null}
     * @return the entity with the given inn and name or {@literal null} if none found
     */
    Distributor findByInnAndName(String inn, String name);

    /**
     * Checks whether the data store contains elements that match the given inn of distributor.
     * @param inn- must not be {@literal null}
     * @return {@literal true} if the data store contains elements that match the given inn.
     */
    boolean existsByInn(String inn);

    /**
     * Checks whether the data store contains elements that match the given  inn and name of distributor.
     * @param inn- must not be {@literal null}
     * @return {@literal true} if the data store contains elements that match the given  inn and name.
     */
    boolean existsByInnAndName(String inn, String name);
}
