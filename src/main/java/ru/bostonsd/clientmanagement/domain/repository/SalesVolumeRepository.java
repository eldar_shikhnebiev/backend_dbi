package ru.bostonsd.clientmanagement.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.bostonsd.clientmanagement.domain.models.Client;
import ru.bostonsd.clientmanagement.domain.models.Distributor;
import ru.bostonsd.clientmanagement.domain.models.SalesVolume;

import java.util.List;

/**
 * Repository for {@link SalesVolume} domain model
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 9, 2019
 */

public interface SalesVolumeRepository extends JpaRepository<SalesVolume, Long> {

    /**  Returns list instances of the type {@link SalesVolume}
     *   that are associated with the given client id.
     *
     * @param clientId - id instance {@link Client} in DB. must not be {@literal null}
     * @return list entity with the given client id or {@literal null} if none found
     */
    List<SalesVolume> findByClientId(Long clientId);

    /**  Returns list instances of the entity {@link SalesVolume}
     *   that are associated with the given distributor id.
     *
     * @param distributorId - id instance {@link Distributor} in DB. must not be {@literal null}
     * @return list entity with the given distributor id or {@literal null} if none found
     */
    List<SalesVolume> findByDistributorId(Long distributorId);

    /**  Returns instance of the entity {@link SalesVolume}
     *   that are associated with the given client and distributor ids.
     *
     * @param distributorId - id instance {@link Distributor} in DB. must not be {@literal null}
     * @param clientId - id instance {@link Client} in DB. must not be {@literal null}
     *
     * @return instance of entity with the given distributor and client ids or {@literal null} if none found
     */
    SalesVolume findByClientIdAndDistributorId(Long clientId, Long distributorId);

    /**
     * Checking the presence of a link client and distributor
     *
     * @param distributorId - id instance {@link Distributor} in DB. must not be {@literal null}
     * @param clientId - id instance {@link Client} in DB. must not be {@literal null}

     * @return {@literal true} if the data store contains a link instance client and distributor
     */
    boolean existsByClientIdAndDistributorId(Long clientId, Long distributorId);


}
