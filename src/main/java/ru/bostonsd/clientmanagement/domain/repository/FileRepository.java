package ru.bostonsd.clientmanagement.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bostonsd.clientmanagement.domain.models.File;

/**
 * Repository for {@link File} domain model
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 5, 2019
 */

@Repository
public interface FileRepository extends JpaRepository<File, Long> {
}
