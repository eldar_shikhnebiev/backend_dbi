package ru.bostonsd.clientmanagement.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bostonsd.clientmanagement.domain.models.ClientClassification;

import java.util.List;

/**
 * Repository for {@link ClientClassification} domain model
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 2, 2019
 */

@Repository
public interface ClientClassificationRepository extends JpaRepository<ClientClassification, Long> {

    /**
     * Retrieves an entity by its name in classification
     *
     * @param name - must not be {@literal null}
     * @return the entity with the given name or {@literal null} if none found
     */
    ClientClassification findByName(String name);

    /**
     * Returns list instances of the type {@code ClientClassification} with the given specified active .
     *
     * @param active - must not be {@literal null}
     * @return all entity with the given active or {@literal null} if none found
     */
    List<ClientClassification> findByIsActive(boolean active);


    /**
     * Checks whether the data store contains elements that match the given name of class.
     * @param name - must not be {@literal null}
     * @return {@literal true} if the data store contains elements that match the given name.
     */
    boolean existsByName(String name);
}
