package ru.bostonsd.clientmanagement.domain.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.PrintWriter;
import java.io.StringWriter;


/**
 * Exception for Transformation Service Client Entity
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 5, 2019
 *
 * */

@Slf4j
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Incorrect Client Type")
public class IncorrectClientType extends AppException{
    public IncorrectClientType(String message) {
        super(message);
        printLog();
    }

    public void printLog () {
        StringWriter trace = new StringWriter();
        printStackTrace(new PrintWriter(trace));
        log.error(trace.toString());
    }
}