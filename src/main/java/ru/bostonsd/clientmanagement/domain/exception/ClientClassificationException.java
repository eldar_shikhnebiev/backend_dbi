package ru.bostonsd.clientmanagement.domain.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.bostonsd.clientmanagement.domain.models.ClientClassification;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Exception for ClientClassification Service
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 5, 2019
 *
 * */

@Slf4j
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Bad Request for DB Classification ")
public class ClientClassificationException extends AppException{
    public ClientClassificationException(String message) {
        super(message);
        printLog();
    }

    public void printLog () {
        StringWriter trace = new StringWriter();
        printStackTrace(new PrintWriter(trace));
        log.error(trace.toString());
    }
}
