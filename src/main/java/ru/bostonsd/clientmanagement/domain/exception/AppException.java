package ru.bostonsd.clientmanagement.domain.exception;

public abstract class AppException extends RuntimeException {
    public AppException(String message) {
        super(message);
    }
}
