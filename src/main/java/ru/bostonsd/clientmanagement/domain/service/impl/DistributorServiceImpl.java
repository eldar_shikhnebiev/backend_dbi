package ru.bostonsd.clientmanagement.domain.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.domain.exception.DistributorException;
import ru.bostonsd.clientmanagement.domain.models.Client;
import ru.bostonsd.clientmanagement.domain.models.Distributor;
import ru.bostonsd.clientmanagement.domain.models.dto.DistributorDto;
import ru.bostonsd.clientmanagement.domain.repository.DistributorRepository;
import ru.bostonsd.clientmanagement.domain.service.DistributorService;
import ru.bostonsd.clientmanagement.domain.service.TransformationService;

import java.util.*;
import java.util.stream.Collectors;


/**
 * Implementation for {@link DistributorService}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 5, 2019
 * Updated on December 17, 2019 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 */

@Service
public class DistributorServiceImpl implements DistributorService {

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private TransformationService transformationService;

    public DistributorServiceImpl(DistributorRepository distributorRepository, TransformationService transformationService) {
        this.distributorRepository = distributorRepository;
        this.transformationService = transformationService;
    }

    /**
     * Implementation of the method defined in the interface {@link DistributorService}
     * A list of all instances entity  {@link Distributor} in the data store is requested.
     * Checking that it is not empty and transforming all instances from the domain model
     * into a DTO model {@link DistributorDto}.
     *
     * @return  list instances entity {@link DistributorDto}, if data store not empty
     *          empty list, if the store does not contain  data
     */
    @Override
    public List<DistributorDto> getAllList() {
        List<Distributor> distributorList = distributorRepository.findAll();
        if (distributorList.isEmpty()) {
            return new ArrayList<>();
        } else {
            return distributorList
                    .stream()
                    .map(transformationService::transformationFromDistributor)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Implementation of the method defined in the interface {@link DistributorService}
     * A list of all instances entity  {@link Distributor} in the data store is requested.
     * Checking that it is not empty and transforming fields id and partner id all instances
     * from the domain model into Map.
     *
     * @return  map with key id and value partner id, if data store not empty
     *          empty map, if the store does not contain  data
     */

    @Override
    public Map<Long, Long> getMapIdAndPartnerId() {
        List<Distributor> distributorList = distributorRepository.findAll();
        if (distributorList.isEmpty()) {
            return new HashMap<>();
        } else {
            return distributorList.stream()
                    .filter(d -> d.getPartnerId() != null)
                    .collect(Collectors.toMap(Distributor::getId, Distributor::getPartnerId));
        }
    }

    /**
     * Implementation of the method defined in the interface {@link DistributorService}
     * A list of all instances entity  {@link Distributor} in the data store is requested.
     * Checking that it is not empty and transforming fields inn and id all instances
     * from the domain model into Map.
     *
     * @return  map with key inn and value id, if data store not empty
     *          empty map, if the store does not contain  data
     */
    @Override
    public Map<String, Long> getMapInnAndId() {
        List<Distributor> distributorList = distributorRepository.findAll();
        if (distributorList.isEmpty()) {
            return new HashMap<>();
        } else {
            return distributorList.stream()
                    .collect(Collectors.toMap(Distributor::getInn, Distributor::getId));
        }
    }

    /**
     * Implementation of the method defined in the interface {@link DistributorService}
     *
     * Finding instance of entity {@link Distributor} in the data store by given name.
     *
     * If not found in data store throw exception {@link DistributorException}
     *
     * Checking that it is not empty and transforming the domain model into a DTO model {@link DistributorDto}.
     *
     * @param name - inn distributor for find.
     * @return  instance entity {@link DistributorDto}, if finding data store not empty.
     */
    @Override
    public DistributorDto getByName(String name) {
        if (name != null) {
            Distributor distributor = distributorRepository.findByName(name);
            if (distributor != null) {
                return transformationService.transformationFromDistributor(distributor);
            } else {
                throw new DistributorException("Distributor with Name " + name + " not found");
            }
        } else {
            throw new DistributorException("The name of the distributor for the searching is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link DistributorService}
     *
     * Finding instance of entity {@link Distributor} in the data store by given inn.
     *
     * If not found in data store throw exception {@link DistributorException}
     *
     * Checking that it is not empty and transforming the domain model into a DTO model {@link DistributorDto}.
     *
     * @param inn - inn distributor for find.
     * @return  instance entity {@link DistributorDto}, if finding data store not empty.
     */
    @Override
    public DistributorDto getByInn(String inn) {
        if (inn != null) {
            Distributor distributor = distributorRepository.findByInn(inn);
            if (distributor != null) {
                return transformationService.transformationFromDistributor(distributor);
            } else throw  new DistributorException("Distributor with INN " + inn + " not found");
        }  else {
            throw new DistributorException("The inn of the distributor for the searching is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link DistributorService}
     *
     * Finding instance of entity {@link Distributor} in the data store by given id.
     *
     * Checking that it is  empty throw exception {@link DistributorException}
     *
     * @param id - id distributor in datastore for find.
     * @return  instance entity {@link Distributor}, if finding data store not empty.
     */
    @Override
    public Distributor getDistributorById(Long id) {
        if (id != null ) {
            Optional<Distributor> distributorOptional = distributorRepository.findById(id);
            if (distributorOptional.isPresent()) {
                return distributorOptional.get();
            } else {
                throw new DistributorException("Distributor with id " + id + " not found");
            }
        } else {
            throw new DistributorException("The id of the distributor for the searching is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link DistributorService}
     *
     * Finding instance of entity {@link Distributor} in the data store by given id.
     *
     * If not found in data store throw exception {@link DistributorException}
     *
     * Checking that it is not empty and transforming the domain model into a DTO model {@link DistributorDto}.
     *
     * @param id - id distributor in datastore for find.
     * @return  instance entity {@link DistributorDto}, if finding data store not empty.
     */
    @Override
    public DistributorDto getDistributorDtoById(Long id) {
        if (id != null ) {
            Optional<Distributor> distributorOptional = distributorRepository.findById(id);
            if (distributorOptional.isPresent()) {
                return transformationService.transformationFromDistributor(distributorOptional.get());
            } else {
                throw new DistributorException("Distributor with id " + id + " not found");
            }
        } else throw new DistributorException("The id of the distributor for the searching is not set");
    }

    /**
     * Implementation of the method defined in the interface {@link DistributorService}
     *
     * Finding instance of entity {@link Distributor} in the data store by given inn and name.
     *
     * If not found in data store throw exception {@link DistributorException}
     *
     * Checking that it is not empty and transforming the domain model into a DTO model {@link DistributorDto}.
     *
     * @param inn - name distributor for find. must not be {@literal null}
     * @param name - inn distributor for find. must not be {@literal null}
     * @return  instance entity {@link DistributorDto}, if finding data store not empty.
     */
    @Override
    public DistributorDto getByInnAndName(String inn, String name) {
        if (inn != null && name != null) {
            if (distributorRepository.existsByInnAndName(inn, name)) {
                return transformationService.transformationFromDistributor(distributorRepository.findByInnAndName(inn, name));
            } else {
                throw new DistributorException("Distributor with INN " + inn + " and Name " + name + " not found");
            }
        } else {
            throw new DistributorException("The name or inn of the distributor for the searching is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link DistributorService}
     *
     * Saves given instance of entity {@link Distributor} in the data store.
     * Before saving:
     *    - the given entity instance is transformed from DTO to domain model
     *    - it checks that a distributor with the same inn is missing from the repository
     *      If exist in data store throw exception {@link DistributorException}
     *
     * After saving reverse transformation of a saved entity instance into a DTO.
     *
     * @param distributorDto - instance of entity {@link DistributorDto} for saved.
     * @return  instance entity {@link DistributorDto}, saved in data store.
     */
    
    @Override
    public DistributorDto save(DistributorDto distributorDto) {
        if (distributorDto.getInn() != null) {
            Distributor distributorDb = transformationService.transformationFromDistributorDto(distributorDto);
            if (!distributorRepository.existsByInn(distributorDb.getInn())) {
                distributorDb = distributorRepository.save(distributorDb);
                return transformationService.transformationFromDistributor(distributorDb);
            } else {
                throw new DistributorException("Distributor with inn " + distributorDto.getInn()
                        + " and name " + distributorDto.getName() + " not found for update");
            }
        }  else {
            throw new DistributorException("The inn of the distributor for saving is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link DistributorService}
     *
     * Updated fields given instance of entity {@link Distributor} in the data store.
     *
     * Finding instance entity {@link Distributor} by given id.
     * If it was found, given instance from parameter trans formatied to domain model
     * and setter new value in fields instance of entity from data store.
     * After that update instanced of entity in data store
     *
     * If not exist in data store throw exception {@link DistributorException}
     *
     * @param distributorDto - instance of entity {@link DistributorDto} for updated.
     * @return  instance entity {@link DistributorDto}, saved in data store.
     */
    @Override
    public DistributorDto update(DistributorDto distributorDto) {
        if (distributorDto.getId() != null) {
            Optional<Distributor> distributorOptional = distributorRepository.findById(distributorDto.getId());
            if (distributorOptional.isPresent()) {
                Distributor distributorDb = distributorOptional.get();
                Distributor distributorTemp = transformationService.transformationFromDistributorDto(distributorDto);
                distributorDb.setName(distributorTemp.getName());
                distributorDb.setPartnerId(distributorTemp.getPartnerId());
                distributorDb.setEmail(distributorTemp.getEmail());
                distributorDb.setMobilePhone(distributorTemp.getMobilePhone());
                distributorDb.setPhone(distributorTemp.getPhone());
                distributorDb.setContactPerson(distributorTemp.getContactPerson());
                distributorDb.setAddress(distributorTemp.getAddress());
                distributorDb.setDescription(distributorTemp.getDescription());

                distributorDb = distributorRepository.save(distributorDb);
                return transformationService.transformationFromDistributor(distributorDb);

            } else {
                throw new DistributorException("Distributor with inn " + distributorDto.getInn()
                        + " and name " + distributorDto.getName() + " not found for update");
            }
        } else {
            throw new DistributorException("The id of the distributor for the updating is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link DistributorService}
     *
     * Deletes instance of entity {@link Distributor} by id in the data store.
     * Before deleted:
     *    - finding instance entity {@link Distributor} by given id.
     *      If it was found, it is deleted to the data store and return true
     *      otherwise false
     *
     * @param id - id distributor for deleted.
     * @return boolean answer from result of the delete operation
     */
    @Override
    public boolean delete(Long id){
        if (id != null ) {
            Optional<Distributor> distributorOptional = distributorRepository.findById(id);
            if (distributorOptional != null) {
                distributorRepository.delete(distributorOptional.get());
                return true;
            } else {
                return false;
            }
        } else {
            throw new DistributorException("The id of the distributor for the deleting is not set");
        }
    }
}

