package ru.bostonsd.clientmanagement.domain.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.domain.exception.ClientClassificationException;
import ru.bostonsd.clientmanagement.domain.models.ClientClassification;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientClassificationDto;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientClassificationDtoSave;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientClassificationDtoUpdate;
import ru.bostonsd.clientmanagement.domain.repository.ClientClassificationRepository;
import ru.bostonsd.clientmanagement.domain.service.ClientClassificationService;
import ru.bostonsd.clientmanagement.domain.service.TransformationService;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Implementation for {@link ClientClassificationService}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 5, 2019
 * Updated on December 11, 2019 | Denis Yarovoy (dyarovoy@bostonsd.ru)
 */

@Slf4j
@Service
public class ClientClassificationServiceImpl implements ClientClassificationService {


    @Autowired
    private ClientClassificationRepository clientClassificationRepository;

    @Autowired
    private TransformationService transformationService;

    /**
     * PostConstruct method for adding a list of client classes to the repository.
     * Before adding data, the data in the store is checked.
     * If empty, data is added. The list of classes is specified inside the method
     */
    @PostConstruct
    private void postConstruct() {
        List<ClientClassification> classificationList = clientClassificationRepository.findAll();
        if (classificationList.isEmpty()) {
            List<String> classifications = new ArrayList<>();
            classifications.add("Конечный заказчик");
            classifications.add("Дилер");
            classifications.add("Строительно-монтажная организация");
            classifications.add("Промышленное предприятие");
            classifications.add("Подрядчик");
            classifications.add("Застройщик");
            classifications.add("Щитовик");
            classifications.add("DIY (сети)");
            classifications.add("OEM");
            classifications.add("Другое");
            classifications.add("Системный интегратор");
            for (String classificationName : classifications) {
                clientClassificationRepository.save(ClientClassification.builder()
                        .isActive(true)
                        .name(classificationName)
                        .dateRegistration(new Date())
                        .build());
            }
            log.info("End process");
        }
    }

    public ClientClassificationServiceImpl(ClientClassificationRepository clientClassificationRepository,
                                           TransformationService transformationService) {
        this.clientClassificationRepository = clientClassificationRepository;
        this.transformationService = transformationService;
    }

    /**
     * Implementation of the method defined in the interface {@link ClientClassificationService}
     * A list of all instances entity  {@link ClientClassification} in the data store is requested.
     * Checking that it is not empty and transforming all instances from the domain model into a DTO model {@link ClientClassificationDto}.
     *
     * @return  list instances entity {@link ClientClassificationDto}, if data store not empty
     *          empty list, if the store does not contain classification data
     */

    @Override
    public List<ClientClassificationDto> getAllList() {
        List<ClientClassification> clientClassifications = clientClassificationRepository.findAll();
        if (clientClassifications.isEmpty()) {
            return new ArrayList<>();
        } else {
            return clientClassifications.stream()
                    .map(transformationService::transformationFromClientClassification)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientClassificationService}
     *
     * Requests a list of instances of the {@link ClientClassification} entity in the data store
     * with the parameter  isActive specified for the field "active".
     *
     * Checking that it is not empty and transforming all instances from the domain model into a DTO model {@link ClientClassificationDto}.
     *
     * @param isActive - must be true/false
     * @return  list instances entity {@link ClientClassificationDto}, if data store not empty
     *          empty list, if the store does not contain classification data
     */
    @Override
    public List<ClientClassificationDto> getIsActiveList(boolean isActive) {
        List<ClientClassification> clientClassifications = clientClassificationRepository.findByIsActive(isActive);
        if (clientClassifications.isEmpty()) {
            return new ArrayList<>();
        } else {
            return clientClassifications.stream()
                    .map(transformationService::transformationFromClientClassification)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientClassificationService}
     *
     * Finding instance of entity {@link ClientClassification} in the data store
     * by given name.
     * If not found in data store throw exception {@link ClientClassificationException}
     *
     * Checking that it is not empty and transforming the domain model into a DTO model {@link ClientClassificationDto}.
     *
     * @param name - name class for find.
     * @return  instance entity {@link ClientClassificationDto}, if finding data store not empty.
     */
    @Override
    public ClientClassificationDto getByName(String name) {
        if (name != null) {
            ClientClassification clientClassification = clientClassificationRepository.findByName(name);
            if (clientClassification != null) {
                return transformationService.transformationFromClientClassification(clientClassification);
            } else {
                throw new ClientClassificationException("Classification with name " + name + " not found");
            }
        } else {
            throw new ClientClassificationException("The name of the Classification for the search is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientClassificationService}
     *
     * Saves given instance of entity {@link ClientClassification} in the data store.
     * Before saving:
     *    - the given entity instance is transformed from DTO to domain model
     *    - it checks that a class with the same name is missing from the repository
     *      If exist in data store throw exception {@link ClientClassificationException}
     *
     * After saving reverse transformation of a saved entity instance into a DTO.
     *
     * @param clientClassificationDtoSave - instance of entity {@link ClientClassificationDto} for saved.
     * @return  instance entity {@link ClientClassificationDto}, saved in data store.
     */

    @Override
    public ClientClassificationDto save(ClientClassificationDtoSave clientClassificationDtoSave) {
        ClientClassification clientClassificationDb =
                transformationService.transformationFromClientClassificationDto(clientClassificationDtoSave);

        if (!clientClassificationRepository.existsByName(clientClassificationDb.getName())) {
            clientClassificationDb = clientClassificationRepository.save(clientClassificationDb);
            return transformationService.transformationFromClientClassification(clientClassificationDb);

        } else {
            throw new ClientClassificationException("Classification with name "
                    + clientClassificationDtoSave.getName() + " exist in DB");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientClassificationService}
     *
     * Updated fields given instance of entity {@link ClientClassification} in the data store.
     *
     * Finding instance entity {@link ClientClassification} by given name.
     * If it was found, given instance from parameter trans formatied to domain model
     * and setter new value in fields instance of entity from data store.
     * After that update instanced of entity in data store
     *
     * If not exist in data store throw exception {@link ClientClassificationException}
     *
     * @param clientClassificationDtoUpdate - instance of entity {@link ClientClassificationDto} for updated.
     * @return  instance entity {@link ClientClassificationDto}, saved in data store.
     */
    @Override
    public ClientClassificationDto update(ClientClassificationDtoUpdate clientClassificationDtoUpdate) {
        Optional<ClientClassification> clientClassificationOptional =
                clientClassificationRepository.findById(clientClassificationDtoUpdate.getId());

        if (clientClassificationOptional.isPresent()) {
            ClientClassification clientClassificationDb = clientClassificationOptional.get();

            ClientClassification clientClassificationOther =
                    clientClassificationRepository.findByName(clientClassificationDtoUpdate.getName());

            if (clientClassificationOther != null &&
                    (!clientClassificationOther.getId().equals(clientClassificationDtoUpdate.getId()) &&
                clientClassificationOther.getName().equals(clientClassificationDtoUpdate.getName()))) {
                throw new ClientClassificationException("Rename current classification impossible. " +
                        "Classification with name "
                        + clientClassificationDtoUpdate.getName() + " exist in DB.");
            } else {
                ClientClassification clientClassification = transformationService
                        .transformationFromClientClassificationDto(clientClassificationDtoUpdate);

                clientClassificationDb.setName(clientClassification.getName());
                clientClassificationDb.setActive(clientClassification.isActive());

                clientClassificationDb = clientClassificationRepository.save(clientClassificationDb);
                return transformationService.transformationFromClientClassification(clientClassificationDb);
            }
        } else {
            throw new ClientClassificationException("Classification with id "
                    + clientClassificationDtoUpdate.getId() + " not found for update");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientClassificationService}
     *
     * Deletes instance of entity {@link ClientClassification} by name in the data store.
     * Before deleted:
     *    - finding instance entity {@link ClientClassification} by given name.
     *      If it was found, it is deleted to the data store and return true
     *      otherwise false
     *
     *
     * @param name - name class for deleted.
     * @return boolean answer from result of the delete operation
     */
    @Override
    public boolean delete(String name) {
        if (name != null) {
            ClientClassification clientClassificationDb = clientClassificationRepository.findByName(name);
            if (clientClassificationDb != null) {
                clientClassificationRepository.delete(clientClassificationDb);
                return true;
            } else {
                return false;
            }
        } else {
                throw new ClientClassificationException("The name of the Classification for the deleting is not set");
        }
    }
}

