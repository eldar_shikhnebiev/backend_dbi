package ru.bostonsd.clientmanagement.domain.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.domain.exception.ClientClassificationException;
import ru.bostonsd.clientmanagement.domain.exception.SalesVolumeException;
import ru.bostonsd.clientmanagement.domain.models.*;
import ru.bostonsd.clientmanagement.domain.models.dto.*;
import ru.bostonsd.clientmanagement.domain.models.enums.ClientType;
import ru.bostonsd.clientmanagement.domain.repository.*;
import ru.bostonsd.clientmanagement.domain.service.TransformationService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation for {@link TransformationService}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 4, 2019
 * Updated on May 21, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 */

@Service
public class TransformationServiceImpl implements TransformationService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private ClientClassificationRepository clientClassificationRepository;

    @Autowired
    private DiscountCollectionRepository discountCollectionRepository;

    @Autowired
    private SalesVolumeRepository salesVolumeRepository;


    public TransformationServiceImpl(ClientRepository clientRepository,
                                     DistributorRepository distributorRepository,
                                     ClientClassificationRepository clientClassificationRepository,
                                     DiscountCollectionRepository discountCollectionRepository,
                                     SalesVolumeRepository salesVolumeRepository) {
        this.clientRepository = clientRepository;
        this.distributorRepository = distributorRepository;
        this.clientClassificationRepository = clientClassificationRepository;
        this.discountCollectionRepository = discountCollectionRepository;
    }

    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of Client entity from DTO to domain model
     * In process transformation
     * - the client id is checked that it is not empty
     * - class of Client exist  in  datastore
     *   if class not found throw exception {@link ClientClassificationException}
     * - the value of the sales volume field is set to empty
     *
     * @param clientDto - instance of entity {@link ClientDto} for transformation
     * @return instance of entity {@link Client}
     */
    @Override
    public Client transformationFromClientDto(ClientDto clientDto) {
        Client client = Client.builder()
                .name(clientDto.getName())
                .inn(clientDto.getInn())
                .build();

        if (clientDto.getId() != null) {
            client.setId(clientDto.getId());
        }

        client.setPhone(clientDto.getPhone());
        client.setAddress(clientDto.getAddress());
        client.setEmail(clientDto.getEmail());
        client.setSite(clientDto.getSite());
        client.setActive(clientDto.isActive());

        if (clientDto.getClientTypeString() != null) {
            client.setClientType(ClientType.getByString(clientDto.getClientTypeString()));
        }

        if (clientDto.getClientClassificationString() != null) {
            ClientClassification clientClassification =clientClassificationRepository.findByName(clientDto.getClientClassificationString());
            if (clientClassification != null ) {
                client.setClientClassification(clientClassification);
            } else {
                throw new ClientClassificationException("Classification Type " + clientDto.getClientClassificationString() + " not found ");
            }
        }
        client.setSalesVolumes(new ArrayList<>());
        client.setSellOutSalesVolumes(new HashSet<>());
        client.setClientChildren(new HashSet<>());
        client.setClientParents(new HashSet<>());
        return client;
    }

    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of Client entity from domain model to DTO {@link ClientDto}
     * In process transformation
     * - checking type of Client it is not null
     * - checking class of Client it is not null
     * - the sales volume instance list is transformed by calling a separate method
     *
     * @param client - instance of entity {@link Client} for transformation
     * @return instance of entity {@link ClientDto}
     */
    @Override
    public ClientDto transformationFromClient(Client client) {
        return ClientDto.builder()
                .id(client.getId())
                .name(client.getName())
                .inn(client.getInn())
                .date(client.getDateRegistration().getTime())
                .phone(client.getPhone())
                .email(client.getEmail())
                .address(client.getAddress())
                .site(client.getSite())
                .clientTypeString(client.getClientType() != null ? ClientType.getByType(client.getClientType()) : null)
                .clientClassificationString(client.getClientClassification() != null ? client.getClientClassification().getName() : null)
                .salesVolume(client.getSalesVolumes().stream()
                                                .map(this::transformationFromSalesVolumeClient)
                                                .collect(Collectors.toList()))
               .sellOutSalesVolume(client.getSellOutSalesVolumes().stream()
                       .map(this::transformationFromSellOutSalesVolume)
                       .collect(Collectors.toSet()))
                .clientLinks(getClientLinkedDtos(client))
                .isActive(client.isActive())
                .build();
    }

    private List<ClientLinkedDto> getClientLinkedDtos(Client client) {
        List<ClientLinkedDto> clientLinkedDtoList = null;
        if (client != null) {
            clientLinkedDtoList = client.getClientChildren()
                    .stream()
                    .map(this::transformationFromClientLinked)
                    .collect(Collectors.toList());
            clientLinkedDtoList.addAll(client.getClientParents()
                    .stream()
                    .map(this::transformationFromClientLinked).collect(Collectors.toList()));
        }
        return clientLinkedDtoList;
    }


    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of Client entity from domain model to DTO {@link ClientDto}
     * In process transformation
     * - checking type of Client it is not null
     * - checking class of Client it is not null
     * - the sales volume instance list is filtered by the given distributor ID and transformed by calling a separate method
     *
     * @param client - instance of entity {@link Client} for transformation
     * @param distributorId  - id distributor for filtered sales volume instance.
     * @return instance of entity {@link ClientDto}
     */
    @Override
    public ClientDto transformationFromClientByDistributor(Long distributorId,  Client client) {
        return ClientDto.builder()
                .id(client.getId())
                .name(client.getName())
                .inn(client.getInn())
                .date(client.getDateRegistration().getTime())
                .phone(client.getPhone())
                .email(client.getEmail())
                .address(client.getAddress())
                .site(client.getSite())
                .clientTypeString(client.getClientType() != null ? ClientType.getByType(client.getClientType()) : null)
                .clientClassificationString(client.getClientClassification() != null ? client.getClientClassification().getName() : null)
                .salesVolume(client.getSalesVolumes().stream()
                                .filter(salesVolume -> salesVolume.getDistributor().getId() == distributorId)
                                .map(this::transformationFromSalesVolumeClient)
                                .collect(Collectors.toList()))
                .sellOutSalesVolume(client.getSellOutSalesVolumes().stream()
                                        .map(this::transformationFromSellOutSalesVolume)
                                        .collect(Collectors.toSet()))
                .clientLinks(getClientLinkedDtos(client))
                .isActive(client.isActive())
                .build();
    }


    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of Client entity from domain model to DTO short form  {@link ClientShortDto}
     * In process transformation
     * - checking type of Client it is not null
     * - checking class of Client it is not null
     *
     * @param client - instance of entity {@link Client} for transformation
     * @return instance of entity {@link ClientShortDto}
     */
    @Override
    public ClientShortDto transformationFromClientShort(Client client) {
        return ClientShortDto.builder()
                .id(client.getId())
                .name(client.getName())
                .inn(client.getInn())
                .date(client.getDateRegistration().getTime())
                .clientTypeString(client.getClientType() != null ? ClientType.getByType(client.getClientType()) : null)
                .clientClassificationString(client.getClientClassification() != null ? client.getClientClassification().getName() : null)
                .isActive(client.isActive())
                .build();
    }

    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of Client entity from domain model to DTO short form  {@link ClientWithOnlySellOutSalesVolumeDto}
     *
     * @param client - instance of entity {@link Client} for transformation
     * @return instance of entity {@link ClientWithOnlySellOutSalesVolumeDto}
     */
    @Override
    public ClientWithOnlySellOutSalesVolumeDto transformationFromClientOnlySalesVolume(Client client) {
        return ClientWithOnlySellOutSalesVolumeDto.builder()
                .inn(client.getInn())
                .sellOutSalesVolume(client.getSellOutSalesVolumes().stream()
                        .map(this::transformationFromSellOutSalesVolume)
                        .collect(Collectors.toSet()))
                .build();
    }

    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of Client entity from domain model to DTO short form  {@link ClientLinkedDto}
     *
     * @param client - instance of entity {@link Client} for transformation
     * @return instance of entity {@link ClientLinkedDto}
     */
    @Override
    public ClientLinkedDto transformationFromClientLinked(Client client) {
        return ClientLinkedDto.builder()
                .id(client.getId())
                .name(client.getName())
                .inn(client.getInn())
                .build();
    }


    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of Distributor entity from DTO to domain model
     * In process transformation
     * - the client id is checked that it is not empty
     *
     * @param distributorDto - instance of entity {@link DistributorDto} for transformation
     * @return instance of entity {@link Distributor}
     */
    @Override
    public Distributor transformationFromDistributorDto(DistributorDto distributorDto) {
        Distributor distributor = Distributor.builder()
                .name(distributorDto.getName())
                .inn(distributorDto.getInn())
                .build();

        if (distributorDto.getId() != null) {
            distributor.setId(distributorDto.getId());
        }

        distributor.setPartnerId(distributorDto.getPartnerId());
        distributor.setEmail(distributorDto.getEmail());
        distributor.setMobilePhone(distributorDto.getMobilePhone());
        distributor.setPhone(distributorDto.getPhone());
        distributor.setContactPerson(distributorDto.getContactPerson());
        distributor.setAddress(distributorDto.getAddress());
        distributor.setDescription(distributorDto.getDescription());
        return distributor;
    }

    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of Distributor entity from domain model to DTO {@link DistributorDto}
     *
     * @param distributor - instance of entity {@link Distributor} for transformation
     * @return instance of entity {@link DistributorDto}
     */
    @Override
    public DistributorDto transformationFromDistributor(Distributor distributor) {
        return  DistributorDto.builder()
                .id(distributor.getId())
                .name(distributor.getName())
                .partnerId(distributor.getPartnerId())
                .date(distributor.getDateRegistration().getTime())
                .inn(distributor.getInn())
                .email(distributor.getEmail())
                .mobilePhone(distributor.getMobilePhone())
                .phone(distributor.getPhone())
                .contactPerson(distributor.getContactPerson())
                .address(distributor.getAddress())
                .description(distributor.getDescription())
                .build();
    }

    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of Client Classification entity from DTO to domain model
     *
     * @param clientClassificationDto - instance of entity {@link ClientClassificationDto} for transformation
     * @return instance of entity {@link ClientClassification}
     */
    @Override
    public ClientClassification transformationFromClientClassificationDto
            (ClientClassificationDto clientClassificationDto) {
            return ClientClassification.builder()
                        .id(clientClassificationDto.getId())
                        .name(clientClassificationDto.getName())
                        .dateRegistration(clientClassificationDto.getDate() != null
                                ? new Date(clientClassificationDto.getDate()) : new Date())
                        .isActive(clientClassificationDto.isActive())
                        .build();
    }

    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of Client Classification entity from DTO got for save to domain model
     *
     * @param clientClassificationDtoSave - instance of entity {@link ClientClassificationDtoSave} for transformation
     * @return instance of entity {@link ClientClassification}
     */
    @Override
    public ClientClassification transformationFromClientClassificationDto
            (ClientClassificationDtoSave clientClassificationDtoSave) {
        ClientClassification clientClassification = ClientClassification.builder()
                .name(clientClassificationDtoSave.getName())
                .isActive(clientClassificationDtoSave.isActive())
                .build();

        return clientClassification;
    }

    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of Client Classification entity from DTO got for update to domain model
     *
     * @param clientClassificationDtoUpdate - instance of entity {@link ClientClassificationDtoUpdate} for transformation
     * @return instance of entity {@link ClientClassification}
     */
    @Override
    public ClientClassification transformationFromClientClassificationDto
            (ClientClassificationDtoUpdate clientClassificationDtoUpdate) {
        ClientClassification clientClassification = ClientClassification.builder()
                .id(clientClassificationDtoUpdate.getId())
                .name(clientClassificationDtoUpdate.getName())
                .isActive(clientClassificationDtoUpdate.isActive())
                .build();
        return clientClassification;
    }

    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of Client entity from domain model to DTO {@link ClientClassificationDto}
     *
     * @param clientClassification - instance of entity {@link ClientClassification} for transformation
     * @return instance of entity {@link ClientClassificationDto}
     */
    @Override
    public ClientClassificationDto transformationFromClientClassification
            (ClientClassification clientClassification) {
        return ClientClassificationDto.builder()
                .id(clientClassification.getId())
                .name(clientClassification.getName())
                .date(clientClassification.getDateRegistration().getTime())
                .isActive(clientClassification.isActive())
                .build();
    }

    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of Discount Collection entity from DTO to domain model
     *
     * @param discountCollectionDto - instance of entity {@link DiscountCollectionDto} for transformation
     * @return instance of entity {@link DiscountCollection}
     */
    @Override
    public DiscountCollection transformationFromDiscountCollectionDto
            (DiscountCollectionDto discountCollectionDto) {
        return DiscountCollection.builder()
                .name(discountCollectionDto.getName())
                .dateRegistration(discountCollectionDto.getDate() != null
                        ? new Date(discountCollectionDto.getDate()) : new Date())
                .isActive(discountCollectionDto.isActive())
                .bronze(discountCollectionDto.getBronze())
                .silver(discountCollectionDto.getSilver())
                .gold(discountCollectionDto.getGold())
                .build();
    }

    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of Discount Collection entity from domain model to DTO {@link DiscountCollectionDto}
     *
     * @param discountCollection - instance of entity {@link DiscountCollection} for transformation
     * @return instance of entity {@link DiscountCollectionDto}
     */
    @Override
    public DiscountCollectionDto transformationFromDiscountCollection(DiscountCollection discountCollection) {
        return DiscountCollectionDto.builder()
                .name(discountCollection.getName())
                .date(discountCollection.getDateRegistration().getTime())
                .isActive(discountCollection.isActive())
                .bronze(discountCollection.getBronze())
                .silver(discountCollection.getSilver())
                .gold(discountCollection.getGold())
                .build();
    }

    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of SalesVolume entity from DTO to domain model
     * In process transformation
     * - the client and distributor ids is checked that it is not empty
     *   if if one of them is empty throw exception {@link SalesVolumeException}
     * - requesting instances of entities Client and Distributor in data store and checking them is not empty
     *
     * @param salesVolumeDto - instance of entity {@link SalesVolumeDto} for transformation
     * @return instance of entity {@link SalesVolume}
     */
    @Override
    public SalesVolume transformationFromSalesVolumeDto(SalesVolumeDto salesVolumeDto) {
        if (salesVolumeDto.getIdClient() != null && salesVolumeDto.getIdDistributor() != null) {
            Optional<Client> clientOptional = clientRepository.findById(salesVolumeDto.getIdClient());
            Optional<Distributor> distributorOptional = distributorRepository.findById(salesVolumeDto.getIdDistributor());
            if (clientOptional.isPresent() && distributorOptional.isPresent()) {
                return SalesVolume.builder()
                        .id(ClientDistributorKey.builder().clientId(clientOptional.get().getId())
                                .distributorId(distributorOptional.get().getId()).build())
                        .client(clientOptional.get())
                        .distributor(distributorOptional.get())
                        .build();
            }
        }
        throw new SalesVolumeException( " Client with id " + salesVolumeDto.getIdClient() +
                " or Distributor id " + salesVolumeDto.getIdDistributor() + " not found");
    }

    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of SalesVolume entity from DTO to domain model
     * In process transformation
     * - the client and distributor ids is checked that it is not empty
     *   if if one of them is empty throw exception {@link SalesVolumeException}
     * - requesting instances of entities Client and Distributor in data store and checking them is not empty
     *
     * @param clientId - id client for requesting instances of entities Client
     * @param salesVolumeClientDto - instance of entity {@link SalesVolumeClientDto} for transformation
     * @return instance of entity {@link SalesVolume}
     */
    @Override
    public SalesVolume transformationFromClientSalesVolumeDto(Long clientId, SalesVolumeClientDto salesVolumeClientDto) {
        if (clientId != null && salesVolumeClientDto.getIdDistributor() != null) {
            Optional<Client> clientOptional = clientRepository.findById(clientId);
            Optional<Distributor> distributorOptional = distributorRepository.findById(salesVolumeClientDto.getIdDistributor());
            if (clientOptional.isPresent() && distributorOptional.isPresent()) {
                return SalesVolume.builder()
                        .id(ClientDistributorKey.builder().clientId(clientOptional.get().getId())
                                .distributorId(distributorOptional.get().getId()).build())
                        .client(clientOptional.get())
                        .distributor(distributorOptional.get())
                        .build();
            }
        }
        throw new SalesVolumeException( " Client with id " + clientId +
                " or Distributor id " + salesVolumeClientDto.getIdDistributor() + " not found");
    }

    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of SalesVolume entity DTO from short form to full  form {@link SalesVolumeDto}
     *
     * @param clientId - id client for instance DTO
     * @param salesVolumeClientDto - instance of entity {@link SalesVolumeClientDto} for transformation
     * @return instance of entity {@link SalesVolumeDto}
     */
    @Override
    public SalesVolumeDto transformationFromClientSalesVolumeDtoToFull(Long clientId, SalesVolumeClientDto salesVolumeClientDto) {
        return SalesVolumeDto.builder()
                .idClient(clientId)
                .idDistributor(salesVolumeClientDto.getIdDistributor())
                .build();
    }

    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of SalesVolume entity from domain model to DTO {@link SalesVolumeDto}
     *
     * @param salesVolume - instance of entity {@link SalesVolume} for transformation
     * @return instance of entity {@link SalesVolumeDto}
     */
    @Override
    public SalesVolumeDto transformationFromSalesVolume(SalesVolume salesVolume) {
        return SalesVolumeDto.builder()
                .idClient(salesVolume.getClient().getId())
                .idDistributor(salesVolume.getDistributor().getId())
                .nameDistributor(salesVolume.getDistributor().getName())
                .build();
    }

    /**
     * Implementation of the method defined in the interface {@link TransformationService}
     *
     * Transformation of SalesVolume entity from domain model to DTO {@link SalesVolumeClientDto} short form
     *
     * @param salesVolume - instance of entity {@link SalesVolume} for transformation
     * @return instance of entity {@link SalesVolumeClientDto}
     */
    @Override
    public SalesVolumeClientDto transformationFromSalesVolumeClient(SalesVolume salesVolume) {
        return SalesVolumeClientDto.builder()
                .idDistributor(salesVolume.getDistributor().getId())
                .nameDistributor(salesVolume.getDistributor().getName())
                .build();
    }

    @Override
    public SellOutSalesVolumeClientDto transformationFromSellOutSalesVolume(SellOutSalesVolume sellOutSalesVolume) {
        return SellOutSalesVolumeClientDto.builder()
                .namePartner(sellOutSalesVolume.getPartnerName())
                .volumeFirstYear(sellOutSalesVolume.getVolumeFirstYear())
                .volumeSecondYear(sellOutSalesVolume.getVolumeSecondYear())
                .build();
    }
}
