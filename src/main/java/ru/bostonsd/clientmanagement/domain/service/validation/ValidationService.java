package ru.bostonsd.clientmanagement.domain.service.validation;

import ru.bostonsd.clientmanagement.domain.models.dto.ClientDto;

/**
 * @author asaburov on 12/10/2019
 */
public interface ValidationService {

    boolean validateClient(ClientDto clientDto);

    boolean validateTicket();
}
