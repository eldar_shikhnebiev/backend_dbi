package ru.bostonsd.clientmanagement.domain.service.impl;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bostonsd.clientmanagement.domain.exception.ClientException;
import ru.bostonsd.clientmanagement.domain.models.Client;
import ru.bostonsd.clientmanagement.domain.models.SellOutSalesVolume;
import ru.bostonsd.clientmanagement.domain.models.dto.*;
import ru.bostonsd.clientmanagement.domain.models.enums.ClientType;
import ru.bostonsd.clientmanagement.domain.repository.ClientRepository;
import ru.bostonsd.clientmanagement.domain.service.ClientService;
import ru.bostonsd.clientmanagement.domain.service.DistributorService;
import ru.bostonsd.clientmanagement.domain.service.SalesVolumeService;
import ru.bostonsd.clientmanagement.domain.service.TransformationService;
import ru.bostonsd.clientmanagement.sellout.models.ClientAnnualSalesDto;
import ru.bostonsd.clientmanagement.sellout.service.ClientAnnualSalesService;
import ru.bostonsd.clientmanagement.smtp.service.PreparatorSenderService;

import java.time.Year;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementation for {@link ClientService}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 *
 * Created on December 5, 2019
 * Updated on May 21, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 */

@Slf4j
@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private TransformationService transformationService;

    @Autowired
    private DistributorService distributorService;

    @Autowired
    private SalesVolumeService salesVolumeService;

    @Autowired
    private PreparatorSenderService preparatorSenderService;

    @Autowired
    private ClientAnnualSalesService clientAnnualSalesService;

    public ClientServiceImpl(ClientRepository clientRepository,
                             TransformationService transformationService,
                             DistributorService distributorService) {
        this.clientRepository = clientRepository;
        this.transformationService = transformationService;
        this.distributorService = distributorService;
    }


    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A list of all instances entity  {@link Client} in the data store is requested.
     * Checking that it is not empty and transforming all instances from the domain model
     * into a DTO model {@link ClientDto}.
     *
     * @return  list instances entity {@link ClientDto}, if data store not empty
     *          empty list, if the store does not contain data
     */
    @Override
    public List<ClientDto> getAllList() {
        List<Client> clientList =  clientRepository.findAll();
        if (clientList.isEmpty()) {
            return new ArrayList<>();
        } else {
            return clientList.stream()
                    .map(transformationService::transformationFromClient)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A list of  instances entity  {@link Client} in the data store is requested by given active status.
     * Checking that it is not empty and transforming all instances from the domain model
     * into a DTO model {@link ClientDto}.
     *
     * @param isActive - active status of requested clients
     * @return  list instances entity {@link ClientDto}, if data store not empty
     *          empty list, if the store does not contain data
     */
    @Override
    public List<ClientDto> getAllList(Boolean isActive) {
        List<Client> clientList =  clientRepository.findByIsActive(isActive);
        if (clientList.isEmpty()) {
            return new ArrayList<>();
        } else {
            return clientList.stream()
                    .map(transformationService::transformationFromClient)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A list of all instances entity  {@link Client} in the data store is requested.
     * Checking that it is not empty and transforming fields id and inn all instances
     * from the domain model into Map.
     *
     * @return  map with key id and value inn, if data store not empty
     *          empty map, if the store does not contain  data
     */

    @Override
    public Map<Long, String> getMapIdAndInn() {
        List<Client> clientList = clientRepository.findAll();
        if (clientList.isEmpty()) {
            return new HashMap<>();
        } else {
            return clientList.stream().collect(Collectors.toMap(Client::getId, Client::getInn));
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A list of all instances entity  {@link Client} in the data store is requested.
     * Checking that it is not empty and transforming all instances from the domain model
     * into a DTO model {@link ClientShortDto}.
     *
     * @return  list instances entity {@link ClientShortDto}, if data store not empty
     *          empty list, if the store does not contain  data
     */
    @Override
    public List<ClientShortDto> getShortFormAllList() {
        List<Client> clientList = clientRepository.findAll();
        if (clientList.isEmpty()) {
            return new ArrayList<>();
        } else {
            return clientList.stream()
                    .map(transformationService::transformationFromClientShort)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A list of  instances entity  {@link Client} in the data store is requested by given active status.
     * Checking that it is not empty and transforming all instances from the domain model
     * into a DTO model {@link ClientShortDto}.
     *
     * @param isActive - active status of requested clients
     * @return  list instances entity {@link ClientShortDto}, if data store not empty
     *          empty list, if the store does not contain data
     */
    @Override
    public List<ClientShortDto> getShortFormAllList(Boolean isActive) {
        List<Client> clientList = clientRepository.findByIsActive(isActive);
        if (clientList.isEmpty()) {
            return new ArrayList<>();
        } else {
            return clientList.stream()
                    .map(transformationService::transformationFromClientShort)
                    .collect(Collectors.toList());
        }
    }


    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A list of instances entity {@link Client} in the data store
     * with the specified pagination parameters  is requested.
     *
     * Checking that it is not empty and transforming all instances from the domain model
     * into a DTO model {@link ClientDto}.
     *
     * @return  list instances entity {@link ClientDto}, if data store not empty
     *          empty list, if the store does not contain  data
     */
    @Override
    public List<ClientDto> getPage(Pageable pageable) {
        Page<Client> clientPage =  clientRepository.findAll(pageable);
        if (clientPage.isEmpty()) {
            return new ArrayList<>();
        } else {
            return clientPage.get()
                    .map(transformationService::transformationFromClient)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A list of instances entity {@link Client} in the data store
     * with the specified pagination parameters and active status is requested.
     *
     * Checking that it is not empty and transforming all instances from the domain model
     * into a DTO model {@link ClientDto}.
     *
     * @return  list instances entity {@link ClientDto}, if data store not empty
     *          empty list, if the store does not contain  data
     */
    @Override
    public List<ClientDto> getPage(Pageable pageable, Boolean isActive) {
        Page<Client> clientPage = clientRepository.findByIsActive(isActive, pageable);
        if (clientPage.isEmpty()) {
            return new ArrayList<>();
        } else {
            return clientPage.get()
                    .map(transformationService::transformationFromClient)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A list of instances entity {@link Client} in the data store
     * with the specified pagination parameters  is requested.
     *
     * Checking that it is not empty and transforming all instances from the domain model
     * into a DTO model {@link ClientShortDto}.
     *
     * @return  list instances entity {@link ClientShortDto}, if data store not empty
     *          empty list, if the store does not contain  data
     */
    @Override
    public List<ClientShortDto> getShortFormPage(Pageable pageable) {
        Page<Client> clientPage = clientRepository.findAll(pageable);
        if (clientPage.isEmpty()) {
            return new ArrayList<>();
        } else {
             return clientPage.get()
                    .map(transformationService::transformationFromClientShort)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A list of instances entity {@link Client} in the data store
     * with the specified pagination parameters   and active status is requested.
     *
     * Checking that it is not empty and transforming all instances from the domain model
     * into a DTO model {@link ClientShortDto}.
     *
     * @return  list instances entity {@link ClientShortDto}, if data store not empty
     *          empty list, if the store does not contain  data
     */
    @Override
    public List<ClientShortDto> getShortFormPage(Pageable pageable, Boolean isActive) {
        Page<Client> clientPage = clientRepository.findByIsActive(isActive, pageable);
        if (clientPage.isEmpty()) {
            return new ArrayList<>();
        } else {
            return clientPage.get()
                    .map(transformationService::transformationFromClientShort)
                    .collect(Collectors.toList());
        }
    }


    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A list of instances entity {@link Client} in the data store
     * with the specified id distributor is requested.
     *
     * Before that the presence of the distributor with the specified id is checked
     *
     * After receiving the data - checking that it is not empty
     * and transforming all instances from the domain model into a DTO model {@link ClientDto}
     * only data related to distributor with specified id.
     *
     * If data not found in data store throw exception {@link ClientException}
     *
     * @return  list instances entity {@link ClientDto}, if data store not empty
     */
    @Override
    public List<ClientDto> getListByDistributorId(Long distributorId) {
        List<Client> clientList = clientRepository.findByDistributorId(distributorService.getDistributorById(distributorId));
        if (!clientList.isEmpty()) {
            return clientList.stream()
                    .map(client -> transformationService.transformationFromClientByDistributor(distributorId, client))
                    .collect(Collectors.toList());
        } else {
            throw new ClientException("List Client by id Distributor " + distributorId + " not found");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A list of instances entity {@link Client} in the data store
     * with the specified id distributor and active status is requested.
     *
     * Before that the presence of the distributor with the specified id is checked
     *
     * After receiving the data - checking that it is not empty
     * and transforming all instances from the domain model into a DTO model {@link ClientDto}
     * only data related to distributor with specified id.
     *
     * If data not found in data store throw exception {@link ClientException}
     *
     * @return  list instances entity {@link ClientDto}, if data store not empty
     */
    @Override
    public List<ClientDto> getListByDistributorId(Long distributorId, Boolean isActive) {
        List<Client> clientList = clientRepository.findByDistributorIdAndActive(distributorService.getDistributorById(distributorId), isActive);
        if (!clientList.isEmpty()) {
            return clientList.stream()
                    .map(client -> transformationService.transformationFromClientByDistributor(distributorId, client))
                    .collect(Collectors.toList());
        } else {
            throw new ClientException("List Client by id Distributor " + distributorId + " not found");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A list of instances entity {@link Client} in the data store
     * with the specified id distributor is requested.
     *
     * Before that the presence of the distributor with the specified id is checked
     *
     * After receiving the data - checking that it is not empty
     * and transforming all instances from the domain model into a DTO model {@link ClientShortDto}
     *
     * If data not found in data store throw exception {@link ClientException}
     *
     * @return  list instances entity {@link ClientShortDto}, if data store not empty
     */
    @Override
    public List<ClientShortDto> getShortFormByDistributorId(Long distributorId) {
        List<Client> clientList = clientRepository.findByDistributorId(distributorService.getDistributorById(distributorId));
        if (!clientList.isEmpty()) {
            return clientList.stream()
                    .map(transformationService::transformationFromClientShort)
                    .collect(Collectors.toList());
        } else {
            throw new ClientException("List Client by id Distributor " + distributorId + " not found");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A list of instances entity {@link Client} in the data store
     * with the specified id distributor and active status is requested.
     *
     * Before that the presence of the distributor with the specified id is checked
     *
     * After receiving the data - checking that it is not empty
     * and transforming all instances from the domain model into a DTO model {@link ClientShortDto}
     *
     * If data not found in data store throw exception {@link ClientException}
     *
     * @return  list instances entity {@link ClientShortDto}, if data store not empty
     */
    @Override
    public List<ClientShortDto> getShortFormByDistributorId(Long distributorId, Boolean isActive) {
        List<Client> clientList = clientRepository.findByDistributorIdAndActive(distributorService.getDistributorById(distributorId), isActive);
        if (!clientList.isEmpty()) {
            return clientList.stream()
                    .map(transformationService::transformationFromClientShort)
                    .collect(Collectors.toList());
        } else {
            throw new ClientException("List Client by id Distributor " + distributorId + " not found");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A {@link Page} of instances entity {@link Client} in the data store
     * with the specified id distributor and pagination parameters is requested.
     *
     * Before that the presence of the distributor with the specified id is checked
     *
     * After receiving the data - checking that it is not empty
     * and transforming all instances of page from the domain model into a DTO model {@link ClientDto}
     * only data related to distributor with specified id.
     *
     * If data not found in data store throw exception {@link ClientException}
     *
     * @return  list instances entity {@link ClientDto}, if data store not empty
     */
    @Override
    public List<ClientDto> getPageByDistributorId(Long distributorId, Pageable pageable) {
        Page<Client> clientPage = clientRepository
                .findByDistributorId(distributorService.getDistributorById(distributorId),  pageable);
        if (!clientPage.isEmpty()) {
            return clientPage.get()
                    .map(client -> transformationService.transformationFromClientByDistributor(distributorId, client))
                    .collect(Collectors.toList());
        } else {
            throw new ClientException("List Client by id Distributor " + distributorId + " not found");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A {@link Page} of instances entity {@link Client} in the data store
     * with the specified id distributor, active status and pagination parameters is requested.
     *
     * Before that the presence of the distributor with the specified id is checked
     *
     * After receiving the data - checking that it is not empty
     * and transforming all instances of page from the domain model into a DTO model {@link ClientDto}
     * only data related to distributor with specified id.
     *
     * If data not found in data store throw exception {@link ClientException}
     *
     * @return  list instances entity {@link ClientDto}, if data store not empty
     */
    @Override
    public List<ClientDto> getPageByDistributorId(Long distributorId, Pageable pageable, Boolean isActive) {
        Page<Client> clientPage = clientRepository
                .findByDistributorIdAndActive(distributorService.getDistributorById(distributorId), isActive,  pageable);
        if (!clientPage.isEmpty()) {
            return clientPage.get()
                    .map(client -> transformationService.transformationFromClientByDistributor(distributorId, client))
                    .collect(Collectors.toList());
        } else {
            throw new ClientException("List Client by id Distributor " + distributorId + " not found");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A {@link Page} of instances entity {@link Client} in the data store
     * with the specified id distributor and pagination parameters is requested.
     *
     * Before that the presence of the distributor with the specified id is checked
     *
     * After receiving the data - checking that it is not empty
     * and transforming all instances of page from the domain model into a DTO model {@link ClientShortDto}
     * only data related to distributor with specified id.
     *
     * If data not found in data store throw exception {@link ClientException}
     *
     * @return  list instances entity {@link ClientShortDto}, if data store not empty
     */
    @Override
    public List<ClientShortDto> getShortFormPageByDistributorId(Long distributorId, Pageable pageable) {
        Page<Client> clientPage = clientRepository
                .findByDistributorId(distributorService.getDistributorById(distributorId), pageable);
        if (!clientPage.isEmpty()) {
            return clientPage.get()
                    .map(transformationService::transformationFromClientShort)
                    .collect(Collectors.toList());
        } else {
            throw new ClientException("List Client by id Distributor " + distributorId + " not found");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A {@link Page} of instances entity {@link Client} in the data store
     * with the specified id distributor, active status and pagination parameters is requested.
     *
     * Before that the presence of the distributor with the specified id is checked
     *
     * After receiving the data - checking that it is not empty
     * and transforming all instances of page from the domain model into a DTO model {@link ClientShortDto}
     * only data related to distributor with specified id.
     *
     * If data not found in data store throw exception {@link ClientException}
     *
     * @return  list instances entity {@link ClientShortDto}, if data store not empty
     */
    @Override
    public List<ClientShortDto> getShortFormPageByDistributorId(Long distributorId, Pageable pageable, Boolean isActive) {
        Page<Client> clientPage = clientRepository
                .findByDistributorIdAndActive(distributorService.getDistributorById(distributorId), isActive, pageable);
        if (!clientPage.isEmpty()) {
            return clientPage.get()
                    .map(transformationService::transformationFromClientShort)
                    .collect(Collectors.toList());
        } else {
            throw new ClientException("List Client by id Distributor " + distributorId + " not found");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     *
     * Finding instance of entity {@link Client} in the data store by given inn.
     *
     * If not found in data store throw exception {@link ClientException}
     *
     * Checking that it is not empty and transforming the domain model into a DTO model {@link ClientDto}.
     *
     * @param inn - inn client for find.
     * @return  instance entity {@link ClientDto}, if finding data store not empty.
     */
    @Override
    public ClientDto getByInn(String inn) {
        if (inn != null) {
            Client client = clientRepository.findByInn(inn);
            if (client != null) {
                ClientDto clientDto = transformationService.transformationFromClient(client);
                return clientDto;
            } else throw new ClientException("Client with INN " + inn + " not found");
        } else {
            throw new ClientException("The inn of the client for the searching is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     *
     * Finding instance of entity {@link Client} in the data store by given inn.
     *
     * If not found in data store throw exception {@link ClientException}
     *
     * Checking that it is not empty and transforming the domain model into
     * a DTO model {@link ClientWithOnlySellOutSalesVolumeDto}.
     *
     * @param inn - inn client for find.
     * @return  instance entity {@link ClientDto}, if finding data store not empty.
     */
    @Override
    public ClientWithOnlySellOutSalesVolumeDto getWithOnlySalesVolumeByInn(String inn) {
        if (inn != null) {
            Client client = clientRepository.findByInn(inn);
            if (client != null) {
                ClientWithOnlySellOutSalesVolumeDto clientDto =
                        transformationService.transformationFromClientOnlySalesVolume(client);
                return clientDto;
            } else throw new ClientException("Client with INN " + inn + " not found");
        } else {
            throw new ClientException("The inn of the client for the searching is not set");
        }
    }

    @Override
    public Boolean existByInn(String inn) {
        if (inn != null) {
            return clientRepository.existsByInn(inn);
        } else {
            throw new ClientException("The inn of the client for the searching is not set");
        }
    }


    /**
     * Implementation of the method defined in the interface {@link ClientService}
     *
     * Finding instance of entity {@link Client} in the data store by given inn.
     *
     * If not found in data store throw exception {@link ClientException}
     *
     * Checking that it is not empty and active is true;
     * After that transforming the domain model into a DTO model {@link ClientDto}.
     *
     * @param inn - inn client for find.
     * @return  instance entity {@link ClientDto}, if finding data store not empty.
     */
    @Override
    public ClientDto getActiveClientByInn(String inn, Boolean forAdmin) {
        if (inn != null) {
            Client client = clientRepository.findByInn(inn);
            if (client != null) {
                if (client.isActive()) {
                    ClientDto clientDto = transformationService.transformationFromClient(client);
                    if (!forAdmin) {
                        this.clearClientForRoleNotAdmin(clientDto);
                    }
                    return clientDto;
                } else {
                    throw new ClientException("Клиент с ИНН " + inn + " не активен.");
                }
            } else throw new ClientException("Client with INN " + inn + " not found");
        } else {
            throw new ClientException("The inn of the client for the searching is not set");
        }
    }

    private void clearClientForRoleNotAdmin(ClientDto clientDto) {
        clientDto.setSalesVolume(null);
        clientDto.setSellOutSalesVolume(null);
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     *
     * Finding instance of entity {@link Client} in the data store by given inn.
     *
     * If not found in data store throw exception {@link ClientException}
     *
     * Checking that it is not empty and transforming the domain model into a DTO model {@link ClientShortDto}.
     *
     * @param inn - inn client for find.
     * @return  instance entity {@link ClientShortDto}, if finding data store not empty.
     */
    @Override
    public ClientShortDto getShortByInn(String inn) {
        if (inn != null) {
            Client client = clientRepository.findByInn(inn);
            if (client != null) {
                    return  transformationService.transformationFromClientShort(client);
            } else throw new ClientException("Client with INN " + inn + " not found");
        } else {
            throw new ClientException("The inn of the client for the searching is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     *
     * Finding instance of entity {@link Client} in the data store by given inn.
     *
     * If not found in data store throw exception {@link ClientException}
     *
     * Checking that it is not empty and active is true;
     * After that transforming the domain model into a DTO model {@link ClientShortDto}.
     *
     * @param inn - inn client for find.
     * @return  instance entity {@link ClientShortDto}, if finding data store not empty.
     */
    @Override
    public ClientShortDto getActiveClientShortByInn(String inn) {
        if (inn != null) {
            Client client = clientRepository.findByInn(inn);
            if (client != null) {
                if (client.isActive()) {
                    return  transformationService.transformationFromClientShort(client);
                } else {
                    throw new ClientException("Клиент с ИНН " + inn + " не активен.");
                }
            } else throw new ClientException("Client with INN " + inn + " not found");
        } else {
            throw new ClientException("The inn of the client for the searching is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     *
     * Finding instance of entity {@link Client} in the data store by given id.
     *
     * Checking that it is  empty throw exception {@link ClientException}
     *
     * @param id - id client in datastore for find.
     * @return  instance entity {@link Client}, if finding data store not empty.
     */
    @Override
    public Client getClientById(Long id) {
        if (id != null) {
            Optional<Client> clientOptional = clientRepository.findById(id);
            if (clientOptional.isPresent()) {
                return clientOptional.get();
            } else {
                throw new ClientException("Client with id " + id + "not found");
            }
        } else {
            throw new ClientException("The id of the client for the searching is not set");
        }
    }

    @Override
    public Client getClientByInn(String inn) {
        if (inn != null) {
            return clientRepository.findByInn(inn);
        } else {
            throw new ClientException("The id of the client for the searching is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     *
     * Finding instance of entity {@link Client} in the data store by given id.
     *
     * If not found in data store throw exception {@link ClientException}
     *
     * Checking that it is not empty and transforming the domain model into a DTO model {@link ClientDto}.
     *
     * @param id - id client in datastore for find.
     * @return  instance entity {@link ClientDto}, if finding data store not empty.
     */
    @Override
    public ClientDto getClientDtoById(Long id) {
        if (id != null) {
            Optional<Client> clientOptional = clientRepository.findById(id);
            if (clientOptional.isPresent()) {
                Client client = clientOptional.get();
                    ClientDto clientDto = transformationService.transformationFromClient(client);
                    return clientDto;
            } else {
                throw new ClientException("Client with id " + id + " not found");
            }
        } else {
            throw new ClientException("The id of the client for the searching is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     *
     * Finding instance of entity {@link Client} in the data store by given id.
     *
     * If not found in data store throw exception {@link ClientException}
     *
     * Checking that it is not empty and active is true;
     * After that transforming the domain model into a DTO model {@link ClientDto}.
     *
     * @param id - id client in datastore for find.
     * @return  instance entity {@link ClientDto}, if finding data store not empty.
     */
    @Override
    public ClientDto getActiveClientById(Long id, Boolean forAdmin) {
        if (id != null) {
            Optional<Client> clientOptional = clientRepository.findById(id);
            if (clientOptional.isPresent()) {
                Client client = clientOptional.get();
                if (client.isActive()) {
                    ClientDto clientDto = transformationService.transformationFromClient(client);
                    if (!forAdmin) {
                        clearClientForRoleNotAdmin(clientDto);
                    }
                    return clientDto;
                } else {
                    throw new ClientException("Клиент с ид " + id + " не активен.");
                }
            } else {
                throw new ClientException("Client with id " + id + " not found");
            }
        } else {
            throw new ClientException("The id of the client for the searching is not set");
        }
    }

    @Override
    public List<ClientDto> getListByIdIn(List<Long> clientsId) {
        List<Client> clientList = clientRepository.findByIdIn(clientsId);
        if (!clientList.isEmpty()) {
            return clientList.stream()
                    .map(transformationService::transformationFromClient)
                    .collect(Collectors.toList());
        } else {
            throw new ClientException("List Client by ids  " + clientsId.get(0) + " not found");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     * A list of instances entity  {@link Client} in the data store is requested
     * by given list of inn of clients.
     * Checking that it is not empty and transforming fields inn and instance
     * from the domain model into Map.
     *
     * @return  map with key id and value instance of client, if data store not empty
     *          empty map, if the store does not contain  data
     */

    @Override
    public Map<String, ClientDto> getMapInnAndClientByInnIn(List<String> clientsInn) {
        if (clientsInn != null) {
            List<Client> clientList = clientRepository.findByInnIn(clientsInn);
            if (clientList.isEmpty()) {
                return new HashMap<>();
            } else {
                return clientList.stream()
                        .map(client -> transformationService.transformationFromClient(client))
                        .collect(Collectors.toMap(ClientDto::getInn, client -> client));
            }
        }
        return null;
    }

    /**
     * Sending a request to create a connection between a client and a distributor
     * to a service {@link SalesVolumeService}
     *
     * @param clientId - the client ID that you want to associate with the distributor
     * @param distributorId - the distributor  ID that you want to associate with the client
     */

    @Override
    public void linkClientWithDistributor(Long clientId, Long distributorId) {
        salesVolumeService.linkClientWithDistributor(clientId, distributorId);
    }

    /**
     * Sending a request to verify the linked between the client and the distributor
     * with the specified identifiers to a service {@link SalesVolumeService}
     *
     * @param clientId - the client ID that you want checking to associate with the distributor
     * @param distributorId - the distributor  ID that you want checking to associate with the client
     * @return
     */
    @Override
    public boolean existLinkClientWithDistributor(Long clientId, Long distributorId) {
        return salesVolumeService.existLinkClientWithDistributor(clientId, distributorId);
    }

    @Override
    public List<ClientLinkedDto> getClientAllLink(Long id) {
        if (id == null) return null;

        List<Long> clientAllLinks = clientRepository.getClientAllLinks(id);

        if (clientAllLinks.isEmpty()) return new ArrayList<>();

        List<Client> listLinkedClients = clientRepository.findByIdIn(clientAllLinks);
        List<ClientLinkedDto> listClientDto = listLinkedClients.stream()
                .map(client -> transformationService.transformationFromClientLinked(client))
                .collect(Collectors.toList());
        return listClientDto;

    }

    /**
     * Create link between two Clients with given ids
     * - finding client with given id
     * - checking exist link between Clients
     * - if not exist, added client child in Set Client parent
     * - save Client parent in db
     *
     *
     * @param clientParentId - must not be {@literal null}
     * @param clientChildId - must not be {@literal null}
     * @return
     */
    @Override
    public ClientDto createLinkClientToClient(Long clientParentId, Long clientChildId) {
        if (clientParentId != null && clientChildId != null && !clientParentId.equals(clientChildId)) {
            Optional<Client> optionalClientParent = clientRepository.findById(clientParentId);
            Optional<Client> optionalClientChild = clientRepository.findById(clientChildId);
            if (optionalClientParent.isPresent() && optionalClientChild.isPresent()) {
                Client clientParent = optionalClientParent.get();
                if (!this.existLinkedClientToClient(clientParent, clientChildId)) {
                    clientParent.getClientChildren().add(optionalClientChild.get());
                    Client clientSaved = clientRepository.save(clientParent);
                    return transformationService.transformationFromClient(clientSaved);
                } else {
                    throw new ClientException("Link Client -" + clientParent.toStringShort()
                            + "  and Client - " + optionalClientChild.get().toStringShort() + " is existed");
                }
            } else {
                throw new ClientException("Client by id " + clientParentId
                        + "  or Client by id " + clientChildId + " not found in DB");
            }
        } else {
            throw new ClientException("Client by id " + clientParentId
                    + "  or Client by id " + clientChildId + " is not correct");
        }

    }

    /**
     * Checking exist link exist link between Clients
     * - check at Set of Client Parent contains client child by given id
     * - check at Set of Client Children contains client parent by given id
     *
     * @param client
     * @param clientId
     * @return
     */
    @Override
    public boolean existLinkedClientToClient(Client client, Long clientId) {
        boolean presentChild = checkInCollection(client.getClientChildren(), clientId);
        boolean presentParents = checkInCollection(client.getClientParents(), clientId);
        return presentChild || presentParents;
    }

    /**
     * Deleting link between two clients by given ids
     *
     * @param clientParentId - must not be {@literal null}
     * @param clientChildId - must not be {@literal null}
     * @return
     */
    @Override
    public ClientDto deleteLinkClientToClient(Long clientParentId, Long clientChildId) {
        if (clientParentId != null && clientChildId != null && !clientParentId.equals(clientChildId)) {
            Optional<Client> optionalClientParent = clientRepository.findById(clientParentId);
            Optional<Client> optionalClientChild = clientRepository.findById(clientChildId);
            if (optionalClientParent.isPresent() && optionalClientChild.isPresent()) {
                Client clientParent = optionalClientParent.get();
                Client clientChild = optionalClientChild.get();
                Set<Client> clientParentClientChildren = clientParent.getClientChildren();
                Set<Client> clientParentClientParents = clientParent.getClientParents();
                if (this.checkInCollection(clientParentClientChildren, clientChildId)) {
                     clientParent.setClientChildren(clientParentClientChildren
                            .stream()
                            .filter(client -> !client.getId().equals(clientChildId))
                            .collect(Collectors.toSet()));
                    clientChild.setClientParents(clientChild.getClientParents()
                            .stream()
                            .filter(client -> !client.getId().equals(clientParentId))
                            .collect(Collectors.toSet()));
                } else if (this.checkInCollection(clientParentClientParents, clientChildId)) {
                    clientParent.setClientParents(clientParentClientParents
                            .stream()
                            .filter(client -> !client.getId().equals(clientChildId))
                            .collect(Collectors.toSet()));
                    clientChild.setClientChildren(clientChild.getClientChildren()
                            .stream()
                            .filter(client -> !client.getId().equals(clientParentId))
                            .collect(Collectors.toSet()));
                } else {
                    throw new ClientException("Link Client -" + clientParent.toStringShort()
                            + "  and Client - " + clientChild.toStringShort() + " is not existed");
                }
                clientRepository.save(clientChild);
                Client clientSaved = clientRepository.save(clientParent);
                return transformationService.transformationFromClient(clientSaved);
            } else {
                throw new ClientException("Client by id " + clientParentId
                        + "  or Client by id " + clientChildId + " not found in DB");
            }
        } else {
            throw new ClientException("Client by id " + clientParentId
                    + "  or Client by id " + clientChildId + " is not correct");
        }
    }

    private boolean checkInCollection (Collection<Client> clients, Long id) {
        return clients.stream()
                .anyMatch(clientChild -> clientChild.getId().equals(id));
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     *
     * Saves given instance of entity {@link Client} in the data store.
     * Before saving:
     *    - the given entity instance is transformed from DTO to domain model
     *    - it checks that a client with the same inn is missing from the repository
     *      If exist in data store throw exception {@link ClientException}
     *
     * After saving:
     *  - set value for field salesVolume on result calling method to a service {@link SalesVolumeService}
     *  - reverse transformation of a saved entity instance into a DTO.
     *
     * @param clientDto - instance of entity {@link ClientDto} for saved.
     * @return  instance entity {@link ClientDto}, saved in data store.
     */
    @Override
    public ClientDto save(ClientDto clientDto) {
        if (clientDto.getInn() != null) {
            Client clientDb = transformationService.transformationFromClientDto(clientDto);
            if (!clientRepository.existsByInn(clientDb.getInn())) {
                if (clientDto.getClientLinks()!=null && !clientDto.getClientLinks().isEmpty()) {
                    List<Long> clientsIdLinked = clientDto.getClientLinks().stream()
                            .filter(clientLinkedDto -> !clientLinkedDto.getInn().equals(clientDb.getInn()))
                            .map(ClientLinkedDto::getId)
                            .collect(Collectors.toList());
                    List<Client> clientsList = clientRepository.findByIdIn(clientsIdLinked);
                    if (clientsList != null) {
                        clientDb.setClientChildren(new HashSet<>(clientsList));
                    }
                }
                Client clientSave = clientRepository.save(clientDb);
                if (clientDto.getSalesVolume() != null) {
                    clientSave.setSalesVolumes(salesVolumeService.operationsWithList(clientSave.getId(), clientDto.getSalesVolume()));
                }
                return transformationService.transformationFromClient(clientSave);
            } else {
                throw new ClientException("Client with inn " + clientDto.getInn() + "exist in DB");
            }
        } else {
            throw new ClientException("The inn of the client for saving is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     *
     * Updated fields given instance of entity {@link Client} in the data store.
     *
     * Finding instance entity {@link Client} by given name.
     * If it was found, given instance from parameter trans formatied to domain model
     * and setter new value in fields instance of entity from data store.
     * Value in field salesVolume set on result calling method to a service {@link SalesVolumeService}
     * After that update instanced of entity in data store
     *
     * If not exist in data store throw exception {@link ClientException}
     *
     *  Also, a message is generated for newsletter information about changes in the discount collection and sent via
     *  {@link PreparatorSenderService}
     *
     * @param clientDto - instance of entity {@link ClientDto} for updated.
     * @return  instance entity {@link ClientDto}, saved in data store.
     */
    @Override
    public ClientDto updateWithSendInfo(ClientDto clientDto, Client clientDb) {
        if (clientDto.getId() != null) {
            if (clientDb != null) {
                ClientDto oldClient = transformationService.transformationFromClient(clientDb);
                Client clientTemp = transformationService.transformationFromClientDto(clientDto);
                clientDb.setName(clientTemp.getName());
                clientDb.setPhone(clientTemp.getPhone());
                clientDb.setEmail(clientTemp.getEmail());
                clientDb.setAddress(clientTemp.getAddress());
                clientDb.setSite(clientTemp.getSite());
                clientDb.setClientType(clientTemp.getClientType());
                clientDb.setClientClassification(clientTemp.getClientClassification());
                clientDb.setActive(clientTemp.isActive());

                clientDb.setSalesVolumes(salesVolumeService.operationsWithList(clientTemp.getId(), clientDto.getSalesVolume()));
                updateClientLinks(clientDb, clientDto.getClientLinks());
                Client clientUpdated = clientRepository.save(clientDb);

                ClientDto newClient = transformationService.transformationFromClient(clientUpdated);

                //SEND MESSAGE TO DISTRIBUTORS ABOUT CHANGING CLIENT INFO
                if (clientUpdated != null && clientUpdated.getId() != null) {
                    preparatorSenderService.prepareChangeClientMessage(clientUpdated);
                }
                //SEND MESSAGE TO ADMINS ABOUT ADDING NEW DISTRIBUTES
                if (oldClient != null && clientUpdated != null && clientUpdated.getId() != null) {
                    preparatorSenderService.prepareChangeDistributorsInUser(newClient, oldClient);
                }
                return newClient;
            } else {
                throw new ClientException("Client with Id" + clientDto.getId() + ", inn " + clientDto.getInn()
                        + " and name " + clientDto.getName() + " not found for update");
            }
        }  else {
            throw new ClientException("The id of the client for the updating is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     *
     * Updated fields given instance of entity {@link Client} in the data store.
     *
     * Finding instance entity {@link Client} by given name.
     * If it was found, given instance from parameter trans formatied to domain model
     * and setter new value in fields instance of entity from data store.
     * Value in field salesVolume set on result calling method to a service {@link SalesVolumeService}
     * After that update instanced of entity in data store
     *
     * If not exist in data store throw exception {@link ClientException}
     *
     * @param clientDto - instance of entity {@link ClientDto} for updated.
     * @return  instance entity {@link ClientDto}, saved in data store.
     */
    @Override
    public ClientDto update(ClientDto clientDto, Client clientDb) {
        if (clientDto.getId() != null) {
            if (clientDb != null) {
                Client clientTemp = transformationService.transformationFromClientDto(clientDto);
                clientDb.setName(clientTemp.getName());
                clientDb.setPhone(clientTemp.getPhone());
                clientDb.setEmail(clientTemp.getEmail());
                clientDb.setAddress(clientTemp.getAddress());
                clientDb.setSite(clientTemp.getSite());
                clientDb.setClientType(clientTemp.getClientType());
                clientDb.setClientClassification(clientTemp.getClientClassification());
                clientDb.setActive(clientTemp.isActive());

                clientDb.setSalesVolumes(salesVolumeService.operationsWithList(clientTemp.getId(), clientDto.getSalesVolume()));
                updateClientLinks(clientDb, clientDto.getClientLinks());
                Client clientUpdated = clientRepository.save(clientDb);

                ClientDto newClient = transformationService.transformationFromClient(clientUpdated);

                return newClient;
            } else {
                throw new ClientException("Client with Id" + clientDto.getId() + ", inn " + clientDto.getInn()
                        + " and name " + clientDto.getName() + " not found for update");
            }
        }  else {
            throw new ClientException("The id of the client for the updating is not set");
        }
    }

    public void updateClientLinks(Client clientDb, List<ClientLinkedDto> clientLinks) {
        if (clientLinks != null && !clientLinks.isEmpty()) {
            clientRepository.deleteClientAllLinks(clientDb.getId());
            List<Long> clientsIdLinked = clientLinks.stream()
                    .filter(clientLinkedDto -> !clientLinkedDto.getId().equals(clientDb.getId()))
                    .map(ClientLinkedDto::getId)
                    .collect(Collectors.toList());
            List<Client> clientsList = clientRepository.findByIdIn(clientsIdLinked);
            if (clientsList != null) {
                clientDb.setClientChildren(new HashSet<>(clientsList));
            }
        } else {
            clientDb.setClientChildren(new HashSet<>());
        }

        clientDb.setClientParents(new HashSet<>());
    }


    @Override
    public boolean updateForCamundaApproveUpdateClient(ClientDto clientDto) {
        if (clientDto == null) return false;
        Client clientDbOld = clientRepository.findByInn(clientDto.getInn());
        if (clientDbOld == null) return false;
        clientDbOld.setClientType(this.parseType(clientDto.getClientTypeString()));
        updateClientLinks(clientDbOld, clientDto.getClientLinks());
        return update(clientDbOld);
    }

    private ClientType parseType(String clientType) {
        ClientType type = null;
        try {
            type = ClientType.getByString(clientType);
        } catch (Exception e) {
            log.error("Parse client type error, message: {}", e.getMessage());
        }
        return type;
    }

    @Override
    public boolean update(Client client){
        if(client != null){
            Client save = clientRepository.save(client);
            return save != null && client.getId() != null;
        }
        return false;
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     *
     * Mass update of field Type of given instance of entity {@link Client} in the data store.
     *
     * @param clientDtoList -  instance of entity {@link ClientDto} for updated.
     *                      Contains new value of type of Client and list ids of client in data store
     * @return - instance entity {@link ClientDto}, with new type.
     */
    @Override
    public List<ClientDto> updateListClientDtoByType(ClientsUpdateTypeDto clientDtoList) {
        ClientType clientType = ClientType.getByString(clientDtoList.getTypeClient());
        if (clientDtoList !=null && !clientDtoList.getClientsId().isEmpty() && clientType != null ) {
            List<Client> clientList = clientRepository.findByIdIn(clientDtoList.getClientsId());
            if (!clientList.isEmpty()) {
                clientList = clientList.stream().peek(e -> e.setClientType(clientType)).collect(Collectors.toList());
                clientList = clientRepository.saveAll(clientList);
                clientList.stream().forEach(preparatorSenderService::prepareChangeClientMessage);
                return clientList.stream()
                        .map(transformationService::transformationFromClient)
                        .collect(Collectors.toList());
            } else {
                throw new ClientException("Clients by ids not found in DB ");
            }
        } else {
            throw new ClientException("List Client is empty  or unknown Type Client + " + clientDtoList.getTypeClient());
        }
    }

    /**
     * Implementation of the method defined in the interface {@link ClientService}
     *
     * Deletes instance of entity {@link Client} by id in the data store.
     * Before deleted:
     *    - finding instance entity {@link Client} by given id.
     *      If it was found, it is deleted to the data store and return true
     *      otherwise false
     *
     * @param id - id client for deleted.
     * @return boolean answer from result of the delete operation
     */
    @Override
    public boolean delete(Long id) {
        if (id != null ) {
            Optional<Client> clientOptional = clientRepository.findById(id);
            if (clientOptional.isPresent()) {
                salesVolumeService.deleteByClientId(id);
                clientRepository.deleteClientAllLinks(id);
                clientRepository.delete(clientOptional.get());
                return true;
            }
            return false;
        } else {
            throw new ClientException("The id of the client  for the deleting is not set");
        }
    }

    /**
     * Scheduled method update value of SellOut Sales volume
     * - determining the current and previous year
     * - getting all clients for DB;
     * - transformation list of client to map with key inn and value - instance of client equals this inn;
     * - extract inn of clients for map for search;
     * - requested data in {@link ClientAnnualSalesService};
     * - the resulting sales volume data from SellOut is passed to the corresponding instance
     *   of the {@link SellOutSalesVolume} entity found by the inn and added to the list
     * - the resulting list with updated data is written to the database
     */
    @Scheduled(cron = "0 0 8 ? * MON-FRI")
    @Transactional
    void updateSalesVolumeFromSellOut() {
        log.info("---Scheduled task by update sales volume from SellOut----");
        log.info("----------------------START------------------------------");
        Integer firstYear =  Year.now().minusYears(1).getValue();
        Integer secondYear =  Year.now().getValue();
        List<Integer> years = Arrays.asList(firstYear, secondYear);

        log.info("---GET Client Data for search in SellOut---------------");
        List<Client> clientList = clientRepository.findAll();
        Map<@NonNull String, Client> clientDtoMap = clientList.stream()
                .filter(client -> client.getInn() != null)
                .collect(Collectors.toMap(Client::getInn, client -> client, (client1, client2) -> client1));

        log.info("---GET NEW SALES FROM SellOut----------------------------");
        List<String> listInnClient= new ArrayList<>(clientDtoMap.keySet());
        Map<String, List<ClientAnnualSalesDto>> salesByListInnClientAndListYears = clientAnnualSalesService
                .getSalesByListInnClientAndListYears(listInnClient, years);

        log.info("---Process Update Sales Volume into Client----------------");
        clientList.clear();

        for (Map.Entry<String, List<ClientAnnualSalesDto>> saleClient : salesByListInnClientAndListYears.entrySet()) {
            Client client = clientDtoMap.get(saleClient.getKey());
            Set<SellOutSalesVolume> sellOutSalesVolumes = new HashSet<>();
            SellOutSalesVolume sellOutSalesVolume;
            Long saleYear;
            for (ClientAnnualSalesDto clientAnnualSalesDto: saleClient.getValue()) {
                sellOutSalesVolume = SellOutSalesVolume.builder()
                        .partnerName(clientAnnualSalesDto.getNamePartner())
                        .build();
                saleYear = clientAnnualSalesDto.getSaleYears().get(firstYear);
                if (saleYear != null) {
                    sellOutSalesVolume.setVolumeFirstYear(saleYear);
                }
                saleYear = clientAnnualSalesDto.getSaleYears().get(secondYear);
                if (saleYear != null) {
                    sellOutSalesVolume.setVolumeSecondYear(saleYear);
                }
                sellOutSalesVolumes.add(sellOutSalesVolume);
            }
            client.setSellOutSalesVolumes(sellOutSalesVolumes);
            clientList.add(client);
        }

        log.info("---Saves in DB Updated Sales Volume----------------------");
        clientRepository.saveAll(clientList);
        log.info("-----------------------END-------------------------------");
    }

    public boolean updateFromSellOut() {
        try {
            updateSalesVolumeFromSellOut();
            return true;
        }  catch (Throwable e) {
            throw new ClientException(e.getMessage());
        }
    }
}
