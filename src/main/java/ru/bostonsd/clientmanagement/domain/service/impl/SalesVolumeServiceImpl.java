package ru.bostonsd.clientmanagement.domain.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bostonsd.clientmanagement.domain.exception.SalesVolumeException;
import ru.bostonsd.clientmanagement.domain.models.Client;
import ru.bostonsd.clientmanagement.domain.models.ClientDistributorKey;
import ru.bostonsd.clientmanagement.domain.models.Distributor;
import ru.bostonsd.clientmanagement.domain.models.SalesVolume;
import ru.bostonsd.clientmanagement.domain.models.dto.SalesVolumeClientDto;
import ru.bostonsd.clientmanagement.domain.models.dto.SalesVolumeDto;
import ru.bostonsd.clientmanagement.domain.repository.SalesVolumeRepository;
import ru.bostonsd.clientmanagement.domain.service.ClientService;
import ru.bostonsd.clientmanagement.domain.service.DistributorService;
import ru.bostonsd.clientmanagement.domain.service.SalesVolumeService;
import ru.bostonsd.clientmanagement.domain.service.TransformationService;
import ru.bostonsd.clientmanagement.sellout.service.ClientAnnualSalesService;
import ru.bostonsd.clientmanagement.smtp.service.PreparatorSenderService;

import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Implementation for {@link SalesVolumeService}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 5, 2019
 * Updated on February 25, 2020 | Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 */

@Slf4j
@Service
public class SalesVolumeServiceImpl implements SalesVolumeService {

    @Autowired
    private SalesVolumeRepository salesVolumeRepository;

    @Autowired
    private TransformationService transformationService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private DistributorService distributorService;

    @Autowired
    private PreparatorSenderService preparatorSenderService;


    public SalesVolumeServiceImpl(SalesVolumeRepository salesVolumeRepository,
                                  TransformationService transformationService,
                                  ClientService clientService,
                                  DistributorService distributorService) {
        this.salesVolumeRepository = salesVolumeRepository;
        this.transformationService = transformationService;
        this.clientService = clientService;
        this.distributorService = distributorService;
    }

    /**
     * Implementation of the method defined in the interface {@link SalesVolumeService}
     * A list of instances entity {@link SalesVolume} in the data store
     * with the specified id client is requested.
     * <p>
     * After receiving the data - checking that it is not empty
     * and transforming list of instances from the domain model into a DTO model {@link SalesVolumeClientDto}
     *
     * @param clientId - id client for find  in data store.
     * @return list instances entity {@link SalesVolumeClientDto}, if data store not empty
     * empty list, if the store does not contain classification data
     */
    @Override
    public List<SalesVolumeClientDto> getListByClientId(Long clientId) {
        if (clientId != null) {
            List<SalesVolume> salesVolumeList = salesVolumeRepository.findByClientId(clientId);
            if (salesVolumeList.isEmpty()) {
                return new ArrayList<>();
            } else {
                return salesVolumeList.stream()
                        .map(transformationService::transformationFromSalesVolumeClient)
                        .collect(Collectors.toList());
            }
        } else {
            throw new SalesVolumeException("The id of the Client for the sales volume searching  is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link SalesVolumeService}
     * A list of instances entity {@link SalesVolume} in the data store
     * with the specified id distributor is requested.
     * <p>
     * After receiving the data - checking that it is not empty
     * and transforming list of instances from the domain model into a DTO model {@link SalesVolumeDto}
     *
     * @param distributorId - id distributor for find  in data store.
     * @return list instances entity {@link SalesVolumeDto}, if data store not empty
     * empty list, if the store does not contain data
     */
    @Override
    public List<SalesVolumeDto> getListByDistributorId(Long distributorId) {
        if (distributorId != null) {
            List<SalesVolume> salesVolumeList = salesVolumeRepository.findByDistributorId(distributorId);
            if (salesVolumeList.isEmpty()) {
                return new ArrayList<>();
            } else {
                return salesVolumeList.stream()
                        .map(transformationService::transformationFromSalesVolume)
                        .collect(Collectors.toList());
            }
        } else {
            throw new SalesVolumeException("The id of the Distributor for the sales volume searching  is not set");
        }

    }

    /**
     * Implementation of the method defined in the interface {@link SalesVolumeService}
     * <p>
     * Finding instance of entity {@link SalesVolume} in the data store by given id client a nd distributor.
     * <p>
     * If not found in data store throw exception {@link SalesVolumeException}
     * <p>
     * Checking that it is not empty and transforming the domain model into a DTO model {@link SalesVolumeDto}.
     *
     * @param clientId      - id client for find  in data store.
     * @param distributorId - id distributor for find  in data store.
     * @return instance entity {@link SalesVolumeDto}, if finding data store not empty.
     */
    @Override
    public SalesVolumeDto getByClientIdAndDistributorId(Long clientId, Long distributorId) {
        if (distributorId != null && clientId != null) {
            SalesVolume salesVolume = salesVolumeRepository.findByClientIdAndDistributorId(clientId, distributorId);
            if (salesVolume != null) {
                return transformationService.transformationFromSalesVolume(salesVolume);
            } else {
                throw new SalesVolumeException("Sales Volume by id Client " + clientId
                        + " and id Distributor " + distributorId + "  not found");
            }
        } else {
            throw new SalesVolumeException("The id of the Distributor or Client for the sales volume searching is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link SalesVolumeService}
     * <p>
     * Checking given ids client and distributor that are not empty.
     * If empty in data store throw exception {@link SalesVolumeException}
     * <p>
     * Checking existence of instance of entity {@link SalesVolume} in the data store
     * with given id client a nd distributor.
     *
     * @param clientId      - id client for find  in data store.
     * @param distributorId - id distributor for find  in data store.
     * @return boolean answer from the result of the check operation
     */

    @Override
    public boolean existLinkClientWithDistributor(Long clientId, Long distributorId) {
        if (clientId != null && distributorId != null) {
            return salesVolumeRepository.existsByClientIdAndDistributorId(clientId, distributorId);
        } else {
            throw new SalesVolumeException("Sales Volume by id Client " + clientId
                    + "  or id Distributor " + distributorId + " is null");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link SalesVolumeService}
     * <p>
     * Creating instance entity {@link SalesVolume} with given ids client a nd distributor.
     * <p>
     * Before created:
     * - Checking given ids client and distributor that are not empty.
     * - Not existence of instance of entity {@link SalesVolume} in the data store
     * - query entity instances {@link Client} and @ {@link Distributor} by id
     *
     * @param clientId      - id client for find  in data store.
     * @param distributorId - id distributor for find  in data store.
     */
    @Override
    public void linkClientWithDistributor(Long clientId, Long distributorId) {
        if (clientId != null && distributorId != null) {
            if (!existLinkClientWithDistributor(clientId, distributorId)) {
                Client client = clientService.getClientById(clientId);
                Distributor distributor = distributorService.getDistributorById(distributorId);
                if (client != null && distributor != null) {
                    SalesVolume salesVolume = SalesVolume.builder()
                            .id(ClientDistributorKey.builder()
                                    .clientId(client.getId())
                                    .distributorId(distributor.getId())
                                    .build())
                            .client(client)
                            .distributor(distributor)
                            .build();
                    SalesVolume saved = salesVolumeRepository.save(salesVolume);
                    if (saved != null) {
                        preparatorSenderService.prepareCreateLinkClientAndDist(client, distributor);
                    }
                } else {
                    throw new SalesVolumeException("Client by id=" + clientId
                            + "  or Distributor by id=" + distributorId + " is found");
                }
            }
        } else {
            throw new SalesVolumeException("Sales Volume by id Client " + clientId
                    + "  or id Distributor " + distributorId + " is null");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link SalesVolumeService}
     * <p>
     * Saves given instance of entity {@link SalesVolume} in the data store.
     * Before saving:
     * - the given entity instance is transformed from DTO to domain model
     * - it checks that a sales volume with the same client and distributor ids
     * is missing from the repository.
     * If exist in data store throw exception {@link SalesVolumeException}
     * <p>
     * After saving reverse transformation of a saved entity instance into a DTO.
     *
     * @param salesVolumeDto - instance of entity {@link SalesVolumeDto} for saved.
     * @return instance entity {@link SalesVolumeDto}, saved in data store.
     */
    @Override
    public SalesVolumeDto save(SalesVolumeDto salesVolumeDto) {
        if (salesVolumeDto.getIdClient() != null && salesVolumeDto.getIdDistributor() != null) {
            SalesVolume salesVolume = transformationService.transformationFromSalesVolumeDto(salesVolumeDto);
            SalesVolume salesVolumeDb = salesVolumeRepository
                    .findByClientIdAndDistributorId(salesVolume.getClient().getId(), salesVolume.getDistributor().getId());

            if (salesVolumeDb == null) {
                salesVolume = salesVolumeRepository.save(salesVolume);
                return transformationService.transformationFromSalesVolume(salesVolume);
            } else {
                throw new SalesVolumeException("Sales Volume by id Client " + salesVolume.getClient().getId()
                        + "  and id Distributor " + salesVolume.getDistributor().getId() + " exist in DB");
            }
        } else {
            throw new SalesVolumeException("The id of the Distributor or Client for the sales volume saving is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link SalesVolumeService}
     * <p>
     * Updated fields given instance of entity {@link SalesVolume} in the data store.
     * <p>
     * Finding instance entity {@link SalesVolume} by given client and distributor ids.
     * If it was found, given instance from parameter trans formatied to domain model
     * and setter new value in fields instance of entity from data store.
     * After that update instanced of entity in data store
     * <p>
     * If not exist in data store throw exception {@link SalesVolumeException}
     *
     * @param salesVolumeDto - instance of entity {@link SalesVolumeDto} for updated.
     * @return instance entity {@link SalesVolumeDto}, saved in data store.
     */
    @Override
    public SalesVolumeDto update(SalesVolumeDto salesVolumeDto) {
        if (salesVolumeDto.getIdClient() != null && salesVolumeDto.getIdDistributor() != null) {
            SalesVolume salesVolume = transformationService.transformationFromSalesVolumeDto(salesVolumeDto);
            SalesVolume salesVolumeDb = salesVolumeRepository
                    .findByClientIdAndDistributorId(salesVolume.getClient().getId(), salesVolume.getDistributor().getId());

            if (salesVolumeDb != null) {
                salesVolumeDb = salesVolumeRepository.save(salesVolumeDb);
                return transformationService.transformationFromSalesVolume(salesVolumeDb);
            } else {
                throw new SalesVolumeException("Sales Volume by id Client " + salesVolume.getClient().getId()
                        + "  and id Distributor " + salesVolume.getDistributor().getId() + " not found for update");
            }
        } else {
            throw new SalesVolumeException("The id of the Distributor or Client for the sales volume updating is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link SalesVolumeService}
     * <p>
     * Deletes instance of entity {@link SalesVolume} by client and distributor ids in the data store.
     * Before deleted:
     * - finding instance entity {@link SalesVolume} by given client and distributor  ids.
     * If it was found, it is deleted to the data store and return true,
     * otherwise false
     *
     * @param clientId      - id client for find in data store.
     * @param distributorId - id distributor for find  in data store.
     * @return boolean answer from result of the delete operation
     */
    @Override
    public boolean delete(Long clientId, Long distributorId) {
        if (clientId != null && distributorId != null) {
            SalesVolume salesVolume = salesVolumeRepository.findByClientIdAndDistributorId(clientId, distributorId);
            if (salesVolume != null) {
                salesVolumeRepository.delete(salesVolume);
                return true;
            } else {
                return false;
            }
        } else {
            throw new SalesVolumeException("The id of the Distributor or Client for the sales volume deleting is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link SalesVolumeService}
     * <p>
     * Deletes list of instances entity {@link SalesVolume} by client id in the data store.
     * Before deleted:
     * - finding list instance of entity {@link SalesVolume} by given client and distributor  ids.
     * If it was found, it is deleted to the data store and return true,
     * otherwise false
     *
     * @param clientId - id client for find in data store.
     * @return boolean answer from result of the delete operation
     */
    @Override
    public boolean deleteByClientId(Long clientId) {
        List<SalesVolume> salesVolumeListDb = salesVolumeRepository.findByClientId(clientId);
        if (salesVolumeListDb != null && !salesVolumeListDb.isEmpty()) {
            salesVolumeRepository.deleteAll(salesVolumeListDb);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Implementation of the method defined in the interface {@link SalesVolumeService}
     * <p>
     * Saves/updated given list of instance entity {@link SalesVolume} in the data store for client with given id
     * <p>
     * Before saving:
     * - checking list of instance entity {@link SalesVolumeClientDto} not null and not empty.
     * If it is empty or null  return empty list;
     * - the given list of instance entity is transformed from DTO to list of domain model
     * - request a list of entity instances by client ID in data store.
     * if not empty and not null, it is deleted to the data store
     * <p>
     * After saving reverse transformation of a saved entity instance into a DTO.
     *
     * @param clientId                 - id client for saved sales volume in data store.
     * @param salesVolumeClientDtoList - instance of entity {@link SalesVolumeClientDto} for saved.
     * @return instance entity {@link SalesVolume}, saved/updated in data store.
     */
    @Override
    public List<SalesVolume> operationsWithList(Long clientId, List<SalesVolumeClientDto> salesVolumeClientDtoList) {
        if (salesVolumeClientDtoList != null && !salesVolumeClientDtoList.isEmpty()) {
            List<SalesVolume> salesVolumeClient = salesVolumeClientDtoList.stream()
                    .map(salesVolumeClientDto -> transformationService.transformationFromClientSalesVolumeDto(clientId, salesVolumeClientDto))
                    .collect(Collectors.toList());

            List<SalesVolume> salesVolumeListDb = salesVolumeRepository.findByClientId(clientId);

            if (salesVolumeListDb != null && !salesVolumeListDb.isEmpty()) {
                salesVolumeRepository.deleteAll(salesVolumeListDb);
            }
            if (!salesVolumeClient.isEmpty()) {
                salesVolumeClient = salesVolumeRepository.saveAll(salesVolumeClient);
            }
            return salesVolumeClient;
        } else {
            return new ArrayList<>();
        }
    }


}
