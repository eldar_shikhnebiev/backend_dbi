package ru.bostonsd.clientmanagement.domain.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.bostonsd.clientmanagement.domain.models.File;
import ru.bostonsd.clientmanagement.domain.models.dto.FileDto;
import ru.bostonsd.clientmanagement.domain.repository.FileRepository;
import ru.bostonsd.clientmanagement.domain.service.FileService;

import java.io.IOException;

/**
 * Implementation for {@link FileService}
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 5, 2019
 */
@Service
@Slf4j
public class FileServiceImpl implements FileService {

    @Autowired
    private FileRepository fileRepository;

    /**
     * Implementation of the method defined in the interface {@link FileService}
     *
     * Saves given instance of entity {@link File} in the data store.
     * Before saving create instance entity {@link File}
     *
     * After saving reverse transformation of a saved entity instance into a DTO.
     *
     * @param file - instance of entity {@link MultipartFile} for saved.
     * @return  instance entity {@link FileDto}, saved in data store.
     */

    @Override
    public FileDto save(MultipartFile file) {
        try {
            File result = fileRepository.save(File.builder()
                    .name(file.getOriginalFilename())
                    .type(file.getContentType())
                    .value(file.getBytes())
                    .build());
            return FileDto.builder().type(result.getType()).name(result.getName()).id(result.getId()).build();
        } catch (IOException e) {
            log.error("PERSIST FILE ERROR: {}", e.getMessage());
        }
        return null;
    }

    /**
     * Implementation of the method defined in the interface {@link FileService}
     *
     * Finding instance of entity {@link File} in the data store by given id.
     *
     * @param id - id distributor in datastore for find.
     * @return  instance entity {@link File}.
     */
    @Override
    public File getFileById(Long id) {
        return fileRepository.findById(id).get();
    }

    /**
     * Implementation of the method defined in the interface {@link FileService}
     *
     * Deletes instance of entity {@link File} by id in the data store.
     * Before deleted:
     *    - finding instance entity {@link File} by given id.
     *      If it was found, it is deleted to the data store and return true
     *      otherwise false
     *
     * @param id - id file for deleted.
     * @return boolean answer from result of the delete operation
     */
    @Override
    public boolean delete(Long id) {
        File file = fileRepository.getOne(id);
        if(file != null) {
            fileRepository.delete(file);
            return true;
        }
        return false;
    }

}
