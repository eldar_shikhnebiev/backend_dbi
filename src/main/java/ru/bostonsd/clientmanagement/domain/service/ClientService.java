package ru.bostonsd.clientmanagement.domain.service;

import org.springframework.data.domain.Pageable;
import ru.bostonsd.clientmanagement.domain.models.Client;
import ru.bostonsd.clientmanagement.domain.models.dto.*;

import java.util.List;
import java.util.Map;

/**
 * Service for managing the entity {@link Client} using the Dto format {@link ClientDto}, {@link ClientShortDto}
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 3, 2019
 * Updated on May 21, 2020 | vkondaurov
 */

public interface ClientService {
    /**
     * All instances of the entity {@link Client} in format DTO {@link ClientDto}
     *
     * @return all instances current entity
     */
    List<ClientDto> getAllList();

    /**
     * All instances of the entity {@link Client} in format DTO {@link ClientDto}
     * with the given active status
     *
     * @param isActive - must be true/false
     * @return all instances current entity
     */
    List<ClientDto> getAllList(Boolean isActive);

    /**
     * All instances of the entity {@link Client} in format Map include Id (key) and  Inn (value)
     *
     * @return Map
     */
    Map<Long, String> getMapIdAndInn();

    /**
     * All instances of the entity {@link Client} in format DTO {@link ClientShortDto}
     *
     * @return all instances current entity
     */
    List<ClientShortDto> getShortFormAllList();

    /**
     * All instances of the entity {@link Client} in format DTO {@link ClientShortDto}
     * with the given active status
     * @param isActive - must be true/false
     * @return all instances current entity
     */

    List<ClientShortDto> getShortFormAllList(Boolean isActive);


    /**
     * List instances of the entity {@link Client} in format DTO {@link ClientDto}
     * with the given pagination parameters
     *
     * @param pageable - must not be {@literal null}
     * @return list instances current entity
     */
    List<ClientDto> getPage(Pageable pageable);

    /**
     * List of instances of the entity {@link Client} in format DTO  {@link ClientDto}
     * with the given pagination parameters and active status
     *
     * @param pageable - must not be {@literal null}
     * @param isActive - must be true/false
     * @return list instances current entity
     */
    List<ClientDto> getPage(Pageable pageable, Boolean isActive);


    /**
     * List instances of the entity {@link Client} in format DTO  {@link ClientShortDto}
     * with the given pagination parameters
     *
     * @param pageable - must not be {@literal null}
     * @return list instances current entity
     */

    List<ClientShortDto> getShortFormPage(Pageable pageable);

    /**
     * List of instances of the entity {@link Client} in format DTO  {@link ClientShortDto}
     * with the given pagination parameters and active status
     *
     * @param pageable - must not be {@literal null}
     * @param isActive - must be true/false
     * @return list instances current entity
     */
    List<ClientShortDto> getShortFormPage(Pageable pageable, Boolean isActive);

    /**
     * List instances of the entity {@link Client} in format DTO {@link ClientDto}
     * with the given distributor id
     *
     * @param distributorId - must not be {@literal null}
     * @return list instances current entity
     */
    List<ClientDto> getListByDistributorId(Long distributorId);

    /**
     * List of instances of the entity {@link Client} in format DTO {@link ClientDto}
     * with the given distributor id and active status
     *
     * @param distributorId - must not be {@literal null}
     * @param isActive - must be true/false
     * @return list instances current entity
     */
    List<ClientDto> getListByDistributorId(Long distributorId, Boolean isActive);

    /**
     * List instances of the entity {@link Client} in format DTO {@link ClientShortDto}
     * with the given distributor id
     *
     * @param distributorId - must not be {@literal null}
     * @return list instances current entity
     */
    List<ClientShortDto> getShortFormByDistributorId(Long distributorId);

    /**
     * List of instances of the entity {@link Client} in format DTO {@link ClientShortDto}
     * with the given distributor id and active status
     *
     * @param distributorId - must not be {@literal null}
     * @param isActive - must be true/false
     * @return list instances current entity
     */
    List<ClientShortDto> getShortFormByDistributorId(Long distributorId, Boolean isActive);

    /**
     * List instances of the entity {@link Client} in format DTO {@link ClientDto}
     * with the given distributor id and pagination parameters
     *
     * @param distributorId - must not be {@literal null}
     * @param pageable - must not be {@literal null}
     * @return list instances current entity
     */
    List<ClientDto> getPageByDistributorId(Long distributorId, Pageable pageable);

    /**
     * List of instances of the entity {@link Client} in format DTO {@link ClientDto}
     * with the given distributor id, pagination parameters and active status
     *
     * @param distributorId - must not be {@literal null}
     * @param pageable - must not be {@literal null}
     * @param isActive - must be true/false
     * @return list instances current entity
     */
    List<ClientDto> getPageByDistributorId(Long distributorId, Pageable pageable, Boolean isActive);

    /**
     * List instances of the entity {@link Client} in format DTO {@link ClientShortDto}
     * with the given distributor id and pagination parameters
     *
     * @param distributorId - must not be {@literal null}
     * @param pageable - must not be {@literal null}
     * @return list instances current entity
     */
    List<ClientShortDto> getShortFormPageByDistributorId(Long distributorId, Pageable pageable);

    /**
     * List of instances of the entity {@link Client} in format DTO {@link ClientShortDto}
     * with the given distributor id, pagination parameters and active status
     *
     * @param distributorId - must not be {@literal null}
     * @param pageable - must not be {@literal null}
     * @param isActive - must be true/false
     * @return list instances current entity
     */
    List<ClientShortDto> getShortFormPageByDistributorId(Long distributorId, Pageable pageable, Boolean isActive);

    /**
     * Retrieves an entity {@link Client} in format DTO {@link ClientDto}
     * by  the given inn of client
     *
     * @param inn - must not be {@literal null}
     * @return the current entity with the given inn or {@literal null} if none found
     */
    ClientDto getByInn(String inn);


    /**
     * Retrieves an entity {@link Client} in format DTO {@link ClientWithOnlySellOutSalesVolumeDto}
     * by  the given inn of client
     *
     * @param inn - must not be {@literal null}
     * @return the current entity with the given inn or {@literal null} if none found
     */
    ClientWithOnlySellOutSalesVolumeDto getWithOnlySalesVolumeByInn(String inn);

    /**
     * Checking exist instance of entity {@link Client} by  the given inn of client
     *
     * @param inn - must not be {@literal null}
     * @return the current entity with the given id or {@literal null} if none found
     */
    Boolean existByInn(String inn);

    /**
     * Retrieves an instance of entity {@link Client}  with active status  in format DTO {@link ClientDto}
     * by  the given inn of client
     *
     * @param inn - must not be {@literal null}
     * @param forAdmin - must be true/false
     * @return the current entity with the given inn or {@literal null} if none found
     */
    ClientDto getActiveClientByInn(String inn, Boolean forAdmin);

    /**
     * Retrieves an entity {@link Client} in format DTO {@link ClientShortDto}
     * by  the given inn of client
     *
     * @param inn - must not be {@literal null}
     * @return the current entity with the given inn or {@literal null} if none found
     */
    ClientShortDto getShortByInn(String inn);

    /**
     * Retrieves an instance of entity {@link Client}  with active status  in format DTO {@link ClientShortDto}
     * by  the given inn of client
     *
     * @param inn - must not be {@literal null}
     * @return the current entity with the given inn or {@literal null} if none found
     */
    ClientShortDto getActiveClientShortByInn(String inn);

    /**
     * Retrieves an entity {@link Client} in format DTO {@link ClientDto}
     * by  the given id of client
     *
     * @param id - must not be {@literal null}
     * @return the current entity with the given id or {@literal null} if none found
     */
    ClientDto getClientDtoById(Long id);

    /**
     * Retrieves an instance of entity {@link Client}  with active status  in format DTO {@link ClientDto}
     * by  the given id of client
     *
     * @param id - must not be {@literal null}
     * @param forAdmin - must be true/false
     * @return the current entity with the given id or {@literal null} if none found
     */
    ClientDto getActiveClientById(Long id, Boolean forAdmin);


    /**
     * Retrieves an list of entity {@link Client} in format DTO {@link ClientDto}
     * by  the given list id of clients
     *
     * @param clientsId - must not be {@literal null}
     * @return the current list of entities with the given ids or {@literal null} if none found
     */

    List<ClientDto> getListByIdIn(List<Long> clientsId);


    /**
     * Retrieves instances of the entity {@link Client} in format Map include Id (key) and  ClientDto (value)
     * by  the given list of inns of clients
     *
     * @return Map
     */
    Map<String, ClientDto> getMapInnAndClientByInnIn(List<String> clientsInn);

    /**
     * Retrieves an entity {@link Client} by  the given id of client
     *
     * @param id - must not be {@literal null}
     * @return the current entity with the given id or {@literal null} if none found
     */
    Client getClientById(Long id);

    /**
     * Retrieves an entity {@link Client} by  the given inn of client
     *
     * @param inn - must not be {@literal null}
     * @return the current entity with the given id or {@literal null} if none found
     */
    Client getClientByInn(String inn);
    /**
     * Adding the binding for entity {@link Client} by the specified id with Distributor by the given id
     *
     * @param clientId - must not be {@literal null}
     * @param distributorId - must not be {@literal null}
     */
    void linkClientWithDistributor(Long clientId, Long distributorId);


    /**
     * Checking the binding to entity {@link Client} with the specified id, Distributor by  the given id
     *
     * @param clientId - must not be {@literal null}
     * @param distributorId - must not be {@literal null}
     * @return - true if link between client and distributor  exist,
     *           false if - not;
     */

    boolean existLinkClientWithDistributor(Long clientId, Long distributorId);

    List<ClientLinkedDto> getClientAllLink (Long id);

    /**
     * Adding the binding for entity {@link Client} by the specified id with other Client by the given id
     *
     * @param clientParentId - must not be {@literal null}
     * @param clientChildId - must not be {@literal null}
     */
    ClientDto createLinkClientToClient(Long clientParentId, Long clientChildId);

    /**
     * Deleting the binding for entity {@link Client} by the specified id with other Client by the given id
     *
     * @param clientParentId - must not be {@literal null}
     * @param clientChildId - must not be {@literal null}
     */
    ClientDto deleteLinkClientToClient(Long clientParentId, Long clientChildId);

    /**
     * Checking the binding to entity {@link Client} with the specified id with other Client by  the given id
     *
     * @param clientParentId - must not be {@literal null}
     * @param clientChildId - must not be {@literal null}
     * @return - true if link between client exist,
     *           false if - not;
     */
    boolean existLinkedClientToClient(Client clientParentId, Long clientChildId);

    /**
     * Saves a given entity. Use the returned instance entity {@link Client} in format DTO {@link ClientDto}
     *
     * @param clientDto must not be {@literal null}.
     * @return the saved entity; will never be {@literal null}.
     */
    ClientDto save(ClientDto clientDto);

    /**
     * Updated a given entity with sending information about update.
     * Use the returned instance entity {@link Client} in format DTO {@link ClientDto}
     *
     * @param clientDto must not be {@literal null}.
     * @return the updated entity; will never be {@literal null}.
     */


    ClientDto updateWithSendInfo(ClientDto clientDto, Client clientDb);

    /**
     * Updated a given entity. Use the returned instance entity {@link Client} in format DTO {@link ClientDto}
     *
     * @param clientDto must not be {@literal null}.
     * @return the updated entity; will never be {@literal null}.
     */

    ClientDto update(ClientDto clientDto, Client clientDb);

    boolean updateForCamundaApproveUpdateClient(ClientDto clientDto);

    boolean update(Client client);

    /**
     * Updated  entity by given ids. Use the returned instances entity {@link Client} in format DTO {@link ClientDto}
     *
     * @param clientList must not be {@literal null}.
     * @return the updated entities; will never be {@literal null}.
     */

    List<ClientDto> updateListClientDtoByType(ClientsUpdateTypeDto clientList);

    /**
     * Deletes the entity {@link Client}  with the given id .
     *
     * @param id must not be {@literal null}.
     * @return  {@literal true} if the entity with given name deleted in data store.
     *          {@literal false} if the entity with given name not found in data store.
     */
    boolean delete(Long id);

    boolean updateFromSellOut();
}
