package ru.bostonsd.clientmanagement.domain.service;

import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.domain.models.ClientClassification;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientClassificationDto;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientClassificationDtoSave;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientClassificationDtoUpdate;

import java.util.List;

/**
 * Service for managing the entity {@link ClientClassification} using the Dto format {@link ClientClassificationDto}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 3, 2019
 */


@Service
public interface ClientClassificationService {
    /**
     * All instances of the entity {@link ClientClassification} in format DTO {@link ClientClassificationDto}
     *
     * @return all instances current entity
     */
    List<ClientClassificationDto> getAllList();

    /**
     * List instances of the entity {@link ClientClassification} in format DTO  {@link ClientClassificationDto}
     * with the given isActive.
     *
     * @param isActive - must be true/false
     * @return all instances current entity with the given isActive
     */
    List<ClientClassificationDto> getIsActiveList(boolean isActive);

    /**
     * Retrieves an entity {@link ClientClassification} in format DTO  {@link ClientClassificationDto}
     * by the given name of classification
     *
     * @param name - must not be {@literal null}
     * @return the current entity with the given name or {@literal null} if none found
     */
    ClientClassificationDto getByName(String name);

    /**
     * Saves a given entity. Use the returned instance entity {@link ClientClassification}
     * in format DTO  {@link ClientClassificationDtoSave}
     *
     * @param clientClassificationDtoSave must not be {@literal null}.
     * @return the saved entity; will never be {@literal null}.
     */
    ClientClassificationDto save(ClientClassificationDtoSave clientClassificationDtoSave);

    /**
     * Updated a given entity. Use the returned instance entity {@link ClientClassification}
     * in format DTO  {@link ClientClassificationDtoUpdate}
     *
     * @param clientClassificationDtoUpdate must not be {@literal null}.
     * @return the updated entity; will never be {@literal null}.
     */
    ClientClassificationDto update(ClientClassificationDtoUpdate clientClassificationDtoUpdate);

    /**
     * Deletes the entity {@link ClientClassification} in format DTO  {@link ClientClassification}
     * with the given name .
     *
     * @param name must not be {@literal null}.
     * @return  {@literal true} if the entity with given name deleted in data store.
     *          {@literal false} if the entity with given name not found in data store.
     */
    boolean delete(String name);

}
