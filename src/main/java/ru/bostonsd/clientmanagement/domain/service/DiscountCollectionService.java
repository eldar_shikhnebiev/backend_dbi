package ru.bostonsd.clientmanagement.domain.service;

import ru.bostonsd.clientmanagement.domain.models.DiscountCollection;
import ru.bostonsd.clientmanagement.domain.models.dto.DiscountCollectionDto;

import java.util.List;


/**
 * Service for managing the entity {@link DiscountCollection} using the Dto format {@link DiscountCollectionDto}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * @date 03.12.2019
 */

public interface DiscountCollectionService {

    /**
     * All instances of the entity {@link DiscountCollection} in format DTO {@link DiscountCollectionDto}
     *
     * @return all instances current entity
     */
    List<DiscountCollectionDto> getAllList();

    /**
     * All instances of the entity {@link DiscountCollection} in format DTO {@link DiscountCollectionDto}
     * with the given isActive
     * @param isActive - must be true/false
     * @return all instances current entity with the given isActive
     */
    List<DiscountCollectionDto> getIsActiveList(boolean isActive);

    /**
     * Retrieves an entity {@link DiscountCollection} in format DTO {@link DiscountCollectionDto}
     * by  the given name of collection
     *
     * @param name - must not be {@literal null}
     * @return the current entity with the given name or {@literal null} if none found
     */
    DiscountCollectionDto getByName(String name);

    /**
     * Saves a given entity. Use the returned instance entity {@link DiscountCollection}
     * in format DTO {@link DiscountCollectionDto}
     *
     * @param discountCollectionDto must not be {@literal null}.
     * @return the saved entity; will never be {@literal null}.
     */
    DiscountCollectionDto save(DiscountCollectionDto discountCollectionDto);

    /**
     * Updated a given entity. Use the returned instance entity {@link DiscountCollection}
     * in format DTO {@link DiscountCollectionDto}
     *
     * @param discountCollectionDto must not be {@literal null}.
     * @return the updated entity; will never be {@literal null}.
     */
    DiscountCollectionDto update(DiscountCollectionDto discountCollectionDto);

    /**
     * Deletes the entity {@link DiscountCollection}  with the given id .
     *
     * @param name must not be {@literal null}.
     * @return  {@literal true} if the entity with given name deleted in data store.
     *          {@literal false} if the entity with given name not found in data store.
     */
    boolean delete(String name);
}
