package ru.bostonsd.clientmanagement.domain.service;

import ru.bostonsd.clientmanagement.domain.models.*;
import ru.bostonsd.clientmanagement.domain.models.dto.*;

/**
 * Service for Transformation Domain entity to/from DTO inside services entity
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 3, 2019
 * Updated on Nay 21, 2020 | vkondaurov
 */

public interface TransformationService {

    /** Retrieves an entity {@link Client} trans formatied from {@link ClientDto}
     *
     * @param clientDto - must not be {@literal null}
     * @return the current entity;
     */
    Client transformationFromClientDto(ClientDto clientDto);

    /** Retrieves an entity {@link ClientDto} trans formatied from {@link Client}
     *
     * @param client - must not be {@literal null}
     * @return the current entity;
     */
    ClientDto transformationFromClient(Client client);

    /** Retrieves an entity {@link ClientDto} trans formated from {@link Client}
     * with filter data in linked entity {@link SalesVolume} by distributor id
     *
     * @param distributorId - must not be {@literal null}
     * @param client - must not be {@literal null}
     * @return the current entity;
     */
    ClientDto transformationFromClientByDistributor(Long distributorId,  Client client);

    /** Retrieves an entity {@link ClientShortDto} trans formatied from {@link Client}
     *
     * @param client - must not be {@literal null}
     * @return the current entity;
     */
    ClientShortDto transformationFromClientShort(Client client);

    /** Retrieves an entity {@link ClientWithOnlySellOutSalesVolumeDto} trans formatied from {@link Client}
     *
     * @param client - must not be {@literal null}
     * @return the current entity;
     */
    ClientWithOnlySellOutSalesVolumeDto transformationFromClientOnlySalesVolume(Client client);

    /** Retrieves an entity {@link ClientLinkedDto} trans formatied from {@link Client}
     *
     * @param client - must not be {@literal null}
     * @return the current entity;
     */
    ClientLinkedDto transformationFromClientLinked(Client client);



    /** Retrieves an entity {@link Distributor} trans formatied from {@link DistributorDto}
     *
     * @param distributorDto - must not be {@literal null}
     * @return the current entity;
     */
    Distributor transformationFromDistributorDto(DistributorDto distributorDto);

    /** Retrieves an entity {@link DistributorDto} trans formatied from {@link Distributor}
     *
     * @param distributor - must not be {@literal null}
     * @return the current entity;
     */
    DistributorDto transformationFromDistributor(Distributor distributor);


    /** Retrieves an entity {@link ClientClassification} trans formatied from {@link ClientClassificationDto}
     *
     * @param clientClassificationDto - must not be {@literal null}
     * @return the current entity;
     */
    ClientClassification transformationFromClientClassificationDto(ClientClassificationDto clientClassificationDto);

    /** Retrieves an entity {@link ClientClassification} trans formatied from {@link ClientClassificationDtoSave}
     *
     * @param clientClassificationDtoSave - must not be {@literal null}
     * @return the current entity;
     */
    ClientClassification transformationFromClientClassificationDto(ClientClassificationDtoSave clientClassificationDtoSave);

    /** Retrieves an entity {@link ClientClassification} trans formatied from {@link ClientClassificationDtoUpdate}
     *
     * @param clientClassificationDtoUpdate - must not be {@literal null}
     * @return the current entity;
     */
    ClientClassification transformationFromClientClassificationDto(ClientClassificationDtoUpdate clientClassificationDtoUpdate);

    /** Retrieves an entity {@link ClientClassificationDto} trans formatied from {@link ClientClassification}
     *
     * @param clientClassification - must not be {@literal null}
     * @return the current entity;
     */
    ClientClassificationDto transformationFromClientClassification(ClientClassification clientClassification);


    /** Retrieves an entity {@link DiscountCollection} trans formatied from {@link DiscountCollectionDto}
     *
     * @param discountCollectionDto - must not be {@literal null}
     * @return the current entity;
     */
    DiscountCollection transformationFromDiscountCollectionDto(DiscountCollectionDto discountCollectionDto);

    /** Retrieves an entity {@link DiscountCollectionDto} trans formatied from {@link DiscountCollection}
     *
     * @param discountCollection - must not be {@literal null}
     * @return the current entity;
     */
    DiscountCollectionDto transformationFromDiscountCollection(DiscountCollection discountCollection);


    /** Retrieves an entity {@link SalesVolume} trans formatied from {@link SalesVolumeDto}
     *
     * @param salesVolumeDto - must not be {@literal null}
     * @return the current entity;
     */
    SalesVolume transformationFromSalesVolumeDto(SalesVolumeDto salesVolumeDto);

    /** Retrieves an entity {@link SalesVolume} trans formatied from {@link SalesVolumeClientDto}
     * for specified client id
     *
     * @param clientId - must not be {@literal null}
     * @param salesVolumeClientDto - must not be {@literal null}
     * @return the current entity;
     */
    SalesVolume transformationFromClientSalesVolumeDto(Long clientId, SalesVolumeClientDto salesVolumeClientDto);

    /** Retrieves an entity {@link SalesVolumeDto} trans formatied from {@link SalesVolumeClientDto}
     * for specified client id
     *
     * @param clientId - must not be {@literal null}
     * @param salesVolumeClientDto - must not be {@literal null}
     * @return the current entity;
     */
    SalesVolumeDto transformationFromClientSalesVolumeDtoToFull(Long clientId, SalesVolumeClientDto salesVolumeClientDto);

    /** Retrieves an entity {@link SalesVolumeDto} trans formatied from {@link SalesVolume}
     *
     * @param salesVolume - must not be {@literal null}
     * @return the current entity;
     */
    SalesVolumeDto transformationFromSalesVolume(SalesVolume salesVolume);

    /** Retrieves an entity {@link SalesVolumeClientDto} trans formatied from {@link SalesVolume}
     *
     * @param salesVolume - must not be {@literal null}
     * @return the current entity;
     */
    SalesVolumeClientDto transformationFromSalesVolumeClient(SalesVolume salesVolume);

    /** Retrieves an entity {@link SellOutSalesVolumeClientDto} trans formatied from {@link SellOutSalesVolume}
     *
     * @param sellOutSalesVolume - must not be {@literal null}
     * @return the current entity;
     */
    SellOutSalesVolumeClientDto transformationFromSellOutSalesVolume(SellOutSalesVolume sellOutSalesVolume);

}
