package ru.bostonsd.clientmanagement.domain.service;

import ru.bostonsd.clientmanagement.domain.models.SalesVolume;
import ru.bostonsd.clientmanagement.domain.models.dto.SalesVolumeClientDto;
import ru.bostonsd.clientmanagement.domain.models.dto.SalesVolumeDto;

import java.util.List;

/**
 * Service for managing the entity {@link SalesVolume}
 * using the Dto format {@link SalesVolumeDto}, {@link SalesVolumeClientDto}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 10, 2019
 * Updated on December 13, 2019 | vkondaurov
 */

public interface SalesVolumeService {

    /**
     * List instances of the entity {@link SalesVolume} in format DTO {@link SalesVolumeClientDto}
     * with the given client id
     *
     * @param clientId - must not be {@literal null}
     * @return list instances current entity
     */
    List<SalesVolumeClientDto> getListByClientId(Long clientId);

    /**
     * List instances of the entity {@link SalesVolume} in format DTO {@link SalesVolumeDto}
     * with the given distributor id
     *
     * @param distributorId - must not be {@literal null}
     * @return list instances current entity
     */
    List<SalesVolumeDto> getListByDistributorId(Long distributorId);

    /**
     * Instance of the entity {@link SalesVolume} with the given ids client and distributor
     *
     * @param distributorId - must not be {@literal null}
     * @param clientId - must not be {@literal null}
     * @return list instances current entity
     */
    SalesVolumeDto getByClientIdAndDistributorId(Long clientId, Long distributorId);

    /**
     * Checks for the existence of an instance of {@link SalesVolume}
     * that contains client-distributor relationship with the given ids
     *
     * @param distributorId - must not be {@literal null}
     * @param clientId - must not be {@literal null}
     * @return  {@literal true} if an instance with the given parameters exists in data store.
     *          {@literal false} if an instance with the given parameters not found in data store.
     */
    boolean existLinkClientWithDistributor(Long clientId, Long distributorId);

    /**
     * Created and saved instance entity {@link SalesVolume} with the given ids client and distributor
     *
     * @param distributorId - must not be {@literal null}
     * @param clientId - must not be {@literal null}
     */
    void linkClientWithDistributor(Long clientId, Long distributorId);

    /**
     * Saves a given entity. Use the returned instance entity {@link SalesVolume} in format DTO {@link SalesVolumeDto}
     *
     * @param salesVolumeDto must not be {@literal null}.
     * @return the saved entity; will never be {@literal null}.
     */
    SalesVolumeDto save(SalesVolumeDto salesVolumeDto);

    /**
     * Updated a given entity. Use the returned instance entity {@link SalesVolume} in format DTO {@link SalesVolumeDto}
     *
     * @param salesVolumeDto must not be {@literal null}.
     * @return the updated entity; will never be {@literal null}.
     */
    SalesVolumeDto update(SalesVolumeDto salesVolumeDto);

    /**
     * Deletes the entity {@link SalesVolume}  with the given ids client and distributor  .
     *
     * @param clientId must not be {@literal null}.
     * @param distributorId must not be {@literal null}.

     * @return  {@literal true} if the entity with given name deleted in data store.
     *          {@literal false} if the entity with given name not found in data store.
     */
    boolean delete(Long clientId, Long distributorId);

    /**
     * Deletes the all entity {@link SalesVolume}  with the given id client .
     *
     * @param clientId must not be {@literal null}.
     *
     * @return  {@literal true} if the entity with given name deleted in data store.
     *          {@literal false} if the entity with given name not found in data store.
     */
    boolean deleteByClientId(Long clientId);


    /**
     * Added/updated the all entity {@link SalesVolume} in format DTO {@link SalesVolumeClientDto} for the given id client .
     *
     * @param clientId - must not be {@literal null}.
     * @param salesVolumeClientDtoList - must not be {@literal null}.
     *
     * @return - saved list  entity; will never be {@literal null}.
     */
    List<SalesVolume> operationsWithList(Long clientId, List<SalesVolumeClientDto> salesVolumeClientDtoList);

//    List<SalesVolumeDto> updateSalesVolumesFromSellOut();
}
