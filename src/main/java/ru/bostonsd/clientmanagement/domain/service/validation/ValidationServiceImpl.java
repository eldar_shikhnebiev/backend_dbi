package ru.bostonsd.clientmanagement.domain.service.validation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.domain.models.dto.ClientDto;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author asaburov on 12/10/2019
 */

@Slf4j
@Service
public class ValidationServiceImpl implements ValidationService {


    @Override
    public boolean validateClient(ClientDto clientDto) {
//        boolean valid = false;
//        if (clientDto != null) {
//            valid = clientDto.getName() != null && !clientDto.getName().trim().isEmpty();
//            valid &= clientDto.getInn() != null && !clientDto.getInn().trim().isEmpty();
//            valid &= clientDto.getDate() != null && clientDto.getDate() > 0;
//            if (clientDto.getEmail() != null) {
//                Pattern emailPattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
//                Matcher matcher = emailPattern.matcher(clientDto.getEmail());
//                valid &= matcher.find();
//            } else {
//                valid = false;
//            }
//            return valid;
//        }
//        log.info("Resalt of client validation: {}", valid);
//        return false;
        return true;
    }

    @Override
    public boolean validateTicket() {
        return false;
    }
}
