package ru.bostonsd.clientmanagement.domain.service;

import ru.bostonsd.clientmanagement.domain.models.Client;
import ru.bostonsd.clientmanagement.domain.models.Distributor;
import ru.bostonsd.clientmanagement.domain.models.dto.DistributorDto;

import java.util.List;
import java.util.Map;

/**
 * Service for managing the entity {@link Distributor} using the Dto format {@link DistributorDto}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 3, 2019
 * Updated on April 9, 2020 | vkondaurov
 */

public interface DistributorService {

    /**
     * All instances of the entity {@link Distributor} in format DTO {@link DistributorDto}
     *
     * @return all instances current entity
     */
    List<DistributorDto> getAllList();

    /**
     * All instances of the entity {@link Distributor} in format Map include Id(key) and PartnerId (value)
     *
     * @return Map
     */
    Map<Long, Long> getMapIdAndPartnerId();

    /**
     * All instances of the entity {@link Distributor} in format Map include Inn(key) and Id (value)
     *
     * @return Map
     */
    Map<String, Long> getMapInnAndId();

    /**
     * Retrieves an entity {@link Distributor} in format DTO {@link DistributorDto}
     * by  the given name of distributor
     *
     * @param name - must not be {@literal null}
     * @return the current entity with the given name or {@literal null} if none found
     */
    DistributorDto getByName(String name);

    /**
     * Retrieves an entity {@link Distributor} in format DTO {@link DistributorDto}
     * by  the given inn of distributor
     *
     * @param inn - must not be {@literal null}
     * @return the current entity with the given inn or {@literal null} if none found
     */
    DistributorDto getByInn(String inn);

    /**
     * Retrieves an entity {@link Distributor}
     * by the given id of distributor
     *
     * @param id - must not be {@literal null}
     * @return the current entity with the given id or {@literal null} if none found
     */
    Distributor getDistributorById(Long id);

    /**
     * Retrieves an entity {@link Distributor} in format DTO {@link DistributorDto}
     * by the given id of distributor
     *
     * @param id - must not be {@literal null}
     * @return the current entity with the given id or {@literal null} if none found
     */
    DistributorDto getDistributorDtoById(Long id);

    /**
     * Retrieves an entity {@link Distributor} in format DTO {@link DistributorDto}
     * by the given id and name of distributor
     *
     * @param inn - must not be {@literal null}
     * @param name - must not be {@literal null}
     * @return the current entity with the given id or {@literal null} if none found
     */
    DistributorDto getByInnAndName(String inn, String name);

    /**
     * Saves a given entity. Use the returned instance entity {@link Distributor} in format DTO {@link DistributorDto}
     *
     * @param distributorDto - must not be {@literal null}.
     * @return the saved entity; will never be {@literal null}.
     */
    DistributorDto save(DistributorDto distributorDto);

    /**
     * Updated a given entity. Use the returned instance entity {@link Distributor} in format DTO {@link DistributorDto}
     *
     * @param distributorDto - must not be {@literal null}.
     * @return the updated  entity; will never be {@literal null}.
     */
    DistributorDto update(DistributorDto distributorDto);

    /**
     * Deletes the entity {@link Distributor}  with the given id .
     *
     * @param id must not be {@literal null}.
     * @return  {@literal true} if the entity with given name deleted in data store.
     *          {@literal false} if the entity with given name not found in data store.
     */
    boolean delete(Long id);
}
