package ru.bostonsd.clientmanagement.domain.service;

import org.springframework.web.multipart.MultipartFile;
import ru.bostonsd.clientmanagement.domain.models.File;
import ru.bostonsd.clientmanagement.domain.models.dto.FileDto;

/**
 * Service for manage entity {@link File} include using the Dto format {@link FileDto}
 *
 * @author Denis Yarovoy (dyarovoy@bostonsd.ru)
 * Created on December 5, 2019
 */
public interface FileService {

    /**
     * Saves a given entity. Use the returned instance entity {@link File} in format DTO {@link FileDto}
     *
     * @param file - must not be {@literal null}.
     * @return the saved entity; will never be {@literal null}.
     */
    FileDto save(MultipartFile file);

    /**
     * Retrieves an entity {@link File}
     * by the given id of file
     *
     * @param id - must not be {@literal null}
     * @return the current entity with the given id or {@literal null} if none found
     */
    File getFileById(Long id);

    /**
     * Deletes the entity {@link File}  with the given id .
     *
     * @param id must not be {@literal null}.
     * @return  {@literal true} if the entity with given name deleted in data store.
     *          {@literal false} if the entity with given name not found in data store.
     */
    boolean delete(Long id);
}
