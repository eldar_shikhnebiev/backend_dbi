package ru.bostonsd.clientmanagement.domain.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bostonsd.clientmanagement.domain.exception.DiscountCollectionException;
import ru.bostonsd.clientmanagement.domain.models.DiscountCollection;
import ru.bostonsd.clientmanagement.domain.models.dto.DiscountCollectionDto;
import ru.bostonsd.clientmanagement.domain.repository.DiscountCollectionRepository;
import ru.bostonsd.clientmanagement.domain.service.DiscountCollectionService;
import ru.bostonsd.clientmanagement.domain.service.TransformationService;
import ru.bostonsd.clientmanagement.smtp.service.PreparatorSenderService;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Implementation for {@link DiscountCollectionService}
 *
 * @author Valeriy Kondaurov (vkondaurov@bostonsd.ru)
 * Created on December 5, 2019
 * Updated on December 11, 2019 | asaburov
 */


@Service
@Slf4j
public class DiscountCollectionServiceImpl implements DiscountCollectionService {

    @Autowired
    private DiscountCollectionRepository discountCollectionRepository;

    @Autowired
    private TransformationService transformationService;

    @Autowired
    private PreparatorSenderService preparatorSenderService;

    public DiscountCollectionServiceImpl(DiscountCollectionRepository discountCollectionRepository, TransformationService transformationService) {
        this.discountCollectionRepository = discountCollectionRepository;
        this.transformationService = transformationService;
    }

    /**
     * PostConstruct method for adding a collection discount to the repository.
     * Before adding data, the data in the store is checked.
     * If empty, data is added. The list of discount is specified inside the method
     */
    @PostConstruct
    private void initCollections(){
        List<DiscountCollection> collections = discountCollectionRepository.findAll();
        if(collections.isEmpty()){
            List<DiscountCollection> result = new ArrayList<>();
            result.add(DiscountCollection.builder().name("Ритейл Опт., Ритейл Проектная").isActive(true).bronze(0.55).silver(0.52).gold(0.5).build());
            result.add(DiscountCollection.builder().name("Ритейл Эконом").isActive(true).bronze(0.77).silver(0.74).gold(0.71).build());
            result.add(DiscountCollection.builder().name("ДЕКРАФТ").isActive(true).bronze(0.74).silver(0.71).gold(0.69).build());
            result.add(DiscountCollection.builder().name("PvP Оптимальная").isActive(true).bronze(0.55).silver(0.52).gold(0.5).build());
            result.add(DiscountCollection.builder().name("PvP Медиум").isActive(true).bronze(0.77).silver(0.72).gold(0.69).build());
            result.add(DiscountCollection.builder().name("Компоненты автоматизации").isActive(true).bronze(0.55).silver(0.52).gold(0.5).build());
            result.add(DiscountCollection.builder().name("Компоненты автоматизации Lite").isActive(true).bronze(0.81).silver(0.77).gold(0.74).build());
            result.add(DiscountCollection.builder().name("Приводная техника").isActive(true).bronze(0.73).silver(0.7).gold(0.675).build());
            for(DiscountCollection collect: result) {
                discountCollectionRepository.save(collect);
            }
        }
    }

    /**
     * Implementation of the method defined in the interface {@link DiscountCollectionService}
     * A list of all instances entity  {@link DiscountCollection} in the data store is requested.
     * Checking that it is not empty and transforming the domain model into a DTO model {@link DiscountCollectionDto}.
     *
     * @return  list instances entity {@link DiscountCollectionDto}, if data store not empty
     *          empty list, if the store does not contain data
     */
    @Override
    public List<DiscountCollectionDto> getAllList() {
        List<DiscountCollection> discountCollection = discountCollectionRepository.findAll();
        if (discountCollection.isEmpty()) {
            return new ArrayList<>();
        } else {
            return discountCollection.stream()
                    .map(transformationService::transformationFromDiscountCollection)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Implementation of the method defined in the interface {@link DiscountCollectionService}
     *
     * Requests a list of instances of the {@link DiscountCollection} entity in the data store
     * with the parameter  isActive specified for the field "active".
     *
     * Checking that it is not empty and transforming all instances from the domain model
     * into a DTO model {@link DiscountCollectionDto}.
     *
     * @param isActive - must be true/false
     * @return  list instances entity {@link DiscountCollectionDto}, if data store not empty
     *          empty list, if the store does not contain data
     */
    @Override
    public List<DiscountCollectionDto> getIsActiveList(boolean isActive) {
        List<DiscountCollection> discountCollection = discountCollectionRepository.findByIsActive(isActive);
        if (discountCollection.isEmpty()) {
            return new ArrayList<>();
        } else {
            return discountCollection.stream()
                    .map(transformationService::transformationFromDiscountCollection)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Implementation of the method defined in the interface {@link DiscountCollectionService}
     *
     * Finding instance of entity {@link DiscountCollection} in the data store
     * by given name.
     * If not found in data store throw exception {@link DiscountCollectionException}
     *
     * Checking that it is not empty and transforming all instances from the domain model
     * into a DTO model {@link DiscountCollectionDto}.
     *
     * @param name - name collection for find.
     * @return  instance entity {@link DiscountCollectionDto}, if finding data store not empty.
     */
    @Override
    public DiscountCollectionDto getByName(String name) {
        if (name != null) {
            DiscountCollection discountCollection = discountCollectionRepository.findByName(name);
            if (discountCollection != null) {
                return transformationService.transformationFromDiscountCollection(discountCollection);
            } else {
                throw new DiscountCollectionException("Collection with Name " + name + " not found");
            }
        } else {
            throw new DiscountCollectionException("The name of the Collection for the searching is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link DiscountCollectionService}
     *
     * Saves given instance of entity {@link DiscountCollection} in the data store.
     * Before saving:
     *    - the given entity instance is transformed from DTO to domain model
     *    - it checks that a collection with the same name is missing from the repository
     *      If exist in data store throw exception {@link DiscountCollectionException}
     *
     * After saving reverse transformation of a saved entity instance into a DTO.
     *
     * @param discountCollectionDto - instance of entity {@link DiscountCollectionDto} for saved.
     * @return  instance entity {@link DiscountCollectionDto}, saved in data store.
     */
    @Override
    public DiscountCollectionDto save(DiscountCollectionDto discountCollectionDto) {
        if (discountCollectionDto.getName() != null) {
            DiscountCollection discountCollectionDb =
                    transformationService.transformationFromDiscountCollectionDto(discountCollectionDto);

            if (!discountCollectionRepository.existsByName(discountCollectionDb.getName())) {
                discountCollectionDb = discountCollectionRepository.save(discountCollectionDb);
                return transformationService.transformationFromDiscountCollection(discountCollectionDb);

            } else {
                throw new DiscountCollectionException("Collection with name "
                        + discountCollectionDto.getName() + "  exist in DB");
            }
        } else {
            throw new DiscountCollectionException("The name of the Collection for the saving is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link DiscountCollectionService}
     *
     * Updated fields given instance of entity {@link DiscountCollection} in the data store.
     *
     * Finding instance entity {@link DiscountCollection} by given name.
     * If it was found, given instance from parameter trans formatied to domain model
     * and setter new value in fields instance of entity from data store.
     * After that update instanced of entity in data store
     *
     * If not exist in data store throw exception {@link DiscountCollectionException}
     *
     *  Also, a message is generated for newsletter information about changes in the discount collection and sent via
     *  {@link PreparatorSenderService}
     *
     * @param discountCollectionDto - instance of entity {@link DiscountCollectionDto} for updated.
     * @return  instance entity {@link DiscountCollectionDto}, saved in data store.
     */
    @Override
    public DiscountCollectionDto update(DiscountCollectionDto discountCollectionDto) {
        if (discountCollectionDto.getName() != null) {
            DiscountCollection discountCollectionDb =
                    discountCollectionRepository.findByName(discountCollectionDto.getName());
            if (discountCollectionDb != null) {
                DiscountCollectionDto oldCollection = transformationService.transformationFromDiscountCollection(discountCollectionDb);
//                try {
//                    //CLONING OLD COLLECTION FOR KEEPING OLD VALUE
//                    oldCollection = (DiscountCollectionDto) oldCollection.clone();
//                } catch (CloneNotSupportedException e) {
//                    log.error("CLONE ERROR, MESSAGE: {}", e.getMessage());
//                }

                DiscountCollection discountCollection = transformationService
                        .transformationFromDiscountCollectionDto(discountCollectionDto);

                discountCollectionDb.setActive(discountCollection.isActive());
                discountCollectionDb.setBronze(discountCollection.getBronze());
                discountCollectionDb.setSilver(discountCollection.getSilver());
                discountCollectionDb.setGold(discountCollection.getGold());

                discountCollectionDb = discountCollectionRepository.save(discountCollectionDb);

                DiscountCollectionDto newCollection =
                        transformationService.transformationFromDiscountCollection(discountCollectionDb);

                //SENDING MESSAGE ABOUT CHANGING DISCOUNT
                if (discountCollectionDb != null && discountCollectionDb.getId() != null && oldCollection != null) {
                    preparatorSenderService.prepareChangeOfCollectionDiscount(oldCollection, newCollection);
                }
                return newCollection;

            } else {
                throw new DiscountCollectionException("Collection with name "
                        + discountCollectionDto.getName() + " not found for update");
            }
        } else {
            throw new DiscountCollectionException("The name of the Collection for the updating is not set");
        }
    }

    /**
     * Implementation of the method defined in the interface {@link DiscountCollectionService}
     *
     * Deletes instance of entity {@link DiscountCollection} by name in the data store.
     * Before deleted:
     *    - finding instance entity {@link DiscountCollection} by given name.
     *      If it was found, it is deleted to the data store and return true
     *      otherwise false
     *
     * @param name - name collection for deleted.
     * @return boolean answer from result of the delete operation
     */
    @Override
    public boolean delete(String name) {
        if (name != null) {
            DiscountCollection discountCollectionDb =
                    discountCollectionRepository.findByName(name);
            if (discountCollectionDb != null) {
                discountCollectionRepository.delete(discountCollectionDb);
                return true;
            } else {
                return false;
            }
        } else {
            throw new DiscountCollectionException("The name of the Collection for the deleting is not set");
        }
    }
}
