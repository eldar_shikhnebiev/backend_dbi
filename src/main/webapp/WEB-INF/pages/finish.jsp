<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*, java.text.*" %>

<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/material.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <title>Восстановление пароля</title>
</head>

<body>
    <div class="mdl-grid my-margin-top">
        <div class="mdl-layout-spacer"></div>
        <div class="mdl-cell mdl-cell--4-col mdl-typography--text-center">
            <h4>Результат восстановления пароля</h4>
            <h4>${message}</h4>
            <a href='${urlRedirect}' style = "color:blue">Вернуться на главную страницу</a>
        </div>
        <div class="mdl-layout-spacer"></div>
    </div>
</body>


</html>