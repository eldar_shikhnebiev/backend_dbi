<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*, java.text.*" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/css/material.min.css">
        <link rel="stylesheet" href="/css/style.css">
        <script src="/js/material.min.js"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <title>Восстановление пароля</title>
    </head>
    <body>
        <div class="mdl-grid my-margin-top">
            <div class="mdl-layout-spacer"></div>
            <div class="mdl-cell mdl-cell--4-col mdl-typography--text-center">
                <h4>Восстановление пароля</h4>
                <h5>${message}</h5>
                <form:form method="POST"
                          action="/recovery/password" modelAttribute="recoveryModel">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <form:input class="mdl-textfield__input" type="password" path="password" id="password1"/>
                        <form:label class="mdl-textfield__label" for="password" path="password">Пароль</form:label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <form:input class="mdl-textfield__input" type="password" path="passwordConfirm" id="password2"/>
                        <form:label class="mdl-textfield__label" for="passwordConfirm" path="passwordConfirm">Повторите пароль</form:label>
                    </div>
                    <div style="display:none" >
                        <form:hidden class="mdl-textfield__input" path="code"/>
                        <form:label class="mdl-textfield__label" for="code" path="code"></form:label>
                    </div>
                    <div class="error-message" id="errorMessage" hidden>
                        Пароли должны совпадать
                    </div>
                    <form:button id="submitButton"
                        class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored schneider-green">
                        Отправить
                    </form:button>
                </form:form>
            </div>
            <div class="mdl-layout-spacer"></div>
        </div>
        <script src="/js/script.js"></script>
    </body>
</html>