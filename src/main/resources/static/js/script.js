const button = document.getElementById('submitButton');
const password1 = document.getElementById('password1');
const password2 = document.getElementById('password2');
const message = document.getElementById('errorMessage');
button.disabled = true;

function onChange() {    
    if (password1.value === '' && password2 === '') {
        button.disabled = true;
        message.hidden = true;
    }
    if (password1.value !== password2.value) {
        button.disabled = true;
        message.hidden = false;
    } else {
        button.disabled = false;
        message.hidden = true;
    }
}

password1.addEventListener('keyup', onChange);
password2.addEventListener('keyup', onChange);
