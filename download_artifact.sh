AUTH=$(echo -ne "$BITBUCKET_USERNAME:$BITBUCKET_APP_PASSWORD" | base64 --wrap 0)
REPO_FULL_NAME="schneiderelectric_front/client-management-web-app"
FRONT_BUILD_FILE=$(curl --header "Authorization: Basic $AUTH" -s -L https://api.bitbucket.org/2.0/repositories/${REPO_FULL_NAME}/downloads | jq '.values|=sort_by(.created_on)|.values[-1].links.self.href' | cut -d\" -f 2)
curl --header "Authorization: Basic $AUTH" -s -L ${FRONT_BUILD_FILE} --output /tmp/front.tar.gz

